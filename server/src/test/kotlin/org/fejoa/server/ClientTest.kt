package org.fejoa.server

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.withTimeout
import org.fejoa.*
import org.fejoa.crypto.CryptoSettings
import org.fejoa.crypto.generateSecretKeyData
import org.fejoa.network.ReturnType
import org.fejoa.repository.sync.AccessRight
import org.fejoa.server.JettyServer.Companion.DEFAULT_PORT
import org.fejoa.server.JettyServer.Companion.FEJOA_PORTAL_PATH
import org.fejoa.storage.HashValue
import org.fejoa.support.Future
import org.fejoa.support.PathUtils
import org.fejoa.support.await
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.io.File
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.test.assertEquals
import kotlin.test.assertTrue

// TODO this doesn't compile for JS at the moment but should be moved to the Future class
// see: https://discuss.kotlinlang.org/t/cant-compile-invokeoncompletion-with-oncancelling-option/6852
suspend fun <T> Future<T>.awaitCancellable() = suspendCancellableCoroutine<T> { cont ->
    cont.invokeOnCancellation {
        cancel()
    }
    whenCompleted { value, exception ->
        if (exception != null)
            cont.resumeWithException(exception)
        else // cast to T in case T is nullable and is actually null
            cont.resume(value as T)
    }
}

class ClientTest {
    companion object {
        internal val TEST_DIR = "jettyTest"
        internal val SERVER_TEST_DIR_1 = TEST_DIR + "/Server1"
        internal val SERVER_TEST_DIR_2 = TEST_DIR + "/Server2"
        internal val SERVER_TEST_DIR_3 = TEST_DIR + "/Server3"
    }

    private val cleanUpDirs: MutableList<String> = ArrayList()
    private var server1: JettyServer? = null
    private val port1 = DEFAULT_PORT
    private val url1 = "http://localhost:$port1/$FEJOA_PORTAL_PATH"
    private var server2: JettyServer? = null
    // cookies are per domain; choose a different localhost to make session management working
    private val port2 = DEFAULT_PORT + 1
    private val url2 = "http://127.0.0.2:$port2/$FEJOA_PORTAL_PATH"
    private var server3: JettyServer? = null
    private val port3 = DEFAULT_PORT + 2
    private val url3 = "http://127.0.0.3:$port3/$FEJOA_PORTAL_PATH"

    private fun setUpServer(dir: String, port: Int): JettyServer {
        val server = JettyServer(dir, port = port)
        //server.setDebugNoAccessControl(true)
        server.start()
        return server
    }

    @Before
    fun setUp() {
        cleanUpDirs.add(TEST_DIR)
        for (dir in cleanUpDirs)
            File(dir).deleteRecursively()

        server1 = setUpServer(SERVER_TEST_DIR_1, port1)
        server2 = setUpServer(SERVER_TEST_DIR_2, port2)
        server3 = setUpServer(SERVER_TEST_DIR_3, port3)
    }

    @After
    fun tearDown() {
        Thread.sleep(1000)
        server1!!.stop()
        server2!!.stop()
        server3!!.stop()

        for (dir in cleanUpDirs)
            File(dir).deleteRecursively()
    }

    private suspend fun waitForBranchesSynced(logger: DefaultLogger, branches: List<String>): Boolean {
        (1 .. 100).forEach {
            val syncedBranches = logger.getLogTypeMap(Logger.Category.INFO)
                    .getEvents(Logger.TYPE.SYNC_MANAGER)
                    .map { it.first.data as SyncEvent }
                    .filter { branches.contains(it.branch)
                            && it.branchType == SyncEvent.BranchType.BRANCH
                            && it.status == SyncEvent.Status.SYNCED
                    }

            if (syncedBranches.size >= branches.size) {
                val done = branches.none { branch -> syncedBranches.find { it.branch == branch } == null }
                if (done)
                    return true
            }
            delay(50)
        }
        return false
    }

    private suspend fun setupClient(serverDir: String, serverUrl: String, clientDir: String, user: String,
                                    password: String, logger: Logger): Client {
        val client = Client.create(clientDir, user, password)

        client.connectionAuthManager.passwordGetter = object : PasswordGetter {
            override suspend fun get(purpose: PasswordGetter.Purpose, resource: String, info: String): String {
                return password
            }
        }
        val registerResult = client.registerAccount(user, serverUrl, password)
        assertEquals(ReturnType.OK, registerResult.first.code)

        val serverAccountIO = platformGetAccountIO(AccountIO.Type.SERVER, serverDir, user)
        assertTrue(serverAccountIO.exists())

        // start syncing (nothing to do yet)
        client.logger.children += logger

        // sync client1 with server
        val remote = registerResult.second!!
        client.setBranchLocation(remote, LoginAuthInfo(),  UserData.USER_DATA_CONTEXT)
        client.userData.commit()
        return client
    }

    @Test
    fun testClient() = runBlocking {
        val clientDir = PathUtils.appendDir(TEST_DIR, "testClient")

        val log: MutableList<String> = ArrayList()
        class MergedLogger(val name: String) : Logger {
            override fun onEvent(category: Logger.Category, event: Logger.Event, level: Logger.LogLevel) {
                log += "$name: $category $event"
            }
        }

        val logger1 = DefaultLogger()
        val client1 = setupClient(SERVER_TEST_DIR_1, url1, clientDir, "user1", "password1", logger1)
        client1.logger.children += MergedLogger("Client1")
        client1.start()

        val logger2 = DefaultLogger()
        val client2 = setupClient(SERVER_TEST_DIR_2, url2, clientDir, "user2", "password2", logger2)
        client2.logger.children += MergedLogger("Client2")
        client2.start()

        try {
            val client1Done = waitForBranchesSynced(logger1, listOf(client1.userData.branch,
                    *client1.userData.getInQueues().map { it.branchInfo.get().branch }.toTypedArray(),
                    *client1.userData.getOutQueues().map { it.branchInfo.get().branch }.toTypedArray()))
            assertTrue(client1Done)

            val client2Done = waitForBranchesSynced(logger2, listOf(client2.userData.branch,
                    *client2.userData.getInQueues().map { it.branchInfo.get().branch }.toTypedArray(),
                    *client2.userData.getOutQueues().map { it.branchInfo.get().branch }.toTypedArray()))
            assertTrue(client2Done)

            client2.withLooper {
                contactRequestHandler.handleManager.addListener {
                    // auto accept
                    client2.context.looper.schedule {
                        it.accept()
                    }
                }
            }

            client1.withLooper {
                sendContactRequest(client2.userData.remotes.listRemotes().first(), null,
                        null, true)
            }

            var contactExist = false
            for (i in 0..500) {
                client1.withLooper {
                    if (userData.contacts.list.get(client2.userData.id.get()).id.exists())
                        contactExist = true
                }
                if (contactExist)
                    break
                delay(50)
            }
            assertTrue(contactExist)

            assertTrue(client2.withLooper {
                userData.contacts.list.get(client1.userData.id.get()).id.exists()
            })

            // create a branch and grant access to a contact
            val storageContext = "org.fejoa.storage"
            val storageBranchName = UserData.generateBranchName()
            client1.withLooper {
                val keyData = CryptoSettings.default.symmetric.generateSecretKeyData()
                val storageBranch = userData.createStorageDir(storageContext, storageBranchName.toHex(),
                        "data branch", keyData)
                val remote = userData.remotes.listRemotes().first()
                storageBranch.updateRemote(RemoteRef(remote.id, LoginAuthInfo()))
                userData.commit()

                val contact = userData.contacts.list.listContacts().first()

                grantAccess(contact, storageBranch, AccessRight.PULL_PUSH)
            }

            val branchAcceptedFuture = Future<Boolean>()
            client2.grantAccessHandler.handles.addListener {
                // auto accept
                client2.context.looper.schedule {
                    it.accept()
                    branchAcceptedFuture.setResult(true)
                }
            }
            withTimeout(10000L) {
                branchAcceptedFuture.awaitCancellable()
            }

            client2.withLooper {
                val contactClient1 = userData.contacts.list.listContacts().first()
                val remoteRefs = contactClient1.branches.get(storageContext).get(storageBranchName)
                        .locations.listRemoteRefs()
                assertEquals(remoteRefs.first().authInfo.type, AuthType.TOKEN)
            }

            val branchUpdateReceivedFuture = Future<Boolean>()
            client2.branchUpdateHandler.handles.addListener {
                assertEquals(it.command.branch, storageBranchName)
                branchUpdateReceivedFuture.setResult(true)
            }

            withTimeout(10001L) {
                branchUpdateReceivedFuture.awaitCancellable()
            }

            // test pulling the shared branch
            val syncJob = client2.withLooper {
                val contact = userData.contacts.list.listContacts().first()
                val branchMap =contact.branches.get(storageContext)
                val branchName = branchMap.list().first()
                val branch = branchMap.get(HashValue.fromHex(branchName))
                val remoteRef = branch.locations.listRemoteRefs().first()
                val branchRemote = contact.remotes.listRemotes().firstOrNull {
                    it.id == remoteRef.remoteId
                } ?: throw Exception("Unknown remote")

                val storageDir = userData.getStorageDir(branch, false)
                val syncManager = getSyncManager()
                syncManager.sync(userData.context, storageDir, branchRemote, remoteRef.authInfo)
            }

            val result = syncJob.await()
            assertTrue { result.code == ReturnType.OK}

            // migration
            val oldRemote = client1.userData.remotes.listRemotes().first()
            // first register a new account
            val registerResult = client1.registerAccount("user3", url3,
                    "password1", oldRemote.id)
            assertEquals(ReturnType.OK, registerResult.first.code)
            val newRemote = registerResult.second!!
            assertEquals(ReturnType.OK, registerResult.first.code)
            client1.stop()
            val migrationManager = MigrationManager(client1)
            migrationManager.migrate(oldRemote, newRemote)
            client1.start()

            var remoteUpdated = false
            for (i in 0 until 200) {
                val remote = client2.withLooper {
                    val contact = userData.contacts.list.listContacts().first()
                    contact.remotes.listRemotes().first()
                }
                if (remote.server == url3) {
                    remoteUpdated = true
                    break
                }
                delay(50)
            }
            assertTrue(remoteUpdated)
            println("done!")
        } catch (e: Exception) {
            println(e)
            System.exit(1)
        }
        finally {
            client1.withLooper { stop() }
            client2.withLooper { stop() }
            // keep the coroutine alive for a bit so that we can finish
            delay(500)
        }
    }
}

