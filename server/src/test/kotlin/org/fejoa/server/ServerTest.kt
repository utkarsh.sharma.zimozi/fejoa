package org.fejoa.server

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.fejoa.*
import org.fejoa.crypto.*
import org.fejoa.network.*

import java.io.File
import java.util.ArrayList

import org.fejoa.server.JettyServer.Companion.DEFAULT_PORT
import org.fejoa.support.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.lang.Exception
import kotlin.test.assertEquals
import kotlin.test.assertTrue


class JettyTest {
    companion object {
        internal const val TEST_DIR = "jettyTest"
        internal const val SERVER_TEST_DIR = "$TEST_DIR/Server"
    }

    private val cleanUpDirs: MutableList<String> = ArrayList()
    private var server: JettyServer? = null
    private val url = "http://localhost:$DEFAULT_PORT/${JettyServer.FEJOA_PORTAL_PATH}"

    @Before
    fun setUp() {
        cleanUpDirs.add(TEST_DIR)
        for (dir in cleanUpDirs)
            File(dir).deleteRecursively()

        server = JettyServer(SERVER_TEST_DIR)
        server!!.setDebugNoAccessControl(true)
        server!!.start()
    }

    @After
    fun tearDown() {
        Thread.sleep(1000)
        server!!.stop()

        for (dir in cleanUpDirs)
            File(dir).deleteRecursively()
    }

    @Test
    fun testSimple() = runBlocking {
        val request = platformCreateHTTPRequest(url)
        val result = PingJob().run(request)

        assertEquals("ping pong", result.headerResponse)
        assertEquals("PING PONG", result.dataResponse)
    }

    @Test
    fun testRegistrationAndAuth() = runBlocking {
        val user = "testUser"
        val password = "password"

        val keyCache = BaseKeyCache()
        val userKeyParams = UserKeyParams(BaseKeyParams(salt = CryptoHelper.crypto.generateSalt16(),
                kdf = CryptoSettings.default.kdf),
                CryptoSettings.HASH_TYPE.SHA256, CryptoSettings.SYM_ALGO.AES_CTR, 256,
                CryptoHelper.crypto.generateSalt16().encodeBase64())
        val secret = keyCache.getUserKey(userKeyParams, password).await()

        val request = platformCreateHTTPRequest(url)
        val authParams = LoginParams(userKeyParams,
                CompactPAKE_SHA256_CTR.getSharedSecret(DH_GROUP.RFC5114_2048_256, secret),
                DH_GROUP.RFC5114_2048_256)

        val registerResult = RegisterJob(user, authParams).run(request)
        assertEquals(ReturnType.OK, registerResult.code)

        val serverAccountIO = platformGetAccountIO(AccountIO.Type.SERVER, SERVER_TEST_DIR, user)
        val readAuthParams = serverAccountIO.readLoginData()
        assertEquals(authParams, readAuthParams)

        assertEquals(1, AuthStatusJob().run(request).response!!.accounts.size)
        var logoutResult = LogoutJob(listOf(user)).run(request)
        assertEquals(0, logoutResult.accounts.size)
        // assert we are not logged in
        assertEquals(0, AuthStatusJob().run(request).response!!.accounts.size)

        val failedAuthResult = LoginJob(user, "wrong password", keyCache).run(request)
        assertEquals(ReturnType.ERROR, failedAuthResult.code)
        assertEquals(0, AuthStatusJob().run(request).response!!.accounts.size)

        val authResult = LoginJob(user, password, keyCache).run(request)
        assertEquals(ReturnType.OK, authResult.code)
        assertEquals(1, AuthStatusJob().run(request).response!!.accounts.size)
        assertEquals(user, AuthStatusJob().run(request).response!!.accounts[0])

        logoutResult = LogoutJob(listOf(user)).run(request)
        assertEquals(0, logoutResult.accounts.size)
        assertEquals(0, AuthStatusJob().run(request).response!!.accounts.size)
    }

    private suspend fun waitForBranchesSynced(logger: DefaultLogger, branches: List<String>): Boolean {
        (1 .. 100).forEach { _ ->
            val syncedBranches = logger.getLogTypeMap(Logger.Category.INFO)
                    .getEvents(Logger.TYPE.SYNC_MANAGER)
                    .asSequence()
                    .map { it.first.data as SyncEvent }
                    .filter { branches.contains(it.branch)
                            && it.branchType == SyncEvent.BranchType.BRANCH
                            && it.status == SyncEvent.Status.SYNCED
                    }
                    .toList()

            if (syncedBranches.size >= branches.size) {
                val done = branches.none { branch -> syncedBranches.find { it.branch == branch } == null }
                if (done)
                    return true
            }
            delay(50)
        }
        return false
    }

    @Test
    fun testRetrieveAccount() = runBlocking {
        val user = "user"
        val password = "password"
        val clientDir = PathUtils.appendDir(TEST_DIR, "userDataCreation")
        val passwordGetter = object : PasswordGetter {
            override suspend fun get(purpose: PasswordGetter.Purpose, resource: String, info: String): String {
                return password
            }
        }

        val client1 = Client.create(clientDir, user, password)
        val looper = client1.context.looper

        client1.connectionAuthManager.passwordGetter = passwordGetter
        val registerResult = client1.registerAccount(user, url, password)
        assertEquals(ReturnType.OK, registerResult.first.code)

        val serverAccountIO = platformGetAccountIO(AccountIO.Type.SERVER, SERVER_TEST_DIR, user)
        assertTrue(serverAccountIO.exists())

        // start syncing (nothing to do yet)
        val logger = DefaultLogger()
        client1.logger.children += logger

        // sync client1 with server
        val remote = registerResult.second!!
        client1.setBranchLocation(remote, LoginAuthInfo(),  UserData.USER_DATA_CONTEXT)
        client1.start()

        try {
            looper.runSync {
                // this should be noticed by the sync manager and then synced
                client1.userData.commit()
            }
            val synced = waitForBranchesSynced(logger, listOf(client1.userData.branch,
                    *client1.userData.getInQueues().map { it.branchInfo.get().branch }.toTypedArray(),
                    *client1.userData.getOutQueues().map { it.branchInfo.get().branch }.toTypedArray()))
            if (!synced)
                throw Exception("Not synced")
            logger.clear()

            // retrieve data to a second account

            looper.runSync {
                val client2 = Client.retrieveAccount(clientDir, "userDir2", url, user, passwordGetter)
                if (client1.userData.branch != client2.userData.branch)
                    throw Exception("Branch mismatch")
            }
        } catch (e: Exception) {
            println(e)
            System.exit(1)
        } finally {
            looper.runSync {
                client1.stop()
            }
        }
    }
}

