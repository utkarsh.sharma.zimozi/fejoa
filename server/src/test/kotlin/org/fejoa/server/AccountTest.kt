package org.fejoa.server

import kotlinx.coroutines.runBlocking
import org.fejoa.*
import org.fejoa.crypto.*
import org.fejoa.network.*

import java.io.File
import java.util.ArrayList

import org.fejoa.server.JettyServer.Companion.DEFAULT_PORT
import org.fejoa.support.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull


class ConfirmRegistrationTest {
    companion object {
        internal val TEST_DIR = "confirmRegistrationTest"
        internal val SERVER_TEST_DIR = TEST_DIR + "/Server"
    }

    internal val cleanUpDirs: MutableList<String> = ArrayList()
    internal var server: JettyServer? = null
    val url = "http://localhost:$DEFAULT_PORT/${JettyServer.FEJOA_PORTAL_PATH}"

    @Before
    fun setUp() {
        cleanUpDirs.add(TEST_DIR)

        server = JettyServer(FejoaServerConfig(directory = SERVER_TEST_DIR,
                registration = ConfirmRegistrationConfig(Portal.RegisterConfirm.DEBUG_TOKEN)))
        server!!.setDebugNoAccessControl(true)
        server!!.start()
    }

    @After
    fun tearDown() {
        Thread.sleep(1000)
        server!!.stop()

        for (dir in cleanUpDirs)
            File(dir).deleteRecursively()
    }

    @Test
    fun testConfirmationRegistrationAndAuth() = runBlocking {
        val user = "testUser"
        val password = "password"

        val keyCache = BaseKeyCache()
        val userKeyParams = UserKeyParams(BaseKeyParams(salt = CryptoHelper.crypto.generateSalt16(),
                kdf = CryptoSettings.default.kdf),
                CryptoSettings.HASH_TYPE.SHA256, CryptoSettings.SYM_ALGO.AES_CTR, 256,
                CryptoHelper.crypto.generateSalt16().encodeBase64())
        val secret = keyCache.getUserKey(userKeyParams, password).await()

        val request = platformCreateHTTPRequest(url)
        val authParams = LoginParams(userKeyParams,
                CompactPAKE_SHA256_CTR.getSharedSecret(DH_GROUP.RFC5114_2048_256, secret),
                DH_GROUP.RFC5114_2048_256)

        val registerResult = RegisterJob(user, authParams).run(request)
        assertEquals(ReturnType.OK, registerResult.code)
        assertEquals(registerResult.reply!!.result, RegisterJob.ReplyType.CONFIRMATION_REQUIRED)

        assertEquals(0, AuthStatusJob().run(request).response!!.accounts.size)

        val confirmationResult = ConfirmRegistrationJob(user, RegisterHandler.DEBUG_TOKEN).run(request)
        assertEquals(ReturnType.OK, confirmationResult.code)
        assertEquals(confirmationResult.reply!!.result, RegisterJob.ReplyType.REGISTERED)

        val serverAccountIO = platformGetAccountIO(AccountIO.Type.SERVER, SERVER_TEST_DIR, user)
        val readAuthParams = serverAccountIO.readLoginData()
        assertEquals(authParams, readAuthParams)

        assertEquals(1, AuthStatusJob().run(request).response!!.accounts.size)
        var logoutResult = LogoutJob(listOf(user)).run(request)
        assertEquals(0, logoutResult.accounts.size)
        // assert we are not logged in
        assertEquals(0, AuthStatusJob().run(request).response!!.accounts.size)

        val failedAuthResult = LoginJob(user, "wrong password", keyCache).run(request)
        assertEquals(ReturnType.ERROR, failedAuthResult.code)
        assertEquals(0, AuthStatusJob().run(request).response!!.accounts.size)

        val authResult = LoginJob(user, password, keyCache).run(request)
        assertEquals(ReturnType.OK, authResult.code)
        assertEquals(1, AuthStatusJob().run(request).response!!.accounts.size)
        assertEquals(user, AuthStatusJob().run(request).response!!.accounts[0])

        logoutResult = LogoutJob(listOf(user)).run(request)
        assertEquals(0, logoutResult.accounts.size)
        assertEquals(0, AuthStatusJob().run(request).response!!.accounts.size)
    }

    @Test
    fun testUpdateAccount() = runBlocking {
        val user = "testUserUpdateAccount"
        val password = "password"

        // setup account
        val keyCache = BaseKeyCache()
        val userKeyParams = UserKeyParams(BaseKeyParams(salt = CryptoHelper.crypto.generateSalt16(),
                kdf = CryptoSettings.default.kdf),
                CryptoSettings.HASH_TYPE.SHA256, CryptoSettings.SYM_ALGO.AES_CTR, 256,
                CryptoHelper.crypto.generateSalt16().encodeBase64())
        val secret = keyCache.getUserKey(userKeyParams, password).await()

        val request = platformCreateHTTPRequest(url)
        val authParams = LoginParams(userKeyParams,
                CompactPAKE_SHA256_CTR.getSharedSecret(DH_GROUP.RFC5114_2048_256, secret),
                DH_GROUP.RFC5114_2048_256)

        val registerResult = RegisterJob(user, authParams).run(request)
        assertEquals(ReturnType.OK, registerResult.code)
        assertEquals(registerResult.reply!!.result, RegisterJob.ReplyType.CONFIRMATION_REQUIRED)

        assertEquals(0, AuthStatusJob().run(request).response!!.accounts.size)

        val confirmationResult = ConfirmRegistrationJob(user, RegisterHandler.DEBUG_TOKEN).run(request)
        assertEquals(ReturnType.OK, confirmationResult.code)
        assertEquals(confirmationResult.reply!!.result, RegisterJob.ReplyType.REGISTERED)

        // update server config
        val userDataConfig = UserDataConfig.create(CryptoHelper.crypto.generateSymmetricKey(
                CryptoSettings.default.symmetric), "password", userKeyParams,
                UserDataRef("testBranch", CryptoSettings.SYM_ALGO.AES_CTR), keyCache)
        val serverConfig = StorageConfig(userDataConfig, "outQueue", "inQueue", "accessStore")
        val updateResult = UpdateServerConfigJob(user, null,
                serverConfig).run(request)
        assertEquals(ReturnType.OK, updateResult.code)
        // validate updated config
        val getResult = GetServerConfigJob(user).run(request)
        assertEquals(ReturnType.OK, getResult.code)
        assertNotNull(getResult.storageConfig)
        assertEquals("inQueue", getResult.storageConfig!!.inQueue)

        // test password update
        val password2 = "password2"
        val userKeyParams2 = UserKeyParams(userKeyParams.baseKeyParams, CryptoSettings.HASH_TYPE.SHA256,
                CryptoSettings.SYM_ALGO.AES_CTR, 256,
                CryptoHelper.crypto.generateSalt16().encodeBase64())
        val secret2 = keyCache.getUserKey(userKeyParams2, password2).await()
        val authParams2 = LoginParams(userKeyParams2,
                CompactPAKE_SHA256_CTR.getSharedSecret(DH_GROUP.RFC5114_2048_256, secret2),
                DH_GROUP.RFC5114_2048_256)
        val updateResult2 = UpdateServerConfigJob(user, authParams2, null)
                .run(request)
        assertEquals(ReturnType.OK, updateResult2.code)

        // logout
        val logoutResult = LogoutJob(listOf(user)).run(request)
        assertEquals(ReturnType.OK, logoutResult.code)

        // and login with the new password
        val loginResult = LoginJob(user, password2, keyCache).run(request)
        assertEquals(ReturnType.OK, loginResult.code)
    }
}

