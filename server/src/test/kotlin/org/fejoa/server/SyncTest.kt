package org.fejoa.server

import kotlinx.coroutines.runBlocking
import org.fejoa.AccountIO
import org.fejoa.FejoaContext
import org.fejoa.network.PullJob
import org.fejoa.network.PushJob
import org.fejoa.network.platformCreateHTTPRequest
import org.fejoa.repository.Repository
import org.fejoa.repository.ThreeWayMerge
import org.fejoa.repository.sync.Request
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.io.File
import java.util.*
import kotlin.test.assertEquals


class SyncTest {
    companion object {
        internal const val TEST_DIR = "jettySyncTest"
        internal const val SERVER_TEST_DIR = "$TEST_DIR/Server"
    }

    private val cleanUpDirs: MutableList<String> = ArrayList()
    private var server: JettyServer? = null
    private val url = "http://localhost:${JettyServer.DEFAULT_PORT}/${JettyServer.FEJOA_PORTAL_PATH}"

    @Before
    fun setUp() {
        cleanUpDirs.add(TEST_DIR)

        server = JettyServer(SERVER_TEST_DIR)
        server!!.setDebugNoAccessControl(true)
        server!!.start()
    }

    @After
    fun tearDown() {
        Thread.sleep(1000)
        server!!.stop()

        for (dir in cleanUpDirs)
            File(dir).deleteRecursively()
    }

    @Test
    fun testPushPull() = runBlocking {
        val serverUser = "user1"
        val localCSDir = "$TEST_DIR/ClientStore"
        val branch = "testBranch"

        // push
        val localContext = FejoaContext(AccountIO.Type.CLIENT, localCSDir, "clientuser1",
                this.coroutineContext)
        val local = localContext.getStorage(branch, null, true)
        local.writeString("testFile", "testData")
        local.commit()

        val request = platformCreateHTTPRequest(url)
        val result = PushJob(local.getBackingDatabase() as Repository, serverUser, branch).run(request)

        assertEquals(Request.ResultType.OK, result.result)

        val localContext2 = FejoaContext(AccountIO.Type.CLIENT, localCSDir, "clientuser2",
                this.coroutineContext)
        val local2 = localContext2.getStorage(branch, null, false)

        val pullResult = PullJob(local2.getBackingDatabase() as Repository, null, ThreeWayMerge(),
                serverUser, branch).run(request)

        assertEquals(null, pullResult.oldHead)
        assertEquals(local.getHead(), pullResult.remoteHead)
        assertEquals(local.getHead(), local2.getHead())
    }

}