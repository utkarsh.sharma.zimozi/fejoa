#!/bin/bash
set -e
set -x

SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`
source `realpath "$SCRIPTPATH/../config.sh"`

certbot renew --post-hook $LETS_ENCRYPT_KEY_STORE_CONVERT_SCRIPT

