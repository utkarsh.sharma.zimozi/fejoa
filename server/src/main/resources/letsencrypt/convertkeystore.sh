#!/bin/bash
set -e
set -x

SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`
source `realpath "$SCRIPTPATH/../config.sh"`

(
  cd $LETS_ENCRYPT_KEYSTORE_PATH
  openssl pkcs12 -export -in fullchain.pem -inkey privkey.pem -out $LETS_ENCRYPT_KEYSTORE_P12_FILE -name $USER -CAfile chain.pem -caname root -passout pass:$LETS_ENCRYPT_KEYSTORE_P12_PASSWORD
  chmod a+r $LETS_ENCRYPT_KEYSTORE_P12_FILE
  chown $USER $LETS_ENCRYPT_KEYSTORE_P12_FILE
)
