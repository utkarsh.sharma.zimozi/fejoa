#!/bin/sh

START_SERVER_SCRIPT=./start.sh
# unix user name
USER=<USERNAME>

# pid and log file for the server
PIDFILE=fejoaserver.pid
LOGFILE=fejoaserver.log

# Lets Encrypt
LETS_ENCRYPT_KEY_STORE_CONVERT_SCRIPT="convertkeystore.sh"

LETS_ENCRYPT_KEYSTORE_PATH="/etc/letsencrypt/live/domain"
LETS_ENCRYPT_KEYSTORE_P12_FILE="keystore.p12"
LETS_ENCRYPT_KEYSTORE_P12_PASSWORD=""
