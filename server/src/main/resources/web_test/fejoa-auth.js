function FejoaAuth() {
    // origin -> listener
    this.authListeners = new Map();

    var that = this;
    window.addEventListener("message", function(event) {
        // only accept messages from ourselves
        if (event.source != window)
            return;

        if (event.data.type && (event.data.type == "fejoa_auth_update"))
            that._notifyAuthStatusChanged(event.data);
    }, false);

    this._notifyAuthStatusChanged = function(data) {
        var listener = this.authListeners.get(data.origin);
        if (listener == undefined)
            return;

        listener(data);
    }

    /*
    The listener must be of the form function(object data).
    The data has the fields:
        user:       the current user or an empty string
        authStatus: LOGGED_INT|LOGGED_OUT|LOGGING_IN|REGISTERING
     */
    this.addAuthStatusListener = function(origin, listener) {
    	if (typeof(listener) != "function")
    		return;
        this.authListeners.set(origin, listener);
    }

    this.removeAuthStatusListener = function(origin) {
        this.authListeners.remove(origin)
    }
}
