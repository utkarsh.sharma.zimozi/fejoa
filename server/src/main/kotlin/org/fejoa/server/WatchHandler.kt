package org.fejoa.server

import kotlinx.coroutines.runBlocking
import org.fejoa.network.JsonRPCRequest
import org.fejoa.network.WatchJob
import org.fejoa.repository.Repository
import org.fejoa.repository.sync.AccessRight
import org.fejoa.support.encodeBase64
import java.io.InputStream
import java.util.ArrayList


class WatchHandler : JsonRequestHandler(WatchJob.METHOD) {
    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?, session: Session)
            = runBlocking {
        val request = JsonRPCRequest.parse(WatchJob.RPCParams.serializer(), json)
        val params = request.params

        var peek: Boolean = params.peek
        val branches = params.branches

        val status: MutableList<WatchJob.BranchStatus> = ArrayList()
        val denied: MutableList<String> = ArrayList()

        watch(session, peek, branches, status, denied)

        if (status.isEmpty() && denied.isEmpty() && !peek) {
            // timeout
            val response = request.makeResponse(WatchJob.RPCResult()).stringify(WatchJob.RPCResult.serializer())
            responseHandler.setResponseHeader(response)
            return@runBlocking
        }

        val response = request.makeResponse(WatchJob.RPCResult(status, denied))
                .stringify(WatchJob.RPCResult.serializer())
        responseHandler.setResponseHeader(response)
    }

    private suspend fun watch(session: Session, peek: Boolean, branches: List<WatchJob.WatchEntry>,
                              status: MutableList<WatchJob.BranchStatus>, denied: MutableList<String>) {
        //TODO: use a file monitor instead of polling
        val TIME_OUT = (60 * 1000).toLong()
        val time = System.currentTimeMillis()
        while (status.isEmpty() && denied.isEmpty()) {
            for (entry in branches) {
                val accessControl = session.getServerAccessManager()
                val branch = entry.branch
                val user = internalUsername(entry.user)


                val rights = accessControl.getBranchAccessRight(user, branch)
                if (rights.value and AccessRight.PULL.value == 0) {
                    denied.add(branch)
                    continue
                }
                // TODO allow to watch all branches not only the index:
                UserThreadContext.withLock(user) {
                    val indexStorage = session.getContext(user).getLocalIndexStorage().getBackingDatabase() as Repository
                    val serverHead = indexStorage.branchBackend.getBranchLog().getHead()
                    val serverTip = serverHead?.entryId
                    val serverMessage = serverHead?.message?.data ?: ByteArray(0)

                    if (entry.logTip != serverTip || peek)
                        status.add(WatchJob.BranchStatus(user, branch, serverTip, serverMessage.encodeBase64()))
                }
            }
            if (System.currentTimeMillis() - time > TIME_OUT)
                break
            if (peek)
                break
            if (status.isEmpty() && denied.isEmpty()) {
                try {
                    Thread.sleep(500L)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

            }
        }
    }
}
