package org.fejoa.server

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.fejoa.support.Locker


/**
 * Provides a per user context thread to synchronize calls to a single user account.
 *
 * Each call runs exclusively.
 */
object UserThreadContext {
    private class RefCountValue<out T>(val value: T, var count: Int = 0)
    private val map: MutableMap<String, RefCountValue<Locker>> = HashMap()

    private fun createLocker(): Locker = object : Locker {
        val mutex = Mutex()
        override suspend fun <T> runSync(block: suspend () -> T): T {
            return mutex.withLock { block.invoke() }
        }
    }

    private fun acquireDispatcher(user: String): Locker = synchronized(map) {
        val ref = map[user] ?: RefCountValue(createLocker()).also { map[user] = it }
        ref.count ++
        ref.value
    }

    private fun releaseDispatcher(user: String) = synchronized(map) {
        val ref = map[user] ?: throw Exception("Internal error")
        ref.count--
        if (ref.count == 0)
            map.remove(user)
    }

    fun getLocker(user: String): Locker = object : Locker {
        override suspend fun <T> runSync(block: suspend () -> T): T {
            return UserThreadContext.withLock(user, block)
        }
    }

    suspend fun <T>withLock(user: String, block: suspend () -> T): T {
        val locker = acquireDispatcher(user)
        return try {
            locker.runSync { block.invoke() }
        } finally {
            releaseDispatcher(user)
        }
    }
}