package org.fejoa.server

import kotlinx.serialization.serializer
import org.fejoa.network.JsonRPCRequest
import org.fejoa.network.LogoutJob
import java.io.InputStream


class LogoutHandler : JsonRequestHandler(LogoutJob.METHOD) {
    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?,
                        session: Session) {

        val request = JsonRPCRequest.parse(LogoutJob.Params.serializer(), json)

        val accessManager = session.getServerAccessManager()
        if (request.params.users.isEmpty()) {
            accessManager.removeAllAccountAccess()
        } else {
            request.params.users.forEach {
                val user = internalUsername(it)
                accessManager.removeAccountAccess(user)
            }
        }

        val authenticatedAccounts = session.getServerAccessManager().getAuthAccounts().map { it }
        val response = request.makeResponse(LogoutJob.Response(authenticatedAccounts))
                .stringify(LogoutJob.Response.serializer())
        responseHandler.setResponseHeader(response)
    }
}