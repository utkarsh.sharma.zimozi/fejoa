package org.fejoa.server

import kotlinx.coroutines.runBlocking
import kotlinx.serialization.internal.StringSerializer
import org.fejoa.BranchIndex
import org.fejoa.ConnectionAuthManager
import org.fejoa.network.*
import org.fejoa.storage.BranchLogEntry
import org.fejoa.repository.Repository
import org.fejoa.repository.sync.BranchLogProtocol
import org.fejoa.repository.sync.ChunkIteratorProtocol
import org.fejoa.repository.sync.ChunkIteratorProtocol.DEFAULT_MAX_CHUNKS_PER_REQUEST
import org.fejoa.repository.sync.Request.BRANCH_REQUEST_METHOD
import org.fejoa.support.await
import java.io.InputStream


class RemotePullHandler : JsonRequestHandler(RemotePullJob.METHOD) {
    class PullRepoJob(val repository: Repository, val user: String, val branch: String)
        : RemoteJob<PullRepoJob.Result>() {

        class Result(code: ReturnType, message: String, val latestEntry: BranchLogEntry?)
            : RemoteJob.Result(code, message)

        private fun getHeader(): String {
            return JsonRPCRequest(id = id, method = BRANCH_REQUEST_METHOD, params = StorageRPCParams(user, branch))
                    .stringify(StorageRPCParams.serializer())
        }

        override suspend fun run(remoteRequest: RemoteRequest): PullRepoJob.Result {
            val pipe = RemotePipeImpl(getHeader(), remoteRequest, null)
            val entries = BranchLogProtocol.getBranchLog(pipe, branch, 0).entries
            val branchLog = repository.branchBackend.getBranchLog()
            branchLog.add(entries)

            val rawAccessor = repository.accessors.getRawAccessor()
            ChunkIteratorProtocol.getAllChunks(pipe, branch, DEFAULT_MAX_CHUNKS_PER_REQUEST) { list ->
                list.map { it.id to rawAccessor.putChunk(it.chunk) }.map { it.first to it.second.await() }.forEach {
                    val expectedId = it.first
                    val putResult = it.second
                    if (putResult.key != expectedId)
                        throw Exception("Hash mismatch; expected: $expectedId got: ${putResult.key}")
                }
            }
            repository.getCurrentTransaction().finishTransaction().await()

            return Result(ReturnType.OK, "ok", entries.firstOrNull())
        }
    }

    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?,
                        session: Session) = runBlocking {
        val request = JsonRPCRequest.parse(RemotePullJob.Params.serializer(),
                json)
        val params = request.params
        val user = internalUsername(params.user)

        val context = session.getContext(user)
        val connectionManager = ConnectionAuthManager(context)

        val storageDir = context.getStorage(params.branch, null, false)
        val repo = storageDir.getBackingDatabase() as Repository


        val result = connectionManager.send(
                PullRepoJob(repo, params.remote.user, params.branch), params.remote, params.token).await()
        if (result.code != ReturnType.OK) {
            responseHandler.setResponseHeader(request.makeError(result.code, result.message))
            return@runBlocking
        }

        // update branch index
        if (result.latestEntry != null) {
            UserThreadContext.withLock(user) {
                context.branchIndexLocker.runSync {
                    val index = BranchIndex(session.getBranchIndex(user))
                    index.update(params.branch, result.latestEntry.entryId)
                    index.commit()
                }
            }
        }

        val response = request.makeResponse(
                "Branch ${params.branch} pulled from ${params.remote.address()}").stringify(StringSerializer)
        responseHandler.setResponseHeader(response)
    }
}
