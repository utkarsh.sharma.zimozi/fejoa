package org.fejoa.server

import kotlinx.serialization.json.JSON
import org.fejoa.network.AuthStatusJob
import org.fejoa.network.JsonRPCSimpleRequest
import java.io.InputStream


class AuthStatusHandler : JsonRequestHandler(AuthStatusJob.METHOD) {
    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?,
                        session: Session) {

        val request = JSON.parse(JsonRPCSimpleRequest.serializer(), json)

        val authenticatedAccounts = session.getServerAccessManager().getAuthAccounts().map {
            fromInternalUsername(it)
        }
        val ongoingRegistrations = session.getOngoingRegistrationManager()
                .getTokens().entries.mapNotNull {
            val user = fromInternalUsername(it.key)
            if (it.value.config.type != Portal.RegisterConfirm.EMAIL)
                return@mapNotNull null
            AuthStatusJob.PendingRegistration(user, AuthStatusJob.RegistrationType.EMAIL)
        }
        val response = request.makeResponse(AuthStatusJob.Response(authenticatedAccounts, ongoingRegistrations))
                .stringify(AuthStatusJob.Response.serializer())
        responseHandler.setResponseHeader(response)
    }
}
