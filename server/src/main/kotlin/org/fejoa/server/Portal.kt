package org.fejoa.server

import org.fejoa.network.*
import org.fejoa.support.*

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import java.io.*
import java.io.IOException
import java.util.ArrayList
import javax.servlet.http.HttpServlet


class Portal(private val baseDir: String, registerConfirm: ConfirmRegistrationConfig) : HttpServlet() {
    private val jsonHandlers = ArrayList<JsonRequestHandler>()

    enum class RegisterConfirm {
        NONE,
        EMAIL,
        DEBUG_TOKEN // uses a predefined token
    }

    init {
        addJsonHandler(JsonPingHandler())
        addJsonHandler(RegisterHandler(registerConfirm))
        addJsonHandler(ConfirmRegistrationHandler())
        addJsonHandler(LoginHandler())
        addJsonHandler(LogoutHandler())
        addJsonHandler(AuthStatusHandler())
        addJsonHandler(GetServerConfigHandler())
        addJsonHandler(UpdateServerConfigHandler())
        addJsonHandler(BranchRequestHandler())
        addJsonHandler(WatchHandler())
        addJsonHandler(RemoteIndexRequestHandler())
        addJsonHandler(CommandHandler())
        addJsonHandler(TokenAuthHandler())
        addJsonHandler(StartMigrationHandler())
        addJsonHandler(RemotePullHandler())
        addJsonHandler(RequestRegistrationTokenHandler())

        // account management
        addJsonHandler(ListBranchesHandler())
        addJsonHandler(DeleteBranchHandler())
        addJsonHandler(DeleteStorageConfigHandler())
    }

    inner class ResponseHandler(val origin: String, private val response: HttpServletResponse) {
        var isHandled = false
            private set
        private val outputStream: OutputStream = response.outputStream

        fun setResponseHeader(header: String) {
            isHandled = true
            response.contentType = "application/octet-stream"
            response.addHeader("Access-Control-Allow-Origin", origin)
            response.addHeader("Access-Control-Allow-Credentials", "true")
            outputStream.write(header.toUTF())
            outputStream.write(0)
        }

        fun addData(): OutputStream {
            if (!isHandled)
                throw Exception("Unhandled")
            return outputStream
        }

        @Throws(IOException::class)
        fun finish() {
            outputStream.flush()
        }
    }

    private fun addJsonHandler(handler: JsonRequestHandler) {
        jsonHandlers.add(handler)
    }

    override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
        handle(req, resp)
    }

    override fun doPost(req: HttpServletRequest, resp: HttpServletResponse) {
        handle(req, resp)
    }

    fun handle(httpServletRequest: HttpServletRequest,
                        response: HttpServletResponse) {
        response.status = HttpServletResponse.SC_OK

        val input = httpServletRequest.inputStream
        
        val session = Session(baseDir, httpServletRequest.session)
        val origin = httpServletRequest.getHeader("Origin") ?: "*"
        val responseHandler = ResponseHandler(origin, response)

        val header = HTTPRequest.parseHeader(input)
        val error = handleJson(responseHandler, header, input, session)

        if (!responseHandler.isHandled || error != null)
            responseHandler.setResponseHeader(error!!)

        responseHandler.finish()
    }


    private fun handleJson(responseHandler: ResponseHandler, message: String, data: InputStream?, session: Session): String? {
        val request = try {
            JsonRPCMethodRequest.parse(message)
        } catch (e: Exception) {
            return JsonRPCMethodRequest.makeError(-1, ReturnType.INVALID_JSON_REQUEST, "can't parse json")
        }
        for (handler in jsonHandlers) {
            if (handler.method != request.method)
                continue

            try {
                handler.handle(responseHandler, message, data, session)
            } catch (e: Exception) {
                e.printStackTrace()
                return request.makeError(ReturnType.EXCEPTION, e.message ?: "")
            }

            if (responseHandler.isHandled)
                return null
        }

        return request.makeError(ReturnType.NO_HANDLER_FOR_REQUEST, "can't handle request")
    }
}