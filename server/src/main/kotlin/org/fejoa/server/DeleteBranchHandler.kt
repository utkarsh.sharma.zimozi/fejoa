package org.fejoa.server

import kotlinx.coroutines.runBlocking
import org.fejoa.BranchIndex
import org.fejoa.network.*
import org.fejoa.storage.Config
import java.io.InputStream


class DeleteBranchHandler : JsonRequestHandler(DeleteBranchJob.METHOD) {
    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?,
                        session: Session) = runBlocking {
        val request = JsonRPCRequest.parse(
                DeleteBranchJob.Params.serializer(), json)

        val user = internalUsername(request.params.user)
        val accessManager = session.getServerAccessManager()
        if (!accessManager.hasAccountAccess(user)) {
            responseHandler.setResponseHeader(request.makeError(ReturnType.ACCESS_DENIED,
                    "User not authenticated"))
            return@runBlocking
        }
        val context = session.getContext(user)
        val branchIndex = BranchIndex(context.getLocalIndexStorage())
        val storage = context.platformStorage
        val deleteBranches: MutableList<String> = ArrayList()
        UserThreadContext.withLock(user) {
            for (branch in request.params.branches) {
                if (!storage.exists(user, branch))
                    continue

                storage.delete(user, branch)
                deleteBranches += branch

                branchIndex.update(branch, Config.newDataHash())
            }
            branchIndex.commit()
        }

        val response = request.makeResponse(DeleteBranchJob.Reply(deleteBranches)).stringify(
                DeleteBranchJob.Reply.serializer())
        responseHandler.setResponseHeader(response)
    }
}
