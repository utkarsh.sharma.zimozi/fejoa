package org.fejoa.server

import kotlinx.coroutines.runBlocking
import kotlinx.serialization.internal.StringSerializer
import org.fejoa.AccountIO
import org.fejoa.MigrationConfig
import org.fejoa.network.JsonRPCRequest
import org.fejoa.network.ReturnType
import org.fejoa.network.StartMigrationJob
import org.fejoa.network.makeError
import org.fejoa.platformGetAccountIO
import java.io.InputStream


class StartMigrationHandler : JsonRequestHandler(StartMigrationJob.METHOD) {
    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?,
                        session: Session) = runBlocking {

        val request = JsonRPCRequest.parse(StartMigrationJob.Params.serializer(), json)
        val params = request.params
        val user = internalUsername(params.user)

        val accessManager = session.getServerAccessManager()
        if (!accessManager.hasAccountAccess(user)) {
            responseHandler.setResponseHeader(request.makeError(ReturnType.ACCESS_DENIED,
                    "Root accesses required for migration"))
            return@runBlocking
        }
        val accountIO = platformGetAccountIO(AccountIO.Type.SERVER, session.baseDir, user)
        val migrationConfig = MigrationConfig(params.token)
        accountIO.writeMigrationConfig(migrationConfig)

        val response = request.makeResponse("Migration started").stringify(StringSerializer)
        responseHandler.setResponseHeader(response)
    }
}
