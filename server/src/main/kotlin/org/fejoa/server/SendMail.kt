package org.fejoa.server

import java.util.*
import javax.mail.Message
import javax.mail.PasswordAuthentication
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage


fun sendMail(config: SMTPConfig, to: String, replyTo: String, subject: String, body: String) {
    /*
    // The following approach would lead to spam on virtual servers:
    val tmpDir = File("sendMailTmp")
    tmpDir.mkdirs()
    val tmpFile = File(tmpDir, CryptoHelper.crypto.generateBytes(16).toHex())
    val outStream = FileOutputStream(tmpFile)
    outStream.write(body.toUTF())
    outStream.close()
    try {
        val command = "sendmail -R hdrs -N never -r $replyTo -s \"$subject\" $to < ${tmpFile.absolutePath}"
        println(command)
        val runtime = Runtime.getRuntime()
        val process = runtime.exec(arrayOf("/bin/sh", "-c", command))
        process.waitFor()
    } finally {
        tmpFile.delete()
    }*/

    val username = config.user
    val password = config.password

    val properties = Properties()
    properties.put("mail.smtp.auth", "${config.auth}")
    properties.put("mail.smtp.starttls.enable", "${config.starttls}")
    properties.put("mail.smtp.host", config.host)
    properties.put("mail.smtp.port", "${config.port}")

    val session = javax.mail.Session.getInstance(properties, object : javax.mail.Authenticator() {
        override fun getPasswordAuthentication(): PasswordAuthentication {
            return PasswordAuthentication(username, password)
        }
    })

    val message = MimeMessage(session)
    message.setFrom(InternetAddress(username))
    message.replyTo = arrayOf(InternetAddress(replyTo))
    message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to))
    message.setSubject(subject)
    message.setText(body)

    Transport.send(message)
}