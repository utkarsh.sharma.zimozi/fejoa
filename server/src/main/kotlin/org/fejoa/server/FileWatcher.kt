/**
 * Taken from:
 * https://gist.github.com/danielflower/f54c2fe42d32356301c68860a4ab21ed
 *
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
package org.fejoa.server

import java.io.IOException
import java.nio.file.*

class FileWatcher {
    private var thread: Thread? = null
    private var watchService: WatchService? = null

    @Throws(IOException::class)
    fun start(file: Path, callback: Runnable) {
        watchService = FileSystems.getDefault().newWatchService()
        val parent = file.parent
        parent.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE)
        println("Going to watch $file")

        thread = Thread {
            while (true) {
                var wk: WatchKey? = null
                try {
                    wk = watchService!!.take()
                    Thread.sleep(500) // give a chance for duplicate events to pile up
                    for (event in wk!!.pollEvents()) {
                        val changed = parent.resolve(event.context() as Path)
                        if (Files.exists(changed) && Files.isSameFile(changed, file)) {
                            println("File change event: $changed")
                            callback.run()
                            break
                        }
                    }
                } catch (e: InterruptedException) {
                    println("Ending my watch")
                    Thread.currentThread().interrupt()
                    break
                } catch (e: Exception) {
                    println("Error while reloading cert ${e.message ?: ""}")
                } finally {
                    if (wk != null) {
                        wk.reset()
                    }
                }
            }
        }
        thread!!.start()
    }

    fun stop() {
        thread!!.interrupt()
        try {
            watchService!!.close()
        } catch (e: IOException) {
            println("Error closing watch service ${e.message ?: ""}")
        }

    }

    companion object {
        /**
         * Starts watching a file and the given path and calls the callback when it is changed.
         * A shutdown hook is registered to stop watching. To control this yourself, create an
         * instance and use the start/stop methods.
         */
        @Throws(IOException::class)
        fun onFileChange(file: Path, callback: Runnable) {
            val fileWatcher = FileWatcher()
            fileWatcher.start(file, callback)
            Runtime.getRuntime().addShutdownHook(Thread(Runnable { fileWatcher.stop() }))
        }
    }

}
