package org.fejoa.server

import kotlinx.coroutines.runBlocking
import kotlinx.serialization.serializer
import org.fejoa.AccountIO
import org.fejoa.network.JsonRPCRequest
import org.fejoa.network.ReturnType
import org.fejoa.network.UpdateServerConfigJob
import org.fejoa.network.makeError
import org.fejoa.platformGetAccountIO
import java.io.InputStream


class UpdateServerConfigHandler : JsonRequestHandler(UpdateServerConfigJob.METHOD) {
    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?,
                        session: Session) = runBlocking {

        val request = JsonRPCRequest.parse(
                UpdateServerConfigJob.RPCParams.serializer(), json)
        val params = request.params
        val user = internalUsername(params.user)

        if (!session.getServerAccessManager().hasAccountAccess(user)) {
            val response = request.makeError(ReturnType.ACCESS_DENIED,
                    "User ${params.user} not authenticated")
            return@runBlocking responseHandler.setResponseHeader(response)
        }

        val accountIO = platformGetAccountIO(AccountIO.Type.SERVER, session.baseDir, user)
        params.loginParams?.let {
            accountIO.writeLoginData(it)
        }
        params.storageConfig?.let {
            accountIO.writeStorageConfig(it)
        }

        val response = request.makeResponse(UpdateServerConfigJob.RPCResult())
                .stringify(UpdateServerConfigJob.RPCResult.serializer())
        responseHandler.setResponseHeader(response)
    }
}