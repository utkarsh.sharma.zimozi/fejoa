package org.fejoa.server

import kotlinx.coroutines.runBlocking
import kotlinx.serialization.serializer
import org.fejoa.AccountIO
import org.fejoa.crypto.CryptoHelper
import org.fejoa.network.*
import org.fejoa.platformGetAccountIO
import java.io.*


class ConfirmRegistrationHandler : JsonRequestHandler(ConfirmRegistrationJob.METHOD) {
    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?,
                        session: Session) = runBlocking {

        val request = JsonRPCRequest.parse(
                ConfirmRegistrationJob.ConfirmParams.serializer(), json)
        val params = request.params
        val user = internalUsername(params.user)

        val registrationManager = session.getOngoingRegistrationManager()
        val token = registrationManager.getToken(user) ?: run {
            val response = request.makeError(ReturnType.ERROR,
                    "No ongoing registration for user ${params.user}")
            return@runBlocking responseHandler.setResponseHeader(response)
        }

        if (token.token != params.token) {
            val response = request.makeResponse(RegisterJob.RegisterReply(
                    RegisterJob.ReplyType.CONFIRMATION_REQUIRED, "Invalid token"))
                    .stringify(RegisterJob.RegisterReply.serializer())
            return@runBlocking responseHandler.setResponseHeader(response)
        }

        if (!RegisterHandler.writeRegisterParms(session, token.params)) {
            val response = request.makeError(ReturnType.ERROR, "Account exists")
            return@runBlocking responseHandler.setResponseHeader(response)
        }

        registrationManager.clear(user)

        // store user info
        val accountIO = platformGetAccountIO(AccountIO.Type.SERVER, session.baseDir, user)
        accountIO.writeServerUserInfo(token.userInfo)

        // add the freshly registered user as authenticated
        session.getServerAccessManager().addAccountAccess(user)

        val response = request.makeResponse(RegisterJob.RegisterReply(RegisterJob.ReplyType.REGISTERED,
                "User ${params.user} registered")).stringify(RegisterJob.RegisterReply.serializer())
        responseHandler.setResponseHeader(response)
    }
}


class RegisterHandler(val registerConfirm: ConfirmRegistrationConfig) : JsonRequestHandler(RegisterJob.METHOD) {
    companion object {
        const val DEBUG_TOKEN = "debug_registration_token"

        /**
         * @return false if account exist
         */
        suspend fun writeRegisterParms(session: Session, params: RegisterJob.Params): Boolean {
            val user = internalUsername(params.user)
            val accountIO = platformGetAccountIO(AccountIO.Type.SERVER, session.baseDir, user)
            if (accountIO.exists())
                return false

            accountIO.writeLoginData(params.loginParams)
            params.storageConfig?.let {
                accountIO.writeStorageConfig(it)
            }

            return true
        }
    }

    fun generateToken(): String {
        val LOWER_LETTERS = "abcdefghijklmnopqrstuvwxyz"
        val CAPITAL_LETTERS = LOWER_LETTERS.toUpperCase()
        val NUMBERS = "0123456789"
        val charset = LOWER_LETTERS + CAPITAL_LETTERS + NUMBERS

        val length = 10
        val random = CryptoHelper.crypto.generateBytes(length)

        var output = ""
        (0 until length)
                .map { i ->
                    // convert the byte to a positive integer
                    random[i].toInt().let { if (it < 0) it + 256 else it }
                }
                .map { it % charset.length }
                .forEach { output += charset[it] }
        return output
    }

    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?,
                        session: Session) = runBlocking {
        val request = JsonRPCRequest.parse(RegisterJob.Params.serializer(), json)
        val params = request.params
        val user = internalUsername(params.user)

        val accountIO = platformGetAccountIO(AccountIO.Type.SERVER, session.baseDir, user)
        if (accountIO.exists()) {
            val response = request.makeError(ReturnType.ERROR, "Account exists")
            responseHandler.setResponseHeader(response)
            return@runBlocking
        }

        val reply = when (registerConfirm.type) {
            Portal.RegisterConfirm.NONE -> {
                val reply = if (!writeRegisterParms(session, params)) {
                    val response = request.makeError(ReturnType.ERROR, "Account exists")
                    return@runBlocking responseHandler.setResponseHeader(response)
                } else RegisterJob.RegisterReply(RegisterJob.ReplyType.REGISTERED,
                        "User ${params.user} registered")

                // add the freshly registered user as authenticated
                session.getServerAccessManager().addAccountAccess(user)
                reply
            }
            Portal.RegisterConfirm.EMAIL -> {
                val manager = session.getOngoingRegistrationManager()
                manager.clear()
                manager.setToken(user, generateToken(), registerConfirm, params)

                RegisterJob.RegisterReply(RegisterJob.ReplyType.CONFIRMATION_REQUIRED, "Confirmation required")
            }
            Portal.RegisterConfirm.DEBUG_TOKEN -> {
                val manager = session.getOngoingRegistrationManager()
                manager.clear()
                manager.setToken(user, DEBUG_TOKEN, registerConfirm, params)

                RegisterJob.RegisterReply(RegisterJob.ReplyType.CONFIRMATION_REQUIRED, "Confirmation required")
            }
        }

        val response = request.makeResponse(reply).stringify(
                RegisterJob.RegisterReply.serializer())
        responseHandler.setResponseHeader(response)
    }
}