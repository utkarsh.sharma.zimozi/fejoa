package org.fejoa.server

import kotlinx.coroutines.runBlocking
import org.fejoa.BranchIndex
import org.fejoa.network.*
import org.fejoa.repository.Repository
import org.fejoa.repository.sync.AccessRight
import org.fejoa.repository.sync.Request
import org.fejoa.repository.sync.RequestHandler
import org.fejoa.storage.BranchLog
import java.io.IOException
import java.io.InputStream


class BranchRequestHandler : JsonRequestHandler(Request.BRANCH_REQUEST_METHOD) {
    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?,
                        session: Session) = runBlocking {
        val request = JsonRPCRequest.parse(StorageRPCParams.serializer(), json)
        val params = request.params
        val user = internalUsername(params.user)
        val branch = params.branch

        val context = session.getContext(user)
        val accessManager = session.getServerAccessManager()
        val branchAccessRights = if (accessManager.noAccessControl) AccessRight.ALL else
            accessManager.getBranchAccessRight(user, branch)

        val branchBackend = UserThreadContext.withLock(user) {
            val storageBackend = context.platformStorage
            // is branch index?
            val branchIndex = context.getLocalIndexStorage()
            return@withLock if (branchIndex.branch == branch) {
                val repo = branchIndex.getBackingDatabase() as Repository
                repo.branchBackend
            } else if (storageBackend.exists(user, branch)) {
                storageBackend.open(user, branch)
            } else if (branchAccessRights.value and AccessRight.PUSH.value != 0) {
                storageBackend.create(user, branch)
            } else {
                responseHandler.setResponseHeader(request.makeError(ReturnType.ACCESS_DENIED,
                        "branch access denied"))
                null
            }
        } ?: return@runBlocking

        val branchLog = branchBackend.getBranchLog()
        val initialHead = branchLog.getHead()
        val handler = RequestHandler(branch, session.getTransactionManager(), branchBackend,
                object : RequestHandler.BranchLogGetter {
                    override operator fun get(b: String): BranchLog {
                        if (branch != b)
                            throw IOException("Branch miss match.")
                        return branchLog
                    }
                })

        if (data == null) {
            responseHandler.setResponseHeader(request.makeError(ReturnType.ERROR,
                    "Storage data expected"))
            return@runBlocking
        }

        // TODO make this a transaction so that the branch index stays in sync
        val pipe = ServerPipe(request.makeResponse(
                RPCPipeResponse("data pipe ok")).stringify(RPCPipeResponse.serializer()),
                responseHandler, data)
        val result = handler.handle(pipe, branchAccessRights)
        if (result !== RequestHandler.Result.OK && !responseHandler.isHandled)
            responseHandler.setResponseHeader(request.makeError(ReturnType.ERROR, result.description))
        else if (!responseHandler.isHandled)
            throw Exception("Internal error")

        // update the server branch index if necessary
        UserThreadContext.withLock(user) {
            val finalHead = branchBackend.getBranchLog().getHead() ?: return@withLock

            if (initialHead == null || initialHead.entryId != finalHead.entryId) {
                context.branchIndexLocker.runSync {
                    val index = BranchIndex(session.getBranchIndex(user))
                    index.update(branch, finalHead.entryId)
                    index.commit()
                    // TODO finish the transaction (see above)
                }
            }
        }
    }
}