package org.fejoa.chunkcontainer

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.fejoa.repository.BlobEntry
import org.fejoa.repository.Ref
import org.fejoa.storage.Hash
import org.fejoa.storage.Mode
import org.fejoa.storage.RandomDataAccess
import org.fejoa.storage.has
import org.fejoa.support.IOException
import org.fejoa.support.nowNano

import kotlin.math.max


open class SharedCCCallback {
    // is called unlocked
    open suspend fun onClosed(sharedContainer: SharedChunkContainer, dataAccess: ChunkContainerRandomDataAccess) {}
}

class SharedChunkContainer(val path: String, val container: ChunkContainer, val blobEntry: BlobEntry?,
                           val callback: SharedCCCallback = SharedCCCallback()) {
    val containerLock = Mutex()
    val openRandomAccess: MutableList<ChunkContainerRandomDataAccess> = ArrayList()
    private var currentWriter: ChunkContainerRandomDataAccess? = null
    var lastModificationDate = 0L // in nano sec

    /**
     * To be called from repository, only method not has to be locked when called
     */
    suspend fun addRandomAccess(randomDataAccess: ChunkContainerRandomDataAccess) = containerLock.withLock {
        openRandomAccess.add(randomDataAccess)
    }

    fun hasChildWithWriteAccess(): Boolean {
        return openRandomAccess.firstOrNull { it.mode has RandomDataAccess.WRITE } != null
    }

    suspend fun flush(): Hash = containerLock.withLock {
        currentWriter?.flushUnlocked()
        container.ref.hash
    }

    private suspend fun flushOngoingWrites(veto: ChunkContainerRandomDataAccess) {
        currentWriter?.let {
            if (it == veto)
                return
            val beforeFlush = it.getChunkContainer().ref.hash.value
            it.flushUnlocked()
            if (beforeFlush == it.getChunkContainer().ref.hash.value)
                return
            // something has been updated; invalidate the readers
            for (randomDataAccess in openRandomAccess) {
                randomDataAccess.inputStream?.invalidateCache()
            }
        }
    }

    suspend fun requestRead(caller: ChunkContainerRandomDataAccess) {
        flushOngoingWrites(caller)
    }

    suspend fun requestWrite(caller: ChunkContainerRandomDataAccess) {
        flushOngoingWrites(caller)
        currentWriter = caller
    }

    fun onClosed(caller: ChunkContainerRandomDataAccess) {
        openRandomAccess.remove(caller)
        if (currentWriter == caller)
            currentWriter = null
    }
}


class ChunkContainerRandomDataAccess(private val sharedContainer: SharedChunkContainer,
                                     override val mode: Mode,
                                     private val normalizeChunkSize: Boolean = sharedContainer.container.ref.boxSpec.dataNormalization)
    : RandomDataAccess {

    constructor(container: ChunkContainer, mode: Mode)
        : this(SharedChunkContainer("no path", container, null), mode,
            container.ref.boxSpec.dataNormalization)

    private var isOpen = true
    private var position: Long = 0
    var inputStream: ChunkContainerInStream? = null
    private var outputStream: ChunkContainerOutStream? = null

    fun getChunkContainer(): ChunkContainer {
        return sharedContainer.container
    }

    private fun getLock(): Mutex {
        return sharedContainer.containerLock
    }

    suspend fun cancel() {
        close()

        position = -1
        inputStream = null
        outputStream = null
    }

    private fun checkNotCanceled() {
        if (position < 0)
            throw IOException("Access has been canceled")
    }

    private suspend fun prepareForWrite() {
        if (!mode.has(RandomDataAccess.WRITE))
            throw IOException("Write permission is missing")

        checkNotCanceled()

        if (inputStream != null) {
            inputStream = null
        }
        if (outputStream == null) {
            outputStream = ChunkContainerOutStream(getChunkContainer(), mode, normalizeChunkSize = normalizeChunkSize)
            outputStream!!.seek(position)
        }

        sharedContainer.requestWrite(this)
    }

    private suspend fun prepareForRead(checkPermission: Boolean = true) {
        if (checkPermission && !mode.has(RandomDataAccess.READ))
            throw IOException("Read permission is missing")

        checkNotCanceled()

        outputStream?.let {
            it.close()
            outputStream = null
        }
        if (inputStream == null) {
            inputStream = ChunkContainerInStream(getChunkContainer()).also {
                it.seek(position)
            }
        }

        sharedContainer.requestRead(this)
    }

    override fun isOpen(): Boolean {
        return isOpen
    }

    override suspend fun length(): Long = getLock().withLock {
        prepareForRead(false)
        return max(getChunkContainer().getDataLength(), position)
    }

    override fun position(): Long {
        return position
    }

    override suspend fun seek(position: Long) = getLock().withLock {
        prepareForRead(false)

        seekUnlocked(position)
    }

    private suspend fun seekUnlocked(position: Long) {
        this.position = position
        if (inputStream != null)
            inputStream!!.seek(position)
        else if (outputStream != null)
            outputStream!!.seek(position)
    }

    override suspend fun write(buffer: ByteArray, offset: Int, length: Int): Int = getLock().withLock {
        prepareForWrite()
        return@withLock writeUnlocked(buffer, offset, length)
    }

    override suspend fun writeAt(position: Long, buffer: ByteArray, offset: Int, length: Int): Int = getLock().withLock {
        prepareForWrite()

        if (this.position != position)
            seekUnlocked(position)

        return@withLock writeUnlocked(buffer, offset, length)
    }

    private suspend fun writeUnlocked(buffer: ByteArray, offset: Int, length: Int): Int {
        outputStream!!.write(buffer, offset, length)
        position += length.toLong()
        sharedContainer.lastModificationDate = nowNano()
        return length
    }

    override suspend fun read(buffer: ByteArray, offset: Int, length: Int): Int = getLock().withLock {
        prepareForRead()

        return@withLock readUnlocked(buffer, offset, length)
    }

    override suspend fun readAt(position: Long, buffer: ByteArray, offset: Int, length: Int): Int = getLock().withLock {
        prepareForRead()

        if (this.position != position)
            seekUnlocked(position)

        return@withLock readUnlocked(buffer, offset, length)
    }

    private suspend fun readUnlocked(buffer: ByteArray, offset: Int, length: Int): Int {
        val read = inputStream!!.read(buffer, offset, length)
        if (read > 0)
            position += read.toLong()
        return read
    }

    suspend fun flushUnlocked() {
        if (!mode.has(RandomDataAccess.WRITE))
            throw IOException("Can't flush in read only mode.")
        outputStream?.let {
            it.flush()
        }
    }

    override suspend fun flush() = getLock().withLock {
        flushUnlocked()
    }

    override suspend fun truncate(size: Long) = getLock().withLock {
        prepareForWrite()
        outputStream!!.truncate(size)
    }

    override suspend fun delete(position: Long, length: Long) = getLock().withLock {
        prepareForWrite()
        outputStream!!.delete(position, length)
    }

    override suspend fun close() {
        getLock().withLock {
            isOpen = false
            if (inputStream != null) {
                inputStream = null
            } else if (outputStream != null) {
                outputStream!!.close()
                outputStream = null
            }
            sharedContainer.onClosed(this)
        }
        // has to be called unlocked
        sharedContainer.callback.onClosed(sharedContainer, this)
    }
}
