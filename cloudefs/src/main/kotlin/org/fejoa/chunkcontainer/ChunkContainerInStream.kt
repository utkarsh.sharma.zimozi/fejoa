package org.fejoa.chunkcontainer

import org.fejoa.support.AsyncInStream

import kotlin.math.min


class ChunkContainerInStream(private val container: ChunkContainer) : AsyncInStream {
    private var position: Long = 0
    private var chunkPosition: ChunkContainer.DataChunkPointer? = null

    fun invalidateCache() {
        chunkPosition = null
    }

    override suspend fun read(buffer: ByteArray, offset: Int, length: Int): Int {
        var currentOffset = offset
        var totalBytesRead = 0
        while (totalBytesRead < length) {
            val bytesRead = readPartial(buffer, currentOffset, length)
            if (bytesRead < 0)
                return if (totalBytesRead > 0) totalBytesRead else -1
            currentOffset += bytesRead
            totalBytesRead += bytesRead
        }
        return totalBytesRead
    }

    private suspend inline fun readPartial(buffer: ByteArray, offset: Int, length: Int): Int {
        if (position >= container.getDataLength())
            return -1
        val pointer = validateCurrentChunk()
        val current = pointer.getDataChunk()
        val data = current.getData()
        val positionInChunk = (position - pointer.position).toInt()
        val bytesToCopy = min((current.getDataLength() - positionInChunk).toInt(), length - offset)
        for (i in 0 until bytesToCopy)
            buffer[offset + i] = data[positionInChunk + i]
        position += bytesToCopy
        return bytesToCopy
    }

    suspend fun seek(position: Long) {
        this.position = position
        chunkPosition?.let {
            if (position >= it.position + it.getDataChunk().getDataLength() || position < it.position)
                chunkPosition = null
        }
    }

    private suspend inline fun validateCurrentChunk(): ChunkContainer.DataChunkPointer {
        chunkPosition?.let {
            if (position < it.position + it.getDataChunk().getDataLength())
                return it
        }
        return container.get(position).also {
            chunkPosition = it
        }
    }
}
