package org.fejoa.chunkcontainer

import org.fejoa.chunkcontainer.ChunkHash.Companion.DATA_LEAF_LEVEL
import org.fejoa.chunkcontainer.ChunkHash.Companion.DATA_LEVEL
import org.fejoa.crypto.CryptoHelper

import org.fejoa.crypto.AsyncHashOutStream
import org.fejoa.storage.*
import org.fejoa.support.*


interface NodeSplitterFactory {
    fun nodeSizeFactor(level: Int): Float
    suspend fun create(level: Int): ChunkSplitter
}

open class ChunkContainerNode : Chunk {
    val that: ChunkPointer
    protected val hashOutStream: AsyncHashOutStream
    var onDisk = false
    var parent: ChunkContainerNode? = null
    val blobAccessor: ChunkAccessor
    private var data: ByteArray? = null
    private var dataHash: HashValue? = null
    protected val slots = ArrayList<ChunkPointer>()
    val nodeWriter: NodeWriteStrategy

    protected constructor(blobAccessor: ChunkAccessor, parent: ChunkContainerNode?,
                          nodeWriter: NodeWriteStrategy, dataLength: Long, level: Int, compact: Boolean,
                          hashOutStream: AsyncHashOutStream) {
        this.blobAccessor = blobAccessor
        this.parent = parent
        this.that = ChunkPointer(this, dataLength, level, compact)
        this.hashOutStream = hashOutStream
        this.nodeWriter = nodeWriter.newInstance(level)
    }

    private constructor(blobAccessor: ChunkAccessor, parent: ChunkContainerNode,
                        nodeWriter: NodeWriteStrategy, that: ChunkPointer, hashOutStream: AsyncHashOutStream) {
        this.blobAccessor = blobAccessor
        this.parent = parent
        this.that = that
        this.hashOutStream = hashOutStream
        this.nodeWriter = nodeWriter.newInstance(level)
    }

    private suspend fun resetNodeSplitter(): ChunkSplitter {
        nodeWriter.reset(level)
        val splitter = nodeWriter.getSplitter()
        // Write dummy to later split at the right position.
        // For example:
        // |dummy|h1|h2|h3 (containing the trigger t)|
        // this results in the node: |h1|h2|h3|..t|
        splitter.write(ByteArray(Config.DATA_HASH_SIZE))
        return splitter
    }

    val isRootNode: Boolean
        get() = parent == null
    val isDataLeafNode: Boolean
        get() = that.getLevel() == DATA_LEAF_LEVEL

    protected open val headerLength: Int
        get() = 0

    override fun getDataLength(): Long {
        return that.dataLength
    }

    protected suspend fun getDataChunk(pointer: ChunkPointer): DataChunk {
        assert(isDataPointer(pointer))
        val cachedChunk = pointer.getCachedChunk()
        if (cachedChunk != null)
            return cachedChunk as DataChunk
        val chunk = blobAccessor.getChunk(pointer.getChunRef()).await()
        val inputStream = ByteArrayInStream(chunk)
        val dataChunk = DataChunk()
        dataChunk.read(inputStream, pointer.dataLength)
        pointer.setCachedChunk(dataChunk)
        return dataChunk
    }

    suspend fun getNode(pointer: ChunkPointer): ChunkContainerNode {
        assert(!isDataPointer(pointer))
        val cachedChunk = pointer.getCachedChunk()
        if (cachedChunk != null)
            return cachedChunk as ChunkContainerNode

        assert(slots.contains(pointer))

        val node = read(blobAccessor, this, pointer)
        pointer.setCachedChunk(node)
        return node
    }

    val level: Int
        get() = that.getLevel()

    /**
     * @param pointerDataPosition absolute position of the pointer that contains the searched data
     */
    protected class SearchResult(val pointerDataPosition: Long, val pointer: ChunkPointer?, val node: ChunkContainerNode)

    /**
     *
     * @param dataPosition relative to this node
     * @return
     */
    protected fun findInNode(node: ChunkContainerNode, dataPosition: Long): SearchResult? {
        if (dataPosition > node.getDataLength())
            return null

        var position: Long = 0
        for (i in node.slots.indices) {
            val pointer = node.slots[i]
            val dataLength = pointer.dataLength
            if (position + dataLength > dataPosition)
                return SearchResult(position, pointer, node)
            position += dataLength
        }
        return SearchResult(position, null, node)
    }

    override suspend fun read(inputStream: InStream, dataLength: Long) {
        slots.clear()
        that.dataLength = 0
        var dataLengthRead: Long = 0
        // temporary disable the parent to not mess miss the parent dataLength when adding child pointers
        val p = parent
        parent = null
        try {
            while (dataLengthRead < dataLength) {
                val pair = ChunkPointer.read(inputStream, level = that.getLevel() - 1, compact = that.compact)
                dataLengthRead += pair.second.dataLength
                addBlobPointer(pair.first)
            }
            if (dataLengthRead != dataLength) {
                throw IOException("Chunk container node addresses " + dataLengthRead + " bytes but " + dataLength
                        + " bytes expected")
            }
        } finally {
            parent = p
        }
        onDisk = true
    }

    private suspend fun findRightNeighbour(): ChunkContainerNode? {
        if (parent == null)
            return null

        // find common parent
        var indexInParent = -1
        var parent: ChunkContainerNode? = this
        var levelDiff = 0
        while (parent!!.parent != null) {
            levelDiff++
            val pointerInParent = parent.that
            parent = parent.parent
            indexInParent = parent!!.indexOf(pointerInParent)
            assert(indexInParent >= 0)
            if (indexInParent != parent.nSlots() - 1)
                break
        }

        // is last pointer?
        if (indexInParent == parent.nSlots() - 1)
            return null

        var neighbour = parent.getNode(parent[indexInParent + 1])
        for (i in 0 until levelDiff - 1)
            neighbour = neighbour.getNode(neighbour[0])

        assert(neighbour.that.getLevel() == that.getLevel())
        return neighbour
    }

    /**
     * Returns the split pair.
     *
     * The left node is usually the input node (this). It differs only when the root node is split.
     */
    private suspend fun splitAt(index: Int): Pair<ChunkContainerNode, ChunkContainerNode> {
        val right = create(blobAccessor, null, nodeWriter, that.getLevel(),
                that.compact, hashOutStream)
        // Only set right.parent after the all pointers are transferred otherwise the right dataLength get added twice
        // to the parent when adding right to the parent.
        while (nSlots() > index)
            right.addBlobPointer(removeBlobPointer(index))
        return if (parent != null) {
            val inParentIndex = parent!!.indexOf(that)
            parent!!.addBlobPointer(inParentIndex + 1, right.that)
            right.parent = parent
            this to right
        } else {
            // we are the root node and we want to stay so, thus move the items to a new child
            val left = create(blobAccessor, null, nodeWriter, that.getLevel(),
                    that.compact, hashOutStream)
            while (nSlots() > 0)
                left.addBlobPointer(removeBlobPointer(0))
            addBlobPointer(left.that)
            addBlobPointer(right.that)
            left.parent = this
            right.parent = this
            that.setLevel(that.getLevel() + 1)
            left to right
        }
    }

    /**
     * The first element of the returned pair is the balanced node. If the node has been split the second field contains
     * the remaining part of the node.
     */
    private suspend fun balance(): Pair<ChunkContainerNode, ChunkContainerNode?> {
        val splitter = resetNodeSplitter()

        val size = nSlots()
        for (i in 0 until size) {
            val child = get(i)
            splitter.write(child.getChunRef().dataHash.bytes)
            if (splitter.isTriggered) {
                // split leftover into a right node
                if (i == size - 1) // all good
                    return this to null
                // split left over into a right node
                return splitAt(i + 1)
            }
        }

        // we are not full; get pointers from the right neighbour till we are full
        findRightNeighbour()?.let {
            var neighbour = it
            while (neighbour.nSlots() > 0) {
                var nextNeighbour: ChunkContainerNode? = null
                // we need one item to find the next right neighbour
                if (neighbour.nSlots() == 1)
                    nextNeighbour = neighbour.findRightNeighbour()

                // The neighbour node pointers might be dirty so flush them first.
                // The neighbour can't be removed now because flushChildren requires an intact tree
                val pointer = neighbour[0]
                if (that.getLevel() > DATA_LEAF_LEVEL)
                    pointer.flushChildren()

                neighbour.removeBlobPointer(0, true)
                addBlobPointer(pointer)
                splitter.write(pointer.getChunRef().dataHash.bytes)
                if (splitter.isTriggered)
                    break

                if (nextNeighbour != null)
                    neighbour = nextNeighbour
            }
        }

        return this to null
    }

    private val root: ChunkContainerNode
        get() {
            var root: ChunkContainerNode? = this
            while (root!!.parent != null)
                root = root.parent
            return root
        }

    private val isRedundantRoot: Boolean
        get() {
            if (level == DATA_LEAF_LEVEL)
                return false
            return slots.size <= 1
        }

    /**
     * The idea is to flush item from the left to the right. The bottommost level is flushed first
     *
     * @param childOnly only child nodes are flushed
     * @throws IOException
     */
    open suspend fun flush(childOnly: Boolean) {
        assert(parent == null)

        var level = -1
        // while root changes because child nodes have been split do:
        while (level != that.getLevel()) {
            level = that.getLevel()
            flushChildren(childOnly)
        }

        // remove redundant root nodes
        // TODO don't write redundant nodes in the first place
        while (isRedundantRoot) {
            val onlyChild = slots[0].getCachedChunk() as ChunkContainerNode
            removeBlobPointer(0)
            // set parent to null otherwise onlyChild's dataLength gets subtracted twice
            onlyChild.parent = null
            while (onlyChild.slots.size > 0)
                addBlobPointer(onlyChild.removeBlobPointer(0))
            that.setLevel(this.level - 1)
        }

        if (!childOnly)
            writeNode()
    }

    private suspend fun ChunkPointer.flushChildren() {
        val chunk: Chunk = this.getCachedChunk() ?: return
        assert(chunk is ChunkContainerNode)
        val node = chunk as ChunkContainerNode
        if (node.onDisk)
            return
        node.flushChildren(false)
    }

    private suspend fun flushChildren(childOnly: Boolean) {
        if (childOnly)
            assert(parent == null)

        // its assumed that all leaf data chunks are already on disk; just flush/balance the tree
        val level = that.getLevel()
        if (level > DATA_LEAF_LEVEL) {
            // IMPORTANT: the slot size may grow/shrink when flushing the child so check in each iteration!
            var i = 0
            while (i < slots.size)  {
                val pointer = slots[i]
                i++

                pointer.flushChildren()
            }
        }

        // do the real work balance the node and write it to disk
        val balancedNode = balance().first
        if (!childOnly || balancedNode != this)
            balancedNode.writeNode()
    }

    /**
     * @param dataHash the data hash (without the meta data)
     * @param chunkHash the hash of chunk that will be written
     */
    private fun getIV(dataHash: ByteArray, chunkHash: ByteArray): HashValue {
        val iv = if (that.compact)
            dataHash
        else
            chunkHash
        return HashValue(iv)
    }

    private suspend fun writeNode() {
        if (onDisk)
            return
        val ref = that.getChunRef()
        val data = getData()
        val outData = nodeWriter.finalizeWrite(data)
        // don't hash in the worker! hashOutStream is reused...
        val dataHash = hash(hashOutStream)
        val rawHash = rawHash()
        val iv = getIV(dataHash.bytes, rawHash.bytes)
        val dataLength = that.dataLength

        val future = blobAccessor.putChunk(outData, iv).then { boxHash ->
            // TODO: remove(?)
            //if (boxHash != oldBoxHash && !oldBoxHash.isZero)
            //    blobAccessor.releaseChunk(oldBoxHash)

            ChunkRef(dataHash, boxHash, rawHash, dataLength, ref.compact)
        }

        that.setChunkRef(future)

        onDisk = true
    }

    override suspend fun getData(): ByteArray {
        if (data != null)
            return data!!
        val outputStream = ByteArrayOutStream()
        for (pointer in slots)
            pointer.getChunRef().write(outputStream)

        return outputStream.toByteArray().also { data = it }
    }

    private suspend fun toStringAsync(): String {
        var string = "Data Hash: " + hash(hashOutStream) + "\n"
        for (pointer in slots)
            string += pointer.toStringAsync() + "\n"
        return string
    }

    open suspend fun printAll(): String {
        var string = toStringAsync()

        if (that.getLevel() == DATA_LEAF_LEVEL)
            return string

        for (pointer in slots)
            string += getNode(pointer).printAll()
        return string
    }

    suspend fun hash(): HashValue {
        return hash(hashOutStream)
    }

    override suspend fun hash(hashOutStream: AsyncHashOutStream): HashValue {
        if (dataHash == null)
            dataHash = calculateDataHash(hashOutStream)
        return dataHash!!
    }

    private suspend fun rawHash(): HashValue {
        return HashValue(CryptoHelper.hash(getData(), hashOutStream))
    }

    private suspend fun calculateDataHash(hashOutStream: AsyncHashOutStream): HashValue {
        // if there is only one data chunk use this hash
        if (parent == null && slots.size == 1)
            return slots[0].getChunRef().dataHash
        hashOutStream.reset()
        for (pointer in slots)
            hashOutStream.write(pointer.getChunRef().dataHash.bytes)
        return HashValue(hashOutStream.hash())
    }

    protected fun invalidate(deltaSize: Long) {
        that.dataLength += deltaSize
        data = null
        dataHash = null
        onDisk = false
        parent?.invalidate(deltaSize)
    }

    suspend fun addBlobPointer(index: Int, pointer: ChunkPointer) {
        slots.add(index, pointer)
        if (!isDataPointer(pointer) && pointer.getCachedChunk() != null)
            (pointer.getCachedChunk() as ChunkContainerNode).parent = this
        invalidate(pointer.dataLength)
    }

    private suspend fun addBlobPointer(pointer: ChunkPointer) {
        addBlobPointer(slots.size, pointer)
    }

    suspend fun removeBlobPointer(i: Int, updateParentsIfEmpty: Boolean = false): ChunkPointer {
        val pointer = slots.removeAt(i)
        invalidate(-pointer.dataLength)

        if (updateParentsIfEmpty && parent != null && slots.size == 0) {
            val inParentIndex = parent!!.indexOf(that)
            parent!!.removeBlobPointer(inParentIndex, true)
        }
        return pointer
    }

    fun nSlots(): Int {
        return slots.size
    }

    operator fun get(index: Int): ChunkPointer {
        return slots[index]
    }

    val chunkPointers: List<ChunkPointer>
        get() = slots

    fun indexOf(pointer: ChunkPointer): Int {
        return slots.indexOf(pointer)
    }

    companion object {
        fun create(blobAccessor: ChunkAccessor, parent: ChunkContainerNode?, nodeWriter: NodeWriteStrategy,
                   level: Int, compact: Boolean, hashOutStream: AsyncHashOutStream): ChunkContainerNode {
            return ChunkContainerNode(blobAccessor, parent, nodeWriter, 0L, level, compact, hashOutStream)
        }

        suspend fun read(blobAccessor: ChunkAccessor, parent: ChunkContainerNode, that: ChunkPointer)
                : ChunkContainerNode {
            val node = ChunkContainerNode(blobAccessor, parent, parent.nodeWriter, that,
                    parent.hashOutStream)
            val chunkRef = that.getChunRef()
            val chunk = blobAccessor.getChunk(chunkRef).await()
            node.read(ByteArrayInStream(chunk), chunkRef.dataLength)
            return node
        }

        fun isDataPointer(pointer: ChunkPointer): Boolean {
            return pointer.getLevel() == DATA_LEVEL
        }
    }
}
