package org.fejoa.chunkcontainer

import org.fejoa.chunkcontainer.ChunkHash.Companion.DATA_LEAF_LEVEL
import org.fejoa.chunkcontainer.ChunkHash.Companion.DATA_LEVEL
import org.fejoa.storage.*
import org.fejoa.storage.Config.IV_SIZE
import org.fejoa.support.*


/**
 * @param chunkRef disk version of the pointer. Future might not be completed in case the data is still written
 * @param cachedChunk represent a read job of the underlying chunk
 * @param dataLength keeps track of the most current dataLength; this might differ to the chunkRef due edits not yet
 * flushed.
 */
class ChunkPointer(private var chunkRef: Future<ChunkRef>, private var cachedChunk: Future<Chunk>?,
                   var dataLength: Long, private var level: Int = 0, val compact: Boolean) {
    constructor(dataHash: HashValue, boxHash: HashValue, iv: HashValue, dataLength: Long, blob: Chunk? = null,
    level: Int = 0, compact: Boolean)
            : this(Future.completedFuture(ChunkRef(dataHash, boxHash, iv, dataLength, compact)),
                if (blob != null) Future.completedFuture(blob) else null, dataLength, level, compact)
    constructor(blob: Chunk? = null, dataLength: Long, level: Int = 0, compact: Boolean)
            : this(Config.newDataHash(), Config.newBoxHash(), HashValue(Config.newIV()),
            dataLength, blob, level, compact)

    companion object {
        private const val LENGTH_SIZE = 8

        val nodePointerLength: Int
            get() = LENGTH_SIZE + Config.DATA_HASH_SIZE + Config.BOX_HASH_SIZE + IV_SIZE

        val compactNodePointerLength: Int
            get() = LENGTH_SIZE + Config.DATA_HASH_SIZE + Config.BOX_HASH_SIZE

        fun read(inputStream: InStream, level: Int, compact: Boolean): Pair<ChunkPointer, ChunkRef> {
            val chunkRef = ChunkRef.read(inputStream, compact)
            return ChunkPointer(Future.completedFuture(chunkRef), null, chunkRef.dataLength, compact = compact,
                    level = level) to chunkRef
        }
    }

    fun setChunkRef(ref: ChunkRef) {
        chunkRef = Future.completedFuture(ref)
    }

    fun setChunkRef(ref: Future<ChunkRef>) {
        chunkRef = ref
    }

    suspend fun getChunRef(): ChunkRef {
        return chunkRef.await()
    }

    fun hasCachedChunk(): Boolean {
        return cachedChunk != null
    }
    suspend fun getCachedChunk(): Chunk? {
        return cachedChunk?.await()
    }

    fun setCachedChunk(chunk: Chunk?) {
        this.cachedChunk = if (chunk != null) Future.completedFuture(chunk) else null
    }

    fun getLevel(): Int {
        return level
    }

    fun setLevel(level: Int) {
        this.level = level
    }

    suspend fun toStringAsync(): String {
        val ref = chunkRef.await()
        var string = "l:" + ref.dataLength
        if (!ref.dataHash.isZero)
            string += " (data:" + ref.dataHash.toString() + " box:" + ref.boxHash.toString() + ")"
        return string
    }
}


internal class CacheManager(private val chunkContainer: ChunkContainer) {
    private class PointerEntry(val dataChunkPointer: ChunkPointer, val parent: ChunkContainerNode) : DoubleLinkedList.Entry<PointerEntry>()

    private val queue: DoubleLinkedList<PointerEntry> = DoubleLinkedList()
    private val pointerMap: MutableMap<ChunkPointer, PointerEntry> = HashMap()

    private val targetCapacity = 20
    private val triggerCapacity = 30
    private val keptMetadataLevels = 2

    private fun bringToFront(entry: PointerEntry) {
        queue.remove(entry)
        queue.addFirst(entry)
    }

    fun update(dataChunkPointer: ChunkPointer, parent: ChunkContainerNode) {
        assert(ChunkContainerNode.isDataPointer(dataChunkPointer))
        var entry: PointerEntry? = pointerMap[dataChunkPointer]
        if (entry != null) {
            bringToFront(entry)
            return
        }
        entry = PointerEntry(dataChunkPointer, parent)
        queue.addFirst(entry)
        pointerMap[dataChunkPointer] = entry
        if (pointerMap.size >= triggerCapacity)
            clean(triggerCapacity - targetCapacity)
    }

    fun remove(dataChunkPointer: ChunkPointer) {
        val entry = pointerMap[dataChunkPointer] ?: return
        queue.remove(entry)
        pointerMap.remove(dataChunkPointer)
        // don't clean parents yet, they are most likely being edited right now
    }

    private fun clean(numberOfEntries: Int) {
        for (i in 0 until numberOfEntries) {
            val entry = queue.removeTail() ?: throw Exception("Unexpected null")
            pointerMap.remove(entry.dataChunkPointer)
            clean(entry)
        }
    }

    private fun clean(entry: PointerEntry) {
        // always clean the data cache
        entry.dataChunkPointer.setCachedChunk(null)

        var currentPointer = entry.dataChunkPointer
        var currentParent: ChunkContainerNode? = entry.parent
        while (currentParent != null && chunkContainer.level - currentParent.level >= keptMetadataLevels) {
            currentPointer.setCachedChunk(null)
            if (hasCachedPointers(currentParent))
                break

            currentPointer = currentParent.that
            currentParent = currentParent.parent
        }
    }

    private fun hasCachedPointers(node: ChunkContainerNode): Boolean {
        for (pointer in node.chunkPointers) {
            if (pointer.hasCachedChunk())
                return true
        }
        return false
    }
}


class ContainerSpec(val hashSpec: HashSpec, val boxSpec: BoxSpec)


/**
 * Container for data chunks.
 *
 * Data chunks are split externally, i.e. in ChunkContainerOutStream. However, all leaf nodes are managed here.
 */
class ChunkContainer : ChunkContainerNode {
    val ref: ChunkContainerRef
    private val cacheManager: CacheManager = CacheManager(this)

    /**
     * Create a new chunk container.
     *
     * @param blobAccessor
     */
    private constructor(blobAccessor: ChunkAccessor, ref: ChunkContainerRef)
            : super(blobAccessor, null, ref.hash.spec.getNodeWriteStrategy(ref.boxSpec.nodeNormalization),
                    ref.length, DATA_LEAF_LEVEL, ref.hash.spec.compact, ref.hash.spec.getBaseHashOutStream()) {
        this.ref = ref
    }

    companion object {
        fun create(blobAccessor: ChunkAccessor, spec: ContainerSpec)
                : ChunkContainer {
            val ref = ChunkContainerRef(spec.hashSpec, spec.boxSpec)
            return ChunkContainer(blobAccessor, ref)
        }

        suspend fun read(blobAccessor: ChunkAccessor, ref: ChunkContainerRef)
                : ChunkContainer {
            val chunkContainer = ChunkContainer(blobAccessor, ref)
            chunkContainer.that.setChunkRef(ChunkRef(ref.dataHash, ref.boxHash, ref.iv, ref.length,
                    ref.hash.spec.compact))

            val level = ref.nLevel
            if (level > DATA_LEVEL) {
                chunkContainer.that.setLevel(level)
                val chunk = blobAccessor.getChunk(ref.chunkRef).await()
                chunkContainer.read(ByteArrayInStream(chunk), ref.length)
            } else if (!ref.boxHash.isZero){
                // this means there is exactly one data chunk
                val chunk = blobAccessor.getChunk(ref.chunkRef).await()
                chunkContainer.clear()
                chunkContainer.append(DataChunk(chunk, ref.chunkRef.dataLength.toInt()))
            }
            return chunkContainer
        }

        val compactNodeSplittingRatio: Float
            get() = Config.DATA_HASH_SIZE.toFloat() / ChunkPointer.compactNodePointerLength
        val nodeSplittingRatio: Float
            get() = Config.DATA_HASH_SIZE.toFloat() / ChunkPointer.nodePointerLength
    }

    override suspend fun flush(childOnly: Boolean) {
        if (!isShortData()) {
            super.flush(childOnly)

            val chunkRef = that.getChunRef()
            ref.hash.value = chunkRef.dataHash
            ref.boxHash = chunkRef.boxHash
            ref.iv = chunkRef.getIV()

            ref.nLevel = level
        } else {
            if (nSlots() > 0) {
                val chunkRef = get(0).getChunRef()
                ref.hash.value = chunkRef.dataHash
                ref.boxHash = chunkRef.boxHash
                ref.iv = chunkRef.getIV()
            } else {
                // the container is empty
                ref.hash.value = Config.newDataHash()
                ref.boxHash = Config.newBoxHash()
                ref.iv = Config.newIV()
            }
            // There is only one data chunk, this is indicated by the DATA_LEVEL level.
            ref.nLevel = DATA_LEVEL
        }

        ref.length = getDataLength()

        // write chunks to disk
        blobAccessor.flush()
    }

    fun clear() {
        slots.clear()
        that.setLevel(DATA_LEAF_LEVEL)
        invalidate(-that.dataLength)
    }

    /**
     * Indicates if data fit into a single chunk.
     *
     * In this case only the data chunk is stored and no extra chunk for the chunk container is needed
     */
    fun isShortData(): Boolean {
        return level == DATA_LEAF_LEVEL && nSlots() <= 1
    }

    inner class DataChunkPointer(private val pointer: ChunkPointer, val position: Long, dataLength: Long) {
        private var cachedChunk: DataChunk? = null
        val dataLength: Long = dataLength

        suspend fun getDataChunk(): DataChunk {
            val chunk = this@ChunkContainer.getDataChunk(pointer)
            if (cachedChunk == null)
                cachedChunk = chunk
            return cachedChunk!!
        }
    }

    fun getChunkIterator(startPosition: Long): AsyncIterator<DataChunkPointer> {
        return object : AsyncIterator<DataChunkPointer> {
            private var position = startPosition

            override suspend fun hasNext(): Boolean {
                return position < getDataLength()
            }

            override suspend fun next(): DataChunkPointer {
                val dataChunkPointer = get(position)
                position = dataChunkPointer.position + dataChunkPointer.dataLength
                return dataChunkPointer
            }
        }
    }

    suspend fun get(position: Long): DataChunkPointer {
        val searchResult = findLevel0Node(position)
        if (searchResult.pointer == null)
            throw IOException("Invalid position: $position")
        cacheManager.update(searchResult.pointer, searchResult.node)
        return DataChunkPointer(searchResult.pointer, searchResult.pointerDataPosition,
                searchResult.pointer.dataLength)
    }

    private suspend fun findLevel0Node(position: Long): SearchResult {
        var currentPosition: Long = 0
        var containerNode: ChunkContainerNode = this
        var pointer: ChunkPointer? = null
        for (i in 0 until that.getLevel()) {
            val result = findInNode(containerNode, position - currentPosition) ?: // find right most node blob
                    return SearchResult(getDataLength(), null, findRightMostNode())
            currentPosition += result.pointerDataPosition
            pointer = result.pointer ?: throw Exception("Internal error")
            if (i == that.getLevel() - 1)
                break
            else
                containerNode = containerNode.getNode(pointer)
        }

        return SearchResult(currentPosition, pointer, containerNode)
    }

    private suspend inline fun findRightMostNode(): ChunkContainerNode {
        var current: ChunkContainerNode = this
        for (i in 0 until that.getLevel() - 1) {
            val pointer = current[current.nSlots() - 1]
            current = current.getNode(pointer)
        }
        return current
    }

    private suspend inline fun putDataChunk(blob: DataChunk): ChunkPointer {
        val rawBlob = blob.getData()
        // don't hash in the worker! hashOutStream is reused...
        val dataHash = blob.hash(hashOutStream)
        val future = blobAccessor.putChunk(rawBlob, dataHash).then { boxedHash ->
            ChunkRef(dataHash, boxedHash, dataHash, blob.getDataLength(), ref.hash.spec.compact)
        }
        return ChunkPointer(future, Future.completedFuture(blob), blob.getDataLength(), DATA_LEVEL,
                ref.hash.spec.compact)
    }

    internal class InsertSearchResult(val containerNode: ChunkContainerNode, val index: Int)

    private suspend inline fun findInsertPosition(position: Long): InsertSearchResult {
        var currentPosition: Long = 0
        var node: ChunkContainerNode = this
        var index = 0
        for (i in 0 until that.getLevel()) {
            var nodePosition: Long = 0
            val inNodeInsertPosition = position - currentPosition
            index = 0
            var pointer: ChunkPointer? = null
            while (index < node.nSlots()) {
                pointer = node[index]
                val dataLength = pointer.dataLength
                if (nodePosition + dataLength > inNodeInsertPosition)
                    break

                // Don't advance when at the last pointer in the node (this also means the insert point is at the end of
                // the node). Only advanced when the data level is reached.
                if (index < node.nSlots() - 1 || i == that.getLevel() - 1)
                    nodePosition += dataLength
                index++
            }
            currentPosition += nodePosition
            if (nodePosition > inNodeInsertPosition || i == that.getLevel() - 1
                    && nodePosition != inNodeInsertPosition) {
                throw IOException("Invalid insert position $position")
            }

            if (i < that.getLevel() - 1) {
                if (pointer == null)
                    throw Exception("Invalid pointer for insert position $position, size ${nSlots()}")
                node = node.getNode(pointer)
            }
        }

        return InsertSearchResult(node, index)
    }

    suspend fun insert(blob: DataChunk, position: Long): ChunkPointer {
        val searchResult = findInsertPosition(position)
        val containerNode = searchResult.containerNode
        val blobChunkPointer = putDataChunk(blob)
        containerNode.addBlobPointer(searchResult.index, blobChunkPointer)

        cacheManager.update(blobChunkPointer, containerNode)
        return blobChunkPointer
    }

    suspend fun append(blob: DataChunk): ChunkPointer {
        return insert(blob, getDataLength())
    }

    suspend fun remove(position: Long, dataChunk: DataChunk) {
        remove(position, dataChunk.getDataLength())
    }

    suspend fun remove(position: Long, length: Long) {
        val searchResult = findLevel0Node(position)
        if (searchResult.pointer == null)
            throw IOException("Invalid position")
        if (searchResult.pointerDataPosition != position)
            throw IOException("Invalid position. Position must be at a chunk boundary")
        if (searchResult.pointer.dataLength != length)
            throw IOException("Data length mismatch: ${searchResult.pointer.dataLength} but $length expected")

        val containerNode = searchResult.node
        val indexInParent = containerNode.indexOf(searchResult.pointer)
        containerNode.removeBlobPointer(indexInParent, true)

        cacheManager.remove(searchResult.pointer)
    }

    // 1 byte for number of levels
    override val headerLength: Int
        get() = 1

    override suspend fun printAll(): String {
        var string = "Levels=" + that.getLevel() + ", length=" + getDataLength() + "\n"
        string += super.printAll()
        return string
    }
}
