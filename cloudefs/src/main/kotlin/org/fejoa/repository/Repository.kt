package org.fejoa.repository

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.serialization.SerialId
import kotlinx.serialization.Serializable
import kotlinx.serialization.protobuf.ProtoBuf
import org.fejoa.chunkcontainer.*
import org.fejoa.crypto.SecretKeyData
import org.fejoa.protocolbufferlight.ProtocolBufferLight
import org.fejoa.storage.*
import org.fejoa.support.*


class RepositoryConfig(val hashSpec: HashSpec,
                       val boxSpec: BoxSpec = BoxSpec(),
                       val revLog: RevLogConfig = RevLogConfig()) {
    val containerSpec: ContainerSpec
        get() = ContainerSpec(hashSpec.createChild(), boxSpec)

    companion object {
        const val HASH_SPEC_TAG = 0
        const val BOX_SPEC_TAG = 1
        const val REV_LOG_TAG = 2

        /**
         * @param protoBuffer if hashSpec or boxSpec are not included in the buffer these values are taken from default
         */
        fun read(protoBuffer: ProtocolBufferLight): RepositoryConfig {
            val hashSpec = protoBuffer.getBytes(HASH_SPEC_TAG)?.let {
                return@let HashSpec.read(ByteArrayInStream(it), null)
            } ?: throw Exception("Failed to read repo config hash spec")

            val boxSpec = protoBuffer.getBytes(BOX_SPEC_TAG)?.let {
                return@let BoxSpec.read(ByteArrayInStream(it))
            } ?: throw Exception("Failed to read repo config box spec")

            val revLog = protoBuffer.getBytes(REV_LOG_TAG)?.let {
                return@let ProtoBuf.load(RevLogConfig.serializer(), it)
            } ?: throw Exception("Rev log config expected")

            return RepositoryConfig(hashSpec, boxSpec, revLog)
        }
    }

    /**
     * @param protoBuffer hashSpec and boxSpec are only written if they are different from the parameters in default
     */
    fun write(protoBuffer: ProtocolBufferLight) {
        var outStream = ByteArrayOutStream()

        hashSpec.write(outStream)
        protoBuffer.put(HASH_SPEC_TAG, outStream.toByteArray())

        outStream = ByteArrayOutStream()
        boxSpec.write(outStream)
        protoBuffer.put(BOX_SPEC_TAG, outStream.toByteArray())

        protoBuffer.put(REV_LOG_TAG, ProtoBuf.dump(RevLogConfig.serializer(), revLog))
    }
}


@Serializable
class RevLogConfig(@SerialId(0) val maxEntrySize: Long = 1 * 1024 * 1024,
                   @SerialId(1) val maxEntries: Int = 50)

class RepositoryRef(val objectIndexObjectIndexRef: ObjectIndex.ObjectIndexRef, val head: ObjectRef, val config: RepositoryConfig) {
    companion object {
        const val OBJECT_INDEX_REF_TAG = 0
        const val HEAD_TAG = 1
        const val CONFIG_TAG = 2

        fun read(protoBuffer: ProtocolBufferLight): RepositoryRef {
            val config = protoBuffer.getBytes(CONFIG_TAG)?.let {
                val configBuffer = ProtocolBufferLight(it)
                return@let RepositoryConfig.read(configBuffer)
            } ?: throw Exception("Missing repo config")

            val objectIndexConfig = protoBuffer.getBytes(OBJECT_INDEX_REF_TAG)?.let {
                return@let ObjectIndex.ObjectIndexRef.read(it, config.hashSpec.chunkingConfig)
            } ?: throw Exception("Missing object index ref")

            val headRef = protoBuffer.getBytes(HEAD_TAG)?.let {
                val inStream = ByteArrayInStream(it)
                val headHash = Hash.read(inStream, config.hashSpec.chunkingConfig)
                val headRef = Ref.read(inStream)
                ObjectRef(headHash, headRef)
            } ?: throw Exception("Missing repo head")


            return RepositoryRef(objectIndexConfig, headRef, config)
        }
    }

    fun write(protoBuffer: ProtocolBufferLight) {
        val configBuffer = ProtocolBufferLight()
        config.write(configBuffer)
        protoBuffer.put(CONFIG_TAG, configBuffer.toByteArray())

        var outStream = ByteArrayOutStream()
        objectIndexObjectIndexRef.write(outStream)
        protoBuffer.put(OBJECT_INDEX_REF_TAG, outStream.toByteArray())

        outStream = ByteArrayOutStream()
        head.hash.write(outStream)
        head.ref.write(outStream)
        protoBuffer.put(HEAD_TAG, outStream.toByteArray())
    }
}


/**
 * A repository can store multiple versions of data. Data is organized in a directory tree.
 */
class Repository private constructor(private val branch: String,
                                     val branchBackend: StorageBackend.BranchBackend,
                                     private val chunkStorage: ChunkStorage,
                                     transaction: LogRepoTransaction,
                                     val log: BranchLog,
                                     objectIndex: ObjectIndex,
                                     config: RepositoryConfig,
                                     val crypto: SecretKeyData?): Database {
    private val lock = Mutex()
    private var transaction: LogRepoTransaction = transaction
    var accessors: ChunkAccessors = RepoChunkAccessors(transaction, config, crypto)
        private set
    private var headCommit: Commit? = null
    val commitCache = CommitCache(this)
    var config: RepositoryConfig = config
        private set
    var objectIndex: ObjectIndex = objectIndex
        private set
    // TODO restrict access to avoid possible coroutine concurrency problems when using ioDatabase without lock However,
    // currently it is only used in a safe way.
    var ioDatabase = IODatabaseCC(Directory("", config.hashSpec.createChild()), objectIndex, accessors,
            config.containerSpec, lock)
        private set
    private val mergeParents: MutableMap<Hash, Ref> = HashMap()
    val branchLogIO: BranchLogIO = if (crypto != null)
        RepositoryBuilder.getEncryptedBranchLogIO(crypto.key, crypto.algo)
    else
        RepositoryBuilder.getPlainBranchLogIO()

    var maintainFSAttr
        set(value) { ioDatabase.maintainFSAttr = value }
        get() = ioDatabase.maintainFSAttr

    companion object {
        /** The freshly created repository can be in an empty state and an uninitialized state.
         * In the uninitialized state the branch log head (BranchLogEntry) is null. In the empty state the head point to
         * a zero commit. This means there is no commit yet; the repository is initialized but empty.
         *
         * This distinction is necessary when synchronising repositories. For example, when the client is pushing a
         * repository, but the operation is canceled, already pushed chunks may remain in the chunkstore and be reused
         * for later operations. When another client tries to pull this uninitialized repository, it is essential to
         * know if the remote repository is empty (no commits) or uninitialized. For example, without this knowledge a
         * client may think an uninitialized repository is empty and then wrongly starts with a new root commit.
         *
         * @param init indicates if the repository should be initialized, i.e. it becomes an empty repository
         */
        suspend fun create(branch: String, branchBackend: StorageBackend.BranchBackend, config: RepositoryConfig,
                           crypto: SecretKeyData?, init: Boolean): Repository {
            val containerSpec = config.containerSpec
            val chunkStorage = branchBackend.getChunkStorage()
            val transaction = LogRepoTransaction(chunkStorage.startTransaction())
            val accessors: ChunkAccessors = RepoChunkAccessors(transaction, config, crypto)

            val log: BranchLog = branchBackend.getBranchLog()

            val objectIndex = ObjectIndex.create(config, containerSpec, accessors)
            val repo = Repository(branch, branchBackend, chunkStorage, transaction, log, objectIndex, config, crypto)

            if (!init)
                return repo
            // Write a first zero log entry, this means the repo is initialized but there are no commits yet
            val repoRef = repo.getRepositoryRefUnlocked()
            val logIO = repo.branchLogIO
            branchBackend.getBranchLog().add(logIO.logHash(repoRef), logIO.writeToLog(repoRef), emptyList())

            return repo
        }

        suspend fun open(branch: String, ref: RepositoryRef, branchBackend: StorageBackend.BranchBackend,
                         crypto: SecretKeyData?): Repository {
            // if there are no commits simply call create
            if (ref.head.hash.value.isZero)
                return create(branch, branchBackend, ref.config, crypto, false)

            val repoConfig = ref.config
            val chunkStorage = branchBackend.getChunkStorage()
            val transaction = LogRepoTransaction(chunkStorage.startTransaction())
            val accessors: ChunkAccessors = RepoChunkAccessors(transaction, repoConfig, crypto)

            val log: BranchLog = branchBackend.getBranchLog()
            val objectIndex = ObjectIndex.open(ref.objectIndexObjectIndexRef, repoConfig, accessors)

            val repository = Repository(branch, branchBackend, chunkStorage, transaction, log, objectIndex, repoConfig,
                    crypto)
            repository.setHeadCommitUnlocked(ref.head)
            return repository
        }
    }

    fun getCurrentTransaction(): ChunkTransaction {
        return transaction
    }

    override fun getBranch(): String {
        return branch
    }

    fun getHeadCommit(): Commit? {
        return headCommit
    }

    suspend fun setHeadCommit(commit: ObjectRef) = lock.withLock {
        setHeadCommitUnlocked(commit)
    }

    private suspend fun setHeadCommitUnlocked(commit: ObjectRef) {
        headCommit = commitCache.getCommit(commit)
        val rootDir = Directory.readRoot(headCommit!!.dir, objectIndex)
        ioDatabase.setRootDirectory(rootDir)
    }

    override suspend fun getHead(): Hash? = lock.withLock {
        getHeadUnlocked()
    }

    private fun getHeadUnlocked(): Hash? {
        return getHeadCommit()?.hash?.clone()
    }

    /**
     * @param mergeParents the databases that should be merged into the current branch
     */
    override suspend fun merge(mergeParents: Collection<Database>, mergeStrategy: MergeStrategy)
            : Database.MergeResult = lock.withLock {
        return mergeUnlocked(mergeParents, mergeStrategy)
    }

    private suspend fun mergeUnlocked(mergeParents: Collection<Database>, mergeStrategy: MergeStrategy)
            : Database.MergeResult {
        var result = Database.MergeResult.FAST_FORWARD
        val allowFastForward = mergeParents.size == 1
        mergeParents.forEach {
            val singleResult = mergeSingleBranch(it, mergeStrategy, allowFastForward)
            if (singleResult == Database.MergeResult.MERGED)
                result = singleResult
        }
        if (result == Database.MergeResult.FAST_FORWARD) {
            // TODO move flush into mergeSingleBranch and only flush when really necessary?
            objectIndex.flush()
            val currentEntryId = branchBackend.getBranchLog().getHead()?.entryId
            val repoRef = getRepositoryRefUnlocked()
            val newEntryId = branchLogIO.logHash(repoRef)
            if (currentEntryId == null || currentEntryId != newEntryId)
                log.add(newEntryId, branchLogIO.writeToLog(repoRef), transaction.getObjectsWritten())
        }
        return result
    }

    suspend fun pullMerge(remoteRep: Repository, mergeStrategy: MergeStrategy,
                                 commitSignature: CommitSignature?): Hash = lock.withLock {
        // Note, we have to inverse the merge strategy because we swap remote and local repo.
        val merged = remoteRep.mergeUnlocked(listOf(this), mergeStrategy)
        val newHead = when (merged) {
            Database.MergeResult.MERGED -> {
                remoteRep.commit("Merge after pull".toUTF(), commitSignature)
            }
            Database.MergeResult.FAST_FORWARD -> {
                remoteRep.getHeadCommit()!!.hash
            }
        }
        setToUnlocked(remoteRep.objectIndex, ObjectRef(newHead, remoteRep.getHeadCommit()?.ref!!))
        return newHead
    }

    suspend fun isModified(): Boolean = lock.withLock {
        return isModifiedUnlocked()
    }

    private suspend inline fun isModifiedUnlocked(): Boolean {
        if (headCommit == null)
            return ioDatabase.treeAccessor.root.getChildren().isNotEmpty()

        val headHash = headCommit!!.dir
        val dirHash = ioDatabase.flushUnlocked()
        if (headHash.hash != dirHash)
            return true
        return false
    }

    /**
     * Reconfigures an empty repository (with no commits)
     *
     * All changes are discarded
     */
    fun reset(config: RepositoryConfig) {
        if (headCommit != null)
            throw Exception("Repository is not empty")
        this.config = config

        // reset the object index
        objectIndex = ObjectIndex.create(config, config.containerSpec, accessors)

        ioDatabase = IODatabaseCC(Directory("", config.hashSpec.createChild()), objectIndex, accessors,
                config.containerSpec, lock)
    }

    private suspend fun setToUnlocked(index: ObjectIndex, commit: ObjectRef) {
        commitCache.clear()
        objectIndex = index
        ioDatabase.objectIndex = index
        setHeadCommitUnlocked(commit)
    }

    private suspend fun copyAndSetTo(theirs: Repository) {
        val theirsHead = theirs.getHeadCommit() ?: return
        val theirRepoRef = theirs.getRepositoryRefUnlocked()
        fetchRepository(theirs.transaction, theirRepoRef)

        val theirObjectIndex = ObjectIndex.open(theirRepoRef.objectIndexObjectIndexRef, theirRepoRef.config,
                accessors)
        val theirCommit = ObjectRef(theirsHead.hash, theirsHead.ref!!)
        setToUnlocked(theirObjectIndex, theirCommit)
    }

    private suspend inline fun mergeSingleBranch(theirs: Database, mergeStrategy: MergeStrategy,
                                                 allowFastForward: Boolean): Database.MergeResult {
        if (theirs !is Repository)
            throw Exception("Unsupported repository")
        val theirsHead = theirs.getHeadCommit()
        val oursIsModified = isModifiedUnlocked()
        val theirsIsModified = theirs.isModifiedUnlocked()
        if (allowFastForward && !theirsIsModified) {
            if (theirsHead == null)
                return Database.MergeResult.FAST_FORWARD

            if (!oursIsModified && getHeadUnlocked() == theirsHead.hash)
                return Database.MergeResult.FAST_FORWARD
        }
        if (theirsHead == null)
            throw Exception("Invalid branch")
        if (allowFastForward && !oursIsModified && !theirsIsModified) {
            if (headCommit == null) {
                // we are empty; copy all chunks from theirs and use their branch head
                copyAndSetTo(theirs)
                return Database.MergeResult.FAST_FORWARD
            }
        }

        val findResult = CommonAncestorsFinder.find(commitCache, headCommit!!,
                theirs.commitCache, theirs.getHeadCommit()!!)
        findResult.commonAncestors.forEach { ancestor ->
            if (allowFastForward && !oursIsModified && !theirsIsModified) {
                if (ancestor.hash.value == headCommit!!.hash.value) {
                    // remote is ahead; copy all missing chunks
                    copyAndSetTo(theirs)
                    return Database.MergeResult.FAST_FORWARD
                }
                if (ancestor.hash.value == theirsHead.hash.value) {
                    // we are ahead
                    return Database.MergeResult.FAST_FORWARD
                }
            }
        }
        // TODO clone their index before using it?
        // merge their changes into our's
        val theirsCommit = copyMissingObjectRefs(findResult, theirs)
        // flush to get a consistent state
        val objectIndexConfig = objectIndex.flush()
        val ancestorHash = findResult.commonAncestors.firstOrNull()?.hash?.value
                ?: throw IOException("Branches don't have common ancestor.")
        val ancestor = findResult.touchedCommitsOurs[ancestorHash]
                ?: throw IOException("Ancestor expected in our touched commits.")

        // flush writes, TODO reuse transaction in Repository.open
        transaction.finishTransaction().await()
        transaction = LogRepoTransaction(chunkStorage.startTransaction())
        val ancestorRepo = open(branch,
                RepositoryRef(objectIndexConfig, ObjectRef(ancestor.hash, ancestor.ref!!), config), branchBackend,
                crypto)
        // open their repo from our object index (getting the correct object refs)
        val theirsRepo = open(branch,
                RepositoryRef(objectIndexConfig, ObjectRef(theirsCommit.hash, theirsCommit.ref!!), config),
                branchBackend, crypto)

        this.mergeParents[theirsCommit.hash] = theirsCommit.ref
        // merge branches
        mergeStrategy.merge(this, theirsRepo, ancestorRepo)
        return Database.MergeResult.MERGED
    }

    /**
     * Copies all missing chunks, as listed in the object index, from the source to chunk store to our store.
     */
    private suspend fun fetchRepository(source: ChunkTransaction, theirRepoRef: RepositoryRef) {
        val chunkFetcher = ChunkFetcher.createLocalFetcher(accessors, source)
        chunkFetcher.enqueueRepositoryJob(theirRepoRef)
        chunkFetcher.fetch()
    }

    /**
     * Adds missing objects to our object index
     *
     * @return the copied head commit of theirs (which has ref in our repo now)
     */
    private suspend fun copyMissingObjectRefs(findResult: CommonAncestorsFinder.Result, theirs: Repository): Commit {
        // start with all touched commits on ours
        val added: MutableMap<HashValue, Commit> = HashMap(findResult.touchedCommitsOurs)
        val commitsToAdd = CommonAncestorsFinder.topologicalSort(
                findResult.commits, findResult.touchedCommitsTheirs)
        commitsToAdd.reversed().asSequence().filter {
            !added.containsKey(it.hash.value)
        }.forEach { current ->
            val dirTheirs = Directory.readRoot(current.dir, theirs.objectIndex)
            val prevCommitOurs = current.parents.asSequence().mapNotNull { added[it.hash.value] }.first()
            val prevDirRefOurs = prevCommitOurs.dir
            val prevDirOurs = Directory.readRoot(prevDirRefOurs, objectIndex)
            val diff = prevDirOurs.getDiff(dirTheirs,
                    includeAllAdded = true, includeAllRemoved = false).getAll()
            val nextOursTree = TreeAccessor(prevDirOurs)
            diff.asSequence().forEach { entry ->
                if (entry.type == DiffIterator.Type.REMOVED) {
                    nextOursTree.remove(entry.path) ?: throw Exception("Item should exist")
                } else if (entry.theirs!!.isFile()) {
                    // update existing entry
                    val theirEntry = entry.theirs as BlobEntry
                    val container = theirs.objectIndex.getChunkContainerRef(theirEntry.onDiskRef!!)
                    val prevBlobOurs = if (entry.type == DiffIterator.Type.MODIFIED) entry.ours!! as BlobEntry
                        else null
                    val newRef = objectIndex.putContainer(container, prevBlobOurs?.onDiskRef)
                    nextOursTree.putBlob(entry.path, theirEntry.getHash(), newRef)
                }
            }
            val newDir = writeTree(nextOursTree.root, prevDirRefOurs.ref)
            assert(newDir.hash == dirTheirs.getHash())

            val cloneMutable = MutableCommit(newDir, current.hash.spec, message = current.message)
            current.parents.forEach { parentRefTheirs ->
                val parent = added[parentRefTheirs.hash.value] ?: run {
                    throw Exception("Missing Parent")
                }
                cloneMutable.parents += ObjectRef(parent.hash, parent.ref!!)
            }
            val clone = writeCommit(cloneMutable, prevCommitOurs.ref)
            added[current.hash.value] = clone
        }
        return added[theirs.getHeadUnlocked()!!.value]!!
    }

    override suspend fun commit(message: ByteArray, signature: CommitSignature?): Hash = lock.withLock {
        val hash = commitInternal(message, signature, mergeParents = mergeParents)
        mergeParents.clear()
        return hash
    }

    var allowCommit = true
    private suspend fun commitInternal(message: ByteArray, commitSignature: CommitSignature? = null,
                                       mergeParents: Map<Hash, Ref>): Hash {
        if (!allowCommit)
            throw Exception("Commits are not allowed in this repo")

        // check if we need to commit
        if (!isModifiedUnlocked() && mergeParents.isEmpty())
            return headCommit?.hash ?: Hash.createChild(config.hashSpec.createChild())

        // flush in any case to write open write handles and to be able to determine if we need to commit
        val rootTree = ioDatabase.flushUnlocked()
        if (mergeParents.isEmpty() && headCommit != null && headCommit!!.dir.hash == rootTree)
            return headCommit!!.hash

        // write entries to the objectIndex
        ioDatabase.getModifiedChunkContainer().forEach {
            val newRef = objectIndex.putContainer(it.value.container, it.value.blobEntry?.onDiskRef)
            ioDatabase.treeAccessor.putBlob(it.value.path, it.value.container.ref.hash, newRef)
            //TODO the tree is updated twice: here and in flushUnlocked, check if that should be optimized
        }
        ioDatabase.clearModifiedChunkContainer()

        val newDirRef = writeTree(ioDatabase.getRootDirectory(), headCommit?.dir?.ref)
        // write the rootTree to the object index
        val commit = MutableCommit(newDirRef, config.hashSpec)

        headCommit?.let {
            commit.parents.add(ObjectRef(it.hash, it.ref!!))
        }
        for ((hash, ref) in mergeParents)
            commit.parents.add(ObjectRef(hash, ref))
        commit.parents.sortBy { it.hash.value }

        if (commitSignature != null)
            commit.message = commitSignature.signMessage(message, rootTree.value, commit.parents.map { it.hash.value })
        else
            commit.message = message
        val newHead = writeCommit(commit, headCommit?.ref).also {
            headCommit = it
        }

        val objectIndexRef = objectIndex.flush()
        val repoRef = RepositoryRef(objectIndexRef, ObjectRef(newHead.hash, newHead.ref!!), config)
        transaction.finishTransaction().await()
        log.add(branchLogIO.logHash(repoRef), branchLogIO.writeToLog(repoRef), transaction.getObjectsWritten())
        transaction = LogRepoTransaction(chunkStorage.startTransaction())
        accessors = RepoChunkAccessors(transaction, config, crypto)
        ioDatabase.setAccessors(accessors)

        return newHead.hash
    }

    suspend fun getRepositoryRef(): RepositoryRef = lock.withLock {
        getRepositoryRefUnlocked()
    }

    private fun getRepositoryRefUnlocked(): RepositoryRef {
        val headCommit = getHeadCommit()
        val head = headCommit?.hash ?: Hash(config.hashSpec, Config.newDataHash())
        val ref = headCommit?.ref ?: Ref(0, Ref.Type.CONTAINER, 0)
        return RepositoryRef(objectIndex.getRef(), ObjectRef(head, ref), config)
    }

    private suspend fun writeTree(tree: Directory, parentDirRef: Ref?): ObjectRef {
        val containerSpec = config.containerSpec
        val chunkContainer = ChunkContainer.create(accessors.getTreeAccessor(containerSpec),
                containerSpec)
        val outStream = ChunkContainerOutStream(chunkContainer, normalizeChunkSize = config.boxSpec.dataNormalization)
        tree.write(outStream, true)
        outStream.close()
        val newTreeRef = objectIndex.putContainer(chunkContainer, parentDirRef)
        return ObjectRef(tree.getHash(), newTreeRef)
    }

    private suspend fun writeCommit(commit: MutableCommit, prevCommitRef: Ref?): Commit {
        val containerSpec = config.containerSpec
        val chunkContainer = ChunkContainer.create(accessors.getCommitAccessor(containerSpec),
                containerSpec)
        val outStream = ChunkContainerOutStream(chunkContainer, normalizeChunkSize = config.boxSpec.dataNormalization)
        commit.write(outStream, true)
        outStream.close()
        val newRef = objectIndex.putContainer(chunkContainer, prevCommitRef)
        return Commit(commit.dir, commit.getHash(), commit.parents, newRef, commit.message)
    }

    override suspend fun getDiff(baseCommit: HashValue, endCommit: HashValue): DatabaseDiff = lock.withLock {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

     override suspend fun getHash(path: String): HashValue {
        return ioDatabase.getHash(path)
    }

    override suspend fun probe(path: String): IODatabase.FileType {
        return ioDatabase.probe(path)
    }

   override suspend fun open(path: String, mode: Mode): RandomDataAccess {
       return ioDatabase.open(path, mode)
    }

    override suspend fun remove(path: String) {
        ioDatabase.remove(path)
    }

    override suspend fun listFiles(path: String): Collection<String> {
        return ioDatabase.listFiles(path)
    }

    override suspend fun listDirectories(path: String): Collection<String> {
        return ioDatabase.listDirectories(path)
    }

    override suspend fun readBytes(path: String): ByteArray {
        return ioDatabase.readBytes(path)
    }

    override suspend fun putBytes(path: String, data: ByteArray) {
        ioDatabase.putBytes(path, data)
    }

    override suspend fun mkDir(path: String) {
        ioDatabase.mkDir(path)
    }

    override suspend fun getAttribute(path: String, attrType: AttrType): Attribute? {
        return ioDatabase.getAttribute(path, attrType)
    }

    override suspend fun setAttribute(path: String, attribute: Attribute) {
        return ioDatabase.setAttribute(path, attribute)
    }

    suspend fun printContent(dataPrinter: (data: ByteArray) -> String = { it.toUTFString() })
            = printContent("", 0, dataPrinter)

    suspend fun printContent(path: String, nSubDir: Int,
                             dataPrinter: (data: ByteArray) -> String = { it.toUTFString() }): String {
        var out = ""
        for (dir in listDirectories(path)) {
            for (i in 0 until nSubDir)
                out += "  "
            out += dir + "\n"
            out += printContent(PathUtils.appendDir(path, dir), nSubDir + 1, dataPrinter)
        }
        for (file in listFiles(path)) {
            for (i in 0 until nSubDir)
                out += "  "
            out += file
            out += " -> "
            out += try {
                dataPrinter.invoke(readBytes(PathUtils.appendDir(path, file)))
            } catch (e: Throwable) {
                "Error loading $path"
            }
            out += "\n"
        }
        return out
    }

    suspend fun printHistory(): String {
        var out = ""
        val head = getHeadCommit() ?: return out

        val handled: MutableSet<HashValue> = HashSet()
        val ongoing: MutableList<Commit> = ArrayList()
        ongoing.add(head)
        while (ongoing.isNotEmpty()) {
            val current = ongoing.removeAt(0)
            if (handled.contains(current.hash.value))
                continue
            else
                handled.add(current.hash.value)
            out += current.hash.value.toHex()
            val currentCommit = commitCache.getCommit(ObjectRef(current.hash, current.ref!!))
            if (currentCommit.hash.value != current.hash.value)
                throw Exception()
            if (currentCommit.parents.isNotEmpty()) {
                out += " parents=["
                currentCommit.parents.map {
                    commitCache.getCommit(it)
                }.forEachIndexed { index, commit ->
                    ongoing.add(commit)
                    out += commit.hash.value
                    if (index != currentCommit.parents.lastIndex)
                        out += ", "
                }
                out += "]"
            }
            out += "\n"
        }
        return out
    }
}