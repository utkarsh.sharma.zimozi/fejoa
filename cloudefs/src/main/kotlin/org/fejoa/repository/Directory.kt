package org.fejoa.repository

import org.fejoa.chunkcontainer.ChunkContainerInStream
import org.fejoa.chunkcontainer.readVarIntDelimited
import org.fejoa.chunkcontainer.writeVarIntDelimited
import org.fejoa.storage.Hash
import org.fejoa.storage.HashSpec
import org.fejoa.repository.DirectoryEntry.EntryType.*
import org.fejoa.storage.*
import org.fejoa.support.*


abstract class DirectoryEntry(val entryType: EntryType, val nameAttrData: NameAttrData,
                              protected var hash: Hash) {
    // Box entry type
    enum class EntryType(val value: Int) {
        SYMBOLIC_LINK(1),
        HARD_LINK(2),
        EMPTY_DIR(10),
        FLAT_DIR(11),
        FLAT_DIR_END(12), // end of flat directory entries
        EXTERNAL_DIR(13), // TODO implement
        BLOB(20),
    }

    // Entry types for calculating the directory hash
    enum class EntryHashType(val value: Int) {
        DIR(0),
        BLOB(1),
    }

    var name: String
        get() = nameAttrData.name
        set(value) { nameAttrData.name = value }

    override fun toString(): String {
        return name
    }

    /**
     * Writes the directory entry to the outStream.
     *
     * @param box indicates if box specific information such as Refs should be written. Box information should, for
     * example not be written when calculating the directory hash.
     *
     * For example, a FLAT_DIR writes all child entries to the outStream while an EXTERNAL_DIR only writes the
     * information where the external directory can be found.
     */
    abstract suspend fun write(outStream: AsyncOutStream, box: Boolean)

    fun isFile(): Boolean = entryType == BLOB
    fun isDir(): Boolean = !isFile()

    fun markModified() {
        hash = Hash(hash.spec, HashValue(Config.newDataHash()))
    }

    protected fun isModified(): Boolean = hash.value.isZero

    abstract suspend fun getHash(): Hash
}

open class Directory(nameAttrData: NameAttrData, hash: Hash) : DirectoryEntry(FLAT_DIR, nameAttrData, hash) {
    constructor(name: String, parent: HashSpec) : this(NameAttrData(name), Hash.createChild(parent))

    internal val children: MutableList<DirectoryEntry> = ArrayList()

    companion object {
        suspend fun readRoot(objectRef: ObjectRef, objectIndex: ObjectIndex): Directory {
            val container = objectIndex.getChunkContainerRef(objectRef.ref)
            val inStream = ByteArrayInStream(ChunkContainerInStream(container).readAll())
            val root = readRoot(inStream, objectRef.hash.spec.createChild())
            if (objectRef.hash.value != root.getHash().value)
                throw Exception("Unexpected directory hash (expected ${objectRef.hash} got ${root.getHash()})")
            return root
        }

        private fun readRoot(inStream: InStream, parent: HashSpec): Directory {
            val type = inStream.readByteValue()
            if (type != EntryType.FLAT_DIR.value && type != EntryType.EMPTY_DIR.value)
                throw Exception("Unsupported directory type: $type")
            val nameAttrData = NameAttrData.read(inStream)

            val dir = Directory(nameAttrData, Hash.createChild(parent))
            if (type == EntryType.FLAT_DIR.value)
                readChildren(dir, inStream, parent)
            return dir
        }

        private fun readChildren(parent: Directory, inStream: InStream, parentSpec: HashSpec) {
            while (true) {
                val typeValue = inStream.readByteValue()
                val type = values().firstOrNull { it.value == typeValue }
                        ?: throw IOException("Unknown dir entry type: $typeValue")
                if (type == FLAT_DIR_END)
                    break
                parent.children.add(readSingleEntry(type, inStream, parentSpec))
            }
        }

        private fun readSingleEntry(type: EntryType, inStream: InStream, parent: HashSpec)
                : DirectoryEntry {
            return when (type) {
                FLAT_DIR_END -> throw Exception("Should't not happen")
                EMPTY_DIR,
                FLAT_DIR -> {
                    val nameAttrData = NameAttrData.read(inStream)
                    val dir = Directory(nameAttrData, Hash.createChild(parent))
                    if (type != EMPTY_DIR)
                        readChildren(dir, inStream, parent)
                    dir
                }
                BLOB -> BlobEntry.read(inStream, parent)
                SYMBOLIC_LINK,
                HARD_LINK -> TODO()
                EXTERNAL_DIR -> TODO()
            }
        }
    }

    fun getChildren(): List<DirectoryEntry> {
        return children
    }

    fun getEntry(name: String): DirectoryEntry? {
        return children.firstOrNull { it.name == name }
    }

    fun put(entryBase: DirectoryEntry) {
        getEntry(entryBase.name)?.let { children.remove(it) }
        children.add(entryBase)
    }

    fun remove(name: String): DirectoryEntry? {
        val index = children.indexOfFirst { it.name == name }
        if (index < 0)
            return null
        return children.removeAt(index)
    }

    private suspend fun writeChildren(outStream: AsyncOutStream, includeRef: Boolean) {
        children.sortBy { it.name }

        for (child in children)
            child.write(outStream, includeRef)

        outStream.write(FLAT_DIR_END.value)
    }

    override suspend fun write(outStream: AsyncOutStream, box: Boolean) {
        if (children.size == 0) {
            outStream.write(EMPTY_DIR.value)
            nameAttrData.write(outStream)
        } else {
            outStream.write(FLAT_DIR.value)
            nameAttrData.write(outStream)
            writeChildren(outStream, box)
        }
    }

    override suspend fun getHash(): Hash {
        if (!isModified())
            return hash

        val hashOutStream = hash.spec.getBaseHashOutStream()
        val outStream: AsyncOutStream = AsyncHashOutStream(AsyncByteArrayOutStream(), hashOutStream)

        children.sortBy { it.name }
        children.forEach {
            val entryHashType = when (it.entryType) {
                FLAT_DIR,
                EMPTY_DIR,
                EXTERNAL_DIR -> EntryHashType.DIR
                BLOB -> EntryHashType.BLOB
                SYMBOLIC_LINK -> TODO()
                HARD_LINK -> TODO()

                FLAT_DIR_END -> throw IOException("Unexpected type")
            }
            outStream.write(entryHashType.value)
            it.nameAttrData.write(outStream)
            it.getHash().write(outStream)
        }

        outStream.close()
        hash.value = HashValue(hashOutStream.hash())
        return hash
    }
}

/**
 * Iterates through changes relative to ours
 *
 * For example, files ADDED in theirs or files REMOVED from theirs.
 *
 */
fun Directory.getDiff(theirs: Directory, includeAllAdded: Boolean, includeAllRemoved: Boolean)
        : TreeIterator {
    return TreeIterator(this, theirs = theirs, includeAllAdded = includeAllAdded,
            includeAllRemoved = includeAllRemoved)
}

suspend fun Directory.getDiffEntries(theirs: Directory, includeAllAdded: Boolean, includeAllRemoved: Boolean)
        : Collection<DatabaseDiff.Entry> {
    val changes = ArrayList<DatabaseDiff.Entry>()
    val diffIterator = TreeIterator(this, theirs = theirs, includeAllAdded = includeAllAdded,
            includeAllRemoved = includeAllRemoved)
    while (diffIterator.hasNext()) {
        val change = diffIterator.next()
        when (change.type) {
            DiffIterator.Type.MODIFIED ->
                // filter out directories
                if (change.ours!!.isFile() || change.theirs!!.isFile())
                    changes.add(DatabaseDiff.Entry(change.path, DatabaseDiff.ChangeType.MODIFIED))

            DiffIterator.Type.ADDED -> changes.add(DatabaseDiff.Entry(change.path, DatabaseDiff.ChangeType.ADDED))

            DiffIterator.Type.REMOVED -> changes.add(DatabaseDiff.Entry(change.path, DatabaseDiff.ChangeType.REMOVED))
        }
    }

    return changes
}

class BlobEntry(nameAttrData: NameAttrData, hash: Hash, var onDiskRef: Ref?, val currentIsOnDisk: Boolean) : DirectoryEntry(BLOB, nameAttrData, hash) {
    constructor(name: String, hash: Hash, onDiskRef: Ref?, currentIsOnDisk: Boolean)
            : this(NameAttrData(name), hash, onDiskRef, currentIsOnDisk)

    companion object {
        fun read(inStream: InStream, parent: HashSpec?): BlobEntry {
            val nameAttrData = NameAttrData.read(inStream)
            val hash = Hash.read(inStream, parent?.chunkingConfig)
            val ref = Ref.read(inStream)
            return BlobEntry(nameAttrData, hash, ref, true)
        }
    }

    override suspend fun write(outStream: AsyncOutStream, box: Boolean) {
        outStream.write(entryType.value)
        nameAttrData.write(outStream)
        hash.write(outStream)

        if (box)
            onDiskRef?.write(outStream) ?: throw IOException("Reference must be set")
    }

    override suspend fun getHash(): Hash  {
        return hash
    }
}

enum class AttrType(val value: Int) {
    FS_ATTR(0),
    EXTENDED_ATTR_DIR(1), // TODO: attributes stored in a separate object
    EXTENDED_ATTR_LOCAL(2) // TODO: attributes stored in the dir entry
}

class NameAttrData(var name: String) {
    val attributeMap: MutableMap<Int, Attribute> = HashMap()

    // {name}
    // |Attribute Flags (1| // 8 types of attribute data
    // {Basic FS attributes (optional)}
    // {Extended attributes (optional EntryHash)} // refers another object
    companion object {
        fun read(inStream: InStream): NameAttrData {
            val name = inStream.readVarIntDelimited().first.toUTFPath()
            val data = NameAttrData(name)
            val attributeFlags = inStream.readByteValue()

            for (i in 0 until 8) {
                if (attributeFlags and (1 shl i) != 0) {
                    // always read the raw attribute data if the flag is set
                    val rawAttributeData = inStream.readVarIntDelimited().first
                    if (i == AttrType.FS_ATTR.value)
                        data.attributeMap[AttrType.FS_ATTR.value] = FSAttribute.read(rawAttributeData)
                }
            }

            return data
        }
    }

    suspend fun write(outStream: AsyncOutStream) {
        outStream.writeVarIntDelimited(name.toUTFPath())

        // build attr flag byte
        val attributes = (0 until 8).mapNotNull {
            val attr = attributeMap[it]
            if (attr == null) null else it to attr
        }
        var attrFlags = 0
        attributes.forEach {
            attrFlags = attrFlags or (1 shl it.first)
        }
        outStream.write(attrFlags)
        attributes.forEach {
            outStream.writeVarIntDelimited(it.second.toByteArray())
        }
    }

    fun getFSAttr(): FSAttribute? {
        return attributeMap[AttrType.FS_ATTR.value] as? FSAttribute
    }

    fun setFSAttr(attr: FSAttribute?) {
        if (attr != null) attributeMap[AttrType.FS_ATTR.value] = attr else attributeMap.remove(AttrType.FS_ATTR.value)
    }
}



