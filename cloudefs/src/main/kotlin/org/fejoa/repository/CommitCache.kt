package org.fejoa.repository

import org.fejoa.storage.Hash
import org.fejoa.storage.HashValue


interface CommitGetter {
    suspend fun getCommit(objectRef: ObjectRef): Commit
}

class CommitCache(private val repository: Repository): CommitGetter {
    private val commitCache = HashMap<HashValue, Commit>()
    private val queue: MutableList<HashValue> = ArrayList()
    var maxEntries = 50000

    /**
     * Finds a commit that belongs to the repository. The search is started from the head.
     */
    suspend fun findCommit(hashValue: Hash): Commit? {
        fromCache(hashValue)?.let { return it }

        val head = repository.getHeadCommit() ?: return null
        if (head.hash == hashValue)
            return head

        val ongoing: MutableList<Commit> = ArrayList()
        ongoing += head

        while (ongoing.isNotEmpty()) {
            val current = ongoing.removeAt(0)
            for (parentRef in current.parents) {
                val parent = fromCache(parentRef.hash) ?: Commit.read(parentRef, repository.objectIndex)
                addToCache(parentRef.hash, parent)
                if (parent.hash == hashValue)
                    return parent
                ongoing += parent
            }
        }

        return null
    }

    override suspend fun getCommit(objectRef: ObjectRef): Commit {
        fromCache(objectRef.hash)?.let { return it }

        val commit = Commit.read(objectRef, repository.objectIndex)
        addToCache(objectRef.hash, commit)
        return commit
    }

    private fun fromCache(hashValue: Hash): Commit? {
        return commitCache[hashValue.value]
    }

    private fun addToCache(hashValue: Hash, commit: Commit) {
        commitCache[hashValue.value] = commit
        queue += hashValue.value

        while (commitCache.size > maxEntries) {
            commitCache.remove(queue.removeAt(0))
        }
    }

    fun clear() {
        commitCache.clear()
        queue.clear()
    }
}
