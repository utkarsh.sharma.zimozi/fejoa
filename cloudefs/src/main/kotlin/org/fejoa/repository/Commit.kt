package org.fejoa.repository

import org.fejoa.chunkcontainer.ChunkContainerInStream
import org.fejoa.chunkcontainer.readVarIntDelimited
import org.fejoa.chunkcontainer.writeVarIntDelimited
import org.fejoa.storage.Hash
import org.fejoa.storage.HashSpec
import org.fejoa.protocolbufferlight.VarInt
import org.fejoa.storage.*
import org.fejoa.support.*


/**
 * Commit not written to disk yet
 */
class MutableCommit(val dir: ObjectRef, hashSpec: HashSpec,
                    val parents: MutableList<ObjectRef> = ArrayList(),
                    var ref: Ref? = null, var message: ByteArray = ByteArray(0)) {
    private val hash: Hash = Hash.createChild(hashSpec)

    suspend fun write(outStream: AsyncOutStream, writeRef: Boolean) {
        // calculate the hash value
        writeInternal(outStream, writeRef)
    }

    private suspend fun writeInternal(outStream: AsyncOutStream, writeRef: Boolean) {
        outStream.write(Commit.CommitType.COMMIT_V1.value)
        dir.hash.write(outStream)
        if (writeRef)
            dir.ref.write(outStream)
        VarInt.write(outStream, parents.size)
        for (parent in parents) {
            parent.hash.write(outStream)
            if (writeRef)
                parent.ref.write(outStream)
        }
        outStream.writeVarIntDelimited(message)
    }

    suspend fun getHash(): Hash {
        hash.value = calculateHash()
        return hash
    }

    internal suspend inline fun calculateHash(): HashValue {
        val hashOutStream = hash.spec.getBaseHashOutStream()
        val outStream = AsyncHashOutStream(AsyncByteArrayOutStream(), hashOutStream)
        writeInternal(outStream, false)
        outStream.close()
        return HashValue(hashOutStream.hash())
    }

    override fun toString(): String {
        return hash.value.toString()
    }
}

/**
 * Immutable commit
 */
class Commit constructor(val dir: ObjectRef, val hash: Hash,
                         val parents: List<ObjectRef>,
                         val ref: Ref? = null, val message: ByteArray) {
    // |type (1|
    // |Directory ObjectRef|
    // [n parents]
    // {list of parent ObjectRefs}
    // {message}


    enum class CommitType(val value: Int) {
        COMMIT_V1(1)
    }

    companion object {
        suspend fun read(ref: ObjectRef, objectIndex: ObjectIndex): Commit {
            val container = objectIndex.getChunkContainerRef(ref.ref)
            val inStream = ByteArrayInStream(ChunkContainerInStream(container).readAll())
            return read(inStream, ref.hash.spec.createChild(), ref)
        }

        private suspend fun read(inStream: InStream, parent: HashSpec, ref: ObjectRef): Commit {
            val type = inStream.readByteValue()
            if (type != Commit.CommitType.COMMIT_V1.value)
                throw Exception("Unexpected commit type; $type")

            val dir = Hash.read(inStream, parent.chunkingConfig)
            val dirRef = Ref.read(inStream)
            val nParents = VarInt.read(inStream).first

            val parents: MutableList<ObjectRef> = ArrayList()

            for (i in 0 until nParents) {
                val parentHash = Hash.read(inStream, parent.chunkingConfig)
                val parentRef = Ref.read(inStream)
                parents += ObjectRef(parentHash, parentRef)
            }

            if (ref.hash.value.toHex() == "acccd67db414bd9d02adc6b4cea10bfeaf3bfd3d4720460e34f5d16351d36143") {
                println("${ref.ref.objectId}, ${ref.ref.position}")
            }
            val message = inStream.readVarIntDelimited().first
            val commit = Commit(ObjectRef(dir, dirRef), ref.hash, parents, ref.ref, message)
            val actualHash = commit.calculateHash()
            if (actualHash != ref.hash.value)
                throw Exception("Expected hash: ${ref.hash.value}, read hash: $actualHash")
            return commit
        }
    }

    private suspend inline fun calculateHash(): HashValue {
        // our parent list is actually a MutableList (see read)
        return MutableCommit(dir, hash.spec, parents as MutableList<ObjectRef>, ref, message).calculateHash()
    }

    override fun toString(): String {
        return hash.value.toString()
    }
}


suspend fun Commit.verify(commitSignature: CommitSignature): Boolean {
    return commitSignature.verifySignedMessage(message, dir.hash.value, parents.map { it.hash.value })
}
