package org.fejoa.repository.sync

import kotlinx.serialization.*
import kotlinx.serialization.protobuf.ProtoBuf
import org.fejoa.protocolbufferlight.ProtocolBufferLight
import org.fejoa.storage.HashValue
import org.fejoa.support.AsyncInStream
import org.fejoa.support.AsyncOutStream


//@Serializable
class ProtocolChunk(@SerialId(ID_TAG)val id: HashValue, @SerialId(CHUNK_TAG)val chunk: ByteArray) {
    companion object {
        const val ID_TAG = 0
        const val CHUNK_TAG = 1

        fun read(data: ByteArray): ProtocolChunk {
            val buffer = ProtocolBufferLight(data)
            val id = buffer.getBytes(ID_TAG) ?: throw Exception("Missing chunk id")
            val chunk = buffer.getBytes(CHUNK_TAG) ?: throw Exception("Missing chunk data")
            return ProtocolChunk(HashValue(id), chunk)
        }
    }

    fun toProtoBuffer(): ProtocolBufferLight {
        val buffer = ProtocolBufferLight()
        buffer.put(ID_TAG, id.bytes)
        buffer.put(CHUNK_TAG, chunk)
        return buffer
    }
}


/**
 * Each command is send using the stream protocol. This means a command is build out of multiple blocks. A command and
 * a reply always starts with a header followed by a set of blocks
 *
 * Command grammar:
 *
 * <CommandData> ::= BLOCK*
 * <Command> ::= Header | <CommandData>*
 * <Response> ::= <Command>
 *
 * Transaction: Commands are wrapped into transaction blocks:
 * <TransactionCommand> ::= START_TRANSACTION <CommandBlock>
 *                          | CONTINUE_TRANSACTION <CommandBlocks>
 * <Transaction> ::= <TransactionCommand> | <TransactionCommand> FINISH_TRANSACTION
 *
 */
class SyncProtocol {
    enum class CommandType(val value: Int) {
        ERROR(1),
        RESULT(2),

        GET_REMOTE_TIP(5),
        HAS_CHUNKS(6),
        GET_CHUNKS(7),
        GET_ALL_CHUNKS(8),
        PUT_CHUNKS(9)
    }

    companion object {
        val VERSION = 1
        val MAX_HAS_CHUNGS_PER_REQUEST = 300000
        val MAX_CHUNKS_PER_REQUEST = 200
        val MAX_CHUNKS_PER_BLOCK = 100

        private var id = 0L

        private fun nextRequestId(): Long {
            return ++id
        }

        suspend fun handle(inStream: AsyncInStream): Receiver {
            return Receiver.receive(inStream, null)
        }
    }

    @Serializable
    class Header(@SerialId(0) val version: Int, @SerialId(1) val id: Long, @SerialId(2)val command: Int) {
        fun getCommandType(): CommandType {
            return CommandType.values().firstOrNull { it.value == command }
                    ?: throw Exception("Unknown request type $command")
        }
    }

    @Serializable
    class Error private constructor(@SerialId(0) val code: Long, @SerialId(1) val message: String) {
        constructor(code: Type, message: String): this(code.value, message)

        enum class Type(val value: Long) {
            UNKNOWN(0),
            EXCEPTION(1),
            PROTOCOL_ERROR(2),
            ACCESS_DENIED(3),
            PUT_LOG_MISS_MATCH(4) // put failed because remote log has been updated by another client
        }

        fun type(): Type {
            return Type.values().firstOrNull { it.value == code} ?: Type.UNKNOWN
        }
    }

    /**
     * Block types within a command
     */
    enum class BlockType(val value: Long) {
        ERROR(0),

        START_TRANSACTION(1),
        CONTINUE_TRANSACTION(2),
        FINISH_TRANSACTION(3),
        ABORT_TRANSACTION(4),
        TRANSACTION_REPLY(5),

        GET_BRANCH_LOG(6),
        PUT_BRANCH_LOG_TIP(7),
        HAS_CHUNKS(8),
        GET_CHUNKS(9),
        PUT_CHUNKS(10),
        CHUNK_ITERATOR(11)
    }

    enum class TransactionType(val value: Long) {
        START_TRANSACTION(BlockType.START_TRANSACTION.value),
        CONTINUE_TRANSACTION(BlockType.CONTINUE_TRANSACTION.value),
        FINISH_TRANSACTION(BlockType.FINISH_TRANSACTION.value),
        ABORT_TRANSACTION(BlockType.ABORT_TRANSACTION.value)
    }

    /**
     * @param blockId can be used to match request blocks to response blocks
     */
    //@Serializable
    class ProtocolBlock(@SerialId(TYPE_TAG) val type: Long, @SerialId(DATA_TAG) val data: ByteArray,
                @Optional @SerialId(BLOCK_ID_TAG) val blockId: Long = -1) {
        constructor(type: BlockType, data: ByteArray, blockId: Long = -1): this(type.value, data, blockId)

        companion object {
            const val TYPE_TAG = 0
            const val DATA_TAG = 1
            const val BLOCK_ID_TAG = 2

            fun read(inData: ByteArray): ProtocolBlock {
                val buffer = ProtocolBufferLight(inData)
                val type = buffer.getLong(TYPE_TAG) ?: throw Exception("Type expected")
                val data = buffer.getBytes(DATA_TAG) ?: throw Exception("Data expected")
                val blockId = buffer.getLong(BLOCK_ID_TAG) ?: -1
                return ProtocolBlock(type, data, blockId)
            }
        }

        fun toProtoBuffer(): ProtocolBufferLight {
            val buffer = ProtocolBufferLight()
            buffer.put(TYPE_TAG, type)
            buffer.put(DATA_TAG, data)
            if (blockId >= 0)
                buffer.put(BLOCK_ID_TAG, blockId)
            return buffer
        }
    }

    class Sender(private val streamWriter: StreamProtocol.Writer, command: CommandType, val id: Long) {
        constructor(outStream: AsyncOutStream, command: CommandType)
                : this(StreamProtocol.Writer(outStream), command, nextRequestId())
        constructor(outStream: AsyncOutStream, command: CommandType, id: Long)
                : this(StreamProtocol.Writer(outStream), command, id)

        private var headerWritten = false
        var command: CommandType = command
            private set

        private suspend fun writeHeader() {
            val header = Header(VERSION, id, command.value)
            streamWriter.write(ProtoBuf.dump(Header.serializer(), header))
        }

        private suspend fun write(byteArray: ByteArray) {
            if (!headerWritten) {
                writeHeader()
                headerWritten = true
            }
            streamWriter.write(byteArray)
        }


        suspend fun write(block: ProtocolBlock) {
            write(block.toProtoBuffer().toByteArray())
        }

        private suspend fun writeErrorBlock(error: Error) {
            command = CommandType.ERROR
            write(ProtocolBlock(BlockType.ERROR, ProtoBuf.dump(Error.serializer(), error)))
        }

        suspend fun writeErrorBlockSubmit(error: Error) {
            writeErrorBlock(error)
            submit()
        }

        suspend fun submit() {
            streamWriter.finish()
        }

        private suspend fun submit(inStream: AsyncInStream): Receiver {
            submit()
            return Receiver.receive(inStream, this)
        }

        /**
         * Throws an exception if the remote reported an error
         */

        suspend fun submitWithErrorCheck(inStream: AsyncInStream): Receiver {
            val reply = submit(inStream)
            if (reply.header.getCommandType() == SyncProtocol.CommandType.ERROR)
                throw Exception(reply.readErrorBlock().message)
            return reply
        }
    }

    class Receiver(val iterator: StreamProtocol.Parser.GroupIterator, val header: Header) {
        companion object {
            suspend fun receive(inStream: AsyncInStream, sender: Sender?): Receiver {
                val streamParser = StreamProtocol.Parser(inStream)
                val groupIterator = streamParser.nextGroup()
                        ?: throw Exception("No group found")
                val headerBlock = groupIterator.next()
                val header = ProtoBuf.load<Header>(Header.serializer(), headerBlock.byteArray)
                if (header.version != VERSION)
                    throw Exception("Unsupported version ${header.version}")
                if (sender != null && header.id != sender.id)
                    throw Exception("Id missmatch")
                return Receiver(groupIterator, header)
            }
        }

        private var current: ProtocolBlock? = null

        suspend fun readErrorBlock(): Error {
            val errorBlock = ProtocolBlock.read(iterator.next().byteArray)
            if (errorBlock.type != BlockType.ERROR.value)
                throw Exception("Not an error ${header.getCommandType()}")

            return ProtoBuf.load(Error.serializer(), errorBlock.data)
        }

        suspend fun hasNext(): Boolean {
            return iterator.hasNext()
        }

        private suspend fun nextRaw(): ByteArray {
            return iterator.next().byteArray
        }

        suspend fun next(): ProtocolBlock {
            val raw = nextRaw()
            return ProtocolBlock.read(raw).also { current = it }
        }

        fun current(): ProtocolBlock? {
            return current
        }

        fun reply(outStream: AsyncOutStream): Sender {
            return Sender(outStream, CommandType.RESULT, header.id)
        }
    }

    class StartTransaction(val branch: String, val accessRight: AccessRight) {
        companion object {
            const val BRANCH_TAG = 0
            const val PERMISSION_TAG = 1

            fun read(data: ByteArray): StartTransaction {
                val buffer = ProtocolBufferLight(data)
                val branch = buffer.getString(BRANCH_TAG) ?: throw Exception("No branch field")
                val permissionValue = buffer.getLong(PERMISSION_TAG)?.toInt() ?: throw Exception("No permission field")
                val permission = AccessRight.values().firstOrNull { it.value == permissionValue }
                        ?: throw Exception("Invalid permission")
                return StartTransaction(branch, permission)
            }
        }

        fun toProtoBuffer(): ProtocolBufferLight {
            val buffer = ProtocolBufferLight()
            buffer.put(BRANCH_TAG, branch)
            buffer.put(PERMISSION_TAG, accessRight.value)
            return buffer
        }

        fun asBlock(): ProtocolBlock {
            return ProtocolBlock(BlockType.START_TRANSACTION, toProtoBuffer().toByteArray())
        }
    }

    @Serializable
    class Transaction(@SerialId(0) val id: Long) {
        fun asContinueBlock(): ProtocolBlock {
            return ProtocolBlock(BlockType.CONTINUE_TRANSACTION, ProtoBuf.dump(Transaction.serializer(), this))
        }

        fun asFinishBlock(): ProtocolBlock {
            return ProtocolBlock(BlockType.FINISH_TRANSACTION, ProtoBuf.dump(Transaction.serializer(),this))
        }

        fun asReplyBlock(): ProtocolBlock {
            return ProtocolBlock(BlockType.TRANSACTION_REPLY, ProtoBuf.dump(Transaction.serializer(),this))
        }
    }
}