package org.fejoa.repository.sync

import org.fejoa.storage.Hash
import org.fejoa.network.RemotePipe
import org.fejoa.repository.*
import org.fejoa.storage.*
import org.fejoa.support.*


class PullRequest(private val requestRepo: Repository, private val commitSignature: CommitSignature?) {
    /**
     * @returns the remote tip
     */
    suspend fun pull(remotePipe: RemotePipe, branch: String, mergeStrategy: MergeStrategy): Hash? {
        val remoteLogEntry = BranchLogProtocol.getBranchLog(remotePipe, branch, 1).entries
                .firstOrNull() ?: return null
        val remoteTipMessage = remoteLogEntry.message.data
        val remoteRepoRef = requestRepo.branchLogIO.readFromLog(remoteTipMessage)
        if (remoteRepoRef.head.hash.value.isZero) {
            val branchLog = requestRepo.branchBackend.getBranchLog()
            val localHead = branchLog.getHead()
            if (localHead == null) {
                // add the remote log entry to init the repo
                branchLog.add(remoteLogEntry)
                requestRepo.reset(remoteRepoRef.config)
            }

            return remoteRepoRef.head.hash
        }

        // up to date?
        val localTip = requestRepo.getHead()
        if (localTip == remoteRepoRef.head.hash)
            return remoteRepoRef.head.hash
        val chunkFetcher = createRemotePipeFetcher(requestRepo.accessors, branch, remotePipe)
        chunkFetcher.enqueueRepositoryJob(remoteRepoRef)
        chunkFetcher.fetch()
        // Merge change into the remote repo. This helps to have the same ObjectIndex across all local copies.
        val remoteRep = Repository.open(branch, remoteRepoRef, requestRepo.branchBackend, requestRepo.crypto)
        return requestRepo.pullMerge(remoteRep, mergeStrategy, commitSignature)
    }

    companion object {
        fun createRemotePipeFetcher(accessors: ChunkAccessors, branch: String,
                                    remotePipe: RemotePipe): ChunkFetcher {
            return ChunkFetcher(accessors, object : ChunkFetcher.FetcherBackend {
                override suspend fun fetch(transaction: ChunkTransaction, requestedChunks: List<HashValue>) {
                    GetProtocol.getChunks(remotePipe, branch, requestedChunks, 100) {
                        it.forEach { pullRequestReply ->
                            if (pullRequestReply.missingChunks.isNotEmpty())
                                throw Exception("Unexpected missing chunks")
                            pullRequestReply.chunks.forEach { protocolChunk ->
                                val result = transaction.putChunk(protocolChunk.chunk).await()
                                if (result.key != protocolChunk.id)
                                    throw IOException("Hash mismatch; expected: ${protocolChunk.id} got: ${result.key}")
                            }
                            transaction.finishTransaction().await()
                        }
                    }
                }
            })
        }
    }
}
