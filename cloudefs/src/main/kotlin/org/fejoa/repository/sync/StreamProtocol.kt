package org.fejoa.repository.sync

import org.fejoa.protocolbufferlight.VarInt
import org.fejoa.support.AsyncInStream
import org.fejoa.support.AsyncIterator
import org.fejoa.support.AsyncOutStream
import org.fejoa.support.readFully

/**
 * Aim is to split a large data stream with unknown size into smaller data blocks.
 *
 * The stream can be structured into groups of blocks. A group starts with a START_BLOCK followed by a number of BLOCKs.
 * A group ends by either EOF chunk or a START_BLOCK (i.e. another group). Start and normal block can both contain data;
 * only the block type differs.
 *
 * Request grammar:
 *
 * <Blocks> ::= BLOCK | BLOCK <Blocks>
 * <BlockGroup> :: = START_BLOCK | START_BLOCK <Blocks>
 * <Request> ::= <BlockGroup>+ EOF
 *
 */
class StreamProtocol {
    enum class BlockType(val value: Int) {
        START_BLOCK(1),
        BLOCK(2),
        EOF(3)
    }

    companion object {
        val MAX_BLOCK_SIZE = 8 * 1024 * 1024
        val EXTRA_SIZE = 3
    }


    class Block(val type: BlockType, val byteArray: ByteArray)

    class Parser(val inStream: AsyncInStream) {
        private suspend fun parseBlock(): Block {
            val result = VarInt.read(inStream, EXTRA_SIZE)
            val extra = result.third
            val blockType = BlockType.values().firstOrNull { it.value == extra }
                    ?: throw Exception("Invalid block type")
            val length = result.first
            return when (blockType) {
                BlockType.EOF -> Block(BlockType.EOF, ByteArray(0))
                // TODO: remove else; workaround for JS compiler bug (KT-22694)
                //BlockType.START_BLOCK,
                //BlockType.BLOCK -> {
                else -> {
                    if (length > MAX_BLOCK_SIZE)
                        throw Exception("In block to large: $length, MAX_BLOCK_SIZE: $MAX_BLOCK_SIZE")
                    val data = inStream.readFully(length.toInt())
                    Block(blockType, data)
                }
            }
        }

        private var peekedBlock: Block? = null

        private suspend fun parserPeek(): Block {
            return peekedBlock ?: parseBlock().also { peekedBlock = it }
        }

        private suspend fun parserNext(): Block {
            val block = parserPeek()
            peekedBlock = null
            return block
        }

        inner class GroupIterator(val startBlock: Block): AsyncIterator<Block> {
            private var peekedBlock: Block? = startBlock

            private suspend fun groupPeek(): Block {
                return peekedBlock ?: parserNext().also { peekedBlock = it }
            }

            override suspend fun hasNext(): Boolean {
                val peekedBlock = groupPeek()
                if (peekedBlock == startBlock)
                    return true
                return peekedBlock.type == BlockType.BLOCK
            }

            override suspend fun next(): Block {
                if (!hasNext())
                    throw Exception("No next block")
                val block = groupPeek()
                peekedBlock = null
                return block
            }
        }

        suspend fun nextGroup(): GroupIterator? {
            val next = parserNext()
            return when (next.type) {
                StreamProtocol.BlockType.START_BLOCK -> GroupIterator(next)
                StreamProtocol.BlockType.BLOCK -> throw Exception("Unexpected block")
                StreamProtocol.BlockType.EOF -> null
            }
        }
    }

    class Writer(val outStream: AsyncOutStream) {
        private var finished = false

        private suspend fun write(block: Block) {
            if (block.byteArray.size > MAX_BLOCK_SIZE)
                throw Exception("Block longer than MAX_BLOCK_SIZE: $MAX_BLOCK_SIZE")
            VarInt.write(outStream, block.byteArray.size.toLong(), block.type.value, EXTRA_SIZE)
            outStream.write(block.byteArray)
        }

        private var startBlock = true

        suspend fun write(data: ByteArray) {
            if (finished)
                throw Exception("Already finished")
            val blockType = if (startBlock) {
                startBlock = false
                BlockType.START_BLOCK
            } else BlockType.BLOCK
            write(Block(blockType, data))
        }

        suspend fun startGroup() {
            if (finished)
                throw Exception("Already finished")
            startBlock = true
        }

        suspend fun finish() {
            if (finished)
                return
            finished = true
            write(Block(BlockType.EOF, ByteArray(0)))
        }
    }
}
