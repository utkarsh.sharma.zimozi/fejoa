package org.fejoa.repository.sync

import kotlinx.serialization.SerialId
import kotlinx.serialization.Serializable
import org.fejoa.network.RemotePipe
import org.fejoa.protocolbufferlight.ProtocolBufferLight
import org.fejoa.storage.ChunkTransaction
import org.fejoa.storage.HashValue
import org.fejoa.storage.StorageBackend
import org.fejoa.support.await


object PutProtocol {
    //@Serializable
    class PutRequest(@SerialId(CHUNKS_TAG) val chunks: List<ProtocolChunk>) {
        companion object {
            const val CHUNKS_TAG = 0

            fun read(data: ByteArray): PutRequest {
                val buffer = ProtocolBufferLight(data)
                val chunks = buffer.getByteArrayList(CHUNKS_TAG)?.map { ProtocolChunk.read(it) }
                        ?: throw Exception("Missing chunk data")
                return PutRequest(chunks)
            }

            fun replyBlock(): SyncProtocol.ProtocolBlock {
                return SyncProtocol.ProtocolBlock(SyncProtocol.BlockType.PUT_CHUNKS, ByteArray(0))
            }
        }

        fun toProtoBuffer(): ProtocolBufferLight {
            val buffer = ProtocolBufferLight()
            val rawChunks = chunks.map { it.toProtoBuffer().toByteArray() }
            buffer.put(CHUNKS_TAG, rawChunks)
            return buffer
        }

        fun asBlock(): SyncProtocol.ProtocolBlock {
            return SyncProtocol.ProtocolBlock(SyncProtocol.BlockType.PUT_CHUNKS, toProtoBuffer().toByteArray())
        }
    }

    suspend fun putChunks(transaction: ChunkTransaction, remotePipe: RemotePipe, branch: String,
                          chunkHashes: List<HashValue>, log: PutLogEntryProtocol.PutLogRequest,
                          maxChunksPerRequest: Int) {
        var transactionId: Long = -1
        val windowedRequest = chunkHashes.windowed(maxChunksPerRequest, maxChunksPerRequest, true)
        windowedRequest.forEachIndexed { index, list ->
            val finishTransaction = index == windowedRequest.lastIndex
            val chunks = list.map {
                val chunk = transaction.getChunk(it).await()
                ProtocolChunk(it, chunk)
            }
            val transId = putChunks(remotePipe, branch, chunks, if (finishTransaction) log else null,
                    transactionId, finishTransaction)
            if (transactionId >= 0 && transId != transactionId)
                throw Exception("Unexpected transaction id $transId but $transactionId expected")
            transactionId = transId
        }
        if (windowedRequest.isEmpty()) // make sure to write the log
            putChunks(remotePipe, branch, emptyList(), log, transactionId, true)
    }

    private suspend fun putChunks(remotePipe: RemotePipe, branch: String, chunks: List<ProtocolChunk>,
                                  log: PutLogEntryProtocol.PutLogRequest?,
                                  remoteTransactionId: Long, finishTransaction: Boolean): Long {
        val outputStream = remotePipe.outStream
        val request = SyncProtocol.Sender(outputStream, SyncProtocol.CommandType.PUT_CHUNKS)

        TransactionProtocol.send(request, remoteTransactionId, branch, AccessRight.PULL, finishTransaction) {
            // get chunks
            if (chunks.isNotEmpty())
                request.write(PutRequest(chunks).asBlock())

            if (log != null)
                request.write(log.asBlock())
        }

        // receive reply
        val reply = request.submitWithErrorCheck(remotePipe.inStream)
        return TransactionProtocol.receiveReply(reply, remoteTransactionId) {
            while (reply.hasNext()) {
                val block = reply.next()
                when (block.type) {
                    SyncProtocol.BlockType.PUT_CHUNKS.value -> {
                        // do nothing
                    }
                }
            }
        }
    }

    suspend fun handle(branch: String, request: SyncProtocol.Receiver, remotePipe: RemotePipe,
                       transactionManager: TransactionManager,
                       branchBackend: StorageBackend.BranchBackend) {

        var logEntry: PutLogEntryProtocol.PutLogRequest? = null
        TransactionProtocol.Handler(request, branch, transactionManager, branchBackend).receive { transaction ->
            // read requested chunks
            while (request.hasNext()) {
                val block = request.next()
                val putChunks: MutableList<ProtocolChunk> = ArrayList()
                when (block.type) {
                    SyncProtocol.BlockType.PUT_CHUNKS.value -> {
                        val putChunksBlock = PutRequest.read(block.data)
                        putChunks.addAll(putChunksBlock.chunks)
                    }
                    SyncProtocol.BlockType.PUT_BRANCH_LOG_TIP.value -> {
                        logEntry = PutLogEntryProtocol.PutLogRequest.read(block.data)
                    }
                    else -> { }
                }
                putChunks.forEach { chunk ->
                    val result = transaction.chunkTransaction.putChunk(chunk.chunk).await()
                    if (result.key != chunk.id)
                        throw Exception("Hash mismatch; expected: ${chunk.id} got: ${result.key}")
                }
                if (putChunks.isNotEmpty())
                    transaction.chunkTransaction.finishTransaction().await()
            }
        }.reply(remotePipe.outStream) { reply, _ ->
            logEntry?.let {
                if (!branchBackend.getBranchLog().add(it.entry, it.expectedRemoteEntryId)) {
                    // error
                    return@reply reply.writeErrorBlockSubmit(SyncProtocol.Error(
                            SyncProtocol.Error.Type.PUT_LOG_MISS_MATCH,
                            "Log tip is ${branchBackend.getBranchLog().getHead()?.entryId} but ${it.expectedRemoteEntryId} expected"))
                }
            }
            reply.write(PutRequest.replyBlock())
            reply.submit()
        }
    }
}
