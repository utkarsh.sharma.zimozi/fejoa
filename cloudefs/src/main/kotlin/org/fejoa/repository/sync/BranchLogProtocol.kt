package org.fejoa.repository.sync

import kotlinx.serialization.SerialId
import kotlinx.serialization.Serializable
import kotlinx.serialization.dump
import kotlinx.serialization.protobuf.ProtoBuf
import org.fejoa.network.RemotePipe
import org.fejoa.protocolbufferlight.ProtocolBufferLight
import org.fejoa.storage.BranchLogEntry
import kotlin.math.min


//TODO wrap the request into a transaction?
object BranchLogProtocol {
    /**
     * @param maxEntries number of retrieved entries, 0 means all entries should be retrieved
     */
    @Serializable
    class BranchLogRequest(@SerialId(0) val branch: String, @SerialId(1) val maxEntries: Int)

    class BranchLogReply(val entries: List<BranchLogEntry>) {
        companion object {
            const val LOG_HEAD_TAG = 0

            fun read(data: ByteArray): BranchLogReply {
                val buffer = ProtocolBufferLight(data)
                val head = buffer.getByteArrayList(LOG_HEAD_TAG)?.map { BranchLogEntry.read(it) }
                        ?: emptyList()
                return BranchLogReply(head)
            }
        }

        fun toProtoBuffer(): ProtocolBufferLight {
            val buffer = ProtocolBufferLight()
            if (entries.isNotEmpty())
                buffer.put(LOG_HEAD_TAG, entries.map { it.toProtoBuffer().toByteArray() })
            return buffer
        }

        fun asBlock(): SyncProtocol.ProtocolBlock {
            return SyncProtocol.ProtocolBlock(SyncProtocol.BlockType.GET_BRANCH_LOG, toProtoBuffer().toByteArray())
        }
    }

    suspend fun getBranchLog(remotePipe: RemotePipe, branch: String, maxEntries: Int): BranchLogReply {
        val outputStream = remotePipe.outStream

        val request = SyncProtocol.Sender(outputStream, SyncProtocol.CommandType.GET_REMOTE_TIP)
        request.write(SyncProtocol.ProtocolBlock(SyncProtocol.BlockType.GET_BRANCH_LOG.value,
                ProtoBuf.dump(BranchLogRequest.serializer(), BranchLogRequest(branch, maxEntries))))

        // parse reply
        val inputStream = remotePipe.inStream
        val response = request.submitWithErrorCheck(inputStream)
        val branchLogBlock = response.next()
        if (branchLogBlock.type != SyncProtocol.BlockType.GET_BRANCH_LOG.value)
            throw Exception("Unexpected block type ${branchLogBlock.type}")
        return BranchLogReply.read(branchLogBlock.data)
    }

    suspend fun handle(request: SyncProtocol.Receiver, remotePipe: RemotePipe,
                       logGetter: RequestHandler.BranchLogGetter) {
        val requestBlock = request.next()
        if (requestBlock.type != SyncProtocol.BlockType.GET_BRANCH_LOG.value)
            throw Exception("Unexpected type")

        val branchLogRequest = ProtoBuf.load<BranchLogRequest>(BranchLogRequest.serializer(), requestBlock.data)
        val branch = branchLogRequest.branch
        val sender = request.reply(remotePipe.outStream)
        val localBranchLog = logGetter[branch]

        val allEntries = localBranchLog?.getEntries() ?: emptyList()
        val entries = if (branchLogRequest.maxEntries == 0)
            allEntries
        else
            allEntries.take(min(allEntries.size, branchLogRequest.maxEntries))

        sender.write(BranchLogReply(entries).asBlock())
        return sender.submit()
    }
}
