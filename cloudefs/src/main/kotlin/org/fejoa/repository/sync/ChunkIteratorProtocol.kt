package org.fejoa.repository.sync

import kotlinx.serialization.*
import kotlinx.serialization.protobuf.ProtoBuf
import org.fejoa.network.RemotePipe
import org.fejoa.protocolbufferlight.ProtocolBufferLight
import org.fejoa.storage.StorageBackend


object ChunkIteratorProtocol {
    /**
     * @param iteratorId the id == 0 means to start a new iterator
     */
    @Serializable
    class IteratorRequest(@SerialId(0) val iteratorId: Long = 0, @SerialId(1)val maxChunks: Long) {
        fun asBlock(): SyncProtocol.ProtocolBlock {
            return SyncProtocol.ProtocolBlock(SyncProtocol.BlockType.CHUNK_ITERATOR, ProtoBuf.dump(
                    IteratorRequest.serializer(), this))
        }
    }

    @Serializable
    class CloseIteratorRequest(@SerialId(0) val iteratorId: Long)

    /**
     * @param eof indicates if the iterator reached the end, if so the iterator is closed and iteratorId becomes invalid
     */
    class IteratorReply(val iteratorId: Long, val chunks: List<ProtocolChunk>, val eof: Boolean) {
        companion object {
            const val ITERATOR_ID_TAG = 0
            const val CHUNKS_TAG = 1
            const val EOF_TAG = 2

            fun read(data: ByteArray): IteratorReply {
                val buffer = ProtocolBufferLight(data)
                val iteratorId = buffer.getLong(ITERATOR_ID_TAG) ?: throw Exception("Iterator id expected")
                val chunks = buffer.getByteArrayList(CHUNKS_TAG)?.map { ProtocolChunk.read(it) }
                        ?: throw Exception("Missing chunk data")
                val eof = buffer.getBoolean(EOF_TAG)
                        ?: throw Exception("Missing eof value")
                return IteratorReply(iteratorId, chunks, eof)
            }
        }

        fun toProtoBuffer(): ProtocolBufferLight {
            val buffer = ProtocolBufferLight()
            buffer.put(ITERATOR_ID_TAG, iteratorId)
            buffer.put(CHUNKS_TAG, chunks.map { it.toProtoBuffer().toByteArray() })
            buffer.put(EOF_TAG, eof)
            return buffer
        }

        fun asBlock(): SyncProtocol.ProtocolBlock {
            return SyncProtocol.ProtocolBlock(SyncProtocol.BlockType.CHUNK_ITERATOR, toProtoBuffer().toByteArray())
        }
    }

    const val DEFAULT_MAX_CHUNKS_PER_REQUEST: Long = 1000

    suspend fun getAllChunks(remotePipe: RemotePipe, branch: String, maxChunksPerRequest: Long,
                             onChunksFetched: suspend (List<ProtocolChunk>) -> Unit) {
        var transactionId: Long = -1


        var iteratorId: Long = 0
        while (true) {
            val result: MutableList<IteratorReply> = ArrayList()
            val transId = getChunks(remotePipe, branch, result, transactionId, false, iteratorId,
                    maxChunksPerRequest)
            if (transactionId >= 0 && transId != transactionId)
                throw Exception("Unexpected transaction id $transId but $transactionId expected")
            transactionId = transId

            if (result.isEmpty())
                break
            val last = result.last()
            onChunksFetched.invoke(last.chunks)
            if (last.eof) {
                // finish transaction
                val request = SyncProtocol.Sender(remotePipe.outStream, SyncProtocol.CommandType.GET_ALL_CHUNKS)
                TransactionProtocol.send(request, transactionId, branch, AccessRight.PULL, true) {}
                break
            }
            iteratorId = last.iteratorId
        }
    }

    /**
     * @return the transaction id
     */
    private suspend fun getChunks(remotePipe: RemotePipe, branch: String,
                                  result: MutableList<IteratorReply>,
                                  remoteTransactionId: Long = -1, finishTransaction: Boolean,
                                  iteratorId: Long, maxChunksPerRequest: Long): Long {
        val outputStream = remotePipe.outStream
        val request = SyncProtocol.Sender(outputStream, SyncProtocol.CommandType.GET_ALL_CHUNKS)

        TransactionProtocol.send(request, remoteTransactionId, branch, AccessRight.PULL, finishTransaction) {
            // get chunks
            request.write(IteratorRequest(iteratorId, maxChunksPerRequest).asBlock())
        }

        // receive reply
        val reply = request.submitWithErrorCheck(remotePipe.inStream)
        return TransactionProtocol.receiveReply(reply, remoteTransactionId) {
            while (reply.hasNext()) {
                val block = reply.next()
                when (block.type) {
                    SyncProtocol.BlockType.CHUNK_ITERATOR.value -> {
                        val pullBlock = IteratorReply.read(block.data)
                        result += pullBlock
                    }
                }
            }
        }
    }

    suspend fun handle(branch: String, request: SyncProtocol.Receiver, remotePipe: RemotePipe,
                       transactionManager: TransactionManager,
                       branchBackend: StorageBackend.BranchBackend) {
        val requests: MutableList<IteratorRequest> = ArrayList()
        TransactionProtocol.Handler(request, branch, transactionManager, branchBackend).receive {
                // read requested chunks
                while (request.hasNext()) {
                    val block = request.next()
                    when (block.type) {
                        SyncProtocol.BlockType.CHUNK_ITERATOR.value -> {
                            requests += ProtoBuf.load<IteratorRequest>(IteratorRequest.serializer(), block.data)
                        }
                        else -> {
                        }
                    }
                }
            }.reply(remotePipe.outStream) { reply, transaction ->
                    requests.forEach {
                        val result: MutableList<ProtocolChunk> = ArrayList()
                        var eof = false
                        val iterId = if (it.iteratorId == 0L) transaction.newIterator() else it.iteratorId
                        val iterator = transaction.getIterator(iterId) ?: throw Exception("Internal error")

                        for (i in 0 until it.maxChunks) {
                            if (!iterator.hasNext())
                                break
                            val next = iterator.next()
                            result.add(ProtocolChunk(next.first, next.second))
                        }
                        if (!iterator.hasNext()) {
                            eof = true
                            transaction.closeIterator(iterId)
                        }

                        // write results
                        val maxChunksPerBlock = SyncProtocol.MAX_CHUNKS_PER_BLOCK
                        val outBlocks = result.windowed(maxChunksPerBlock, maxChunksPerBlock, true)
                        outBlocks.forEachIndexed { index, list ->
                            val isEOF = if (index == outBlocks.lastIndex) eof else false
                            reply.write(IteratorReply(iterId, list, isEOF).asBlock())
                        }
                    }
                    reply.submit()
            }
    }
}
