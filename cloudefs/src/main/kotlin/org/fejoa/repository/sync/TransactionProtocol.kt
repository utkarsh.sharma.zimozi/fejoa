package org.fejoa.repository.sync

import kotlinx.serialization.protobuf.ProtoBuf
import org.fejoa.storage.StorageBackend
import org.fejoa.support.AsyncOutStream


object TransactionProtocol {

    suspend fun <T>send(request: SyncProtocol.Sender, remoteTransactionId: Long, branch: String,
                     accessRight: AccessRight, finishTransaction: Boolean, body: suspend () -> T) {
        // start/continue a new transaction
        if (remoteTransactionId <= 0)
            request.write(SyncProtocol.StartTransaction(branch, accessRight).asBlock())
        else
            request.write(SyncProtocol.Transaction(remoteTransactionId).asContinueBlock())

        body.invoke()

        if (finishTransaction)
            request.write(SyncProtocol.Transaction(remoteTransactionId).asFinishBlock())
    }

    suspend fun receiveReply(reply: SyncProtocol.Receiver, expectedTransactionId: Long, body: suspend () -> Unit)
            : Long {
        val transactionBlock = ProtoBuf.load<SyncProtocol.Transaction>(
                SyncProtocol.Transaction.serializer(), reply.next().data)
        if (expectedTransactionId >= 0 && transactionBlock.id != expectedTransactionId)
            throw Exception("Transaction id missmatch ${transactionBlock.id} but $expectedTransactionId expected")

        body.invoke()
        return transactionBlock.id
    }

    class Handler(val request: SyncProtocol.Receiver, val branch: String, val transactionManager: TransactionManager,
                  val branchBackend: StorageBackend.BranchBackend) {
        private var finishTransaction: Boolean = false

        suspend fun receive(handler: suspend (transaction: TransactionManager.Transaction) -> Unit): Reply {
            val transactionBlock = request.next()
            val transaction = when (transactionBlock.type) {
                SyncProtocol.BlockType.START_TRANSACTION.value -> {
                    val start = SyncProtocol.StartTransaction.read(transactionBlock.data)
                    transactionManager.startTransaction(branch, start.accessRight,
                            branchBackend.getChunkStorage().startTransaction())
                }
                SyncProtocol.BlockType.CONTINUE_TRANSACTION.value -> {
                    val t = ProtoBuf.load<SyncProtocol.Transaction>(
                            SyncProtocol.Transaction.serializer(), transactionBlock.data)
                    transactionManager.getTransaction(t.id) ?: throw Exception("Invalid transaction id $t")
                }
                else -> throw Exception("Unsupported transaction type: ${transactionBlock.type}")
            }

            handler.invoke(transaction)

            val last = if (request.hasNext())
                request.next()
            else
                request.current()

            last?.let {
                when (it.type) {
                    SyncProtocol.BlockType.FINISH_TRANSACTION.value -> {
                        //ProtoBuf.load<SyncProtocol.Transaction>(block.data)
                        finishTransaction = true
                    }
                    else -> {}
                }
            }
            return Reply(transaction)
        }

        inner class Reply(val transaction: TransactionManager.Transaction) {
            suspend fun reply(outStream: AsyncOutStream,
                              handlerReply: suspend (reply: SyncProtocol.Sender,
                                                     transaction: TransactionManager.Transaction) -> Unit) {
                val reply = request.reply(outStream)
                // write reply
                reply.write(SyncProtocol.Transaction(transaction.id).asReplyBlock())

                handlerReply.invoke(reply, transaction)

                if (finishTransaction)
                    transactionManager.finishTransaction(transaction.id)
            }
        }
    }
}