package org.fejoa.repository

import org.fejoa.storage.HashValue
import org.fejoa.storage.ChunkAccessor
import org.fejoa.storage.ChunkTransaction
import org.fejoa.chunkcontainer.ChunkContainer
import org.fejoa.chunkcontainer.ChunkContainerNode
import org.fejoa.chunkcontainer.ChunkContainerRef
import org.fejoa.support.assert
import org.fejoa.support.await


abstract class Job(private val parent: Job?) {
    private val childJobs = ArrayList<Job>()

    abstract suspend fun getRequestedChunks(): List<HashValue>

    init {
        parent?.childJobs?.add(this)
    }

    protected abstract suspend fun enqueueJobsAfterChunksFetched(chunkFetcher: ChunkFetcher)

    suspend fun onChunksFetched(chunkFetcher: ChunkFetcher) {
        enqueueJobsAfterChunksFetched(chunkFetcher)
        checkIfDone(chunkFetcher)
    }

    open suspend fun onDone(chunkFetcher: ChunkFetcher) {

    }

    private suspend fun onChildDone(job: Job, chunkFetcher: ChunkFetcher) {
        val removed = childJobs.remove(job)
        assert(removed)
        checkIfDone(chunkFetcher)
    }

    private suspend fun checkIfDone(chunkFetcher: ChunkFetcher) {
        if (childJobs.size == 0) {
            onDone(chunkFetcher)
            // have new jobs been added?
            if (parent != null && childJobs.size == 0)
                parent.onChildDone(this, chunkFetcher)
        }
    }
}

internal abstract class RootObjectJob(parent: Job?, protected val accessor: ChunkAccessor,
                                      protected val containerRef: ChunkContainerRef) : Job(parent) {

    override suspend fun getRequestedChunks(): List<HashValue> {
        return listOf(containerRef.boxHash)
    }
}

internal class GetChunkContainerNodeJob(parent: Job?, private val accessor: ChunkAccessor, private val node: ChunkContainerNode) : Job(parent) {

    override suspend fun getRequestedChunks(): List<HashValue> {
        val children = ArrayList<HashValue>()
        for (pointer in node.chunkPointers)
            children.add(pointer.getChunRef().boxHash)
        return children
    }

    override suspend fun enqueueJobsAfterChunksFetched(chunkFetcher: ChunkFetcher) {
        if (node.isDataLeafNode)
            return
        for (child in node.chunkPointers) {
            val childNode = ChunkContainerNode.read(accessor, node, child)
            chunkFetcher.enqueueJob(GetChunkContainerNodeJob(this, accessor, childNode))
        }
    }
}

internal open class GetChunkContainerJob(parent: Job?, accessor: ChunkAccessor, pointer: ChunkContainerRef)
    : RootObjectJob(parent, accessor, pointer) {
    protected var chunkContainer: ChunkContainer? = null

    override suspend fun enqueueJobsAfterChunksFetched(chunkFetcher: ChunkFetcher) {
        chunkContainer = ChunkContainer.read(accessor, containerRef)
        chunkFetcher.enqueueJob(GetChunkContainerNodeJob(this, accessor, chunkContainer!!))
    }
}

internal class GetObjectIndexJob(parent: Job?, val transaction: ChunkAccessors, val repositoryRef: RepositoryRef)
    : GetChunkContainerJob(parent, transaction.getCommitAccessor(repositoryRef.config.containerSpec),
        repositoryRef.objectIndexObjectIndexRef.chunkContainerRef) {

    private var doneCount = 0
    var objectIndex: ObjectIndex? = null
        private set

    override suspend fun onDone(chunkFetcher: ChunkFetcher) {
        if (doneCount > 0)
            return
        doneCount++

        objectIndex = ObjectIndex.open(repositoryRef.objectIndexObjectIndexRef, repositoryRef.config, transaction)
        chunkFetcher.enqueueObjectIndexJob(objectIndex!!)
    }
}


/**
 * @param transaction the target transaction
 */
class ChunkFetcher(private val transaction: ChunkAccessors, private val fetcherBackend: FetcherBackend) {
    private var ongoingJobs: MutableList<Job> = ArrayList()

    interface FetcherBackend {
        suspend fun fetch(transaction: ChunkTransaction, requestedChunks: List<HashValue>)
    }

    fun enqueueRepositoryJob(repositoryRef: RepositoryRef) {
        enqueueJob(GetObjectIndexJob(null, transaction, repositoryRef))
    }

    suspend fun enqueueObjectIndexJob(objectIndex: ObjectIndex) {
        val rawAccessor = transaction.getRawAccessor()

        (0 until objectIndex.getNEntries())
                .map {
                    objectIndex.getChunkContainerRef(it.toInt(), objectIndex.config.hashSpec)
                }
                .filterNot { it.boxHash.isZero || rawAccessor.hasChunk(it.boxHash).await() }
                .forEach {
                    enqueueJob(GetChunkContainerJob(null, objectIndex.getChunkAccessor(it),
                            it))
                }
    }

    fun enqueueJob(job: Job) {
        ongoingJobs.add(job)
    }

    suspend fun fetch() {
        val rawAccessor = transaction.getRawAccessor()
        while (ongoingJobs.size > 0) {
            val currentJobs = ongoingJobs
            ongoingJobs = ArrayList()

            val requestedChunks = ArrayList<HashValue>()
            for (job in currentJobs)
                requestedChunks.addAll(job.getRequestedChunks())

            fetcherBackend.fetch(rawAccessor, requestedChunks)
            for (job in currentJobs)
                job.onChunksFetched(this)
        }
        rawAccessor.finishTransaction().await()
    }

    companion object {

        fun createLocalFetcher(targetTransaction: ChunkAccessors,
                               source: ChunkTransaction): ChunkFetcher {
            return ChunkFetcher(targetTransaction, object : FetcherBackend {
                override suspend fun fetch(transaction: ChunkTransaction, requestedChunks: List<HashValue>) {
                    for (requestedChunk in requestedChunks) {
                        val buffer = source.getChunk(requestedChunk).await()
                        val result = transaction.putChunk(buffer).await()
                        if (result.key != requestedChunk)
                            throw Exception("Hash mismatch; expected: $requestedChunk got: ${result.key}")
                    }
                }
            })
        }
    }
}
