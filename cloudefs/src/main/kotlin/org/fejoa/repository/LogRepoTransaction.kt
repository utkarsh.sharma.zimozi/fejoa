package org.fejoa.repository

import org.fejoa.storage.*
import org.fejoa.support.Future


class LogRepoTransaction(private val parent: ChunkTransaction): ChunkTransaction {
    private val objectsWritten: MutableList<HashValue> = ArrayList()

    override fun iterator(): ChunkTransaction.Iterator {
        return parent.iterator()
    }

    override fun getChunk(boxHash: HashValue): Future<ByteArray> = parent.getChunk(boxHash)

    override fun prepareChunk(data: ByteArray): Future<Pair<HashValue, ByteArray>> {
        return parent.prepareChunk(data)
    }

    override fun putChunk(key: HashValue, rawData: ByteArray): Future<Boolean> {
        return parent.putChunk(key, rawData)
    }

    override fun putChunk(data: ByteArray): Future<PutResult<HashValue>> {
        return parent.putChunk(data).then { result ->
            if (!result.wasInDatabase)
                objectsWritten.add(result.key)

            return@then result
        }
    }
    override fun hasChunk(boxHash: HashValue): Future<Boolean> = parent.hasChunk(boxHash)

    override fun finishTransaction(): Future<Unit> {
        return parent.finishTransaction()
    }

    override fun cancel(): Future<Unit> {
        return parent.cancel()
    }

    override fun flush(): Future<Unit> {
        return parent.flush()
    }

    fun getObjectsWritten(): List<HashValue> {
        return objectsWritten
    }
}
