package org.fejoa.repository

import org.fejoa.storage.HashValue


object CommonAncestorsFinder {
    private class Graph(commits: Collection<Commit>, val commitGetter: CommitGetter) {
        var tails: MutableList<Commit> = ArrayList(commits)

        val touchedCommits: MutableMap<HashValue, Commit> = HashMap()

        init {
            commits.forEach {
                touchedCommits[it.hash.value] = it
            }
        }
        /**
         * Load more commits.
         *
         * @return true if more commits have been loaded
         */
        suspend fun loadCommits(): Boolean {
            if (tails.isEmpty())
                return false

            var allLoaded = false
            val newTails: MutableList<Commit> = ArrayList()
            for (tail in tails) {
                for (parent in tail.parents) {
                    val parentCommit = commitGetter.getCommit(parent)
                    if (touchedCommits.contains(parentCommit.hash.value))
                        continue
                    allLoaded = true
                    touchedCommits[parentCommit.hash.value] = parentCommit
                    newTails += parentCommit
                }
            }
            tails = newTails
            return allLoaded
        }

        suspend fun loadCommits(n: Int): Boolean {
            for (i in 0 until n) {
                val reachedRoot = loadCommits()
                if (reachedRoot)
                    return reachedRoot
            }
            return false
        }
    }

    /**
     * @param commonAncestors theirs common ancestors
     * @param commits their commits reaching from the heads to the common ancestors
     */
    class Result(val commonAncestors: List<Commit>, val commits: Map<HashValue, Commit>,
                 val touchedCommitsOurs: Map<HashValue, Commit>, val touchedCommitsTheirs: Map<HashValue, Commit>)

    suspend fun find(oursGetter: CommitGetter, oursCommit: Commit,
                     theirsGetter: CommitGetter, theirsCommit: Commit): Result {
        val oursGraph = Graph(listOf(oursCommit), oursGetter)
        val theirsGraph = Graph(listOf(theirsCommit), theirsGetter)

        if (oursCommit.hash.value == theirsCommit.hash.value) {
            return Result(listOf(theirsCommit), mapOf(theirsCommit.hash.value to theirsCommit),
                    mapOf(oursCommit.hash.value to oursCommit), mapOf(theirsCommit.hash.value to theirsCommit))
        }

        var commitsToLoad = 10
        oursGraph.loadCommits(commitsToLoad)
        theirsGraph.loadCommits(commitsToLoad)

        while (true) {
            var needToLoadMore = false
            val commonAncestors: MutableList<Commit> = ArrayList()
            val commits: MutableMap<HashValue, Commit> = HashMap()
            val ongoing: MutableList<Commit> = mutableListOf(theirsCommit)
            var reachedRoot = false
            while (ongoing.isNotEmpty() && !needToLoadMore) {
                val current = ongoing.removeAt(0)
                if (oursGraph.touchedCommits.contains(current.hash.value)) {
                    commonAncestors.firstOrNull { it.hash == current.hash } ?: run {
                        commonAncestors += current
                    }
                    continue
                }
                commits[current.hash.value] = current

                if (current.parents.isEmpty()) {
                    reachedRoot = true
                    commitsToLoad *= 2 // speed up loading; we might need to load all commits
                }
                for (parent in current.parents) {
                    val parentCommit = theirsGraph.touchedCommits[parent.hash.value]
                    if (parentCommit == null) {
                        needToLoadMore = true
                        break
                    }
                    ongoing += parentCommit
                }
            }

            if (needToLoadMore) {
                oursGraph.loadCommits(commitsToLoad)
                theirsGraph.loadCommits(commitsToLoad)
                continue
            }

            if (!reachedRoot
                    || (!oursGraph.loadCommits(commitsToLoad) && !theirsGraph.loadCommits(commitsToLoad))) {
                return Result(commonAncestors, commits, oursGraph.touchedCommits, theirsGraph.touchedCommits)
            }
        }
    }

    /**
     * @return a topological sorted list of all commits reaching from the ancestorCommitNodes to the head. The head is
     * the first item in the returned list.
     */
    fun topologicalSort(commits: Map<HashValue, Commit>, commitMap: Map<HashValue, Commit>): List<Commit> {
        if (commits.isEmpty())
            return emptyList()

        // commitHash -> children
        val childMap: MutableMap<HashValue, MutableSet<HashValue>> = HashMap()
        // commitHash -> parents
        val parentMap: MutableMap<HashValue, MutableList<Commit>> = HashMap()
        commits.forEach { entry ->
            val commit = entry.value
            val parentHashes: MutableList<Commit> = commit.parents.mapNotNull { commits[it.hash.value] }
                    .toMutableList()
            parentMap[commit.hash.value] = parentHashes

            if (childMap[commit.hash.value] == null)
                childMap[commit.hash.value] = HashSet()
            for (parent in parentHashes) {
                childMap[parent.hash.value]?.add(commit.hash.value) ?: run {
                    childMap[parent.hash.value] = mutableSetOf(commit.hash.value)
                }
            }
        }

        val sortedList: MutableList<Commit> = ArrayList()
        val ongoing: MutableList<Commit> = ArrayList()
        for ((key, value) in childMap) {
            if (value.isEmpty())
                ongoing += commitMap.getValue(key)
        }
        if (ongoing.size != 1)
            throw Exception("Only one head expected")

        while (ongoing.isNotEmpty()) {
            val current = ongoing.removeAt(0)
            sortedList += current

            val currentSet = parentMap.remove(current.hash.value)!!
            for (parent in currentSet) {
                childMap[parent.hash.value]!!.remove(current.hash.value)
                if (childMap[parent.hash.value]!!.isEmpty())
                    ongoing += parent
            }
        }
        if (parentMap.isNotEmpty())
            throw Exception("ParentMap should be empty")
        return sortedList
    }
}
