package org.fejoa.repository

import org.fejoa.chunkcontainer.ChunkContainer
import org.fejoa.chunkcontainer.ChunkContainerNode
import org.fejoa.support.AsyncIterator
import org.fejoa.storage.HashValue
import org.fejoa.storage.ChunkTransaction
import org.fejoa.support.CombinedIterator
import org.fejoa.support.IOException
import org.fejoa.support.await


class ChunkInfo(val box: HashValue, val data: HashValue, val iv: ByteArray, var chunkType: ChunkType,
                var ccOrigin: CCOrigin) {
    enum class ChunkType {
        ROOT,
        LEAF,
        DATA_LEAF,
        DATA
    }

    enum class CCOrigin {
        UNSET,
        OBJECT_INDEX,
        COMMIT,
        DIR,
        BLOB
    }

    override fun toString(): String {
        return "Box: ${box.toHex().substring(0, 8)}, Data: ${data.toHex().substring(0, 8)}, IV: ${HashValue(iv).toHex().substring(0, 8)} Type: ${chunkType.name}, Origin: ${ccOrigin.name}"
    }
}

fun ChunkContainer.chunkIterator(ccOrigin: ChunkInfo.CCOrigin): AsyncIterator<ChunkInfo> {
    val hashes = ArrayList<ChunkInfo>()
    val ongoingNodes: MutableList<ChunkContainerNode> = ArrayList()
    ongoingNodes += this
    return object : AsyncIterator<ChunkInfo> {
        private suspend fun add(node: ChunkContainerNode) {
            val currentType = when {
                node.isRootNode -> ChunkInfo.ChunkType.ROOT
                node.isDataLeafNode -> ChunkInfo.ChunkType.DATA_LEAF
                else -> ChunkInfo.ChunkType.LEAF
            }

            // TODO: for short data, node.that is invalid (see below), should this be handled in more transparent way?
            val pointer = if (node.isRootNode) (node as ChunkContainer).ref.chunkRef else node.that.getChunRef()
            val info = ChunkInfo(pointer.boxHash, pointer.dataHash, pointer.getIV(), currentType, ccOrigin)
            hashes.add(info)
        }

        override suspend fun hasNext(): Boolean {
            if (hashes.isNotEmpty())
                return true
            if (ongoingNodes.isEmpty())
                return false

            // add children
            val current = ongoingNodes.removeAt(0)
            add(current)
            if (!current.isDataLeafNode) {
                for (pointer in current.chunkPointers) {
                    val next = ChunkContainerNode.read(blobAccessor, current, pointer)
                    ongoingNodes.add(next)
                }
            } else if (!(current is ChunkContainer && current.isShortData())) {
                current.chunkPointers.map { it.getChunRef() }.forEach {
                    val info = ChunkInfo(it.boxHash, it.dataHash, it.getIV(),
                            ChunkInfo.ChunkType.DATA, ccOrigin)
                    hashes.add(info)
                }
            }

            return true
        }

        override suspend fun next(): ChunkInfo {
            return hashes.removeAt(0)
        }
    }
}

suspend fun Repository.chunkIterator(): AsyncIterator<ChunkInfo> {
    val hashes = CombinedIterator<ChunkInfo>()

    val commit = getHeadCommit()
    val commitId = commit?.ref?.objectId ?: -1
    val dirId = commit?.dir?.ref?.objectId ?: -1

    hashes.add(objectIndex.chunkContainer.chunkIterator(ChunkInfo.CCOrigin.OBJECT_INDEX))

    val nEntries = objectIndex.getNEntries()
    var index = 0
    return object : AsyncIterator<ChunkInfo> {
        override suspend fun hasNext(): Boolean {
            if (hashes.hasNext())
                return true
            if (index >= nEntries)
                return false

            val type = when (index) {
                commitId -> ChunkInfo.CCOrigin.COMMIT
                dirId -> ChunkInfo.CCOrigin.DIR
                else -> ChunkInfo.CCOrigin.BLOB
            }
            val ref = objectIndex.getChunkContainerRef(index, config.hashSpec)
            index++

            val container = ChunkContainer.read(objectIndex.getChunkAccessor(ref), ref)
            hashes.add(container.chunkIterator(type))
            return hashes.hasNext()
        }

        override suspend fun next(): ChunkInfo {
            return hashes.next()
        }
    }
}

suspend fun Repository.gc(target: ChunkTransaction) {
    if (isModified())
        throw Exception("Only a clean repository can be garbage collected")

    val source = getCurrentTransaction()
    val iterator = chunkIterator()
    while (iterator.hasNext()) {
        val chunkHash = iterator.next().box
        val chunk = source.getChunk(chunkHash).await()
        val result = target.putChunk(chunk).await()
        if (result.key != chunkHash)
            throw IOException("Hash mismatch; expected: $chunkHash got: ${result.key}")
    }
}