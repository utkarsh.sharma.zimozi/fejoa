package org.fejoa.repository

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.fejoa.chunkcontainer.*
import org.fejoa.storage.*
import org.fejoa.storage.Hash
import org.fejoa.support.IOException
import org.fejoa.support.nowNano
import org.fejoa.support.readAll


class IODatabaseCC(root: Directory, internal var objectIndex: ObjectIndex, private var accessors: ChunkAccessors,
                   val config: ContainerSpec, val lock: Mutex) : IODatabase {
    var treeAccessor = TreeAccessor(root)
    // Keeps track of potentially modified chunk containers not yet added to the object index.
    // maps path to ChunkContainer and the initial ChunkContainer hash
    private val modifiedChunkContainers: MutableMap<String, Pair<SharedChunkContainer, Hash>> = HashMap()
    // path -> shared chunk container
    val openHandles: MutableMap<String, SharedChunkContainer> = HashMap()
    private val dirChangeListener: (Pair<Directory, String>) -> Any = { changedEntry ->
        val dir = changedEntry.first
        val dirTime = dir.nameAttrData.getFSAttr()?.mTime ?: 0L
        val updateTime = dir.getEntry(changedEntry.second)?.nameAttrData?.getFSAttr()?.mTime?.let {
            if (dirTime == 0L && it == 0L)
                nowNano()
            if (dirTime < it) {
                it
            } else
                0L
        } ?: run {
            nowNano()
        }

        if (updateTime > 0L) {
            val attr = dir.nameAttrData.getFSAttr() ?: FSAttribute()
            attr.mTime = updateTime
            dir.nameAttrData.setFSAttr(attr)
        }
    }

    var maintainFSAttr = false
        set(value) {
            field = value
            if (value) {
                treeAccessor.dirChangeListener = dirChangeListener
            } else treeAccessor.dirChangeListener = null
        }
    // must be called with lock
    fun getModifiedChunkContainer(): Map<String, SharedChunkContainer> {
        return modifiedChunkContainers.mapValues { it.value.first }
    }

    // must be called with lock
    fun clearModifiedChunkContainer() {
        modifiedChunkContainers.clear()
        openHandles.asSequence().filter { it.value.hasChildWithWriteAccess() }.forEach { entry ->
            val shared = entry.value
            modifiedChunkContainers[entry.key] = shared to shared.container.ref.hash.clone()
        }
    }

    fun setRootDirectory(root: Directory) {
        val changeListener = this.treeAccessor.dirChangeListener
        this.treeAccessor = TreeAccessor(root)
        this.treeAccessor.dirChangeListener = changeListener
    }

    fun setAccessors(transaction: ChunkAccessors) {
        val changeListener = this.treeAccessor.dirChangeListener
        this.accessors = transaction
        this.treeAccessor = TreeAccessor(treeAccessor.root)
        this.treeAccessor.dirChangeListener = changeListener
    }

    private fun checkPath(path: String): String {
        // remove trailing "/"
        var out = path
        // TODO can be optimized:
        while (out.startsWith("/"))
            out = out.substring(1)

        return out
    }

    fun getRootDirectory(): Directory {
        return treeAccessor.root
    }

    private fun createIOCallback(path: String): SharedCCCallback {
        return object : SharedCCCallback() {
            override suspend fun onClosed(sharedContainer: SharedChunkContainer,
                                          dataAccess: ChunkContainerRandomDataAccess) = lock.withLock{
                sharedContainer.containerLock.withLock {
                    if (dataAccess.mode.has(RandomDataAccess.WRITE)) {
                        treeAccessor.putBlob(path, sharedContainer.container.ref.hash, null)

                        updateFSAttr(path, sharedContainer)
                    }

                    if (sharedContainer.openRandomAccess.size == 0)
                        openHandles.remove(sharedContainer.path)
                }
            }
        }
    }

    private suspend fun updateFSAttr(path: String, sharedContainer: SharedChunkContainer) {
        updateFSAttr(path, sharedContainer.lastModificationDate)
    }

    private suspend fun updateFSAttr(path: String, mTime: Long) {
        if (maintainFSAttr) {
            (treeAccessor.getAttribute(path, AttrType.FS_ATTR) as? FSAttribute)?.let {
                it.mTime = mTime
            } ?: run {
                FSAttribute(mTime = mTime).also {
                    treeAccessor.setAttribute(path, it)
                }
            }
        }
    }

    override suspend fun probe(path: String): IODatabase.FileType {
        val p = checkPath(path)
        val entry = treeAccessor[p] ?: return IODatabase.FileType.NOT_EXISTING
        return when (entry.entryType) {
            DirectoryEntry.EntryType.FLAT_DIR_END,
            DirectoryEntry.EntryType.EMPTY_DIR -> throw Exception("Unexpected entry type")
            DirectoryEntry.EntryType.FLAT_DIR -> IODatabase.FileType.DIRECTORY
            DirectoryEntry.EntryType.BLOB -> IODatabase.FileType.FILE
            DirectoryEntry.EntryType.SYMBOLIC_LINK -> TODO()
            DirectoryEntry.EntryType.HARD_LINK -> TODO()
            DirectoryEntry.EntryType.EXTERNAL_DIR -> TODO()
        }
    }

    override suspend fun getHash(path: String): HashValue = lock.withLock {
        val p = checkPath(path)
        openHandles[p]?.let {
            return@withLock it.flush().value
        }
        modifiedChunkContainers[p]?.first?.let {
            return@withLock it.container.ref.hash.value
        }
        val entry = treeAccessor[p] ?: throw IOException("File not fount: $p")
        if (!entry.isFile())
            throw IOException("Path is not a file: $p")

        return entry.getHash().value
    }

    suspend fun flush(): Hash = lock.withLock {
        flushUnlocked()
    }

    suspend fun flushUnlocked(): Hash {
        // TODO this can be optimized, i.e. if containers already have been flushed and added
        getModifiedChunkContainer().forEach {
            val path = it.key
            val hash = it.value.flush()
            treeAccessor.putBlob(path, hash, null)
            updateFSAttr(path, it.value)
        }
        return treeAccessor.build()
    }

    override suspend fun open(path: String, mode: Mode): RandomDataAccess = lock.withLock {
        val p = checkPath(path)
        // first try to find an open chunk container
        val randomDataAccess = openHandles[p]?.let {
            val randomDataAccess = ChunkContainerRandomDataAccess(it, mode, config.boxSpec.dataNormalization)
            it.addRandomAccess(randomDataAccess)
            randomDataAccess
        } ?: run {
            // try to open a chunk container
            val (chunkContainer, blobEntry) = try {
                val entry = treeAccessor[p] ?: throw IOException("Entry not found: $p")
                if (!entry.isFile())
                    throw IOException("Path is not a file")

                modifiedChunkContainers[p]?.let {
                    return@let it.first.container to it.first.blobEntry
                } ?: run {
                    val blobEntry = entry as BlobEntry
                    val container = objectIndex.getChunkContainerRef(blobEntry.onDiskRef!!)
                    container to blobEntry
                }
            } catch (e: IOException) {
                if (!mode.has(RandomDataAccess.WRITE))
                    throw e
                // create new chunk container
                val container = ChunkContainer.create(accessors.getFileAccessor(config, p), config)
                val blobEntry = treeAccessor.putBlob(p, container.ref.hash, null)
                container to blobEntry
            }
            val sharedContainer = SharedChunkContainer(p, chunkContainer, blobEntry, createIOCallback(p))
            if (mode.has(RandomDataAccess.WRITE))
                modifiedChunkContainers[p] = sharedContainer to sharedContainer.container.ref.hash.clone()
            val randomDataAccess = ChunkContainerRandomDataAccess(sharedContainer, mode, config.boxSpec.dataNormalization)
            sharedContainer.openRandomAccess.add(randomDataAccess)
            openHandles[p] = sharedContainer
            randomDataAccess
        }

        if (mode.has(RandomDataAccess.TRUNCATE)) {
            randomDataAccess.truncate(0)
        } else if (mode.has(RandomDataAccess.APPEND))
            randomDataAccess.seek(randomDataAccess.length())
        return randomDataAccess
    }

    override suspend fun remove(path: String) = lock.withLock {
        val p = checkPath(path)
        treeAccessor.remove(p)
        Unit
    }

    override suspend fun listFiles(path: String): Collection<String> = lock.withLock {
        val p = checkPath(path)
        val directory = treeAccessor[p] ?: return emptyList()
        if (directory.isFile())
            return emptyList()
        return (directory as Directory).getChildren().filter { it.isFile() }.map { it.name }
    }

    override suspend fun listDirectories(path: String): Collection<String> = lock.withLock {
        val p = checkPath(path)
        val directory = treeAccessor[p] ?: return emptyList()
        if (directory.isFile())
            return emptyList()
        return (directory as Directory).getChildren().filter { !it.isFile() }.map { it.name }
    }

    override suspend fun readBytes(path: String): ByteArray {
        val p = checkPath(path)
        val randomDataAccess = open(p, RandomDataAccess.READ)
        val date = randomDataAccess.readAll()
        randomDataAccess.close()
        return date
    }

    override suspend fun putBytes(path: String, data: ByteArray) {
        val p = checkPath(path)
        val randomDataAccess = open(p, RandomDataAccess.TRUNCATE) as ChunkContainerRandomDataAccess
        randomDataAccess.write(data)
        randomDataAccess.close()
    }

    override suspend fun mkDir(path: String) = lock.withLock {
        treeAccessor.putDir(path)
        updateFSAttr(path, nowNano())
        Unit
    }

    override suspend fun setAttribute(path: String, attribute: Attribute) = lock.withLock {
        val p = checkPath(path)
        treeAccessor.setAttribute(p, attribute)
        Unit
    }

    override suspend fun getAttribute(path: String, attrType: AttrType): Attribute? = lock.withLock {
        val p = checkPath(path)
        return treeAccessor.getAttribute(p, attrType)
    }
}
