package org.fejoa.repository

import kotlinx.serialization.SerialId
import kotlinx.serialization.Serializable
import kotlinx.serialization.dump
import kotlinx.serialization.load
import kotlinx.serialization.protobuf.ProtoBuf


interface Attribute {
    fun toByteArray(): ByteArray
}

const val EXECUTABLE_SHIFT = 0
const val WRITE_SHIFT = 1
const val READ_SHIFT = 2

fun setBit(value: Int, bit: Int, set: Boolean): Int {
    return if (set)
        value or (1 shl bit)
    else
        value and (1 shl bit).inv()
}

fun isBitSet(value: Int, bit: Int): Boolean {
    return value and (1 shl bit) != 0
}

@Serializable
class FSAttribute(@SerialId(0) var mode: Int = 6, // permissions for data owner
                  @SerialId(1) var mTime: Long = 0) : Attribute {

    companion object {
        fun read(data: ByteArray): FSAttribute {
            return ProtoBuf.load(FSAttribute.serializer(), data)
        }
    }

    fun setExecutable(executable: Boolean) {
        mode = setBit(mode, EXECUTABLE_SHIFT, executable)
    }

    fun isExecutable(): Boolean {
        return isBitSet(mode, EXECUTABLE_SHIFT)
    }

    fun setWriteable(writeable: Boolean) {
        mode = setBit(mode, WRITE_SHIFT, writeable)
    }

    fun isWriteable(): Boolean {
        return isBitSet(mode, WRITE_SHIFT)
    }

    override fun toByteArray(): ByteArray {
        return ProtoBuf.dump(FSAttribute.serializer(), this)
    }
}