package org.fejoa.storage

interface CommitSignature {
    suspend fun signMessage(message: ByteArray, rootHashValue: HashValue, parents: Collection<HashValue>): ByteArray
    suspend fun verifySignedMessage(signedMessage: ByteArray, rootHashValue: HashValue, parents: Collection<HashValue>)
            : Boolean
}
