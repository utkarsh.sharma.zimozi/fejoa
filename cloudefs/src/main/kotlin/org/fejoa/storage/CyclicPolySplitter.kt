package org.fejoa.storage

import org.fejoa.support.*


class CyclicPolySplitter private constructor(val seed: ByteArray,
                                             private val cyclicPolyFingerprint: CyclicPolyFingerprint,
                                             targetChunkSize: Int, minChunkSize: Int, maxChunkSize: Int)
    : DynamicSplitter(targetChunkSize, minChunkSize, maxChunkSize, cyclicPolyFingerprint.windowSize) {


    companion object {
        suspend fun create(seed: ByteArray, targetChunkSize: Int, minChunkSize: Int, maxChunkSize: Int,
                           windowSize: Int): CyclicPolySplitter {
            val fingerprint = CyclicPolyFingerprint.create(seed, windowSize)
            return CyclicPolySplitter(seed, fingerprint, targetChunkSize, minChunkSize, maxChunkSize)
        }
    }

    override fun newInstance(): ChunkSplitter {
        return CyclicPolySplitter(seed, cyclicPolyFingerprint.newInstance(), targetChunkSize, minChunkSize,
                maxChunkSize)
    }

    /**
     * 1) circular shift the existing fingerprint to the right
     * 2) if full remove the contribution of the oldest byte
     * 3) add the new byte
     */
    override fun updateFingerprint(byte: Byte): Long {
        cyclicPolyFingerprint.write(byte)
        return cyclicPolyFingerprint.fingerprint
    }

    override fun resetFingerprint() {
        cyclicPolyFingerprint.reset()
    }
}