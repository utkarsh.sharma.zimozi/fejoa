package org.fejoa.storage

import org.fejoa.chunkcontainer.BoxSpec
import org.fejoa.crypto.CryptoSettings
import org.fejoa.crypto.CryptoInterface
import org.fejoa.crypto.SecretKey
import org.fejoa.support.*


interface StorageTransaction {
    fun finishTransaction(): Future<Unit>
    fun cancel(): Future<Unit>
}

interface ChunkTransaction : StorageTransaction {
    interface Iterator : AsyncIterator<Pair<HashValue, ByteArray>> {
        fun close() {}
    }

    fun getChunk(boxHash: HashValue): Future<ByteArray>

    /**
     * Prepares the data, i.e. calculate the key and the value that will be stored in the chunkstore
     *
     * @return the key and the value to be stored in the chunk store
     */
    fun prepareChunk(data: ByteArray): Future<Pair<HashValue, ByteArray>>

    /**
     * @return true if the key was already in the database
     */
    fun putChunk(key: HashValue, rawData: ByteArray): Future<Boolean>

    fun putChunk(data: ByteArray): Future<PutResult<HashValue>> {
        return prepareChunk(data).bind { key ->
            putChunk(key.first, key.second).then {
                PutResult(key.first, it)
            }
        }
    }

    fun hasChunk(boxHash: HashValue): Future<Boolean>
    fun iterator(): Iterator

    fun flush(): Future<Unit>
}

interface ChunkStorage {
    fun startTransaction(): ChunkTransaction
}

interface ChunkAccessor {
    suspend fun getChunk(ref: ChunkRef): Future<ByteArray> {
        return getBoxChunk(ref.boxHash).bind { box -> unboxChunk(box, ref) }
    }

    suspend fun putChunk(data: ByteArray, ivHash: HashValue): Future<HashValue> {
        return boxChunk(data, ivHash).bind { box -> putBoxChunk(box).then { box.first } }
    }

    /**
     * Prepares the data, i.e. calculate the key and the value as stored in the chunkstore
     */
    fun boxChunk(data: ByteArray, ivHash: HashValue): Future<Pair<HashValue, ByteArray>>
    fun unboxChunk(boxData: ByteArray, ref: ChunkRef): Future<ByteArray>

    fun getBoxChunk(key: HashValue): Future<ByteArray>
    fun putBoxChunk(key: HashValue, rawData: ByteArray): Future<Boolean>
    fun putBoxChunk(keyValue: Pair<HashValue, ByteArray>) = putBoxChunk(keyValue.first, keyValue.second)

    suspend fun releaseChunk(data: HashValue)

    suspend fun flush()
}

fun ChunkTransaction.toChunkAccessor(): ChunkAccessor {
    val that = this
    return object : ChunkAccessor {
        override fun unboxChunk(boxData: ByteArray, ref: ChunkRef): Future<ByteArray> {
            return Future.completedFuture(boxData)
        }

        override fun boxChunk(data: ByteArray, ivHash: HashValue): Future<Pair<HashValue, ByteArray>> {
            return that.prepareChunk(data)
        }

        override suspend fun releaseChunk(data: HashValue) {
        }

        override fun getBoxChunk(key: HashValue): Future<ByteArray> {
            return that.getChunk(key)
        }

        override fun putBoxChunk(key: HashValue, rawData: ByteArray): Future<Boolean> {
            return that.putChunk(key, rawData)
        }

        override suspend fun flush() {
            that.flush().await()
        }
    }
}

fun ChunkAccessor.encrypted(crypto: CryptoInterface, secretKey: SecretKey, algo: CryptoSettings.SYM_ALGO): ChunkAccessor {
    val that = this

    return object : ChunkAccessor {
        private fun getIv(hashValue: ByteArray): ByteArray {
            return hashValue.copyOfRange(0, 16)
        }

        override fun getBoxChunk(key: HashValue): Future<ByteArray> {
            return that.getBoxChunk(key)
        }

        override fun putBoxChunk(key: HashValue, rawData: ByteArray): Future<Boolean> {
            return that.putBoxChunk(key, rawData)
        }

        override fun boxChunk(data: ByteArray, ivHash: HashValue): Future<Pair<HashValue, ByteArray>> {
            val encryptedF = crypto.encryptSymmetricAsync(data, secretKey, getIv(ivHash.bytes), algo)
            return encryptedF.bind { encrypted -> that.boxChunk(encrypted, ivHash) }
        }

        override fun unboxChunk(boxData: ByteArray, ref: ChunkRef): Future<ByteArray> {
            val encryptedF = that.unboxChunk(boxData, ref)
            return encryptedF.bind { encrypted ->
                crypto.decryptSymmetricAsync(encrypted, secretKey, getIv(ref.getIV()), algo) }
        }

        override suspend fun releaseChunk(data: HashValue) {
            return that.releaseChunk(data)
        }

        override suspend fun flush() {
            return that.flush()
        }
    }
}

fun ChunkAccessor.compressed(type: BoxSpec.ZipType): ChunkAccessor {
    val that = this
    return object : ChunkAccessor {
        override fun getBoxChunk(key: HashValue): Future<ByteArray> {
            return that.getBoxChunk(key)
        }

        override fun putBoxChunk(key: HashValue, rawData: ByteArray): Future<Boolean> {
            return that.putBoxChunk(key, rawData)
        }

        override fun boxChunk(data: ByteArray, ivHash: HashValue): Future<Pair<HashValue, ByteArray>> {
            return when (type) {
                BoxSpec.ZipType.NONE -> throw Exception("None is an invalid compression")
                BoxSpec.ZipType.DEFLATE -> that.boxChunk(DeflateUtils.deflate(data), ivHash)
                BoxSpec.ZipType.ZSTANDARD -> ZStdCompression().Compress(data).bind { compressed ->
                    that.boxChunk(compressed, ivHash) }
            }
        }

        override fun unboxChunk(boxData: ByteArray, ref: ChunkRef): Future<ByteArray> {
            val compressedF = that.unboxChunk(boxData, ref)
            return compressedF.bind { compressed ->
                return@bind when (type) {
                    BoxSpec.ZipType.NONE -> throw Exception("None is an invalid compression")
                    BoxSpec.ZipType.DEFLATE -> Future.completedFuture(DeflateUtils.inflate(compressed))
                    BoxSpec.ZipType.ZSTANDARD -> ZStdCompression().Decompress(compressed)
                }
            }
        }

        override suspend fun releaseChunk(data: HashValue) {
            return that.releaseChunk(data)
        }

        override suspend fun flush() {
            return that.flush()
        }
    }
}