package org.fejoa.storage

import kotlinx.serialization.*
import kotlinx.serialization.internal.StringDescriptor

import org.fejoa.protocolbufferlight.ProtocolBufferLight
import org.fejoa.support.decodeBase64
import org.fejoa.support.encodeBase64


@Serializable
class BranchLogEntry(@SerialId(TIME_TAG) val time: Long = 0L,
                     @SerialId(ENTRY_ID_TAG)
                     var entryId: HashValue = Config.newDataHash(),
                     @SerialId(MESSAGE_TAG) var message: Message = Message(),
                     @SerialId(CHANGES_TAG)
                     @Optional val changes: MutableList<HashValue> = ArrayList()) {
    @Serializable
    class Message(val data: ByteArray = ByteArray(0)) {
        @Serializer(forClass = Message::class)
        companion object : KSerializer<Message> {
            override val descriptor: SerialDescriptor = StringDescriptor.withName(
                    "org.fejoa.storage.BranchLogEntry.Message")

            override fun serialize(encoder: Encoder, obj: BranchLogEntry.Message) {
                encoder.encodeString(obj.data.encodeBase64())
            }

            override fun deserialize(decoder: Decoder): BranchLogEntry.Message {
                return BranchLogEntry.Message(decoder.decodeString().decodeBase64())
            }
        }
    }

    companion object {
        const val TIME_TAG = 0
        const val ENTRY_ID_TAG = 1
        const val MESSAGE_TAG = 2
        const val CHANGES_TAG = 3

        fun read(buffer: ByteArray): BranchLogEntry {
            val protoBuf = ProtocolBufferLight(buffer)
            val time = protoBuf.getLong(TIME_TAG) ?: throw Exception("Time missing")
            val entryId = protoBuf.getBytes(ENTRY_ID_TAG) ?: throw Exception("Entry id missing")
            val message = protoBuf.getBytes(MESSAGE_TAG) ?: throw Exception("Message missing")
            val changes = protoBuf.getByteArrayList(CHANGES_TAG)?.map { HashValue(it) }?.toMutableList()
                ?: ArrayList()
            return BranchLogEntry(time, HashValue(entryId), Message(message), changes)
        }
    }

    fun toProtoBuffer(): ProtocolBufferLight {
        val buffer = ProtocolBufferLight()
        buffer.put(TIME_TAG, time)
        buffer.put(ENTRY_ID_TAG, entryId.bytes)
        buffer.put(MESSAGE_TAG, message.data)
        buffer.put(CHANGES_TAG, changes.map { it.bytes })
        return buffer
    }
}

@Serializable
class BranchLogList(val entries: MutableList<BranchLogEntry> = ArrayList()) {
    fun add(entry: BranchLogEntry) {
        entries.add(entry)
    }

    fun add(time: Long, id: HashValue, message: ByteArray, changes: MutableList<HashValue>) {
        entries.add(BranchLogEntry(time, id, BranchLogEntry.Message(message), changes))
    }
}


interface BranchLog {
    fun getBranchName(): String
    suspend fun add(id: HashValue, message: ByteArray, changes: List<HashValue>)
    suspend fun add(entry: BranchLogEntry)
    suspend fun add(entries: List<BranchLogEntry>)
    /**
     * Only add the entry if the last entry has the expected entry id
     */
    suspend fun add(entry: BranchLogEntry, expectedEntryId: HashValue?): Boolean

    /**
     * First entry it the latest (the head)
     */
    suspend fun getEntries(): List<BranchLogEntry>
    suspend fun getHead(): BranchLogEntry?
}