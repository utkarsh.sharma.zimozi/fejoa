package org.fejoa.storage

import org.fejoa.support.*


open class ChunkRef(val dataHash: HashValue = Config.newDataHash(),
                    val boxHash: HashValue = Config.newBoxHash(),
                    private val iv: ByteArray = ByteArray(Config.IV_SIZE),
                    val dataLength: Long = 0L,
                    val compact: Boolean) {
    companion object {
        private fun getIv(hashValue: ByteArray): ByteArray {
            return hashValue.copyOfRange(0, Config.IV_SIZE)
        }

        fun read(inputStream: InStream, compact: Boolean): ChunkRef {
            val dataLength = inputStream.readLong()
            val dataHash = Config.newDataHash()
            val boxHash = Config.newBoxHash()
            val iv = ByteArray(Config.IV_SIZE)
            inputStream.readFully(dataHash.bytes)
            inputStream.readFully(boxHash.bytes)
            if (!compact)
                inputStream.readFully(iv)
            return ChunkRef(dataHash, boxHash, iv, dataLength, compact)
        }
    }

    init {
        assert(dataHash.size() == Config.DATA_HASH_SIZE && boxHash.size() == Config.BOX_HASH_SIZE
                && iv.size == Config.IV_SIZE)
    }

    constructor(dataHash: HashValue, boxHash: HashValue, iv: HashValue, dataLength: Long, compact: Boolean)
            : this(dataHash, boxHash, getIv(iv.bytes), dataLength, compact)


    fun write(outputStream: OutStream): Int {
        // update the data length
        var bytesWritten = 0
        bytesWritten += outputStream.writeLong(dataLength)
        bytesWritten += outputStream.write(dataHash.bytes)
        bytesWritten += outputStream.write(boxHash.bytes)
        if (!compact)
            bytesWritten += outputStream.write(iv)
        return bytesWritten
    }

    fun getIV(): ByteArray {
        return if (!compact)
            iv
        else
            dataHash.bytes.copyOfRange(0, Config.IV_SIZE)
    }
}
