package org.fejoa.repository

import org.fejoa.storage.Hash
import org.fejoa.storage.HashSpec
import org.fejoa.storage.HashValue
import org.fejoa.support.toUTF
import org.fejoa.test.testAsync
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull


class MockCommitGetter : CommitGetter {
    val commits: MutableMap<HashValue, Commit> = HashMap()

    override suspend fun getCommit(objectRef: ObjectRef): Commit {
        return commits[objectRef.hash.value] ?: throw Exception("Missing commit")
    }
}

class GraphBuilder(private val commitGetter: MockCommitGetter) {
    fun add(hash: String, parents: List<String> = emptyList()): Commit {
        val commit = Commit(
                dir = ObjectRef(Hash.zeroSHA256(), Ref(0, Ref.Type.CONTAINER, 0)),
                hash = Hash(HashSpec(HashSpec.HashType.SHA_256, null), HashValue.fromHex(hash)),
                parents = parents.map {
                    ObjectRef(Hash(HashSpec(HashSpec.HashType.SHA_256, null),
                            HashValue.fromHex(it)), Ref(0, Ref.Type.CONTAINER, 0))
                }, message = "".toUTF())
        commitGetter.commits[commit.hash.value] = commit
        return commit
    }
}

class CommonAncestorsFinderTest {
    private fun assertCommits(expected: List<String>, actual: Map<HashValue, Commit>) {
        assertEquals(expected.size, actual.size)
        expected.forEach { hash ->
            assertNotNull(actual[HashValue.fromHex(hash)])
        }
    }

    private fun assertCommits(expected: List<String>, actual: Collection<Commit>) {
        assertEquals(expected.size, actual.size)
        expected.forEach { hash ->
            assertNotNull(actual.firstOrNull { it.hash.value.toHex() == hash })
        }
    }

    @Test
    fun testSimple() = testAsync {
        val getter = MockCommitGetter()
        val builder = GraphBuilder(getter)
        builder.add("a0")
        builder.add("a1", listOf("a0"))
        val b0 = builder.add("b0", listOf("a1"))
        val c0 = builder.add("c0", listOf("a1"))

        val result = CommonAncestorsFinder.find(getter, b0, getter, c0)
        assertCommits(listOf("a1"), result.commonAncestors)

        val b1 = builder.add("b1", listOf("b0"))
        val c1 = builder.add("c1", listOf("c0"))

        val result2 = CommonAncestorsFinder.find(getter, c1, getter, b1)
        assertCommits(listOf("a1"), result2.commonAncestors)

        assertCommits(listOf("b0", "b1"), result2.commits)
    }

    @Test
    fun testSimple2() = testAsync {
        val getter = MockCommitGetter()
        val builder = GraphBuilder(getter)
        val a0 = builder.add("a0")
        val b0 = builder.add("b0", listOf("a0"))

        val result = CommonAncestorsFinder.find(getter, b0, getter, a0)
        assertCommits(listOf("a0"), result.commonAncestors)
        assertCommits(listOf(), result.commits)
    }

    @Test
    fun testError1() = testAsync {
        val getter = MockCommitGetter()
        val builder = GraphBuilder(getter)

        // local
        builder.add("a00")
        builder.add("a0", listOf("a00"))
        builder.add("a1", listOf("a0"))
        builder.add("a2", listOf("a1"))
        builder.add("a3", listOf("a2"))
        builder.add("a4", listOf("a3"))

        builder.add("b0", listOf("a1"))
        builder.add("b1", listOf("b0"))
        builder.add("b2", listOf("b1"))

        val l = builder.add("b3", listOf("a4", "b2"))

        // remote
        builder.add("a00")
        builder.add("a0", listOf("a00"))
        builder.add("a1", listOf("a0"))
        builder.add("a2", listOf("a1"))
        builder.add("a3", listOf("a2"))
        builder.add("a4", listOf("a3"))
        builder.add("a5", listOf("a4"))
        builder.add("a6", listOf("a4"))
        val r = builder.add("a7", listOf("a5", "a6"))

        val result = CommonAncestorsFinder.find(getter, r, getter, l)
        assertCommits(listOf("a1", "a4"), result.commonAncestors)
        assertCommits(listOf("b0", "b1", "b2", "b3"), result.commits)

        val result2 = CommonAncestorsFinder.find(getter, l, getter, r)
        assertCommits(listOf("a4"), result2.commonAncestors)
        assertCommits(listOf("a5", "a6", "a7"), result2.commits)
    }

    @Test
    fun testTwoRoots() = testAsync {
        val getter = MockCommitGetter()
        val builder = GraphBuilder(getter)

        // local
        builder.add("a0")
        builder.add("a1", listOf("a0"))
        builder.add("a2", listOf("a1"))
        builder.add("a3", listOf("a2"))
        builder.add("a4", listOf("a3"))


        val l = builder.add("a4", listOf("a3"))

        // remote
        builder.add("b0")
        builder.add("b1", listOf("b0"))
        builder.add("b2", listOf("b1"))
        builder.add("b3", listOf("b2", "a2"))
        builder.add("b4", listOf("b3"))

        val r = builder.add("b5", listOf("b4"))

        val result = CommonAncestorsFinder.find(getter, l, getter, r)
        assertEquals(1, result.commonAncestors.size)
        assertEquals("a2", result.commonAncestors[0].hash.value.toHex())
        assertCommits(listOf("b0", "b1", "b2", "b3", "b4", "b5"), result.commits)

        val result2 = CommonAncestorsFinder.find(getter, r, getter, l)
        assertEquals(1, result2.commonAncestors.size)
        assertEquals("a2", result2.commonAncestors[0].hash.value.toHex())
        assertCommits(listOf("a3", "a4"), result2.commits)
    }
}
