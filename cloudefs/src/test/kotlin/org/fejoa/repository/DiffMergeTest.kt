package org.fejoa.repository

import org.fejoa.storage.Hash
import org.fejoa.storage.HashSpec
import org.fejoa.crypto.CryptoHelper
import org.fejoa.storage.HashValue
import org.fejoa.storage.Database
import org.fejoa.storage.KeepOursUnchanged
import org.fejoa.support.toUTF
import org.fejoa.test.testAsync
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue


class DiffMergeTest : RepositoryTestBase() {
    private suspend fun addFile(box: Directory, name: String): BlobEntry {
        val dataHash = HashValue(CryptoHelper.sha256Hash(CryptoHelper.crypto.generateSalt()))

        val entry = BlobEntry(name, Hash(HashSpec(HashSpec.HashType.SHA_256, null), dataHash), null,
                false)
        box.put(entry)
        return entry
    }

    @Test
    fun testDiff() = testAsync({ setUp() }, { tearDown() }) {
        val seed = ByteArray(1)
        val ours = Directory("ours", HashSpec.createCyclicPoly(HashSpec.HashType.FEJOA_CYCLIC_POLY_2KB_8KB, seed))
        val theirs = Directory("theirs", HashSpec.createCyclicPoly(HashSpec.HashType.FEJOA_CYCLIC_POLY_2KB_8KB, seed))

        val file1 = addFile(ours, "test1")
        var iterator = DirectoryDiffIterator("", ours, theirs)
        assertTrue(iterator.hasNext())
        var change = iterator.next()
        assertEquals(DiffIterator.Type.REMOVED, change.type)
        assertEquals("test1", change.path)
        assertFalse(iterator.hasNext())
        theirs.put(file1)

        iterator = DirectoryDiffIterator("", ours, theirs)
        assertFalse(iterator.hasNext())

        val file2 = addFile(theirs, "test2")
        iterator = DirectoryDiffIterator("", ours, theirs)
        assertTrue(iterator.hasNext())
        change = iterator.next()
        assertEquals(DiffIterator.Type.ADDED, change.type)
        assertEquals("test2", change.path)
        assertFalse(iterator.hasNext())
        ours.put(file2)

        val file3 = addFile(ours, "test3")
        theirs.put(file3)
        val file4 = addFile(ours, "test4")
        theirs.put(file4)
        val file5 = addFile(ours, "test5")
        theirs.put(file5)

        val file31 = addFile(ours, "test31")
        iterator = DirectoryDiffIterator("", ours, theirs)
        assertTrue(iterator.hasNext())
        change = iterator.next()
        assertEquals(DiffIterator.Type.REMOVED, change.type)
        assertEquals("test31", change.path)
        assertFalse(iterator.hasNext())

        theirs.put(file31)
        addFile(theirs, "test41")
        iterator = DirectoryDiffIterator("", ours, theirs)
        assertTrue(iterator.hasNext())
        change = iterator.next()
        assertEquals(DiffIterator.Type.ADDED, change.type)
        assertEquals("test41", change.path)
        assertFalse(iterator.hasNext())

        addFile(ours, "test41")
        iterator = DirectoryDiffIterator("", ours, theirs)
        assertTrue(iterator.hasNext())
        change = iterator.next()
        assertEquals(DiffIterator.Type.MODIFIED, change.type)
        assertEquals("test41", change.path)
        assertFalse(iterator.hasNext())
    }

    @Test
    fun testMerge() = testAsync({ setUp() }, { tearDown() }) {
        val branch = "repoBranch"
        val directory = "testMergeRepoTest"
        val directory2 = "testMergeRepoTest2"
        val directory3 = "testMergeRepoTest3"

        val repository = createRepo(directory, branch)
        val repository2 = createRepo(directory2, branch)
        val repository3 = createRepo(directory3, branch)

        // create a common base
        repository.putBytes("file1", "file1".toUTF())
        repository.commit("Commit0".toUTF(), null)
        val head0 = repository.getHeadCommit()!!
        repository2.putBytes("file1", "file1".toUTF())
        repository2.commit("Commit0".toUTF(), null)
        repository3.putBytes("file1", "file1".toUTF())
        repository3.commit("Commit0".toUTF(), null)

        repository2.putBytes("file2", "file2".toUTF())
        repository2.commit("Commit1".toUTF(), null)
        val head1 = repository.getHeadCommit()!!

        val mergedContent = HashMap<String, DatabaseStringEntry>()
        mergedContent["file1"] = DatabaseStringEntry("file1", "file1")
        mergedContent["file2"] = DatabaseStringEntry("file2", "file2")

        // test common ancestor finder
        val ours = repository.getHeadCommit()!!
        val theirs = repository2.getHeadCommit()!!
        val findResult = CommonAncestorsFinder.find(repository.commitCache, ours, repository2.commitCache, theirs)
        assertEquals(1, findResult.commonAncestors.size)
        val parent = findResult.commonAncestors.first()
        assertEquals(theirs.parents[0].hash, parent.hash)

        var result = repository.merge(listOf(repository2), ThreeWayMerge())
        assertEquals(Database.MergeResult.FAST_FORWARD, result)
        assertEquals(repository.getHeadCommit()!!.hash, repository2.getHeadCommit()!!.hash)
        containsContent(repository, mergedContent)

        // test merging the same heads
        result = repository.merge(listOf(repository2), ThreeWayMerge())
        assertEquals(Database.MergeResult.FAST_FORWARD, result)
        assertEquals(repository.getHeadCommit()!!.hash, repository2.getHeadCommit()!!.hash)

        repository.putBytes("file2", "our file 2".toUTF())
        repository.commit("Commit3".toUTF(), null)
        repository2.putBytes("file2", "their file 2".toUTF())
        repository2.commit("Commit4".toUTF(), null)

        result = repository.merge(listOf(repository2), ThreeWayMerge())
        assertEquals(Database.MergeResult.MERGED, result)
        repository.commit("merge2".toUTF(), null)

        mergedContent.clear()
        mergedContent["file1"] = DatabaseStringEntry("file1", "file1")
        mergedContent["file2"] = DatabaseStringEntry("file2", "our file 2")
        containsContent(repository, mergedContent)

        // manual merge:
        // create commit that we want to merge in
        repository2.putBytes("mergefile", "changeIn2".toUTF())
        repository2.commit("Commit5".toUTF(), null)
        repository3.putBytes("mergefile", "changeIn3".toUTF())
        repository3.commit("Commit6".toUTF(), null)
        // create data that we actually want to have after the merge
        repository.putBytes("mergefile", "change".toUTF())
        mergedContent["mergefile"] = DatabaseStringEntry("mergefile", "change")
        // use the KeepOursUnchanged strategy to keep our local changes
        result = repository.merge(listOf(repository2, repository3), KeepOursUnchanged())
        assertEquals(Database.MergeResult.MERGED, result)
        containsContent(repository, mergedContent)
        repository.commit("merge3".toUTF(), null)
        containsContent(repository, mergedContent)

        // test merging an older heads, i.e. nothing to do
        val repoRef = repository.getRepositoryRef()
        val head0Repo = openRepo(directory, branch, RepositoryRef(repoRef.objectIndexObjectIndexRef,
                ObjectRef(head0.hash, head0.ref!!), repoRef.config))
        result = repository.merge(listOf(head0Repo), ThreeWayMerge())
        assertEquals(Database.MergeResult.FAST_FORWARD, result)
        assertEquals(repository.getHeadCommit()!!.hash, repoRef.head.hash)

        val head1Repo = openRepo(directory, branch,
                RepositoryRef(repoRef.objectIndexObjectIndexRef, ObjectRef(head1.hash, head1.ref!!), repoRef.config))
        result = repository.merge(listOf(head1Repo), ThreeWayMerge())
        assertEquals(Database.MergeResult.FAST_FORWARD, result)
        assertEquals(repository.getHeadCommit()!!.hash, repoRef.head.hash)
    }
}
