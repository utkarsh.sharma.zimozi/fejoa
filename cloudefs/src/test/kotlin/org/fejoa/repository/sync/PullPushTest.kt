package org.fejoa.repository.sync

import org.fejoa.network.RemotePipe
import org.fejoa.repository.Repository
import org.fejoa.storage.BranchLog
import org.fejoa.repository.RepositoryTestBase
import org.fejoa.repository.ThreeWayMerge
import org.fejoa.storage.IODatabase
import org.fejoa.support.*
import org.fejoa.test.testAsync
import kotlin.test.*


class PullPushTest : RepositoryTestBase() {
    private fun connect(handler: RequestHandler): RemotePipe {
        return object : RemotePipe {
            private var backingOutStream: AsyncByteArrayOutStream? = AsyncByteArrayOutStream()
            private var backingInStream: AsyncInStream? = null

            override val outStream: AsyncOutStream = object : AsyncOutStream {
                override suspend fun write(buffer: ByteArray, offset: Int, length: Int): Int {
                    backingInStream = null
                    val outStream = backingOutStream
                            ?: AsyncByteArrayOutStream().also { backingOutStream = it }
                    return outStream.write(buffer, offset, length)
                }
            }

            override val inStream: AsyncInStream = object : AsyncInStream {
                override suspend fun read(buffer: ByteArray, offset: Int, length: Int): Int {
                    val stream = backingInStream ?: run {
                        val inputStream = ByteArrayInStream(backingOutStream!!.toByteArray()).toAsyncInputStream()
                        backingOutStream = null

                        val reply = AsyncByteArrayOutStream()
                        val result = handler.handle(object : RemotePipe {
                            override val inStream: AsyncInStream
                                get() = inputStream

                            override val outStream: AsyncOutStream
                                get() = reply
                        }, AccessRight.ALL)
                        if (result != RequestHandler.Result.OK)
                            throw Exception("Error in handler ${result.value}, ${result.description}")

                        val replyStream = ByteArrayInStream(reply.toByteArray()).toAsyncInputStream()
                        backingInStream = replyStream
                        replyStream
                    }

                    return stream.read(buffer, offset, length)
                }
            }
        }
    }

    @Test
    fun testPull() = testAsync({ setUp() }, { tearDown() }) {
        val pullBranch = "pullBranch"
        val localDir = "LocalPullTest"
        val pullAllDir = "PullAllPullTest"
        val remoteDir = "RemotePullTest"

        val requestRepo = createRepo(localDir, pullBranch, false)
        val remoteRepo = createRepo(remoteDir, pullBranch)
        val transactionManager = TransactionManager()
        val handler = RequestHandler(pullBranch, transactionManager, remoteRepo.branchBackend,
                object : RequestHandler.BranchLogGetter {
                    override fun get(branch: String): BranchLog? {
                        return if (pullBranch == branch)
                            remoteRepo.log
                        else
                            null
                    }
                })
        val senderPipe = connect(handler)

        val missingTip = BranchLogProtocol.getBranchLog(senderPipe, "missingbranch", 1).entries
                .firstOrNull()
        assertNull(missingTip)

        val remoteTip = BranchLogProtocol.getBranchLog(senderPipe, pullBranch, 1).entries.first()
        assertEquals(remoteRepo.log.getHead()!!.entryId, remoteTip.entryId)

        val pullRequest = PullRequest(requestRepo, null)
        var pulledTip = pullRequest.pull(senderPipe, pullBranch, ThreeWayMerge())
        assertTrue(pulledTip!!.value.isZero)

        // change the remote repo
        val testRemoteRepo = TestRepository(remoteRepo)
        testRemoteRepo.putBlob("testFile", "Hello World")
        testRemoteRepo.commit("")
        var remoteHead = remoteRepo.getHead()

        pulledTip = pullRequest.pull(senderPipe, pullBranch, ThreeWayMerge())
                ?: throw Exception("Remote head should not be null")
        testRemoteRepo.verify(requestRepo)

        assertEquals(pulledTip, remoteHead)
        val requestBranchLogEntry = requestRepo.branchLogIO.readFromLog(requestRepo.log.getHead()!!.message.data).head
        assertEquals(pulledTip, requestBranchLogEntry.hash)

        // make another remote
        testRemoteRepo.putBlob("testFile2", "Hello World 2")
        testRemoteRepo.putBlob("sub/testFile", "Hello World 3")
        testRemoteRepo.putBlob("sub/testFile2", "Hello World 4")
        testRemoteRepo.commit("")
        remoteHead = remoteRepo.getHead()

        pulledTip = pullRequest.pull(senderPipe, pullBranch, ThreeWayMerge())
                ?: throw Exception("Remote head should not be null")
        testRemoteRepo.verify(requestRepo)

        assertEquals(pulledTip, remoteHead)
        assertEquals(pulledTip, requestRepo.branchLogIO.readFromLog(requestRepo.log.getHead()!!.message.data).head.hash)

        // pull all
        var pullAllRepo = createRepo(pullAllDir, pullBranch)
        val transaction = pullAllRepo.accessors.getRawAccessor()
        ChunkIteratorProtocol.getAllChunks(senderPipe, pullBranch, 2) {
            it.forEach { protocolChunk ->
                val putResult = transaction.putChunk(protocolChunk.chunk).await()
                assertEquals(protocolChunk.id, putResult.key)
            }
        }
        // just open with the know ref
        pullAllRepo = openRepo(pullAllDir, pullBranch, remoteRepo.getRepositoryRef())
        testRemoteRepo.verify(pullAllRepo)
    }

    @Test
    fun testPullMerge() = testAsync({ setUp() }, { tearDown() }) {
        val pullBranch = "pullBranch"
        val localDir = "LocalPullMergeTest"
        val remoteDir = "RemotePullMergeTest"

        val requestRepo = createRepo(localDir, pullBranch, false)
        val remoteRepo = createRepo(remoteDir, pullBranch)
        val transactionManager = TransactionManager()
        val handler = RequestHandler(pullBranch, transactionManager, remoteRepo.branchBackend,
                object : RequestHandler.BranchLogGetter {
                    override fun get(branch: String): BranchLog? {
                        return if (pullBranch == branch)
                            remoteRepo.log
                        else
                            null
                    }
                })
        val senderPipe = connect(handler)

        // change the remote repo
        val testRemoteRepo = TestRepository(remoteRepo)
        testRemoteRepo.putBlob("testFile", "Hello World")
        testRemoteRepo.commit("")
        var remoteHead = remoteRepo.getHead()

        val pullRequest = PullRequest(requestRepo, null)
        var pulledTip = pullRequest.pull(senderPipe, pullBranch, ThreeWayMerge())
                ?: throw Exception("Remote head should not be null")
        testRemoteRepo.verify(requestRepo)

        assertEquals(pulledTip, remoteHead)
        val requestBranchLogEntry = requestRepo.branchLogIO.readFromLog(requestRepo.log.getHead()!!.message.data).head
        assertEquals(pulledTip, requestBranchLogEntry.hash)

        // make another remote
        testRemoteRepo.putBlob("testFile2", "Hello World 2")
        testRemoteRepo.putBlob("sub/testFile", "Hello World 3")
        testRemoteRepo.putBlob("sub/testFile2", "Hello World 4")
        testRemoteRepo.commit("")
        remoteHead = remoteRepo.getHead()

        pulledTip = pullRequest.pull(senderPipe, pullBranch, ThreeWayMerge())
                ?: throw Exception("Remote head should not be null")
        testRemoteRepo.verify(requestRepo)

        assertEquals(pulledTip, remoteHead)
        assertEquals(pulledTip, requestRepo.branchLogIO.readFromLog(requestRepo.log.getHead()!!.message.data).head.hash)

        // add something to the local repo and pull; expect a fast forward merge
        requestRepo.putBytes("locallyAdded", "data".toUTF())
        requestRepo.commit("Local commit".toUTF(), null)

        (requestRepo.probe("locallyAdded") != IODatabase.FileType.NOT_EXISTING).let { assertTrue { it } }

        pullRequest.pull(senderPipe, pullBranch, ThreeWayMerge()) ?: throw Exception("Remote head should not be null")

        (requestRepo.probe("locallyAdded") != IODatabase.FileType.NOT_EXISTING).let { assertTrue { it } }

        assertTrue { requestRepo.getHeadCommit()!!.parents.size == 1 }

        // remove locally and add on the remote site + multiple commits on both sites
        requestRepo.remove("locallyAdded")
        requestRepo.commit("Local commit".toUTF(), null)
        requestRepo.putBytes("locallyAdded2", "data".toUTF())
        requestRepo.commit("Local commit".toUTF(), null)
        testRemoteRepo.putBlob("remoteAdded", "Hello World 5")
        testRemoteRepo.commit("remoteAdded")
        testRemoteRepo.putBlob("remoteAdded2", "Hello World 5")
        testRemoteRepo.commit("remoteAdded")
        testRemoteRepo.remove("testFile")
        testRemoteRepo.commit("remoteRemoved")
        pullRequest.pull(senderPipe, pullBranch, ThreeWayMerge()) ?: throw Exception("Remote head should not be null")

        (requestRepo.probe("locallyAdded") == IODatabase.FileType.NOT_EXISTING).let {
            assertTrue(it) }
        (requestRepo.probe("locallyAdded2") != IODatabase.FileType.NOT_EXISTING).let {
            assertTrue(it) }
        (requestRepo.probe("remoteAdded") != IODatabase.FileType.NOT_EXISTING).let {
            assertTrue(it) }
        (requestRepo.probe("remoteAdded2") != IODatabase.FileType.NOT_EXISTING).let {
            assertTrue(it) }
        (requestRepo.probe("testFile") == IODatabase.FileType.NOT_EXISTING).let {
            assertTrue(it) }

        // create cyclic commit (challenge the merge process)
        val remoteRepoBranch1 = Repository.open(pullBranch, remoteRepo.getRepositoryRef(),
                remoteRepo.branchBackend, remoteRepo.crypto)
        remoteRepoBranch1.putBytes("branch1", "branch1".toUTF())
        remoteRepoBranch1.commit("branch1".toUTF(), null)
        remoteRepo.putBytes("branch2", "branch2".toUTF())
        remoteRepo.commit("branch2".toUTF(), null)
        println("merge")
        remoteRepo.merge(listOf(remoteRepoBranch1), ThreeWayMerge())
        remoteRepo.commit("merge".toUTF(), null)

        println("remote:")
        println(remoteRepo.printHistory())
        println("local:")
        println(requestRepo.printHistory())

        pullRequest.pull(senderPipe, pullBranch, ThreeWayMerge()) ?: throw Exception("Remote head should not be null")
        (requestRepo.probe("branch1") != IODatabase.FileType.NOT_EXISTING).let {
            assertTrue(it) }
        (requestRepo.probe("branch2") != IODatabase.FileType.NOT_EXISTING).let {
            assertTrue(it) }

    }

    @Test
    fun testPush() = testAsync({ setUp() }, { tearDown() }) {
        val branch = "pushBranch"
        val localDir = "LocalPushTest"
        val remoteDir = "RemotePushTest"

        val localRepo = createRepo(localDir, branch)
        var remoteRepo = createRepo(remoteDir, branch)

        val transactionManager = TransactionManager()
        val handler = RequestHandler(branch, transactionManager, remoteRepo.branchBackend,
                object : RequestHandler.BranchLogGetter {
                    override fun get(branch: String): BranchLog? {
                        return remoteRepo.log
                    }
                })
        val senderPipe = connect(handler)

        val testLocalRepo = TestRepository(localRepo)
        // change the local repo
        testLocalRepo.putBlob("testFile", "Hello World!")
        testLocalRepo.commit("Initial commit")

        val pushRequest = PushRequest(localRepo)
        // push changes
        pushRequest.push(senderPipe, localRepo.accessors, branch)
        // verify
        var remoteHeadRef = remoteRepo.branchLogIO.readFromLog(remoteRepo.log.getHead()!!.message.data)
        remoteRepo = openRepo(remoteDir, branch, remoteHeadRef)
        testLocalRepo.verify(remoteRepo)

        // add more
        testLocalRepo.putBlob("testFile2", "Hello World 2")
        testLocalRepo.putBlob("sub/testFile3", "Hello World 3")
        testLocalRepo.putBlob("sub/sub2/testFile4", "Hello World 4")
        testLocalRepo.commit("Commit 1")

        testLocalRepo.putBlob("sub/sub2/testFile4", "Hello World 5")
        testLocalRepo.commit("Commit 2")

        // push changes
        pushRequest.push(senderPipe, localRepo.accessors, branch)
        // verify
        remoteHeadRef = remoteRepo.branchLogIO.readFromLog(remoteRepo.log.getHead()!!.message.data)
        remoteRepo = openRepo(remoteDir, branch, remoteHeadRef)

        testLocalRepo.verify(remoteRepo)
    }

    @Test
    fun testFastForward() = testAsync({ setUp() }, { tearDown() }) {
        val branch = "testFastForwardBranch"
        val localDir = "LocalFFTest"
        val remoteDir = "RemoteFFTest"

        val localRepo = createRepo(localDir, branch)
        val remoteRepo = createRepo(remoteDir, branch)

        val transactionManager = TransactionManager()
        val handler = RequestHandler(branch, transactionManager, remoteRepo.branchBackend,
                object : RequestHandler.BranchLogGetter {
                    override fun get(branch: String): BranchLog? {
                        return remoteRepo.log
                    }
                })
        val senderPipe = connect(handler)

        remoteRepo.putBytes("test", "test".toUTF())
        remoteRepo.commit("commit".toUTF(), null)

        val newHead = PullRequest(localRepo, null).pull(senderPipe, branch, ThreeWayMerge())
        assertEquals(remoteRepo.getHead()!!.value, newHead!!.value)

        // test to open the pulled repo
        val openedRepo = openRepo(localDir, branch, localRepo.getRepositoryRef())
        assertTrue(openedRepo.getHeadCommit()!!.parents.isEmpty())
    }
}

