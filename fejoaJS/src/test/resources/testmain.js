function init() {
    ZstdCodec.run(zstd => {
        window.zstdCodecSimple = new zstd.Simple();
        window.__karma__.start();
    });
}

requirejs.config({
    // path where all main libs are located, e.g. kotlin.js
    baseUrl: '/base/fejoaJS/build/node_modules',

    paths: { // paths are relative to this file
        'big-integer': '../../../jsbindings/node_modules/big-integer/BigInteger.min',
        'pako': '../../../jsbindings/node_modules/pako/dist/pako.min',
        'zstd-codec': '../../../jsbindings/node_modules/zstd-codec/dist/bundle',

        'fejoaJS': '../../build/classes/kotlin/main/fejoaJS',
        'fejoaJS_test': '../../build/classes/kotlin/test/fejoaJS_test',
    },

    deps: ['big-integer', 'pako', 'zstd-codec', 'fejoaJS_test'],

    // start tests when done
    callback: init
});
