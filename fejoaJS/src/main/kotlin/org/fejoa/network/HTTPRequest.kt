package org.fejoa.network

import org.fejoa.support.*
import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.Int8Array
import org.w3c.xhr.ARRAYBUFFER
import org.w3c.xhr.XMLHttpRequest
import org.w3c.xhr.XMLHttpRequestResponseType
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


actual fun platformCreateHTTPRequest(url: String): RemoteRequest {
    return HTTPRequest(url)
}

class HTTPRequest(val url: String) : RemoteRequest {
    private var connection: XMLHttpRequest? = null

    private class Reply(val header: String, val dataInStream: AsyncInStream) : RemoteRequest.Reply {
        override suspend fun receiveHeader(): String {
            return header
        }

        override suspend fun inStream(): AsyncInStream {
            return dataInStream
        }

        override fun close() {
        }

    }

    private class Sender(val connection: XMLHttpRequest, private val outStream: AsyncByteArrayOutStream,
                         val outgoingData: Boolean)
        : RemoteRequest.DataSender {

        override fun outStream(): AsyncOutStream {
            if (!outgoingData)
                throw Exception("Configured without outgoing data")
            return outStream
        }

        private fun parseHeader(inputStream: InStream): String {
            val outStream = ByteArrayOutStream()
            while (true) {
                val byte = inputStream.read()
                if (byte <= 0)
                    break
                outStream.writeByte(byte)
            }
            return outStream.toByteArray().toUTFString()
        }

        override suspend fun send(): RemoteRequest.Reply {
            connection.responseType = XMLHttpRequestResponseType.ARRAYBUFFER
            connection.send(outStream.toByteArray())

            suspendCoroutine<Unit> { cont ->
                connection.onload = { cont.resume(Unit) }
                connection.onabort = { e ->
                    cont.resumeWithException(Exception("Connection aborted: ${e.target.asDynamic().status}"))
                }
                connection.onerror = { e ->
                    cont.resumeWithException(Exception("Connection failed: ${e.target.asDynamic().status}"))
                }
                connection.ontimeout = { e ->
                    cont.resumeWithException(Exception("Connection timeout: ${e.target.asDynamic().status}"))
                }
            }

            val response = Int8Array(connection.response.unsafeCast<ArrayBuffer>()).unsafeCast<ByteArray>()
            val inStream = ByteArrayInStream(response)
            val header = parseHeader(inStream)

            println("RECEIVED: $header")
            return Reply(header, inStream.toAsyncInputStream())
        }
    }

    override suspend fun send(header: String): RemoteRequest.Reply {
        return sendData(header, false).send()
    }

    override suspend fun sendData(header: String): RemoteRequest.DataSender {
        return sendData(header, true)
    }

    private suspend fun sendData(header: String, outgoingData: Boolean): RemoteRequest.DataSender {
        cancel()

        val connection = XMLHttpRequest()
        connection.open("POST", url)
        connection.withCredentials = true

        val outStream = AsyncByteArrayOutStream()
        outStream.write(header.toUTF())
        outStream.write(0)

        println("SEND: $header")
        this@HTTPRequest.connection = connection
        return Sender(connection, outStream, outgoingData)
    }

    override fun cancel() {
        connection?.abort()
        connection = null
    }
}
