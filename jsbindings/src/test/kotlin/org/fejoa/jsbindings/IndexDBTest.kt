package org.fejoa.jsbindings

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.promise
import kotlin.browser.window
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlin.js.Promise
import kotlin.js.json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue


class IndexDBTest {
    @Test
    fun testChunkStore(): Promise<Unit> = GlobalScope.promise {
        val dbName = "TestDatabase"
        val objectStoreName = "ChunkStore"

        val indexDB = window.indexDB()
        val openRequest = indexDB.open(dbName, 1)

        openRequest.onupgradeneeded = {
            var db = openRequest.result
            db.createObjectStore(objectStoreName, json("keyPath" to "key"))
        }

        val db = openRequest.await()
        val transaction = db.transaction(objectStoreName, "readwrite")
        val chunkStore = transaction.objectStore(objectStoreName)

        val key = ByteArray(5, {1})
        val value = ByteArray(10, {2})
        val key2 = ByteArray(5, {2})
        val value2 = ByteArray(10, {3})

        var result: dynamic = null

        chunkStore.put(json("key" to key, "value" to value)).then {
            chunkStore.put(json("key" to key2, "value" to value2))
        }.then {
            chunkStore.get(key)
        }.then {
            result = it
            chunkStore.put(json("key" to ByteArray(12, {4}), "value" to value2))
        }.await()

        assertTrue { value contentEquals result!!.value.unsafeCast<ByteArray>() }
    }

    @Test
    fun testIndex(): Promise<Unit> = GlobalScope.promise {
        val dbName = "TestIndexDatabase"
        val indexName = "TestIndex"
        val objectStoreName = "IndexObjectStore"

        val indexDB = window.indexDB()
        val openRequest = indexDB.open(dbName, 1)

        openRequest.onupgradeneeded = {
            val db = openRequest.result
            val store = db.createObjectStore(objectStoreName, json("keyPath" to "time"))
            store.createIndex(indexName, "time")
        }

        val db = openRequest.await()
        val transaction = db.transaction(objectStoreName, "readwrite")
        val store = transaction.objectStore(objectStoreName)
        val index = store.index(indexName)

        store.put(json("time" to 1, "value" to "time1"))
        store.put(json("time" to 2, "value" to "time2"))

        // get newest item
        val result = index.openCursor(null, "prev").await()!!
        assertEquals(2, result.key)
        assertEquals("time2", result.value.value)
    }

    @Test
    fun testListings() = GlobalScope.promise {
        val dbName = "TestListingDatabase"
        val objectStoreName = "Log"

        val indexDB = window.indexDB()
        val openRequest = indexDB.open(dbName, 1)

        openRequest.onupgradeneeded = {
            val db = openRequest.result
            db.createObjectStore(objectStoreName, json("keyPath" to "time"))
        }

        val db = openRequest.await()
        val writeTransaction = db.transaction(objectStoreName, "readwrite")
        val logOut = writeTransaction.objectStore(objectStoreName)

        val list = listOf("test4", "test3", "test2", "test1")
        logOut.put(json("time" to "1", "value" to "test1"))
        logOut.put(json("time" to "4", "value"  to "test4"))
        logOut.put(json("time" to "2", "value"  to "test2"))
        logOut.put(json("time" to "3", "value"  to "test3")).await()

        val readTransaction = db.transaction(objectStoreName, "readonly")
        val logIn = readTransaction.objectStore(objectStoreName)
        val eventRequest = logIn.openCursor(null, "prev")

        val output: MutableList<String> = ArrayList()
        eventRequest.onsuccess = { event ->
            event.target.result.unsafeCast<IDBCursor?>()?.let {
                output.add(it.value["value"].unsafeCast<String>())
                it.advance(1)
            }
        }
        suspendCoroutine<Unit> { continuation ->
            readTransaction.oncomplete = {
                continuation.resume(Unit)
            }
        }

        assertEquals(list.size, output.size)
        list.forEachIndexed { index, value ->
            assertTrue { value == output[index] }
        }

        // update only if the last key is "4"
        val updateTransaction = db.transaction(objectStoreName, "readwrite")
        val logUpdate = updateTransaction.objectStore(objectStoreName)
        val lastItemRequest = logUpdate.openCursor(null, "prev")

        lastItemRequest.then {
            val key = it!!.key.unsafeCast<String>()
            if (key != "4")
                throw Exception("Unexpected key")
            else
                logUpdate.put(json("time" to "5", "value" to "test5"))
        }
        // we have to await the transaction instead of the put request (which doesn't fires)...
        updateTransaction.await()

        val lastEntry = db.transaction(objectStoreName, "readonly")
                .objectStore(objectStoreName)
                .openCursor(null, "prev").await()!!
                .value["value"].unsafeCast<String>()
        assertEquals("test5", lastEntry)

    }

    @Test
    fun testFailure() = GlobalScope.promise {
        val dbName = "TestFailureDatabase"
        val objectStoreName = "Log"

        val indexDB = window.indexDB()
        val openRequest = indexDB.open(dbName, 1)

        openRequest.onupgradeneeded = {
            val db = openRequest.result
            db.createObjectStore(objectStoreName, json("keyPath" to "time"))
        }
        val db = openRequest.await()

        val value = db.transaction(objectStoreName, "readonly").objectStore(objectStoreName).get("9").await()
        assertEquals(value, undefined)

        val value2 = db.transaction(objectStoreName, "readonly").objectStore(objectStoreName).openCursor(null, "prev").await()
        assertEquals(value2, null)

    }
}