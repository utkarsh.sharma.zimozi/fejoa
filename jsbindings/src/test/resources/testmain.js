function init() {
    ZstdCodec.run(zstd => {
        window.zstdCodecSimple = new zstd.Simple();
        window.__karma__.start();
    });
}

requirejs.config({
    // path where all main libs are located, e.g. kotlin.js
    baseUrl: '/base/jsbindings/build/node_modules',

    paths: { // paths are relative to this file
        'big-integer': '../../node_modules/big-integer/BigInteger.min',
        'pako': '../../node_modules/pako/dist/pako.min',
        'zstd-codec': '../../node_modules/zstd-codec/dist/bundle',

        'jsbindings': '../../build/classes/kotlin/main/jsbindings',
        'jsbindings_test': '../../build/classes/kotlin/test/jsbindings_test',
    },

    deps: ['big-integer', 'pako', 'zstd-codec', 'jsbindings_test'],

    // start tests when done
    callback: init
});