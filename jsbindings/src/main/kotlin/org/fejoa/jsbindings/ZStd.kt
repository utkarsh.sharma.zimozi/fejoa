package com.github.yoshihitoh.zstdcodec

import org.khronos.webgl.Uint8Array


fun zstdSimpleCompress(data: Uint8Array, level: Int = 3): Uint8Array {
    return js("window.zstdCodecSimple.compress(data, level)").unsafeCast<Uint8Array>()
}

fun zstdSimpleDecompress(data: Uint8Array): Uint8Array {
    return js("window.zstdCodecSimple.decompress(data)").unsafeCast<Uint8Array>()
}

