package org.fejoa.jsbindings

import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.Uint8Array
import org.w3c.dom.Window
import org.w3c.dom.get
import kotlin.js.Json
import kotlin.js.Promise


external interface CryptoKey

external interface CryptoKeyPair: CryptoKey {
    val publicKey: CryptoKey
    val privateKey: CryptoKey
}

external interface SubtleCrypto {
    fun digest(algo: String, buffer: ByteArray): Promise<ArrayBuffer>
    fun generateKey(algo: Json, extractable: Boolean, keyUsages: Array<String>): Promise<CryptoKey>

    fun deriveKey(algo: Json, masterKey: CryptoKey, derivedKeyAlgo: Json, extractable: Boolean,
                  keyUsages: Array<String>): Promise<CryptoKey>

    fun encrypt(algorithm: Json, key: CryptoKey, data: ByteArray): Promise<ArrayBuffer>
    fun decrypt(algorithm: Json, key: CryptoKey, data: ByteArray): Promise<ArrayBuffer>

    fun sign(algorithm: Json, key: CryptoKey, data: ByteArray): Promise<ArrayBuffer>
    fun verify(algorithm: Json, key: CryptoKey, signature: ByteArray, data: ByteArray): Promise<Boolean>

    fun importKey(format: String, keyData: ByteArray, algo: Json, extractable: Boolean,
                  usages: Array<String>): Promise<CryptoKey>
    fun exportKey(format: String, key: CryptoKey): Promise<ArrayBuffer>
}

external interface Crypto {
    val subtle: SubtleCrypto
    fun getRandomValues(typedArray: Any)
}

external val crypto: Crypto
