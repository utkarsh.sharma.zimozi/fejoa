package org.fejoa.jsbindings

import org.w3c.dom.Window
import org.w3c.dom.get
import kotlin.js.Json


external interface DOMStringList {
    val length: Int
    fun item(index: Int): String
    fun contains(item: String): Boolean
}
operator fun DOMStringList.get(name: String): String? = asDynamic()[name]

external interface DOMException {
    val code: Int
    val message: String
    val name: String
}


external interface IDBRequest<out T> {
    val result: T
    val transaction: IDBTransaction?
    val error: DOMException
    var onsuccess: (event: Event) -> Unit
    var onerror: () -> Unit
}

external interface Event {
    val target: dynamic
    val type: String
    val bubbles: Boolean
    val cancelable: Boolean
}

external interface IDBVersionChangeEvent {
    val oldVersion: Long
    val newVersion: Long

    val target: IDBRequest<Unit>
}

external interface IDBOpenDBRequest : IDBRequest<IDBDatabase> {
    var onblocked: () -> Unit
    var onupgradeneeded: (event: IDBVersionChangeEvent) -> Unit
}

external interface IDBCursor {
    val key: dynamic
    val value: dynamic

    fun advance(count: Int)
}

external interface IDBKeyRange

external interface IDBIndex {
    fun openCursor(range: IDBKeyRange?, direction: String): IDBRequest<IDBCursor?>
}

external interface IDBTransaction {
    val error: DOMException

    val objectStoreNames: DOMStringList
    fun objectStore(name: String): IDBObjectStore
    fun abort()

    var oncomplete: () -> Unit
    var onerror: () -> Unit
    var onabort: () -> Unit
}

external interface IDBObjectStore {
    val name: String
    val indexNames: DOMStringList
    val keyPath: String?
    val transaction: IDBTransaction
    val autoIncrement: Boolean

    fun createIndex(indexName: String, keyPath: String): IDBIndex
    fun index(name: String): IDBIndex

    fun openCursor(): IDBRequest<IDBCursor?>
    fun openCursor(range: IDBKeyRange?, direction: String): IDBRequest<IDBCursor?>

    fun put(item: Any): IDBRequest<Unit>
    fun put(item: Any, key: String): IDBRequest<Unit>

    fun get(key: Any): IDBRequest<dynamic>

    fun count(key: Any): IDBRequest<Int>

    fun delete(key: Any): IDBRequest<dynamic>
}

external interface IDBDatabase {
    val name: String
    val version: Long
    val objectStoreNames: DOMStringList

    fun createObjectStore(name: String): IDBObjectStore
    fun createObjectStore(name: String, optionalParameters: Json): IDBObjectStore

    fun transaction(storeNames: String, mode: String): IDBTransaction

    fun close()
}

external interface IDBFactory {
    fun open(name: String, version: Int): IDBOpenDBRequest
    fun deleteDatabase(name: String): IDBOpenDBRequest
}

fun Window.indexDB(): IDBFactory {
    return window["indexedDB"].unsafeCast<IDBFactory>()
}
