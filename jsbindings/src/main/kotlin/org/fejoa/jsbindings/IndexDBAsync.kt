package org.fejoa.jsbindings

import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


suspend fun <T>IDBRequest<T>.await() = suspendCoroutine<T> { cont ->
    this.onerror = {
        cont.resumeWithException(Throwable(error.message))
    }
    this.onsuccess = {
        cont.resume(result)
    }
}

suspend fun <T, R>IDBRequest<T>.then(block: (result: T) -> IDBRequest<R>) = suspendCoroutine<IDBRequest<R>> { cont ->
    this.onerror = {
        cont.resumeWithException(Throwable(error.message))
    }
    this.onsuccess = {
        try {
            val next = block.invoke(result)
            cont.resume(next)
        } catch (e: Throwable) {
            cont.resumeWithException(e)
        }
    }
}

suspend fun IDBTransaction.await() = suspendCoroutine<Unit> { cont ->
    this.onerror = {
        cont.resumeWithException(Throwable(error.message))
    }
    this.oncomplete = {
        cont.resume(Unit)
    }
    this.onabort = {
        cont.resume(Unit)
    }
}
