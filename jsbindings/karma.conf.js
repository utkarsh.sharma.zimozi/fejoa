module.exports = function (config) {
    config.set({
            frameworks: ['mocha', 'requirejs'],
            reporters: ['mocha'],
            basePath: '../',
            files: [
                { pattern: "jsbindings/node_modules/big-integer/BigInteger.min.js", included: false},
                { pattern: "jsbindings/node_modules/pako/dist/pako.min.js", included: false},
                { pattern: "jsbindings/node_modules/zstd-codec/dist/bundle.js", included: false},

                "jsbindings/src/test/resources/testmain.js",

                { pattern: 'jsbindings/build/node_modules/*.js', included: false},
                { pattern: 'jsbindings/build/classes/kotlin/main/*.js', included: false},
                { pattern: 'jsbindings/build/classes/kotlin/test/*.js', included: false},
            ],
            exclude: [],
            colors: true,
            autoWatch: false,
            browsers: [
                //'PhantomJS'
                //'Chrome'
                "Firefox"
            ],
            singleRun: true,
        }
    )
};