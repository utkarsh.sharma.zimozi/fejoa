package org.fejoa.storage

import org.fejoa.crypto.CryptoHelper
import org.fejoa.repository.ChunkStoreBranchLog
import org.fejoa.support.Future
import org.fejoa.support.async
import org.mapdb.*
import java.io.File


class MapDBStorageBackend(baseDir: String) : BaseStorageBackend(baseDir) {
    class MapDBIterator(val iterator: MutableIterator<MutableMap.MutableEntry<ByteArray, ByteArray>>)
        : ChunkTransaction.Iterator {
        override suspend fun hasNext(): Boolean {
            return iterator.hasNext()
        }

        override suspend fun next(): Pair<HashValue, ByteArray> {
            val next = iterator.next()
            return HashValue(next.key) to next.value
        }
    }

    class MapDBChunkTransaction(val db: DB, val map: HTreeMap<ByteArray, ByteArray>) : ChunkTransaction {
        override fun finishTransaction(): Future<Unit> {
            db.commit()
            return Future.completedFuture(Unit)
        }

        override fun cancel(): Future<Unit> {
            db.rollback()
            return Future.completedFuture(Unit)
        }

        override fun getChunk(boxHash: HashValue): Future<ByteArray> {
            try {
                val value = map[boxHash.bytes] ?: return Future.failedFuture("Failed to read chunk: $boxHash")
                return Future.completedFuture(value)
            } catch (e: DBException) {
                return Future.failedFuture("Failed to read chunk: $boxHash")
            }
        }

        override fun prepareChunk(data: ByteArray): Future<Pair<HashValue, ByteArray>> = async {
            val key = HashValue(CryptoHelper.sha256Hash(data))
            key to data
        }

        override fun putChunk(key: HashValue, rawData: ByteArray): Future<Boolean> = async {
            val added = map.putIfAbsentBoolean(key.bytes, rawData)
            !added
        }

        override fun putChunk(data: ByteArray): Future<PutResult<HashValue>> = async {
            val key = CryptoHelper.sha256Hash(data)
            val added = map.putIfAbsentBoolean(key, data)
            PutResult(HashValue(key), !added)
        }

        override fun hasChunk(boxHash: HashValue): Future<Boolean> {
            return Future.completedFuture(map.contains(boxHash.bytes))
        }

        override fun iterator(): ChunkTransaction.Iterator {
            return MapDBIterator(map.iterator())
        }

        override fun flush(): Future<Unit> {
            return Future.completedFuture(Unit)
        }
    }

    class MapDBBranchBackend(val db: DB, val map: HTreeMap<ByteArray, ByteArray>, val baseDir: String,
                             val namespace: String, val branch: String)
        : StorageBackend.BranchBackend {
        override fun getChunkStorage(): ChunkStorage {
            return object : ChunkStorage {
                override fun startTransaction(): ChunkTransaction {
                    return MapDBChunkTransaction(db, map)
                }
            }
        }

        override fun getBranchLog(): BranchLog {
            val branchDir = getBranchDir(baseDir, namespace, branch)
            return ChunkStoreBranchLog(branchDir, branch)
        }
    }

    override suspend fun open(namespace: String, branch: String): StorageBackend.BranchBackend {
        if (!exists(namespace, branch))
            throw Exception("Does not exist")

        return create(namespace, branch)
    }

    object DBBucket {
        private val bucket: MutableMap<String, DB> = HashMap()

        fun get(fileName: String): DB = synchronized(this) {
            return@synchronized bucket[fileName] ?: run {
                DBMaker.fileDB(fileName)
                        .closeOnJvmShutdown()
                        .transactionEnable()
                        .make()
            }.also { bucket[fileName] = it }
        }
    }

    override suspend fun create(namespace: String, branch: String): StorageBackend.BranchBackend {
        val path = File(getBranchDir(namespace, branch).path)
        path.mkdirs()
        val dbFile = File(path, "db.mapDB")
        val db = DBBucket.get(dbFile.absolutePath)
        val map = db.hashMap(branch, Serializer.BYTE_ARRAY, Serializer.BYTE_ARRAY)
                .createOrOpen()
        return MapDBBranchBackend(db, map, baseDir, namespace, branch)
    }

    override suspend fun exists(namespace: String, branch: String): Boolean {
        return getBranchDir(namespace, branch).exists()
    }
}