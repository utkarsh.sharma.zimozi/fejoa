package org.fejoa.storage

import kotlinx.coroutines.*
import org.fejoa.crypto.CryptoHelper
import org.fejoa.repository.ChunkStoreBranchLog
import org.fejoa.support.Future
import org.fejoa.support.await
import java.io.File
import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock
import kotlin.coroutines.CoroutineContext


class SqliteStorageBackend(baseDir: String) : BaseStorageBackend(baseDir) {
    class ThreadChunkTransaction(val parent: SqliteChunkTransaction, private val context: CoroutineContext)
        : ChunkTransaction {
        fun <T> Deferred<T>.toFuture(): Future<T> {
            val future = Future<T>()
            this.invokeOnCompletion {
                if (it != null)
                    future.setError(it)
                else
                    future.setResult(this.getCompleted())
            }
            return future
        }

        private fun <T>onThread(block: suspend () -> T) = CoroutineScope(context).async {
            block.invoke()
        }

        private var writeQueue: MutableMap<HashValue, ByteArray> = HashMap()
        private val flushingChunks: MutableList<Map<HashValue, ByteArray>> = ArrayList()
        private var isFlushing = false
        private var triggerJob: Deferred<Unit>? = null
        private var lock = ReentrantLock()

        private suspend fun work() {
            val chunks = lock.withLock {
                val current = writeQueue
                writeQueue = HashMap()
                flushingChunks += current
                current
            }
            for (entry in chunks)
                parent.putChunk(entry.key, entry.value).await()

            lock.withLock {
                flushingChunks.remove(chunks)
            }
        }

        private fun getDirtyChunk(key: HashValue): ByteArray? = lock.withLock {
            writeQueue[key]?.let {
                return it
            }
            for (flushing in flushingChunks) {
                flushing[key]?.let {
                    return it
                }
            }
            return null
        }

        private fun setTriggerJob(job: Deferred<Unit>?) = lock.withLock {
            triggerJob = job
        }

        private fun triggerWork() {
            if (isFlushing)
                return
            lock.withLock {
                if (triggerJob != null)
                    return

                triggerJob = onThread {
                    work()
                }
                triggerJob!!.invokeOnCompletion {
                    setTriggerJob(null)
                    triggerWork()
                }
            }
        }

        override fun flush(): Future<Unit> = onThread {
            flushInternal()
            parent.flush().await()
        }.toFuture()

        private suspend fun flushInternal() {
            isFlushing = true

            work()

            lock.withLock {
                triggerJob?.await()
            }
            isFlushing = false
        }

        override fun finishTransaction(): Future<Unit> = onThread {
            flushInternal()
            parent.finishTransaction().await()
        }.toFuture()

        override fun cancel(): Future<Unit> = onThread {
            lock.withLock {
                writeQueue.clear()
            }

            parent.cancel().await()
        }.toFuture()

        override fun getChunk(boxHash: HashValue): Future<ByteArray> {
            getDirtyChunk(boxHash)?.let {
                return Future.completedFuture(it)
            }
            return onThread {
                parent.getChunk(boxHash).await()
            }.toFuture()
        }

        override fun prepareChunk(data: ByteArray): Future<Pair<HashValue, ByteArray>> {
            return parent.prepareChunk(data)
        }

        override fun putChunk(key: HashValue, rawData: ByteArray): Future<Boolean> {
            lock.withLock {
                writeQueue[key] = rawData
                triggerWork()
            }

            // TODO wasInDatabase is broken:
            return Future.completedFuture(false)
        }

        override fun hasChunk(boxHash: HashValue): Future<Boolean> {
            getDirtyChunk(boxHash)?.let {
                return Future.completedFuture(true)
            }
            return onThread {
                parent.hasChunk(boxHash).await()
            }.toFuture()
        }

        override fun iterator(): ChunkTransaction.Iterator = runBlocking {
            onThread {
                flushInternal()
                parent.iterator()
            }.await()
        }
    }

    companion object {
        const val TABLE_NAME = "chunkstore"
        const val KEY = "key"
        const val VALUE = "value"
    }

    class SqliteIterator(val resultSet: ResultSet)
        : ChunkTransaction.Iterator {
        private var hasNext = resultSet.next()

        override suspend fun hasNext(): Boolean {
            return hasNext
        }

        override suspend fun next(): Pair<HashValue, ByteArray> {
            val key = resultSet.getBytes(KEY)
            val value = resultSet.getBytes(VALUE)
            hasNext = resultSet.next()
            return HashValue(key) to value
        }
    }

    class SqliteChunkTransaction(val db: Connection) : ChunkTransaction {
        override fun flush(): Future<Unit> {
            return Future.completedFuture(Unit)
        }

        override fun finishTransaction(): Future<Unit> {
            db.commit()
            return Future.completedFuture(Unit)
        }

        override fun cancel(): Future<Unit> {
            db.rollback()
            return Future.completedFuture(Unit)
        }

        override fun getChunk(boxHash: HashValue): Future<ByteArray> {
            val statement = db.prepareStatement("SELECT value FROM $TABLE_NAME WHERE $KEY = ?;")
            try {
                statement.setBytes(1, boxHash.bytes)
                val result = statement.executeQuery()
                if (!result.next())
                    return Future.failedFuture("Failed to read chunk: $boxHash")
                return Future.completedFuture(result.getBytes(VALUE))
            } finally {
                statement.close()
            }
        }

        override fun prepareChunk(data: ByteArray): Future<Pair<HashValue, ByteArray>> = org.fejoa.support.async {
            val key = CryptoHelper.sha256Hash(data)
            return@async HashValue(key) to data
        }

        override fun putChunk(key: HashValue, rawData: ByteArray): Future<Boolean> {
            val statement = db.prepareStatement("INSERT OR IGNORE INTO $TABLE_NAME VALUES(?, ?);")
            try {
                statement.setBytes(1, key.bytes)
                statement.setBytes(2, rawData)
                val changes = statement.executeUpdate()
                val wasInDatabase = changes == 0
                return Future.completedFuture(wasInDatabase)
            } finally {
                statement.close()
            }
        }

        override fun hasChunk(boxHash: HashValue): Future<Boolean> {
            val statement = db.prepareStatement("SELECT $VALUE FROM $TABLE_NAME WHERE $KEY = ?;")
            try {
                statement.closeOnCompletion()
                statement.setBytes(1, boxHash.bytes)
                val result = statement.executeQuery()
                return Future.completedFuture(result.next())
            } finally {
                statement.close()
            }
        }

        override fun iterator(): ChunkTransaction.Iterator {
            val statement = db.createStatement()
            val result = statement.executeQuery("SELECT * FROM $TABLE_NAME;")
            return SqliteIterator(result)
        }
    }

    class SqliteBranchBackend(val db: Connection, val context: CoroutineContext, val baseDir: String,
                              val namespace: String, val branch: String)
        : StorageBackend.BranchBackend {
        override fun getChunkStorage(): ChunkStorage {
            return object : ChunkStorage {
                override fun startTransaction(): ChunkTransaction {
                    return ThreadChunkTransaction(SqliteChunkTransaction(db), context)
                    //return SqliteChunkTransaction(db)
                }
            }
        }

        override fun getBranchLog(): BranchLog {
            val branchDir = getBranchDir(baseDir, namespace, branch)
            return ChunkStoreBranchLog(branchDir, branch)
        }
    }

    private fun getDB(namespace: String, branch: String): Connection {
        val path = File(getBranchDir(namespace, branch).path)
        path.mkdirs()
        val dbFile = File(path, "db.sqlite")
        val db = DBBucket.get(dbFile.absolutePath)
        db.autoCommit = false
        return db
    }

    override suspend fun open(namespace: String, branch: String): StorageBackend.BranchBackend {
        if (!exists(namespace, branch))
            throw Exception("Does not exist")

        return create(namespace, branch)
    }

    object DBBucket {
        val context = newSingleThreadContext("ChunkTransaction Thread")
        private val bucket: MutableMap<String, Connection> = HashMap()

        fun get(fileName: String): Connection = synchronized(this) {
            return@synchronized bucket[fileName] ?: run {
                DriverManager.getConnection("jdbc:sqlite:$fileName")
            }.also { bucket[fileName] = it }
        }

        fun close(connection: Connection) = synchronized(this) {
            bucket.keys.forEach {
                if (bucket[it] == connection) {
                    connection.close()
                    bucket.remove(it)
                }
            }
        }
    }

    override suspend fun create(namespace: String, branch: String): StorageBackend.BranchBackend {
        val db = getDB(namespace, branch)
        val statement = db.createStatement()
        statement.closeOnCompletion()
        statement.executeUpdate("CREATE TABLE IF NOT EXISTS $TABLE_NAME ($KEY blob UNIQUE NOT NULL PRIMARY KEY, " +
                "$VALUE blob NOT NULL)")

        db.commit()
        return SqliteBranchBackend(db, DBBucket.context, baseDir, namespace, branch)
    }

    override suspend fun exists(namespace: String, branch: String): Boolean {
        return getBranchDir(namespace, branch).exists()
    }
}
