package org.fejoa.storage

import kotlinx.coroutines.*
import kotlinx.serialization.context.SimpleModule
import kotlinx.serialization.json.Json
import org.fejoa.support.*
import org.lmdbjava.*
import java.io.File
import java.nio.ByteBuffer
import java.nio.ByteBuffer.allocateDirect
import java.util.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.collections.ArrayList
import kotlin.concurrent.withLock
import kotlin.coroutines.CoroutineContext


class LMDBStorageBackend(baseDir: String) : BaseStorageBackend(baseDir) {
    companion object {
        const val CHUNK_DB = "blocks"
        const val N_READERS = 1

        val writeContext = newSingleThreadContext("LMDB write io")
        val readContext = newFixedThreadPoolContext(N_READERS, "LMDB read io")
    }

    class BatchTransaction(val env: Env<ByteBuffer>): ChunkTransaction  {
        private val db = env.openDbi(CHUNK_DB, DbiFlags.MDB_CREATE)

        val writeContext = LMDBStorageBackend.writeContext
        val readContext = LMDBStorageBackend.readContext

        fun close() {
            db.close()
            env.close()
        }

        private val MAX_DELAY = 10L //[ms]
        private val MAX_QUEUE_SIZE = 1
        private val lock = ReentrantLock()
        private var writeQueue: MutableMap<HashValue, Pair<ByteArray, Future<Boolean>>> = HashMap()
        private var flushingQueue: MutableMap<HashValue, Pair<ByteArray, Future<Boolean>>>? = null
        private var timer: Deferred<Unit>? = null

        override fun putChunk(key: HashValue, value: ByteArray): Future<Boolean> {
            var needsWork = false
            val future = lock.withLock {
                // check if there is already a job for this chunk
                writeQueue[key]?.let { return it.second }
                val future = Future<Boolean>()
                writeQueue[key] = value to future
                //if (writeQueue.size == 1)
                //    scheduleWork()
                if (writeQueue.size >= MAX_QUEUE_SIZE)
                    needsWork = true
                future
            }
            if (needsWork)
                work()
            return future
        }

        override fun getChunk(boxHash: HashValue): Future<ByteArray>  {
            lock.withLock {
                writeQueue[boxHash]?.let { return Future.completedFuture(it.first) }
                flushingQueue?.get(boxHash)?.let { return Future.completedFuture(it.first) }
            }

            return Future(JVMExecutor(readContext.executor)) {
                val keyBuffer = allocateDirect(boxHash.bytes.size)
                keyBuffer.put(boxHash.bytes).flip()
                val tx = env.txnRead()
                try {
                    val result = db.get(tx, keyBuffer) ?: throw Exception("Block $boxHash not found")

                    val out = ByteArray(result.remaining())
                    result.get(out)
                    out
                } finally {
                    tx.close()
                }
            }
        }

        override fun hasChunk(boxHash: HashValue): Future<Boolean> {
            lock.withLock {
                writeQueue[boxHash]?.let { return Future.completedFuture(true) }
                flushingQueue?.get(boxHash)?.let { return Future.completedFuture(true) }
            }

            return Future(JVMExecutor(readContext.executor)) {
                val tx = env.txnRead()
                try {
                    val keyBuffer = allocateDirect(boxHash.bytes.size)
                    keyBuffer.put(boxHash.bytes).flip()
                    db.openCursor(tx).get(keyBuffer, GetOp.MDB_SET)
                } finally {
                    tx.close()
                }
            }
        }

        override fun flush(): Future<Unit> {
            return work()
        }

        private fun scheduleWork() {
            timer = GlobalScope.async {
                delay(MAX_DELAY)
                work().await()
            }
        }

        private fun work() = Future(JVMExecutor(writeContext.executor)) {
            val queue = lock.withLock {
                if (writeQueue.isEmpty()) {
                    return@Future
                }
                writeQueue.also {
                    flushingQueue = it
                    writeQueue = HashMap()
                }
            }

            val tx = env.txnWrite()
            try {
                val keyBuffer = allocateDirect(32)
                for (entry in queue) {
                    keyBuffer.flip()
                    keyBuffer.limit(32)
                    keyBuffer.put(entry.key.bytes).flip()
                    val data = entry.value.first
                    val valueBuffer = allocateDirect(data.size)
                    valueBuffer.put(data).flip()
                    val wasInDB = db.put(tx, keyBuffer, valueBuffer, PutFlags.MDB_NOOVERWRITE)
                    entry.value.second.setResult(wasInDB)
                }
                tx.commit()
            } catch(e: Throwable) {
                for (entry in queue) {
                    if (!entry.value.second.finished())
                        entry.value.second.setError(e)
                }
            } finally {
                lock.withLock {
                    flushingQueue = null
                }
                tx.close()
            }
        }

        override fun finishTransaction(): Future<Unit> {
            return flush().bind { Future.completedFuture(env.sync(true)) }
        }

        override fun cancel(): Future<Unit> {
            return Future.completedFuture(Unit)
        }

        override fun prepareChunk(data: ByteArray): Future<Pair<HashValue, ByteArray>> {
            val digest = java.security.MessageDigest.getInstance("SHA-256")
            val key = digest.digest(data)
            return Future.completedFuture(HashValue(key) to data)
        }

        override fun iterator(): ChunkTransaction.Iterator {
            return object : ChunkTransaction.Iterator {
                var tx: Txn<ByteBuffer>? = null
                var iterator: CursorIterator<ByteBuffer>? = null
                override suspend fun hasNext(): Boolean = GlobalScope.async(readContext) {
                    if (tx == null) {
                        // TODO do that before?
                        flush().await()
                        tx = env.txnRead()
                        iterator = db.iterate(tx, KeyRange.allBackward())
                    }
                    val hasNext = iterator!!.hasNext()
                    if (!hasNext)
                        iterator!!.close()
                    hasNext
                }.await()

                override suspend fun next(): Pair<HashValue, ByteArray> = GlobalScope.async(readContext)  {
                    val next = iterator!!.next()
                    val keyBuffer = next.key()
                    val valueBuffer = next.`val`()

                    val key = ByteArray(keyBuffer.remaining())
                    keyBuffer.get(key)
                    val value = ByteArray(valueBuffer.remaining())
                    valueBuffer.get(value)
                    HashValue(key) to value
                }.await()

                override fun close() {
                    iterator?.close()
                }
            }
        }
    }

    class LMDVBranchLog(val env: Env<ByteBuffer>, val writeContext: CoroutineContext, val readContext: CoroutineContext,
                        val branch: String) : BranchLog {
        private val db = env.openDbi("commits", DbiFlags.MDB_CREATE)

        val COUNTER_KEY = -1L

        private fun getCounter(tx: Txn<ByteBuffer>): Long {
            val counterKeyBuffer = allocateDirect(8)
            counterKeyBuffer.putLong(COUNTER_KEY).flip()
            val c = db.get(tx, counterKeyBuffer)
            return c?.long ?: -1L
        }

        private fun write(entry: BranchLogEntry, tx: Txn<ByteBuffer>) {
            val json = Json().apply { install(SimpleModule(HashValue::class, HashValueDataSerializer)) }
                    .stringify(BranchLogEntry.serializer(), entry).toUTF()

            val newCounter = getCounter(tx) + 1
            val key = allocateDirect(8)
            key.putLong(newCounter).flip()
            val value = allocateDirect(json.size)
            value.put(json).flip()
            db.put(tx, key, value)

            val counterKeyBuffer = allocateDirect(8)
            counterKeyBuffer.putLong(COUNTER_KEY).flip()
            val counterValue = allocateDirect(8)
            counterValue.putLong(newCounter).flip()
            db.put(tx, counterKeyBuffer, counterValue)
        }

        private fun read(index: Long, tx: Txn<ByteBuffer>): BranchLogEntry? {
            val key = allocateDirect(8)
            key.putLong(index).flip()

            val value = db.get(tx, key) ?: return null
            val text = ByteArray(value.remaining())
            value.get(text)

            return Json().apply { install(SimpleModule(HashValue::class, HashValueDataSerializer)) }
                    .parse(BranchLogEntry.serializer(), text.toUTFString())
        }

        override fun getBranchName(): String {
            return branch
        }

        override suspend fun add(id: HashValue, message: ByteArray, changes: List<HashValue>) {
            val entry = BranchLogEntry(Date().time, id, BranchLogEntry.Message(message))
            entry.changes.addAll(changes)
            add(entry)
        }

        override suspend fun add(entry: BranchLogEntry)  = GlobalScope.async(writeContext) {
            val tx = env.txnWrite()
            try {
                write(entry, tx)
                tx.commit()
            } finally {
                tx.close()
            }
        }.await()

        override suspend fun add(entries: List<BranchLogEntry>) {
            entries.reversed().forEach {
                add(it)
            }
        }

        override suspend fun add(entry: BranchLogEntry, expectedEntryId: HashValue?): Boolean = GlobalScope.async(writeContext) {
            val tx = env.txnWrite()
            try {
                val head = read(getCounter(tx), tx)
                if (head?.entryId != expectedEntryId)
                    return@async false
                write(entry, tx)
                tx.commit()
                return@async true
            } finally {
                tx.close()
            }
        }.await()

        override suspend fun getEntries(): List<BranchLogEntry> = GlobalScope.async(readContext) {
            val tx = env.txnRead()
            try {
                val out: MutableList<BranchLogEntry> = ArrayList()
                for (i in getCounter(tx) downTo 0)
                    read(i, tx)?.let { out += it }
                return@async out
            } finally {
                tx.close()
            }
        }.await()

        override suspend fun getHead(): BranchLogEntry? = GlobalScope.async(readContext) {
            val tx = env.txnRead()
            try {
                val c = getCounter(tx)
                if (c < 0)
                    return@async null
                read(c, tx)
            } finally {
                tx.close()
            }
        }.await()
    }

    class LMDBBranchBackend(val transaction: BatchTransaction, val baseDir: String,
                            namespace: String, val branch: String) : StorageBackend.BranchBackend {
        private val branchLog = LMDVBranchLog(transaction.env, transaction.writeContext, transaction.readContext, branch)

        override fun getChunkStorage(): ChunkStorage {
            return object : ChunkStorage {
                override fun startTransaction(): ChunkTransaction {
                    return transaction
                }
            }
        }

        override fun getBranchLog(): BranchLog {
            return branchLog
        }

    }

    class EnvBucket {
        private val bucket: MutableMap<String, BatchTransaction> = HashMap()

        fun get(fileName: File, nDBs: Int): BatchTransaction = synchronized(this) {
            fileName.mkdirs()
            return@synchronized bucket[fileName.path] ?: run {
                val maxSize = 100L * 1024 *1024 * 1024  // 100Gb
                val env = Env.create()
                        .setMapSize(maxSize)
                        .setMaxDbs(nDBs)
                        .setMaxReaders(10)
                        .open(fileName, EnvFlags.MDB_NOSYNC, EnvFlags.MDB_NOTLS)
                BatchTransaction(env)
            }.also { bucket[fileName.path] = it }
        }

        fun close(transaction: BatchTransaction) = synchronized(this) {
            bucket.keys.forEach {
                if (bucket[it] == transaction) {
                    transaction.env.close()
                    bucket.remove(it)
                }
            }
        }

        fun clear() {
            bucket.forEach {
                it.value.close()
            }
            bucket.clear()
        }

        protected fun finalize() {
            clear()
        }
    }

    val bucket = EnvBucket()

    override suspend fun open(namespace: String, branch: String): StorageBackend.BranchBackend {
        if (!exists(namespace, branch))
            throw Exception("Does not exist")

        return create(namespace, branch)
    }

    override suspend fun create(namespace: String, branch: String): StorageBackend.BranchBackend {
        val path = File(getBranchDir(namespace, branch).path)
        path.mkdirs()
        val dbFile = File(path, "db.lmdb")

        val transaction = bucket.get(dbFile, 2)
        return LMDBBranchBackend(transaction, baseDir, namespace, branch)
    }

    override suspend fun exists(namespace: String, branch: String): Boolean {
        return getBranchDir(namespace, branch).exists()
    }

}