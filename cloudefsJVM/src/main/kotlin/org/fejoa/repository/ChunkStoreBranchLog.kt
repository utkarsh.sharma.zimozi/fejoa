package org.fejoa.repository

import kotlinx.serialization.context.SimpleModule
import kotlinx.serialization.json.JSON
import org.fejoa.chunkstore.LockBucket
import org.fejoa.storage.*

import java.io.*
import java.util.*
import java.util.concurrent.locks.Lock


/**
 * There are two parts:
 * 1) log: contains a list of previous versions
 * 2) head: points to the latest or topmost commit
 */
class ChunkStoreBranchLog(logDir: File, val branch: String) : BranchLog {
    private data class LogFileStats(val lastModified: Long = -1, val size: Long = -1)
    private val logFile: File = File(logDir, "$branch.log")
    private var logFileStats: LogFileStats = LogFileStats()

    private val fileLock: Lock = LockBucket.getInstance().getLock(logDir.absolutePath)
    private var entries: BranchLogList? = null

    private fun lock() {
        fileLock.lock()
    }

    private fun unlock() {
        fileLock.unlock()
    }

    override fun getBranchName(): String {
        return branch
    }

    override suspend fun getEntries(): List<BranchLogEntry> {
        return getBranchLogList().entries
    }

    private fun readLogFileStats(): LogFileStats {
        return if (logFile.exists())
            LogFileStats(logFile.lastModified(), logFile.length())
        else
            LogFileStats()
    }

    private fun getBranchLogList(): BranchLogList {
        val currentStats = readLogFileStats()
        if (logFileStats != currentStats)
            entries = null
        return entries ?: readLogs().also {
            entries = it
            logFileStats = currentStats
        }
    }

    private fun readLogs(): BranchLogList {
        try {
            lock()
            // read log
            return try {
                val fileInputStream = FileInputStream(logFile)
                val reader = BufferedReader(InputStreamReader(fileInputStream))
                val text = reader.readText()
                JSON().apply { install(SimpleModule(HashValue::class, HashValueDataSerializer)) }
                        .parse(BranchLogList.serializer(), text)
            } catch (e: FileNotFoundException) {
                BranchLogList()
            } catch (e: Exception) {
                e.printStackTrace()
                throw e
            }
        } finally {
            unlock()
        }
    }

    override suspend fun getHead(): BranchLogEntry? {
        return getBranchLogList().entries.firstOrNull()
    }

    override suspend fun add(id: HashValue, message: ByteArray, changes: List<HashValue>) {
        val entry = BranchLogEntry(Date().time, id, BranchLogEntry.Message(message))
        entry.changes.addAll(changes)
        add(entry)
    }

    override suspend fun add(entry: BranchLogEntry) {
        add(listOf(entry))
    }

    override suspend fun add(entries: List<BranchLogEntry>) {
        try {
            lock()
            entries.reversed().forEach {
                getBranchLogList().entries.add(0, it)
            }
            write()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            unlock()
        }
    }

    override suspend fun add(entry: BranchLogEntry, expectedEntryId: HashValue?): Boolean {
        try {
            lock()
            val lastHead = getHead()
            if (lastHead?.entryId != expectedEntryId)
                return false

            getBranchLogList().entries.add(0, entry)
            write()
            return true
        } catch (e: IOException) {
            e.printStackTrace()
            return false
        } finally {
            unlock()
        }
    }

    @Throws(IOException::class)
    private fun write() {
        if (entries == null)
            return

        // append to log
        if (!logFile.exists()) {
            logFile.parentFile.mkdirs()
            logFile.createNewFile()
        }
        val writer = BufferedWriter(OutputStreamWriter(FileOutputStream(logFile)))
        writer.use {
            val out = JSON(indented = true).apply { install(SimpleModule(HashValue::class, HashValueDataSerializer)) }
                    .stringify(BranchLogList.serializer(), entries!!)
            it.write(out)
        }
        logFileStats = readLogFileStats()
    }
}
