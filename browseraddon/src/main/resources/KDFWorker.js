importScripts("js/libs/require.js");

requirejs.config({
    // path where all main libs are located, e.g. kotlin.js
    baseUrl: 'js',

    paths: { // paths are relative to this file
        'big-integer': 'libs/BigInteger.min',
        'pako': 'libs/pako.min',
        'zstd-codec': 'libs/bundle'
    },

    deps: ['big-integer', 'pako', 'browseraddon'],

    callback: onPackagesLoaded
});

// we most likely receive the request before the browseradddon is loaded; cache the request here
var cachedMessage = null;

onmessage = function(message) {
    cachedMessage = message;
}

function onPackagesLoaded(biginteger, pako, browseraddon) {
    var worker = new browseraddon.org.fejoa.auth.KDFWorker();
    worker.handle_za3rmp$(cachedMessage);
}
