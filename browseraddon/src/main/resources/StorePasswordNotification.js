
function StorePasswordViewModel() {
    this.messageField = document.getElementById("message");
    this.ignoreButton = document.getElementById("ignore-button");
    this.storeButton = document.getElementById("store-button");
    this.selectedAccount = null;

    this.start = function() {
        var that = this;

        chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
            if (request.method == "accountListUpdated") {
                that.update();
            }
        });

        this.ignoreButton.onclick = function() {
                    chrome.runtime.sendMessage({method: "ignoreCredentials"});
                };

        this.storeButton.onclick = function() {
                    chrome.runtime.sendMessage({method: "storeCredentials", account: that.selectedAccount});
                };

        this.update();
    };

    this.update = function() {
        var that = this;

        chrome.runtime.sendMessage({method: "getCredentialsInfo"}, function(response) {
                if (response.openAccounts.length == 0) {
                    that.messageField.innerText = "Open an account to store password";
                    that.storeButton.setAttribute("disabled", true);
                } else {
                    that.selectedAccount = response.openAccounts[0];
                    that.storeButton.removeAttribute("disabled");
                    that.messageField.innerText = "Store password in account \"" + that.selectedAccount + "\"?";
                }
            });
    }
}

(new StorePasswordViewModel()).start();
