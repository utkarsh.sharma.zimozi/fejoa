package org.fejoa.auth.passwordmanager

import kotlinx.serialization.Serializable
import org.fejoa.StorageDirObject
import org.fejoa.storage.*
import org.fejoa.support.PathUtils


class Wallet(storageDir: StorageDir): StorageDirObject(storageDir) {
    val webDir = StorageDir(storageDir, "web")

    @Serializable
    class Entry(val origin: String, val username: String, val password: String)

    fun getId(): String {
        return storageDir.branch
    }

    private fun path(origin: String, username: String): String {
        return PathUtils.toPath(origin) + "/" + PathUtils.toPath(username)
    }

    suspend fun putPassword(origin: String, username: String, password: String) {
        val dbPassword = getDBPassword(origin, username)
        dbPassword.write(password)
        commit()
    }

    suspend fun listPasswords(): List<Entry> {
        val pathPairs = webDir.listDirectories("").map {
            it to webDir.listDirectories(it)
        }
        return pathPairs.flatMap {
            val origin = PathUtils.fromPath(it.first)
            val userNames = it.second.map { PathUtils.fromPath(it) }

            userNames.mapNotNull { userName ->
                val dbPassword = getDBPassword(origin, userName)
                if (!dbPassword.exists())
                    null
                else
                    userName to dbPassword.get()
            }.map { Entry(origin, it.first, it.second) }
        }
    }

    fun getDBPassword(origin: String, username: String): DBString {
        val path = path(origin, username)
        return DBString(webDir, PathUtils.appendDir(path, "password"))
    }

    suspend fun getPassword(origin: String, username: String): Entry? {
        val password = getDBPassword(origin, username)
        if (!password.exists())
            return null
        return Entry(origin, username, password.get())
    }
}
