package org.fejoa.auth.passwordmanager

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.fejoa.*
import org.fejoa.crypto.CryptoSettings
import org.fejoa.storage.HashValue
import org.fejoa.storage.platformCreateStorage
import org.fejoa.support.Future
import org.fejoa.support.PathUtils
import org.fejoa.support.async
import org.fejoa.support.await


class PMAccountManager(val parentContext: FejoaContext, val connectionAuthManager: ConnectionAuthManager) {
    interface Listener {
        fun onAccountListUpdated()
        fun onAccountSelected(account: String?)
    }

    val accountListeners: MutableList<Listener> = ArrayList()
    private val accounts = ArrayList<AccountEntry>()
    var selectedAccountName: String? = null
        set(value) {
            field = value
            accountListeners.forEach { it.onAccountSelected(value) }
        }

    class AccountEntry(val name: String, var type: Type, var account: PMAccount? = null,
                       var future: Future<*>? = null) {
        enum class Type {
            CLOSED,
            OPEN,
            OPENING, // in the process of being opened
            CREATING, // in the process of being created
            RETRIEVING
        }
    }

    init {
        GlobalScope.launch {
            readExistingAccounts()
        }
    }

    fun dispose() {
        accounts.forEach { it.account?.dispose() }
        accounts.clear()
    }

    private suspend fun readExistingAccounts() {
        val accountsNames = parentContext.platformStorage.listNamespaces().map { PathUtils.fromPath(it) }
        // fill index entries into the account entry list
        for (name in accountsNames) {
            val existingAccount = getAccount(name)
            if (existingAccount != null)
                continue
            accounts.add(AccountEntry(name, AccountEntry.Type.CLOSED))
        }
        if (accounts.size > 0)
            notifyAccountsUpdated()
    }

    private fun notifyAccountsUpdated() {
        accountListeners.forEach { it.onAccountListUpdated() }
    }

    fun getSelectedAccount(): AccountEntry? {
        return selectedAccountName?.let { getAccount(it) }
    }

    fun getAccount(name: String): AccountEntry? {
        return accounts.firstOrNull { it.name == name}
    }

    fun getAccounts(): List<AccountEntry> {
        return accounts
    }

    fun getDefaultAccount(): AccountEntry? {
        return accounts.firstOrNull()
    }

    fun getOpenAccount(): AccountEntry? {
        return accounts.firstOrNull { it.type == AccountEntry.Type.OPEN }
    }

    private fun updateAccountEntry(name: String, type: AccountEntry.Type, account: PMAccount? = null,
                                   promise: Future<*>? = null): AccountEntry {
        var accountEntry = getAccount(name)
        if (accountEntry == null) {
            accountEntry = AccountEntry(name, type, account, promise)
            accounts.add(accountEntry)
        } else {
            accountEntry.type = type
            accountEntry.account = account
            accountEntry.future = promise
        }
        notifyAccountsUpdated()
        return accountEntry
    }

    private fun removeAccountEntryInternal(accountName: String): AccountEntry? {
        val accountEntry = getAccount(accountName) ?: return null
        accountEntry.account?.dispose()
        accounts.remove(accountEntry)
        return accountEntry
    }
    private fun removeAccountEntry(accountName: String) {
        removeAccountEntryInternal(accountName) ?: return
        notifyAccountsUpdated()
    }

    private suspend fun prepareClient(client: Client) {
        client.syncer.autoSyncing = false
        client.start()
        client.logger.children.add(object : Logger {
            override fun onEvent(category: Logger.Category, event: Logger.Event, level: Logger.LogLevel) {
                println("$category $event")
            }
        })
    }

    suspend fun createAccount(accountName: String, password: String, kdf: CryptoSettings.KDF): PMAccount {
        val name = accountName.trim()
        if (name == "")
            throw Exception("Invalid account name")
        if (getAccount(name) != null)
            throw Exception("Account already exists")

        val future = async {
            val context = FejoaContext(AccountIO.Type.CLIENT, parentContext.context, name, Dispatchers.Default)
            context.baseKeyCache = parentContext.baseKeyCache

            val client = Client.create(context.context, name, password, kdf,
                    connectionAuthManager = connectionAuthManager)
            prepareClient(client)
            client
        }
        updateAccountEntry(name, AccountEntry.Type.CREATING, null, future)
        try {
            val client = future.await()
            val account = PMAccount(client, name)
            // create one wallet
            account.createWallet()
            client.userData.commit()
            updateAccountEntry(name, AccountEntry.Type.OPEN, account)
            return account
        } catch (e: Exception) {
            removeAccountEntry(name)
            throw e
        }
    }

    suspend fun openAccount(accountName: String, password: String)
            : PMAccount? {
        val name = accountName.trim()

        val future = async {
            val context = FejoaContext(AccountIO.Type.CLIENT, parentContext.context, name, Dispatchers.Default)
            context.baseKeyCache = parentContext.baseKeyCache

            val client = Client.open(context, connectionAuthManager, password)
            prepareClient(client)
            client
        }

        updateAccountEntry(name, AccountEntry.Type.OPENING, null, future)

        return try {
            val client = future.await()
            val account = PMAccount(client, name)
            updateAccountEntry(name, AccountEntry.Type.OPEN, account)
            account
        }  catch (e: dynamic) {
            // failed to decrypt or canceled
            println(e.toString())
            updateAccountEntry(name, AccountEntry.Type.CLOSED)
            null
        }
    }

    suspend fun retrieveAccount(accountName: String, user: String, url: String)
            : PMAccount? {
        val name = accountName.trim()
        if (name == "")
            throw Exception("Invalid account name")
        if (getAccount(name) != null)
            throw Exception("Account already exists")

        val future = async {
            val client = Client.retrieveAccount(parentContext.context, name, url, user,
                    passwordGetter = connectionAuthManager.passwordGetter,
                    connectionAuthManager = connectionAuthManager)

            // sync all pm branches
            val syncJob = client.withLooper {
                userData.getBranches(PMAccount.BRANCH_CONTEXT, true).flatMap { branch ->
                    branch.locations.listRemoteRefs().map {
                        val remote = userData.remotes.get(HashValue.fromHex(it.remoteId)).get()
                        client.syncer.syncForced(branch, remote)
                    }
                }
            }
            syncJob.forEach { it.await() }

            prepareClient(client)
            client
        }

        updateAccountEntry(name, AccountEntry.Type.RETRIEVING, null, future)
        selectedAccountName = name

        return try {
            val client = future.await()
            val account = PMAccount(client, name)
            updateAccountEntry(name, AccountEntry.Type.OPEN, account)
            account
        }  catch (e: Exception) {
            // delete
            platformCreateStorage(parentContext.context).deleteNamespace(name)
            removeAccountEntry(name)
            throw e
        }
    }

    suspend fun closeAccount(accountName: String) {
        val accountEntry = getAccount(accountName) ?: return
        accountEntry.account?.close()
        updateAccountEntry(accountEntry.name, AccountEntry.Type.CLOSED)
    }

    suspend fun closeAccount(account: PMAccount) {
        closeAccount(account.name)
    }

    suspend fun deleteAccount(accountName: String) {
        removeAccountEntryInternal(accountName) ?: return
        parentContext.platformStorage.deleteNamespace(accountName)
        notifyAccountsUpdated()
    }
}


