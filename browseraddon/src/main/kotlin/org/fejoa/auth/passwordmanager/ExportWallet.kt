package org.fejoa.auth.passwordmanager

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JSON


@Serializable
class WalletEntryList(val entries: List<Wallet.Entry> = ArrayList())

suspend fun Wallet.toJson(): String {
    return JSON(indented = true).stringify(WalletEntryList.serializer(), WalletEntryList(listPasswords()))
}