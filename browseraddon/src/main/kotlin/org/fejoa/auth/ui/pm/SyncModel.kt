package org.fejoa.auth.ui.pm

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.fejoa.*


class SyncModel(val client: Client) {
    class RemoteBranchIndexStatus(val remote: String) {
        var remoteIndexStatus: SyncEvent.Status = SyncEvent.Status.SYNCING
    }

    class RemoteEntry(val remote: String) {
        val syncingBranches: MutableSet<String> = HashSet()
        // branch -> error message
        val failedSyncing: MutableMap<String, String> = HashMap()
    }

    interface Listener {
        fun onBranchUpdated(remote: String, branch: String)
        fun onBranchIndexUpdated(remote: String)
    }

    // remote id -> RemoteEntry
    val entryMap: MutableMap<String, RemoteEntry> = HashMap()
    val remoteBranchIndexStatusMap: MutableMap<String, RemoteBranchIndexStatus> = HashMap()
    val listeners: MutableList<Listener> = ArrayList()

    private fun getRemoteBranchIndexEntry(remote: String): RemoteBranchIndexStatus {
        return remoteBranchIndexStatusMap[remote] ?: RemoteBranchIndexStatus(remote).also {
            remoteBranchIndexStatusMap[remote] = it
        }
    }

    private fun getRemoteEntry(remote: String): RemoteEntry {
        return entryMap[remote] ?: RemoteEntry(remote).also { remoteEntry ->
            entryMap[remote] = remoteEntry
        }
    }

    /**
     * To be called from the looper
     */
    suspend fun startSync(remote: Remote, branch: Branch) {
        client.syncer.sync(branch, remote)
    }

    private val eventListener = object : Logger {
        override fun onEvent(category: Logger.Category, event: Logger.Event, level: Logger.LogLevel) {
            if (event.type != Logger.TYPE.SYNC_MANAGER)
                return
            val syncEvent = event.data as SyncEvent
            if (syncEvent.branchType == SyncEvent.BranchType.REMOTE_INDEX) {
                onBranchIndexEvent(syncEvent)
                return
            }

            val current = getRemoteEntry(syncEvent.remote)
            current.failedSyncing.remove(syncEvent.branch)
            return when (syncEvent.status) {
                SyncEvent.Status.TIMEOUT,
                SyncEvent.Status.WATCH_RESULT -> return
                SyncEvent.Status.SYNCING -> {
                    current.syncingBranches.add(syncEvent.branch)
                    notifyBranchUpdated(syncEvent.remote, syncEvent.branch)
                }
                SyncEvent.Status.SYNCED -> {
                    current.syncingBranches.remove(syncEvent.branch)
                    notifyBranchUpdated(syncEvent.remote, syncEvent.branch)
                }
                SyncEvent.Status.ERROR -> {
                    current.syncingBranches.remove(syncEvent.branch)
                    current.failedSyncing[syncEvent.branch] = syncEvent.error
                    notifyBranchUpdated(syncEvent.remote, syncEvent.branch)
                }
            }
        }
    }

    init {
        client.logger.children.add(eventListener)

        // get initial ongoing syncs
        client.getSyncManager().ongoingSyncs.map.forEach {
            val remoteEntry = getRemoteEntry(it.value.first.id)
            remoteEntry.syncingBranches.add(it.key)
        }
    }

    fun dispose() {
        client.logger.children.remove(eventListener)
    }

    private fun onBranchIndexEvent(syncEvent: SyncEvent) = GlobalScope.launch {
        val entry = getRemoteBranchIndexEntry(syncEvent.remote)
        entry.remoteIndexStatus = syncEvent.status
        notifyBranchIndexUpdated(syncEvent.remote)
    }

    private fun notifyBranchIndexUpdated(remote: String) {
        listeners.forEach { it.onBranchIndexUpdated(remote) }
    }

    private fun notifyBranchUpdated(remote: String, branch: String) {
        listeners.forEach { it.onBranchUpdated(remote, branch) }
    }
}