package org.fejoa.auth.ui.pm

import kotlinx.html.*


object RetrieveAccountView {
    const val pmRetrieveButtonId = "pm-retrieve-button"

    fun build(parent: DIV) = parent.apply {
        div("form-group") {
            label {
                attributes += "for" to "pm-retrieve-remote"
                +"Retrieve account from:"
            }

            div("dropdown") {
                id = "pm-retrieve-remote"
                button(classes = "btn btn-info btn-sm dropdown-toggle") {
                    id = "pm-retrieve-remote-dropdown-button"
                    attributes += "type" to "button"
                    attributes += "data-toggle" to "dropdown"
                    +"Remotes"
                    span("caret")
                }

                ul("dropdown-menu") {
                    id = "pm-remote-retrieve-dropdown"
                    li {
                        a {
                            href = "#"
                            +"Remote 1"
                        }
                    }
                }
            }

            p { +"(Only authenticated remotes are listed)" }
        }

        label("label label-primary") {
            id = "pm-retrieve-selected-account"
        }

        div("form-group") {
            label {
                attributes += "for" to "pm-retrieve-account-name"
                +"Local account name"
            }
            input(classes = "form-control") {
                id = "pm-retrieve-account-name"
                placeholder = "Account Name"
            }
        }

        button(classes = "btn btn-primary") {
            id = pmRetrieveButtonId
            +"Retrieve"
        }
    }
}
