package org.fejoa.auth.ui.pm

import kotlinx.html.*
import org.fejoa.auth.ui.buildPasswordRequestView

const val pmOpenAccountContainerId = "pm-open-account-container"
const val pmClosedAccountContainerId = "pm-closed-account-container"
const val pmBusyAccountContainerId = "pm-busy-account-container"

object AccountView {
    const val pmAccountStatusId = "pm-account-status"

    fun build(parent: DIV) = parent.apply {
        div("form-group") {
            id = "pm-account-status-bar"

            span("label label-primary") {
                id = "pm-account-view-title-selected"
            }

            span {
                id = pmAccountStatusId
                style = "padding-left: 5px;"
                +"(Status)"
            }

            // delete account
            button(classes = "btn pull-right") {
                attributes += "data-toggle" to "collapse"
                attributes += "data-target" to "#pm-delete-account-container"

                span("glyphicon glyphicon-trash") {
                    attributes += "data-toggle" to "tooltip"
                    attributes += "title" to "Delete the account"
                }
            }

            // close account
            button(classes = "btn pull-right") {
                id = "pm-close-button"
                attributes += "data-toggle" to "tooltip"
                attributes += "title" to "Close the account"

                span("glyphicon glyphicon-log-out")
            }
        }

        div("collapse") {
            id = "pm-delete-account-container"
            div {
                id = "pm-delete-question"
                h5 {
                    +"Are you sure you want to delete the account?"
                }
                button(classes = "btn btn-danger") {
                    id = "pm-delete-button"
                    attributes += "data-toggle" to "collapse"
                    attributes += "data-target" to "#pm-delete-account-container"
                    +"Yes"
                }

                button(classes = "btn") {
                    attributes += "data-toggle" to "collapse"
                    attributes += "data-target" to "#pm-delete-account-container"
                    +"Cancel"
                }
            }
        }

        div {
            id = pmOpenAccountContainerId

            buildWalletContentView(this)

            buildRemoteView(this)
        }

        div {
            id = pmClosedAccountContainerId

            div("form-group") {
                label {
                    attributes += "for" to "pm-create-account-name"
                    +"Enter password to open the account"
                }
                input(classes = "form-control") {
                    id = "pm-open-account-password"
                    type = InputType.password
                    placeholder = "Password"
                }
            }
            button(classes = "btn btn-primary") {
                id = "pm-open-button"
                +"Open"
            }
        }

        div {
            id = pmBusyAccountContainerId
            h4 {
                +"Busy: "
                b { span { id = "pm-busy-status" } }
            }

            button(classes = "btn") {
                id = "pm-cancel-button"
                +"Cancel"
            }
        }
    }
}