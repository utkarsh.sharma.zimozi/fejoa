package org.fejoa.auth.ui.pm

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.html.*
import kotlinx.html.dom.create
import kotlinx.html.js.li
import kotlinx.html.js.onClickFunction
import org.fejoa.*
import org.fejoa.auth.FejoaAuthClient
import org.fejoa.auth.passwordmanager.PMAccountManager
import org.fejoa.auth.ui.AuthManager
import org.fejoa.auth.ui.LogViewModel
import org.fejoa.storage.HashValue
import org.fejoa.storage.StorageDir
import org.w3c.dom.*
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine
import kotlin.dom.addClass
import kotlin.dom.removeClass


class RemoteViewModel(pwViewModel: PMViewModel, client: FejoaAuthClient, val log: LogViewModel) {
    val window = pwViewModel.window
    val document = window.document
    val authManager = client.authManager
    var pmAccount: PMAccountManager.AccountEntry? = null
    set(value) {
        // remove listener from old account
        unloadCurrentAccount()
        // add listener to new account
        value?.account?.apply {
            userData.storageDir.addListener(userDataListener)
            syncModel.listeners += remoteBranchEventListener
            client.syncer.branchWatcher.listeners.add(branchWatcherListener)
        }

        field = value

        if (!isDead) {
            updateRemoteDropDown()
            updateRemoteList()
        }
    }

    val confirmRemoteContainer = document.getElementById("pm-confirm-remote-container")
            .unsafeCast<HTMLDivElement>()
    val confirmRemoteOrigin = document.getElementById("pm-confirm-remote-origin")
            .unsafeCast<HTMLSpanElement>()
    val confirmRemoteButton = document.getElementById("pm-confirm-remote-button")
            .unsafeCast<HTMLButtonElement>()

    val addRemoteDropdown = document.getElementById("pm-remote-add-dropdown").unsafeCast<HTMLUListElement>()
    val remoteList = document.getElementById("pm-remote-list").unsafeCast<HTMLUListElement>()

    private var isDead = false

    private val userDataListener = object : StorageDir.Listener {
        override suspend fun onAfterCommit(base: HashValue, newTip: HashValue) {
            updateRemoteDropDown()
            updateRemoteList()
        }
    }

    private val statusListener = AuthManager.StatusListener("*", {
        updateRemoteDropDown()
    })

    private val remoteBranchEventListener = object : SyncModel.Listener {
        override fun onBranchUpdated(remote: String, branch: String) {
            updateRemoteList()
        }

        override fun onBranchIndexUpdated(remote: String) {
            updateRemoteList()
        }
    }

    private val branchWatcherListener = object : BranchWatcher.Listener {
        override suspend fun onDirtyBranchListUpdated(remote: Remote) {
            updateRemoteList()
        }
    }

    private fun unloadCurrentAccount() {
        pmAccount?.account?.apply {
            syncModel.listeners.remove(remoteBranchEventListener)
            userData.storageDir.removeListener(userDataListener)
            this.client.syncer.branchWatcher.listeners.remove(branchWatcherListener)
        }
    }

    init {
        client.connectionAuthManager.passwordGetter = object : PasswordGetter {
            override suspend fun get(purpose: PasswordGetter.Purpose, resource: String, info: String): String?
                    = suspendCoroutine {
                val future = client.pwRequestManager.startRequest(purpose, resource, info)
                future.whenCompleted { password, throwable ->
                    if (throwable != null)
                        it.resumeWithException(throwable)
                    else
                        it.resume(password)
                }
            }
        }

        authManager.statusListeners += statusListener
        window.addEventListener("unload", {
            isDead = true
            client.connectionAuthManager.passwordGetter = CanceledPasswordGetter()
            authManager.statusListeners.remove(statusListener)
            unloadCurrentAccount()
        })
    }

    private fun syncButtonId(remote: Remote) = "sync-remote-${remote.id}-button"

    private fun isSyncing(data: RemoteBranchData): Boolean {
        val remoteBranchIndexIsSyncStatus = data.remoteBranchIndexEntry?.remoteIndexStatus
                ?: SyncEvent.Status.SYNCED
        if (remoteBranchIndexIsSyncStatus == SyncEvent.Status.SYNCING)
            return true

        data.dirtyBranches.firstOrNull { it.isSyncing }?.let { return true }

        return false
    }

    val debug = false
    private fun buildRemoteListItemView(data: RemoteBranchData)
            : HTMLLIElement {
        val remote = data.remote
        // we need the bgDocument for creation
        val bgDocument = window.unsafeCast<dynamic>().bgWindow.document.unsafeCast<Document>()
        return bgDocument.create.li("list-group-item") {
            span {
                +remote.address()
                val isSyncing = isSyncing(data)
                button(classes = "btn pull-right") {
                    id = syncButtonId(remote)
                    +"Sync"
                    if (isSyncing)
                        disabled = true
                    onClickFunction = {
                        pmAccount?.account?.let {
                            GlobalScope.launch {
                                try {
                                    it.sync(remote)
                                } catch (e: Exception) {
                                    log.error(e.message ?: "Unknown exception")
                                }
                            }
                        }
                    }
                }

                if (isSyncing)
                    span("fa fa-spinner fa-spin fa-fw pull-right")
            }


            if (data.dirtyBranches.isNotEmpty()) {
                small { p { +"Un-synced branches:" } }

                data.dirtyBranches.forEach {
                    val dirtyBranch = it.dirtyBranch
                    val branchInfo = it.branchInfo
                    if (debug) {
                        val infoString = "(syncing: ${it.isSyncing}, error: ${it.error})"
                        div {
                            small {
                                p { +"- ${branchInfo.description}  $infoString" }
                                p { +"Branch: ${dirtyBranch.branch.branch.toHex().subSequence(0, 20)}" }
                            }

                            ul {
                                li {
                                    small { +"Local log: ${dirtyBranch.localTipId?.toHex()?.subSequence(0, 20)}" }
                                }
                                li {
                                    small { +"Remote log: ${dirtyBranch.remoteTipId?.toHex()?.subSequence(0, 20)}" }
                                }
                            }
                        }
                    } else {
                        val infoString = if (it.isSyncing) "(syncing)" else ""
                        div {
                            small {
                                p {
                                    +"${branchInfo.description}  $infoString"
                                    br
                                    i { +"Branch: ${dirtyBranch.branch.branch.toHex().subSequence(0, 20)}" }
                                }
                            }
                        }
                    }
                }
            } else
                small { p { +"All synced" } }
        }
    }

    class BranchStatus(val dirtyBranch: BranchWatcher.DirtyBranch, val branchInfo: BranchInfo, val isSyncing: Boolean,
                       val error: String)
    class RemoteBranchData(val remote: Remote, val dirtyBranches: List<BranchStatus>,
                           val remoteBranchIndexEntry: SyncModel.RemoteBranchIndexStatus?)

    private fun updateRemoteList() {
        val account = pmAccount?.account ?: return
        GlobalScope.launch {
            val remotes = account.userData.remotes.listRemotes()

            val data = remotes.map { remote ->
                val remoteEntry = account.syncModel.entryMap[remote.id]
                val failedBranches = remoteEntry?.failedSyncing
                        ?: emptyMap<String, String>()
                val syncingBranches = remoteEntry?.syncingBranches
                        ?: emptySet<String>()

                val dirtyBranches = pmAccount?.account?.let {
                    it.client.withLooper {
                        syncer.branchWatcher.dirtyBranches[remote.id]?.let {
                            it.map { dirtyBranch ->
                                val branchInfo = dirtyBranch.branch.branchInfo.get()
                                val isSyncing = syncingBranches.contains(branchInfo.branch)
                                val error = failedBranches[branchInfo.branch] ?: ""

                                BranchStatus(dirtyBranch, branchInfo, isSyncing, error)
                            }
                        }
                    }
                } ?: emptyList()

                val remoteBranchIndexEntry = account.syncModel
                        .remoteBranchIndexStatusMap[remote.id]
                RemoteBranchData(remote, dirtyBranches, remoteBranchIndexEntry)
            }

            // repopulate the remote list
            while (remoteList.firstChild != null)
                remoteList.removeChild(remoteList.firstChild!!)
            data.forEach { it
                val li = buildRemoteListItemView(it)
                remoteList.appendChild(li)
            }
        }
    }

    private fun contains(remotes: Collection<Remote>, user: String, url: String): Boolean {
        return remotes.firstOrNull { it.user == user && it.server == url } != null
    }

    private fun updateRemoteDropDown() = GlobalScope.launch {
        val remotes = pmAccount?.account?.client?.withLooper {
            userData.remotes.listRemotes()
        } ?: emptyList()

        // repopulate the drop down
        while (addRemoteDropdown.firstChild != null)
            addRemoteDropdown.removeChild(addRemoteDropdown.firstChild!!)

        // fill logged in remotes
        authManager.getAutStatusList()
                .filter { it.second.status == AuthManager.Status.LOGGED_IN
                        && !contains(remotes, it.second.user, it.second.url) }
                .forEach {
            val origin = it.first
            val li = document.createElement("li").unsafeCast<HTMLLIElement>()
            li.onclick = {
                confirmRemoteContainer.removeClass("collapse")
                confirmRemoteContainer.addClass("collapse in")

                confirmRemoteOrigin.innerText = origin
                confirmRemoteButton.onclick = {
                    launch {
                        onAddRemote(origin)
                    }
                }
                Unit
            }

            val ref = document.createElement("a").unsafeCast<HTMLLinkElement>()
            ref.href = "#"
            ref.innerText = "${it.second.user}@${it.second.url}"
            li.appendChild(ref)
            addRemoteDropdown.appendChild(li)
        }
    }

    suspend fun onAddRemote(origin: String) {
        val account = pmAccount?.account ?: return
        val authStatus = authManager.getAuthStatus(origin) ?: return
        val user = authStatus.user
        val url = authStatus.url

        try {
            account.addRemote(user, url)
        } catch (e: Exception) {
            log.error(e.message ?: "Unknown Exception")
        }
    }
}
