package org.fejoa.auth.ui.pm

import kotlinx.html.*

fun buildGeneratePWView(parent: DIV) {
    parent.apply {
        div("form-group") {
            label {
                attributes += "for" to "pw-gen-length"
                +"Password length"
            }
            input(classes = "form-control") {
                id = "pw-gen-length"
                type = InputType.number
                value = "12"
                min = "6"
                max = "255"
            }
        }
        div("form-group") {
            label {
                attributes += "for" to "pw-gen-output"
                +"Generated password"
            }
            input(classes = "form-control") {
                id = "pw-gen-output"
            }
        }

        button(classes = "btn") {
            id = "pw-gen-generate-button"
            +"Generate"
        }

        button(classes = "btn") {
            id = "pw-gen-fill-button"
            +"Fill page"
        }
    }
}
