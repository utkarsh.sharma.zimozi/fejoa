package org.fejoa.auth.ui.pm

import org.fejoa.auth.passwordmanager.PMBackground
import org.fejoa.auth.passwordmanager.PWGeneratorParams
import org.fejoa.auth.passwordmanager.generatePassword
import org.fejoa.auth.ui.PageInfoManager
import org.w3c.dom.HTMLButtonElement
import org.w3c.dom.HTMLInputElement
import org.w3c.dom.Window
import kotlin.dom.addClass


class PWGeneratorViewModel(val window: Window, pageInfo: PageInfoManager, val pmBackground: PMBackground) {
    val document = window.document

    val lengthField = document.getElementById("pw-gen-length").unsafeCast<HTMLInputElement>()
    val passwordField = document.getElementById("pw-gen-output").unsafeCast<HTMLInputElement>()
    val generateButton = document.getElementById("pw-gen-generate-button").unsafeCast<HTMLButtonElement>()
    val fillButton = document.getElementById("pw-gen-fill-button").unsafeCast<HTMLButtonElement>()

    init {
        pageInfo.onPageInfoChanged.add({
            val passwordFormType = pageInfo.pageInfo.first?.passwordFormType ?: return@add
            if (passwordFormType != "register" || passwordFormType != "new_password") {
                fillButton.addClass("hidden")
            }
        })

        generateButton.addEventListener("click", {
            generateNewPassword()
        })

        fillButton.addEventListener("click", {
            pmBackground.fillRegisterPassword(passwordField.value)
        })

        generateNewPassword()
    }

    private fun generateNewPassword() {
        val length = lengthField.value.toInt().let { if (it < 1) 8 else it }
        val password = generatePassword(PWGeneratorParams(length = length))
        passwordField.value = password
    }
}