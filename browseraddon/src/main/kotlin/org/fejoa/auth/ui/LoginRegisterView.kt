package org.fejoa.auth.ui

import kotlinx.html.*


fun buildFejoaAuthLoginRegisterView(parent: DIV) {
    parent.div {
        id = "login-register"
        div {
            id = "tabs"
            ul("nav nav-tabs") {
                li("active") {
                    a {
                        attributes += "data-toggle" to "tab"
                        href = "#login-tab"
                        +"Login"
                    }
                }
                li {
                    a {
                        attributes += "data-toggle" to "tab"
                        href = "#register-tab"
                        +"Register"
                    }
                }
            }
            div("tab-content") {
                div("tab-pane active") {
                    id = "login-tab"
                    div("form-group") {
                        label {
                            attributes += "for" to "login-username"
                            +"User name"
                        }

                        input(classes = "form-control") {
                            id = "login-username"
                            placeholder = "User name"
                        }
                    }
                    div("form-group") {
                        label {
                            attributes += "for" to "login-password"
                            +"Password"
                        }
                        input(classes = "form-control") {
                            id="login-password"
                            type = InputType.password
                            placeholder="Password"
                        }
                    }
                    button(classes = "btn btn-primary") {
                        id = "login-button"
                        +"Login"
                    }
                }
                div("tab-pane") {
                    id = "register-tab"
                    div("form-group") {
                        label {
                            attributes += "for" to "register-username"
                            +"User name"
                        }
                        input(classes = "form-control") {
                            id = "register-username"
                            placeholder = "User name"
                        }
                    }
                    div("form-group") {
                        label {
                            attributes += "for" to "register-password"
                            +"Password"
                        }
                        input(classes = "form-control") {
                            id = "register-password"
                            type = InputType.password
                            placeholder = "Password"
                        }
                    }
                    div("form-group") {
                        label {
                            attributes += "for" to "register-password2"
                            +"Retype Password"
                        }
                        input(classes = "form-control") {
                            id = "register-password2"
                            type = InputType.password
                            placeholder = "Password"
                        }
                    }

                    buildBenchmarkView(this, "")

                    button(classes = "btn btn-primary") {
                        id = "register-button"
                        +"Register"
                    }
                }
            }
        }
    }
}