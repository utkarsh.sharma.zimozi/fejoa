package org.fejoa.auth.ui.pm

import kotlinx.html.*


const val pmShowWalletContentButtonId = "pm-show-wallet-content-button"
const val pmShowWalletPasswordContainerId = "pm-show-wallet-password-container"
const val pmShowWalletPasswordButtonId = "pm-show-wallet-password-button"
const val pmShowWalletPasswordInputId = "pm-show-wallet-password-input"
const val pmWalletContentDivId = "pm-wallet-content-div"
const val pmWalletContentRawTextId = "pm-wallet-content-raw-div"


fun buildWalletContentView(parent: DIV) = parent.apply {
    h4 {
        +"Wallet Content:"
    }
    div("form-group") {
        button(classes = "btn btn-info btn-xs") {
            id = pmShowWalletContentButtonId
            attributes += "data-toggle" to "collapse"
            attributes += "data-target" to "#$pmShowWalletPasswordContainerId"
            +"Show Content"
        }
        div("collapse password-request-container") {
            id = pmShowWalletPasswordContainerId

            div("form-group") {
                label {
                    attributes += "for" to pmShowWalletPasswordInputId
                    +"Verify Password:"
                }

                input(classes = "form-control") {
                    id = pmShowWalletPasswordInputId
                    type = InputType.password
                }
            }

            div("form-group") {
                button(classes = "btn btn-primary btn-sm") {
                    id = pmShowWalletPasswordButtonId
                    attributes += "data-toggle" to "collapse"
                    attributes += "data-target" to "#$pmShowWalletPasswordContainerId"
                    +"Ok"
                }

                button(classes = "btn btn-sm") {
                    attributes += "data-toggle" to "collapse"
                    attributes += "data-target" to "#$pmShowWalletPasswordContainerId"
                    +"Cancel"
                }
            }
        }
    }
    div {
        id = pmWalletContentDivId
        hidden = true

        div {
            id = pmWalletContentRawTextId
            style = "white-space: pre-wrap;\n" +
                    "background-color: #F0F0F0;\n" +
                    "font-size: 90%;\n" +
                    "padding: 0.3em 0.3em;"
        }

        p {}

        buildExportWalletView(this)
    }
}


const val pmExportWalletButtonId = "pm-export-wallet-button"

fun buildExportWalletView(parent: DIV) = parent.apply {
    div("control-group") {
        button(classes = "btn") {
            id = pmExportWalletButtonId
            +"Download Wallet"
        }
    }
}