package org.fejoa.auth.ui.pm

import kotlinx.html.*

fun buildRemoteView(parent: DIV) {
    parent.apply {
        div {
            h4 {
                +"Remote storage:"
            }

            div("dropdown") {
                button(classes = "btn btn-primary btn-xs dropdown-toggle") {
                    attributes += "type" to "button"
                    attributes += "data-toggle" to "dropdown"
                    +"Add remote"
                    span("caret")
                }

                ul("dropdown-menu") {
                    id = "pm-remote-add-dropdown"
                    li {
                        a {
                            href = "#"
                            +"Remote 1"
                        }
                    }
                }
            }

            // add remote confirmation
            div("collapse") {
                id = "pm-confirm-remote-container"

                h5 {
                    +"Are you sure you want to add the remote?"
                }
                span {
                    id = "pm-confirm-remote-origin"
                }
                button(classes = "btn btn-success") {
                    id = "pm-confirm-remote-button"
                    attributes += "data-toggle" to "collapse"
                    attributes += "data-target" to "#pm-confirm-remote-container"
                    +"Yes"
                }
                button(classes = "btn") {
                    attributes += "data-toggle" to "collapse"
                    attributes += "data-target" to "#pm-confirm-remote-container"
                    +"Cancel"
                }
            }

            ul("list-group") {
                id = "pm-remote-list"

                li("list-group-item") {
                    +"Remote item"
                }
            }
        }
    }
}