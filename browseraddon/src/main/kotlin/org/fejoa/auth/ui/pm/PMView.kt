package org.fejoa.auth.ui.pm

import kotlinx.html.*
import kotlinx.html.dom.create
import kotlinx.html.js.div
import org.w3c.dom.HTMLElement
import kotlin.browser.document


object PMView {
    const val pmRetrieveAccountViewTabId = "pm-retrieve-account-view-tab"

    fun build(): HTMLElement {
        return document.create.div {
            id = "password-manager-container"

            div {
                id = "pm-tabs"

                ul("nav nav-tabs") {
                    li("dropdown active") {
                        a("dropdown-toggle") {
                            attributes += "data-toggle" to "dropdown"
                            href = "#"
                            +"Accounts"
                            span("caret") { }
                        }
                        ul("dropdown-menu") {
                            id = "pm-account-menu"
                            li {
                                a {
                                    attributes += "data-toggle" to "tab"
                                    href = "#pm-account-view-tab"
                                    +"Submenu 1-1"
                                }
                            }
                        }
                    }

                    li {
                        a {
                            attributes += "data-toggle" to "tab"
                            href = "#pm-create-tab"
                            +"Create"
                        }
                    }

                    li {
                        a {
                            attributes += "data-toggle" to "tab"
                            href = "#pm-generate-password"
                            +"Generator"
                        }
                    }
                }

                div("tab-content") {
                    div("tab-pane") {
                        id = "pm-account-view-tab"

                        AccountView.build(this)
                    }

                    div("tab-pane") {
                        id = pmRetrieveAccountViewTabId

                        RetrieveAccountView.build(this)
                    }

                    div("tab-pane") {
                        id = "pm-create-tab"

                        buildCreateAccountView(this)
                    }

                    div("tab-pane") {
                        id = "pm-generate-password"

                        buildGeneratePWView(this)
                    }
                }
            }
        }
    }
}
