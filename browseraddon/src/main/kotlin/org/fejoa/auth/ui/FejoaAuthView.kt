package org.fejoa.auth.ui

import kotlinx.html.*
import kotlinx.html.dom.create
import kotlinx.html.js.div
import org.w3c.dom.HTMLElement
import kotlin.browser.document


fun buildFejoaAuthView(): HTMLElement {
    return document.create.div {
        id = "fejoa-auth-container"
        div("form-group row") {
            label("fejoa-text") {
                +"On "
                span(classes = "label label-primary") {
                    id = "current-page"
                    +"displayurl.org"
                }
            }
            button(classes = "btn pull-right") {
                id = "logout-button"
                attributes += "data-toggle" to "tooltip"
                title = "Log out"
                span("glyphicon glyphicon-log-out")
            }
        }

        div("alert alert-success") {
            id = "login-status"
            role = "alert"
            +"Logged in as "
            strong {span { id="logged-in-user" } }
        }

        div {
            id = "logged-in-container"
        }

        div {
            id = "fa-busy-container"
            h4 {
                +"Busy: "
                b {span { id="fa-busy-status" } }
            }
            button(classes = "btn") {
                id = "fa-cancel-button"
                +"Cancel"
            }
        }

        buildFejoaAuthLoginRegisterView(this)
    }
}
