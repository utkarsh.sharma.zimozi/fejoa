package org.fejoa.auth.ui.pm

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.fejoa.auth.FejoaAuthClient
import org.fejoa.auth.passwordmanager.PMAccountManager
import org.fejoa.auth.ui.*
import org.fejoa.auth.ui.pm.AccountView.pmAccountStatusId
import org.fejoa.auth.ui.pm.PMView.pmRetrieveAccountViewTabId
import org.fejoa.crypto.CryptoSettings
import org.w3c.dom.*
import kotlin.properties.Delegates


class PMViewModel(val window: Window, fejoaAuthClient: FejoaAuthClient,
                  pageInfoManager: PageInfoManager, val log: LogViewModel) {
    val document = window.document
    val accountManager = fejoaAuthClient.accountManager

    val kdfConfigViewModel = KDFConfigViewModel(window, "pm-")

    val createAccountInput = document.getElementById("pm-create-account-name").unsafeCast<HTMLInputElement>()
    val createPasswordInput = document.getElementById("pm-account-password").unsafeCast<HTMLInputElement>()
    val createPassword2Input = document.getElementById("pm-account-password2").unsafeCast<HTMLInputElement>()

    val openPasswordInput = document.getElementById("pm-open-account-password").unsafeCast<HTMLInputElement>()
    val createButton = document.getElementById("pm-create-button").unsafeCast<HTMLButtonElement>()
    val openButton = document.getElementById("pm-open-button").unsafeCast<HTMLButtonElement>()
    val closeButton = document.getElementById("pm-close-button").unsafeCast<HTMLButtonElement>()
    val deleteButton = document.getElementById("pm-delete-button").unsafeCast<HTMLButtonElement>()
    val cancelButton = document.getElementById("pm-cancel-button").unsafeCast<HTMLButtonElement>()
    val dropDownMenu = document.getElementById("pm-account-menu").unsafeCast<HTMLUListElement>()

    val busyStatus = document.getElementById("pm-busy-status").unsafeCast<HTMLSpanElement>()
    val accountStatus = document.getElementById(pmAccountStatusId).unsafeCast<HTMLSpanElement>()
    val accountStatusBar = document.getElementById("pm-account-status-bar").unsafeCast<HTMLDivElement>()
    val selectedAccountTitle = document.getElementById("pm-account-view-title-selected").unsafeCast<HTMLHeadingElement>()

    val closedContainer = document.getElementById(pmClosedAccountContainerId).unsafeCast<HTMLDivElement>()
    val openedContainer = document.getElementById(pmOpenAccountContainerId).unsafeCast<HTMLDivElement>()
    val busyContainer = document.getElementById(pmBusyAccountContainerId).unsafeCast<HTMLDivElement>()
    val deleteContainer = document.getElementById("pm-delete-account-container").unsafeCast<HTMLDivElement>()

    // Don't use a normal Kotlin list; Kotlin does some type checks and these fail for popup elements...
    var tabContentContainerList: Array<HTMLDivElement> = arrayOf(closedContainer, openedContainer, busyContainer,
            deleteContainer)

    val retrieveAccountViewModel = RetrieveAccountViewModel(window, fejoaAuthClient, accountManager, log)

    private val accountDropDownLinks = HashMap<String, HTMLLinkElement>()
    private val remoteController = RemoteViewModel(this, fejoaAuthClient, log)

    private var isDead = false

    var errorMessage by Delegates.observable("") { _, _, newValue ->
        if (isDead) return@observable
        log.error(newValue)
    }

    val accountListener: PMAccountManager.Listener = object : PMAccountManager.Listener {
        override fun onAccountListUpdated() {
            this@PMViewModel.onAccountListUpdated()
        }

        override fun onAccountSelected(account: String?) {
            if (account != null)
                this@PMViewModel.onAccountSelected(account)
        }
    }

    init {
        PWGeneratorViewModel(window, pageInfoManager, fejoaAuthClient.pmBackground)

        KDFBenchmarkViewModel(window, KDFBenchmark(), kdfConfigViewModel, "pm-")

        WalletContentViewModel(window, fejoaAuthClient, log)

        accountManager.accountListeners += accountListener
        window.addEventListener("unload", {
            isDead = true
            accountManager.accountListeners.remove(accountListener)
        })

        createButton.addEventListener("click", {
            GlobalScope.launch {
                errorMessage = ""
                val password = createPasswordInput.value
                val algo = kdfConfigViewModel.getKdfAlgo()
                val nIterations = kdfConfigViewModel.getNIterations()
                val accountName = createAccountInput.value
                createAccountInput.value = ""
                createPasswordInput.value = ""
                createPassword2Input.value = ""
                val previousSelected = accountManager.selectedAccountName
                try {
                    val kdf = CryptoSettings.default.kdf
                    if (kdf.algo != algo)
                        throw Exception("Unsupported kdf algo: $algo")
                    kdf.params.iterations = nIterations

                    // Already select the account so that as soon as createAccount sends an update the account will be
                    // displayed.
                    accountManager.selectedAccountName = accountName
                    accountManager.createAccount(accountName, password, kdf)
                    accountManager.selectedAccountName = accountName
                } catch (e: Exception) {
                    if (previousSelected != null)
                        accountManager.selectedAccountName = previousSelected
                    errorMessage ="Failed to create account: " + e.message
                }
            }
        })
        openButton.addEventListener("click", {
            GlobalScope.launch {
                accountManager.selectedAccountName?.let {
                    errorMessage = ""
                    val password = openPasswordInput.value
                    openPasswordInput.value = ""
                    val account = accountManager.openAccount(it, password)

                    if (account == null) {
                        errorMessage = "Failed to open account"
                    }
                }
            }
        })
        closeButton.addEventListener("click", {
            GlobalScope.launch {
                accountManager.selectedAccountName?.let {
                    accountManager.closeAccount(it)
                }
            }
        })
        deleteButton.addEventListener("click", {
            GlobalScope.launch {
                accountManager.selectedAccountName?.let {
                    accountManager.selectedAccountName = null
                    accountManager.deleteAccount(it)
                }
            }
        })
        cancelButton.addEventListener("click", {
            GlobalScope.launch {
                accountManager.selectedAccountName?.let {
                    val entry = accountManager.getAccount(it) ?: return@launch
                    entry.future?.cancel()
                }
            }
        })


        openPasswordInput.oninput = {_ -> validateOpenInfo()}
        createAccountInput.oninput = { _ -> validateCreateInfo()}
        createPasswordInput.oninput = {_ -> validateCreateInfo()}
        createPassword2Input.oninput = {_ -> validateCreateInfo()}

        onAccountListUpdated()
    }


    private fun validateOpenInfo() {
        val password = openPasswordInput.value
        openButton.disabled = password == ""
    }

    private fun validateCreateInfo() {
        val accountName = createAccountInput.value
        val password1 = createPasswordInput.value
        val password2 = createPassword2Input.value
        createButton.disabled = (accountManager.getAccount(accountName) != null
                || accountName == "" || password1 == "" || password1 != password2)
    }

    private fun addRetrieveAccountItem() {
        val li = document.createElement("li")
        val link = document.createElement("a").unsafeCast<HTMLLinkElement>()
        link.href = "#$pmRetrieveAccountViewTabId"
        link.setAttribute("data-toggle", "tab")
        link.innerText = "Retrieve Account"
        li.appendChild(link)
        dropDownMenu.appendChild(li)
    }

    private fun onAccountListUpdated() {
        // repopulate the drop down
        while (dropDownMenu.firstChild != null)
            dropDownMenu.removeChild(dropDownMenu.firstChild!!)
        accountDropDownLinks.clear()

        accountManager.getAccounts().forEach {
            val accountName = it.name
            val li = document.createElement("li")
            li.addEventListener("click", {
                errorMessage = ""
                accountManager.selectedAccountName = accountName
            })
            val link = document.createElement("a").unsafeCast<HTMLLinkElement>()
            accountDropDownLinks[accountName] = link
            link.href = "#pm-account-view-tab"
            link.setAttribute("data-toggle", "tab")
            link.setAttribute("data-account", accountName)
            var text =  accountName
            text += when (it.type) {
                PMAccountManager.AccountEntry.Type.OPEN -> " (open)"
                PMAccountManager.AccountEntry.Type.CLOSED -> " (closed)"
                PMAccountManager.AccountEntry.Type.OPENING -> " (opening...)"
                PMAccountManager.AccountEntry.Type.CREATING -> " (creating...)"
                PMAccountManager.AccountEntry.Type.RETRIEVING -> " (retrieving...)"
            }
            link.innerText = text
            li.appendChild(link)
            dropDownMenu.appendChild(li)
        }
        addRetrieveAccountItem()

        if (accountManager.getAccounts().isEmpty())
            selectCreateTab()
        else {
            accountManager.selectedAccountName?.let {
                accountManager.selectedAccountName = accountManager.selectedAccountName
            } ?: run {
                val accountEntry = accountManager.getDefaultAccount()
                        ?: accountManager.getAccounts()[0]
                accountManager.selectedAccountName = accountEntry.name
            }
        }
    }

    private fun selectCreateTab() {
        js("this.window.\$('a[href=\"#pm-create-tab\"]').tab('show');")
    }

    private fun hideDeleteContainer() {
        js("this.window.\$(\"#pm-delete-question\").collapse('hide');")
    }

    private fun showContainer(container: HTMLDivElement) {
        tabContentContainerList.forEach {
            it.hidden = it !== container
        }
    }

    private fun onAccountSelected(accountName: String) {
        val accountEntry = accountManager.getAccount(accountName) ?: return
        onAccountSelected(accountEntry)
        accountDropDownLinks[accountName]?.click()
    }

    private fun onAccountSelected(accountEntry: PMAccountManager.AccountEntry) {
        // update the remote controller
        remoteController.pmAccount = accountEntry

        accountStatusBar.hidden = false
        selectedAccountTitle.innerText = accountEntry.name

        hideDeleteContainer()

        when (accountEntry.type) {
            PMAccountManager.AccountEntry.Type.CLOSED -> {
                showContainer(closedContainer)
                closeButton.disabled = true
                accountStatus.innerText = "(closed)"
            }
            PMAccountManager.AccountEntry.Type.OPEN -> {
                showContainer(openedContainer)
                accountStatus.innerText = "(open)"
                closeButton.disabled = false
            }
            PMAccountManager.AccountEntry.Type.RETRIEVING,
            PMAccountManager.AccountEntry.Type.OPENING,
            PMAccountManager.AccountEntry.Type.CREATING -> {
                showContainer(busyContainer)
                closeButton.disabled = true
                accountStatus.innerText = ""

                when (accountEntry.type) {
                    PMAccountManager.AccountEntry.Type.RETRIEVING -> busyStatus.innerText = "retrieving account..."
                    PMAccountManager.AccountEntry.Type.OPENING -> {
                        accountStatus.innerText = "(closed)"
                        busyStatus.innerText = "opening account..."
                    }
                    PMAccountManager.AccountEntry.Type.CREATING -> busyStatus.innerText = "creating account..."
                }
            }
        }
    }
}