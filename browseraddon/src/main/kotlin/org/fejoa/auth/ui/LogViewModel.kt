package org.fejoa.auth.ui

import org.fejoa.auth.ui.StatusView.errorViewId
import org.fejoa.auth.ui.StatusView.statusViewId
import org.w3c.dom.HTMLParagraphElement
import org.w3c.dom.Window


interface Log {
    fun status(message: String)
    fun error(message: String)
}

class LogViewModel(val window: Window) : Log {
    val document = window.document

    val statusLabel = document.getElementById(statusViewId).unsafeCast<HTMLParagraphElement>()
    val errorLabel = document.getElementById(errorViewId).unsafeCast<HTMLParagraphElement>()

    override fun status(message: String) {
        statusLabel.hidden = message == ""
        statusLabel.innerText = message
    }

    override fun error(message: String) {
        errorLabel.hidden = message == ""
        errorLabel.innerText = message
    }
}
