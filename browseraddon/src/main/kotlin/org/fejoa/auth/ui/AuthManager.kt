package org.fejoa.auth.ui

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.fejoa.*
import org.fejoa.crypto.*
import org.fejoa.jsbindings.Tab
import org.fejoa.jsbindings.chrome
import org.fejoa.network.*
import org.fejoa.support.Future
import org.fejoa.support.async
import org.fejoa.support.await
import org.w3c.dom.url.URL
import kotlin.js.json


class AuthManager(val context: FejoaContext, val authManager: ConnectionAuthManager) {
    enum class Status {
        LOGGED_OUT,
        LOGGED_IN,
        LOGGING_IN,
        REGISTERING,
        CONFIRM_REGISTRATION,
    }

    class StatusRefCount(val status: AuthStatus) {
        private var count = 1

        fun addRef(): Int {
            count++
            return count
        }

        fun removeRef(): Int {
            count--
            return count
        }
    }
    class AuthStatus(var url: String = "", var status: Status = Status.LOGGED_OUT, var promise: Future<*>? = null,
                     var user: String = "", var statusMessage: String? = null, var errorMessage: String? = null)

    class StatusListener(val origin: String, val callback: () -> Unit)

    /**
     * origin -> StatusRefCount
     *
     * Multiple tabs can show the same origin. Only if no tabs refer to the same origin anymore, long running tasks
     * (logging in and registering) are canceled.
     */
    private val statusMap = HashMap<String, StatusRefCount>()
    val statusListeners = ArrayList<StatusListener>()

    fun getAutStatusList(): List<Pair<String, AuthStatus>> {
        return statusMap.map { it.key to it.value.status }
    }

    /**
     * Monitor open tabs and maintain the number of tabs referring to a same domain. If the ref count is 0 the
     * ongoing job, if there is one, is canceled and the status removed.
     */
    class TabMonitor(val statusMap: HashMap<String, StatusRefCount>, val statusListeners: ArrayList<StatusListener>) {
        // tabId -> origin
        private val tabOriginMap = HashMap<Int, String>()

        /**
         * Return all tab id for a given origin
         */
        fun getTabs(origin: String): List<Int> {
            return tabOriginMap.filter { it.value == origin }.map { it.key }
        }

        init {
            chrome.tabs.query(json()) { result ->
                result.forEach { addTab(it) }
            }
            chrome.tabs.onCreated.addListener { tab ->
                addTab(tab)
            }
            chrome.tabs.onRemoved.addListener { tabId, _ ->
                val origin = tabOriginMap.remove(tabId) ?: return@addListener
                removeRef(origin)
            }
            chrome.tabs.onUpdated.addListener { tabId, _, tab ->
                val url = (tab.url ?: return@addListener).unsafeCast<String>()
                val oldOrigin = tabOriginMap[tabId]
                val newOrigin = URL(url).origin
                tabOriginMap[tabId] = newOrigin
                addRef(newOrigin)
                if (oldOrigin != null)
                    removeRef(oldOrigin)
            }
            chrome.tabs.onReplaced.addListener { addedTabId, removedTabId ->
                // the addedTabId has the new tab url/origin
                chrome.tabs.get(addedTabId) { tab ->
                    val oldOrigin = tabOriginMap.remove(removedTabId)
                    val url = tab.url ?: return@get
                    val newOrigin = URL(url).origin
                    tabOriginMap[addedTabId] = newOrigin
                    addRef(newOrigin)

                    if (oldOrigin != null)
                        removeRef(oldOrigin)
                }
            }
        }

        private fun addTab(tab: Tab) {
            val tabId = tab.id ?: return
            val url = tab.url ?: return
            val origin = URL(url).origin
            tabOriginMap[tabId] = origin
            addRef(origin)
        }

        private fun addRef(origin: String) {
            val loginStatus = statusMap[origin]
            if (loginStatus != null) {
                loginStatus.addRef()
            } else {
                statusMap[origin] = StatusRefCount(AuthStatus())
            }
        }

        private fun removeRef(origin: String) {
            val loginStatus = statusMap[origin] ?: return
            val refCount = loginStatus.removeRef()
            if (refCount == 0) {
                loginStatus.status.promise?.cancel()
                statusMap.remove(origin)

                // cleanup listeners (there shouldn't be listeners left though)
                statusListeners.removeAll { it.origin == origin }
            }
        }
    }


    // install and start the tab monitor
    val tabMonitor = TabMonitor(statusMap, statusListeners)

    fun getAuthStatus(origin: String): AuthStatus? {
        return statusMap[origin]?.status
    }

    private fun notifyStatusUpdated(origin: String, user: String?, status: Status) {
        // notify local listeners
        statusListeners.filter { it.origin == "*" || it.origin == origin }.forEach { it.callback.invoke() }

        // notify all tabs
        tabMonitor.getTabs(origin).forEach {
            chrome.tabs.sendMessage(it, json("method" to "updateLoginStatus",
                    "origin" to origin,
                    "user" to user,
                    "authStatus" to status.name))
        }
    }

    private fun updateStatus(origin: String, status: Status = Status.LOGGED_OUT,
                             loginJob: Future<*>? = null, user: String = "",
                             statusMessage: String? = null, errorMessage: String? = null, url: String = "") {
        val authStatus = statusMap[origin]
        if (authStatus == null) {
            // ignore; the tab is gone (don't mess up the ref count by adding a new entry now)
        } else {
            authStatus.status.status = status
            authStatus.status.url = url
            authStatus.status.promise = loginJob
            authStatus.status.user = user
            authStatus.status.statusMessage = statusMessage
            authStatus.status.errorMessage = errorMessage
        }
        notifyStatusUpdated(origin, user, status)
    }

    /**
     * Asynchronously start the login process
     */
    fun launchLogin(authUrl: String, user: String, password: String) {
        GlobalScope.launch {
            val origin = URL(authUrl).origin

            // use PlainAuthInfo because we sending the LoginJob manually
            val loginJob = async {
                authManager.login(Remote("", user, authUrl), password)
            }
            updateStatus(origin, Status.LOGGING_IN, loginJob, user)
            try {
                loginJob.await()
                updateStatus(origin, Status.LOGGED_IN, user = user, url = authUrl)
            } catch (e: dynamic) {
                val errorMessage = (e.message ?: e).unsafeCast<String>()
                updateStatus(origin, Status.LOGGED_OUT, errorMessage = errorMessage)
                throw Exception(errorMessage)
            }
        }
    }

    /**
     * Asynchronously start the register process
     */
    fun launchRegister(algo: CryptoSettings.KDF_ALGO, authUrl: String, user: String, password: String,
                       iterations: Int) {
        GlobalScope.launch {
            val origin = URL(authUrl).origin

            val kdfParams = CryptoSettings.default.kdf
            if (kdfParams.algo != algo)
                throw Exception("unsupported kdf algo")
            kdfParams.params = CryptoSettings.KDFParams(iterations)
            val params = UserKeyParams(BaseKeyParams(kdfParams, CryptoHelper.crypto.generateSalt16()),
                    CryptoSettings.HASH_TYPE.SHA256, CryptoSettings.default.symmetric,
                    CryptoHelper.crypto.generateSalt16())

            val loginSecret = context.baseKeyCache.getUserKey(params, password)

            val job = loginSecret.thenSuspend {
                val loginParams = LoginParams(params,
                        CompactPAKE_SHA256_CTR.getSharedSecret(DH_GROUP.RFC5114_2048_256, it),
                        DH_GROUP.RFC5114_2048_256)

                 authManager.send(RegisterJob(user, loginParams), Remote("", user, authUrl),
                         PlainAuthInfo()).await()
            }

            updateStatus(origin, Status.REGISTERING, job, user)
            try {
                val result = job.await()
                if (result.code != ReturnType.OK || result.reply == null)
                    updateStatus(origin, Status.LOGGED_OUT, errorMessage = result.message)
                else {
                    val dummy = when (result.reply!!.result) {
                        RegisterJob.ReplyType.REGISTERED -> {
                            updateStatus(origin, Status.LOGGED_IN, user = user, statusMessage = result.message,
                                    url = authUrl)
                        }
                        RegisterJob.ReplyType.CONFIRMATION_REQUIRED -> {
                            updateStatus(origin, Status.CONFIRM_REGISTRATION, user = user,
                                    statusMessage = result.message, url = authUrl)
                        }

                    }
                }
            } catch (e: dynamic) {
                val errorMessage = (e.message ?: e).unsafeCast<String>()
                updateStatus(origin, Status.LOGGED_OUT, errorMessage = errorMessage)
                throw Exception(errorMessage)
            }
        }
    }

    suspend fun logOut(authUrl: String): String {
        try {
            // logout all users
            val result = authManager.logout(Remote("", "", authUrl), emptyList()).await()
            updateStatus(URL(authUrl).origin, Status.LOGGED_OUT)
            checkStatus(authUrl)
            return result.message
        } catch (e: dynamic) {
            val errorMessage = (e.message ?: e).unsafeCast<String>()
            throw Exception(errorMessage)
        }
    }

    /**
     * If not currently logging in or registering, query the server status
     *
     * @return the query response in case the server has been queried
     */
    suspend fun checkStatus(authUrl: String) {
        val origin = URL(authUrl).origin
        val currentStatus = getAuthStatus(origin)
        if (currentStatus != null
                && (currentStatus.status == Status.LOGGING_IN || currentStatus.status == Status.REGISTERING)) {
            return
        }
        try {
            val status = authManager.send(AuthStatusJob(), Remote("", "", authUrl),
                    PlainAuthInfo()).await()

            val accounts = status.response?.accounts

            if (status.code == ReturnType.OK && accounts != null && accounts.isNotEmpty()) {
                var users = ""
                // concatenate if more than one user
                accounts.forEachIndexed { index, s ->
                    users += s
                    if (index < accounts.lastIndex)
                        users += ", "
                }
                updateStatus(origin, Status.LOGGED_IN, user = users, url = authUrl)
            } else
                updateStatus(origin, Status.LOGGED_OUT)
        } catch (e: dynamic) {
            val errorMessage = (e.message ?: e).unsafeCast<String>()
            updateStatus(origin, Status.LOGGED_OUT, errorMessage = errorMessage)
            throw Exception(errorMessage)
        }
    }
}