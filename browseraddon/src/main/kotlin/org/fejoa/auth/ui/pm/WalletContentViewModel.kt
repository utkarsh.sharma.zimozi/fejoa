package org.fejoa.auth.ui.pm

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.fejoa.auth.FejoaAuthClient
import org.fejoa.auth.passwordmanager.PMAccountManager
import org.fejoa.auth.passwordmanager.Wallet
import org.fejoa.auth.ui.Log
import org.w3c.dom.HTMLButtonElement
import org.w3c.dom.HTMLDivElement
import org.w3c.dom.HTMLInputElement
import org.w3c.dom.Window


class WalletContentViewModel(val window: Window, fejoaAuthClient: FejoaAuthClient, val log: Log) {
    val document = window.document
    val accountManager = fejoaAuthClient.accountManager

    val showContentButton = document.getElementById(pmShowWalletContentButtonId).unsafeCast<HTMLButtonElement>()
    val passwordInput = document.getElementById(pmShowWalletPasswordInputId).unsafeCast<HTMLInputElement>()
    val passwordButton = document.getElementById(pmShowWalletPasswordButtonId).unsafeCast<HTMLButtonElement>()
    val walletContentDiv = document.getElementById(pmWalletContentDivId).unsafeCast<HTMLDivElement>()
    val walletContentRawTextDiv = document.getElementById(pmWalletContentRawTextId).unsafeCast<HTMLDivElement>()

    var isAlive = true

    init {
        window.addEventListener("unload", {
            isAlive = false
        })

        ExportWalletViewModel(window, ExportWalletModel(accountManager))

        accountManager.accountListeners += object : PMAccountManager.Listener {
            override fun onAccountListUpdated() {
            }

            override fun onAccountSelected(account: String?) {
                reset()
            }

        }

        reset()

        passwordInput.oninput = {
            Unit
        }

        passwordButton.onclick = {
            accountManager.getSelectedAccount()?.account?.let { account ->
                val client = account.client

                GlobalScope.launch {
                    if (client.userData.confirmPassword(passwordInput.value)) {
                        walletContentDiv.hidden = false
                        walletContentRawTextDiv.innerText = account.getDefaultWallet()?.toText() ?: ""
                        showContentButton.classList.add("hidden")
                        Unit
                    } else {
                        passwordInput.value = ""
                        log.error("Invalid password")
                    }
                }
            }

        }
    }

    suspend fun Wallet.toText(): String {
        var out = ""
        for (entry in listPasswords()) {
            out += "${entry.origin}:\n"
            out += " user: ${entry.username}\n"
            out += " password: ${entry.password}\n"
        }
        if (out.isBlank())
            out = "no entries"
        return out
    }

    private fun reset() {
        if (!isAlive)
            return

        showContentButton.classList.remove("hidden")
        passwordInput.value = ""
        walletContentRawTextDiv.innerText = ""

        showContentButton.hidden = false
        walletContentDiv.hidden = true
    }
}
