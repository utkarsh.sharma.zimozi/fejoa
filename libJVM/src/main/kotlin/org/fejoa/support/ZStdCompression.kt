package org.fejoa.support

import com.github.luben.zstd.Zstd
import kotlinx.coroutines.newSingleThreadContext


val zipContext = newSingleThreadContext("Fejoa compression")

actual class ZStdCompression actual constructor() : Compression {
    override fun Compress(data: ByteArray): Future<ByteArray> = Future(JVMExecutor(zipContext.executor)) {
        Zstd.compress(data, 3)
    }

    override fun Decompress(data: ByteArray): Future<ByteArray> = Future(JVMExecutor(zipContext.executor)) {
        val decompressedSize = Zstd.decompressedSize(data).toInt()
        if (decompressedSize > 100 * 1024 * 1024) // check for invalid sizes
            throw Exception("Compressed data is too large: $decompressedSize")
        Zstd.decompress(data, decompressedSize)
    }
}