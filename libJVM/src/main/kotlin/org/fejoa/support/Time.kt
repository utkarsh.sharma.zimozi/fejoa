package org.fejoa.support

actual fun nowNano(): Long {
    return System.nanoTime()
}