package org.fejoa.support


class JVMExecutor(private val executor: java.util.concurrent.Executor) : Executor {
    override fun run(task: () -> Unit) {
        executor.execute { task() }
    }
}