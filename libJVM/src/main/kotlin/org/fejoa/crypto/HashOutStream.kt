package org.fejoa.crypto

import java.security.MessageDigest


open class JVMAsyncHashOutStream(private val messageDigest: MessageDigest) : AsyncHashOutStream {
    override suspend fun hash(): ByteArray {
        return messageDigest.digest()
    }

    override fun reset() {
        messageDigest.reset()
    }

    override suspend fun write(buffer: ByteArray, offset: Int, length: Int): Int {
        messageDigest.update(buffer, offset, length)
        return length
    }

    override suspend fun flush() {

    }

    override suspend fun close() {
    }
}