package org.fejoa.support

import kotlin.test.Test
import kotlin.test.assertTrue


class DeflateTest {
    @Test
    fun deflateTest() {
        var out = ByteArrayOutStream()
        val deflater = DeflateOutStream(out)
        val plain = "Text to compress and some compressible data... DATA DATA DATA DATA".toUTF()
        deflater.write(plain)
        deflater.close()

        val compressed = out.toByteArray()
        assertTrue(compressed.size < plain.size)
        out = ByteArrayOutStream()
        val inflator = InflateOutStream(out)
        inflator.write(compressed)
        inflator.close()
        val inflated = out.toByteArray()

        assertTrue(plain contentEquals inflated)
    }
}