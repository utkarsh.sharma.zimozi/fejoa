package org.fejoa.protocolbufferlight

import org.fejoa.support.AsyncOutStream
import org.fejoa.support.AsyncInStream
import org.fejoa.support.readFully
import org.fejoa.support.*


/*
Loosely (incomplete) based on: https://developers.google.com/protocol-buffers/docs/encoding
 */
class ProtocolBufferLight {
    internal enum class DataType constructor(val value: Int) {
        VAR_INT(0),
        SIZE_64(1),
        LENGTH_DELIMITED(2),
        START_GROUP(3),
        END_GROUP(4),
        SIZE_32(5)
    }

    internal class Key(val type: DataType, val tag: Long)

    internal abstract class KeyValue(val key: Key) {
        /**
         * Merge two value to an array
         */
        abstract fun merge(other: KeyValue)
        abstract fun write(outStream: OutStream)
        abstract suspend fun write(outStream: AsyncOutStream)

        protected fun writeKey(outStream: OutStream, key: Key) {
            var outValue = key.tag shl TAG_SHIFT
            outValue = outValue or key.type.value.toLong()
            VarInt.write(outStream, outValue)
        }

        protected suspend fun writeKey(outStream: AsyncOutStream, key: Key) {
            var outValue = key.tag shl TAG_SHIFT
            outValue = outValue or key.type.value.toLong()
            VarInt.write(outStream, outValue)
        }
    }

    internal class VarIntValue(key: Key, val number: Long) : KeyValue(key) {
        companion object {
            suspend fun read(key: Key, inStream: AsyncInStream): VarIntValue {
                val number = VarInt.read(inStream).first
                return VarIntValue(key, number)
            }

            fun read(key: Key, inStream: InStream): VarIntValue {
                val number = VarInt.read(inStream).first
                return VarIntValue(key, number)
            }
        }

        override fun write(outStream: OutStream) {
            writeKey(outStream, key)
            VarInt.write(outStream, number)
        }

        override suspend fun write(outStream: AsyncOutStream) {
            writeKey(outStream, key)
            VarInt.write(outStream, number)
        }

        override fun merge(other: KeyValue) {
            TODO("not implemented")
        }
    }

    internal class ByteValue(key: Key, val byteList: MutableList<ByteArray> = ArrayList()) : KeyValue(key) {
        constructor(key: Key, bytes: ByteArray): this(key) {
            byteList.add(bytes)
        }

        companion object {
            suspend fun read(key: Key, inStream: AsyncInStream): ByteValue {
                val length = VarInt.read(inStream).first.toInt()
                val bytes = ByteArray(length)
                inStream.readFully(bytes)
                return ByteValue(key, bytes)
            }

            fun read(key: Key, inStream: InStream): ByteValue {
                val length = VarInt.read(inStream).first.toInt()
                val bytes = ByteArray(length)
                inStream.readFully(bytes)
                return ByteValue(key, bytes)
            }
        }

        override fun write(outStream: OutStream) {
            for (bytes in byteList) {
                writeKey(outStream, key)
                VarInt.write(outStream, bytes.size)
                outStream.write(bytes)
            }
        }

        override suspend fun write(outStream: AsyncOutStream) {
            for (bytes in byteList) {
                writeKey(outStream, key)
                VarInt.write(outStream, bytes.size)
                outStream.write(bytes)
            }
        }

        override fun merge(other: KeyValue) {
            byteList.addAll((other as ByteValue).byteList)
        }
    }

    private val map: MutableMap<Int, KeyValue> = HashMap()

    constructor()

    constructor(bytes: ByteArray) {
        read(ByteArrayInStream(bytes))
    }

    companion object {
        const val DATA_TYPE_MASK: Long = 0x7
        const val TAG_SHIFT: Int = 3

        suspend fun read(inStream: AsyncInStream): ProtocolBufferLight {
            val buffer = ProtocolBufferLight()
            buffer.read(inStream)
            return buffer
        }
    }

    /**
     * Try to read a Key
     *
     * @return null if the end of stream is reached
     */
    private fun readKey(inStream: InStream): Key? {
        val number = try {
            VarInt.read(inStream).first
        } catch (e: EOFException) {
            return null
        }
        val dataType = (number and DATA_TYPE_MASK).toInt()
        val tag = number shr TAG_SHIFT
        if (dataType == DataType.VAR_INT.value)
            return Key(DataType.VAR_INT, tag)
        if (dataType == DataType.LENGTH_DELIMITED.value)
            return Key(DataType.LENGTH_DELIMITED, tag)

        throw IOException("Unknown data type: $dataType")
    }

    private suspend fun readKey(inStream: AsyncInStream): Key? {
        val number = try {
            VarInt.read(inStream).first
        } catch (e: EOFException) {
            return null
        }
        val dataType = (number and DATA_TYPE_MASK).toInt()
        val tag = number shr TAG_SHIFT
        if (dataType == DataType.VAR_INT.value)
            return Key(DataType.VAR_INT, tag)
        if (dataType == DataType.LENGTH_DELIMITED.value)
            return Key(DataType.LENGTH_DELIMITED, tag)

        throw IOException("Unknown data type: $dataType")
    }

    fun clear() {
        map.clear()
    }

    fun put(tag: Int, bytes: ByteArray) {
        map[tag] = ByteValue(Key(DataType.LENGTH_DELIMITED, tag.toLong()), bytes)
    }

    fun put(tag: Int, bytesList: List<ByteArray>) {
        map[tag] = ByteValue(Key(DataType.LENGTH_DELIMITED, tag.toLong()), bytesList.toMutableList())
    }

    fun put(tag: Int, string: String) {
        put(tag, string.toUTF())
    }

    fun put(tag: Int, value: Long) {
        map[tag] = VarIntValue(Key(DataType.VAR_INT, tag.toLong()), value)
    }

    fun put(tag: Int, value: Int) {
        map[tag] = VarIntValue(Key(DataType.VAR_INT, tag.toLong()), value.toLong())
    }

    fun put(tag: Int, value: Boolean) {
        put(tag, if (value) 1 else 0)
    }

    fun getBytes(tag: Int): ByteArray? {
        return getByteArrayList(tag)?.firstOrNull()
    }

    fun getByteArrayList(tag: Int): List<ByteArray>? {
        val keyValue = map[tag] ?: return null
        assert(keyValue.key.tag == tag.toLong())

        return if (keyValue.key.type != DataType.LENGTH_DELIMITED) null else (keyValue as ByteValue).byteList
    }

    fun getString(tag: Int): String? {
        return getBytes(tag)?.toUTFString()
    }

    fun getLong(tag: Int): Long? {
        val keyValue = map[tag] ?: return null
        assert(keyValue.key.tag == tag.toLong())

        return if (keyValue.key.type != DataType.VAR_INT) null else (keyValue as VarIntValue).number
    }

    fun getBoolean(tag: Int): Boolean? {
        val long = getLong(tag) ?: return null
        return when (long) {
            1L -> true
            0L -> false
            else -> null
        }
    }

    fun toByteArray(): ByteArray {
        val outputStream = ByteArrayOutStream()
        write(outputStream)
        return outputStream.toByteArray()
    }

    fun write(outStream: OutStream) {
        for ((_, keyValue) in map)
            keyValue.write(outStream)
    }

    suspend fun write(outStream: AsyncOutStream) {
        for ((_, keyValue) in map)
            keyValue.write(outStream)
    }

    private fun readKeyValue(inStream: InStream): KeyValue? {
        val key = readKey(inStream) ?: return null
        if (key.type == DataType.VAR_INT)
            return VarIntValue.read(key, inStream)
        if (key.type == DataType.LENGTH_DELIMITED)
            return ByteValue.read(key ,inStream)
        throw IOException("Unknown data type: " + key.type.value)
    }

    private suspend fun readKeyValue(inStream: AsyncInStream): KeyValue? {
        val key = readKey(inStream) ?: return null
        if (key.type == DataType.VAR_INT)
            return VarIntValue.read(key, inStream)
        if (key.type == DataType.LENGTH_DELIMITED)
            return ByteValue.read(key ,inStream)
        throw IOException("Unknown data type: " + key.type.value)
    }

    /**
     * Read key value pairs till the end of stream is reached.
     *
     * @param inStream
     */
    private fun read(inStream: InStream) {
        map.clear()
        while (true) {
            val keyValue = readKeyValue(inStream) ?: break
            map[keyValue.key.tag.toInt()]?.let {
                if (it.key.tag != keyValue.key.tag)
                    throw Exception("Type missmatch in array")
                it.merge(keyValue)
            } ?: map.put(keyValue.key.tag.toInt(), keyValue)
        }
    }

    private suspend fun read(inStream: AsyncInStream) {
        map.clear()
        while (true) {
            val keyValue = readKeyValue(inStream) ?: break
            map[keyValue.key.tag.toInt()]?.let {
                if (it.key.tag != keyValue.key.tag)
                    throw Exception("Type missmatch in array")
                it.merge(keyValue)
            } ?: map.put(keyValue.key.tag.toInt(), keyValue)
        }
    }
}
