package org.fejoa.support

expect class DeflateOutStream(outStream: OutStream) : OutStream

expect class DeflateCompression : Compression