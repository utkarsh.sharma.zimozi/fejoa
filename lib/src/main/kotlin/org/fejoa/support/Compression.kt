package org.fejoa.support


interface Compression {
    fun Compress(data: ByteArray): Future<ByteArray>
    fun Decompress(data: ByteArray): Future<ByteArray>
}