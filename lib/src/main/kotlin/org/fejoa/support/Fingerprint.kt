package org.fejoa.support

interface Fingerprint : OutStream {
    val fingerprint: Long

    fun reset()
}