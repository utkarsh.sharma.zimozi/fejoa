package org.fejoa.support


class EOFException : Exception {
    constructor()
    constructor(message: String) : super(message)
}

class IOException : Exception {
    constructor()
    constructor(message: String) : super(message)
}

class FileNotFoundException : Exception {
    constructor()
    constructor(message: String) : super(message)
}