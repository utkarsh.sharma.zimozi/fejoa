package org.fejoa.support

expect class BigInteger {
    constructor(value: String)
    constructor(value: String, radix: Int)

    fun modPow(exp: BigInteger, mod: BigInteger): BigInteger
    fun add(value: BigInteger): BigInteger
    fun pow(value: Int): BigInteger
    fun subtract(value: BigInteger): BigInteger
    fun toString(radix: Int): String
    fun shiftRight(n: Int): BigInteger
    fun shiftLeft(n: Int): BigInteger
    override fun equals(other: Any?): Boolean
    fun compareTo(other: BigInteger): Int
    fun or(value: BigInteger): BigInteger
    fun and(value: BigInteger): BigInteger
    fun negate(): BigInteger
    fun setBit(bit: Int): BigInteger
    fun testBit(bit: Int): Boolean

    fun toLong(): Long
    fun toInt(): Int
}
