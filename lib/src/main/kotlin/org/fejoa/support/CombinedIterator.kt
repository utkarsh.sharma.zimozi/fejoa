package org.fejoa.support

class CombinedIterator<T> : AsyncIterator<T> {
    private val queue: MutableList<AsyncIterator<T>> = ArrayList()

    fun add(iterator: AsyncIterator<T>) {
        queue.add(iterator)
    }

    override suspend fun hasNext(): Boolean {
        while (!queue.isEmpty()) {
            if (queue[0].hasNext())
                return true
            else
                queue.removeAt(0)
        }
        return false
    }

    override suspend fun next(): T {
        return queue[0].next()
    }
}
