package org.fejoa.support


object DeflateUtils {
    fun deflate(data: ByteArray): ByteArray {
        val out = ByteArrayOutStream()
        val deflator = DeflateOutStream(out)
        deflator.write(data)
        deflator.close()
        return out.toByteArray()
    }

    fun inflate(data: ByteArray): ByteArray {
        val out = ByteArrayOutStream()
        val inflator = InflateOutStream(out)
        inflator.write(data)
        inflator.close()
        return out.toByteArray()
    }
}