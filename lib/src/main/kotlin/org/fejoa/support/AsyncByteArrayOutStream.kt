package org.fejoa.support


open class AsyncByteArrayOutStream : AsyncOutStream {
    private val output = ByteArrayOutStream()

    override suspend fun write(buffer: ByteArray, offset: Int, length: Int): Int {
        return output.write(buffer, offset, length)
    }

    override suspend fun flush() {
        output.flush()
    }

    override suspend fun close() {
        output.close()
    }

    fun toByteArray(): ByteArray {
        return output.toByteArray()
    }
}