package org.fejoa.support

expect class Random {
    constructor(seed: Long)
    constructor()

    fun readByte(): Byte
    fun read(buffer: ByteArray): Int
    fun readFloat(): Float
}