package org.fejoa.network

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.context.SimpleModule
import kotlinx.serialization.json.Json
import org.fejoa.storage.HashValue
import org.fejoa.storage.HashValueDataSerializer

val RPC_VERSION = "2.0"

@Serializable
class JsonRPCSimpleRequest(val jsonrpc: String = RPC_VERSION, val id: Int, val method: String) {
    fun <T>makeResponse(result: T): JsonRPCResponse<T> {
        return JsonRPCResponse(id, result)
    }
}

@Serializable
class JsonRPCRequest<T>(val jsonrpc: String = RPC_VERSION, val id: Int, val method: String, val params: T) {
    companion object {
        fun <T>parse(serializer: KSerializer<T>, json: String): JsonRPCRequest<T> {
            return Json.parse(JsonRPCRequest.serializer(serializer), json)
        }
    }

    fun stringify(serializer: KSerializer<T>): String {
        return Json.stringify(JsonRPCRequest.serializer(serializer), this)
    }

    fun <T>makeResponse(result: T): JsonRPCResponse<T> {
        return JsonRPCResponse(id, result)
    }
}


// only to parse the method name before a more detailed class can be parsed
@Serializable
class JsonRPCMethodRequest(val jsonrpc: String = RPC_VERSION, val id: Int, val method: String) {
    companion object {
        /**
         * Verifies the jsonrpc version and returns the method field
         */
        fun parse(jsonrpc: String): JsonRPCMethodRequest {
            val response = Json(strictMode = false).parse(JsonRPCMethodRequest.serializer(), jsonrpc)
            if (response.jsonrpc != RPC_VERSION)
                throw Exception("Invalid json rpc version: ${response.jsonrpc}")
            return response
        }
    }
}


@Serializable
class JsonRPCResponse<T>(val id: Int, val result: T, val jsonrpc: String = RPC_VERSION) {
    companion object {
        fun <T>parse(serializer: KSerializer<T>, json: String, expectedId: Int): JsonRPCResponse<T> {
            val response = Json.apply { install(SimpleModule(HashValue::class, HashValueDataSerializer)) }
                    .parse(JsonRPCResponse.serializer(serializer), json)
            if (response.id != expectedId)
                throw Exception("Id miss match. Expected: $expectedId, Actual: ${response.id}")
            return response
        }
    }

    fun stringify(serializer: KSerializer<T>): String {
        return Json.stringify(JsonRPCResponse.serializer(serializer), this)
    }
}


@Serializable
class JsonRPCError<T>(val id: Int?, val error: T, val jsonrpc: String = RPC_VERSION) {
    companion object {
        fun <T>parse(serializer: KSerializer<T>, json: String, expectedId: Int): JsonRPCError<T> {
            val response = Json.parse(JsonRPCError.serializer(serializer), json)
            if (response.id != expectedId)
                throw Exception("Id miss match. Expected: $expectedId, Actual: ${response.id}")
            return response
        }
    }

    fun stringify(serializer: KSerializer<T>): String {
        return Json.stringify(JsonRPCError.serializer(serializer), this)
    }
}
