package org.fejoa.crypto

import kotlinx.serialization.SerialId
import kotlinx.serialization.Serializable
import org.fejoa.protocolbufferlight.ProtocolBufferLight


class CryptoSettings private constructor() {
    enum class ASYM_TYPE {
        SIGNING,
        ENCRYPTION
    }

    enum class ASYM_ALGO(val javaName: String, val javaKey: String, val javaCurve: String = "-",
                         val jsName: String, val jsCurve: String = "-",
                         val hash: HASH_TYPE = HASH_TYPE.SHA256, val type: ASYM_TYPE) {
        // signing
        RSASSA_PKCS1_v1_5_SHA256("SHA256withRSA", "RSA", "-",
                "RSASSA-PKCS1-v1_5","-",
                HASH_TYPE.SHA256, ASYM_TYPE.SIGNING),
        ECDSA_SECP256R1_SHA256("SHA256withECDSA", "ECDSA", "secp256r1",
                "ECDSA", "P-256",
                HASH_TYPE.SHA256, ASYM_TYPE.SIGNING),

        // encryption
        //ECIES("ECIES", "-"),
        RSA_OAEP_SHA256("RSA/NONE/OAEPWithSHA256AndMGF1Padding", "RSA", "-",
                "RSA-OAEP", "-",
                HASH_TYPE.SHA256, ASYM_TYPE.ENCRYPTION),
    }

    enum class SYM_ALGO(val javaName: String, val javaKey: String, val jsName: String, val ivSize: Int) {
        AES_CTR("AES/CTR/NoPadding", "AES", "AES-CTR", 16 * 8)
    }

    enum class HASH_TYPE(val jsName: String) {
        SHA256("SHA-256")
    }

    enum class KDF_ALGO(val javaName: String, val jsName: String, val hash: HASH_TYPE) {
        PBKDF2_SHA256("PBKDF2WithHmacSHA256", "PBKDF2", HASH_TYPE.SHA256)
    }

    enum class PUBLIC_EXPONENT(val javaValue: Int, val jsValue: Array<Byte>) {
        // Fermat numbers:
        F0(3, arrayOf(3.toByte())),
        F4(65537, arrayOf(1.toByte(), 0.toByte(), 1.toByte()))
    }

    class Symmetric(var algo: SYM_ALGO = SYM_ALGO.AES_CTR, val keySize: Int = 32 * 8) {
        companion object {
            const val ALGO_TAG = 0
            const val KEY_SIZE_TAG = 1

            fun read(byteArray: ByteArray): Symmetric {
                val buffer = ProtocolBufferLight(byteArray)
                val algoValue = buffer.getString(ALGO_TAG) ?: throw Exception("Missing algo field")
                val algo = SYM_ALGO.values().firstOrNull { it.name == algoValue }
                        ?: throw Exception("Unknown algo $algoValue")
                val keySize = buffer.getLong(KEY_SIZE_TAG) ?: throw Exception("Missing key size field")
                return Symmetric(algo, keySize.toInt())
            }
        }
        fun toProtcolBuffer(): ProtocolBufferLight {
            val buffer = ProtocolBufferLight()
            buffer.put(ALGO_TAG, algo.name)
            buffer.put(KEY_SIZE_TAG, keySize)
            return buffer
        }
    }

    @Serializable
    data class KDFParams(@SerialId(id = 0) var iterations: Int = -1)

    @Serializable
    data class KDF(
            @SerialId(id = 0)
            var algo: KDF_ALGO = KDF_ALGO.PBKDF2_SHA256,
            @SerialId(id = 1)
            var params: KDFParams = KDFParams(),
            @SerialId(id = 2)
            var keySize: Int = -1,
            @SerialId(id = 3)
            var keyAlgo: SYM_ALGO = SYM_ALGO.AES_CTR
    )

    interface Asymmetric {
        val algo: ASYM_ALGO
        companion object {
            const val ALGO_TAG = 0

            fun read(byteArray: ByteArray): Asymmetric {
                val buffer = ProtocolBufferLight(byteArray)
                val algoValue = buffer.getString(Asymmetric.ALGO_TAG) ?: throw Exception("Missing algo field")
                val algo = ASYM_ALGO.values().firstOrNull { it.name == algoValue }
                        ?: throw Exception("Unknown algo $algoValue")
                return when (algo) {
                    ASYM_ALGO.RSASSA_PKCS1_v1_5_SHA256,
                    ASYM_ALGO.RSA_OAEP_SHA256 -> RSAAsymmetric.read(algo, buffer)
                    ASYM_ALGO.ECDSA_SECP256R1_SHA256 -> ECAsymmetric(ASYM_ALGO.ECDSA_SECP256R1_SHA256)
                }
            }
        }
        fun toProtocolBuffer(): ProtocolBufferLight
    }

    class RSAAsymmetric(override val algo: ASYM_ALGO = ASYM_ALGO.RSASSA_PKCS1_v1_5_SHA256,
                        val modulusLength: Int = 2048,
                        val publicExponent: PUBLIC_EXPONENT = PUBLIC_EXPONENT.F4): Asymmetric {
        companion object {
            const val MODULUS_LENGTH_TAG = 1
            const val PUBLIC_EXPONENT_TAG = 2

            fun read(algo: ASYM_ALGO, buffer: ProtocolBufferLight): RSAAsymmetric {
                val modulusLength = buffer.getLong(MODULUS_LENGTH_TAG)
                        ?: throw Exception("Missing modulusLength field")
                val expValue = buffer.getString(PUBLIC_EXPONENT_TAG)
                        ?: throw Exception("Missing publicExponent field")
                val publicExponent = PUBLIC_EXPONENT.values().firstOrNull { it.name == expValue }
                        ?: throw Exception("Unknown publicExponent $expValue")
                return RSAAsymmetric(algo, modulusLength.toInt(), publicExponent)
            }
        }

        override fun toProtocolBuffer(): ProtocolBufferLight {
            val buffer = ProtocolBufferLight()
            buffer.put(Asymmetric.ALGO_TAG, algo.name)
            buffer.put(MODULUS_LENGTH_TAG, modulusLength)
            buffer.put(PUBLIC_EXPONENT_TAG, publicExponent.name)
            return buffer
        }
    }

    class ECAsymmetric(override val algo: ASYM_ALGO = ASYM_ALGO.ECDSA_SECP256R1_SHA256): Asymmetric {
        override fun toProtocolBuffer(): ProtocolBufferLight {
            val buffer = ProtocolBufferLight()
            buffer.put(Asymmetric.ALGO_TAG, algo.name)
            return buffer
        }
    }


    var kdf = KDF()
    var asymmetric: Asymmetric = RSAAsymmetric()
    var signature: Asymmetric = RSAAsymmetric()
    var symmetric = Symmetric()

    companion object {
        fun getAES_CTR(): CryptoSettings.Symmetric = Symmetric(SYM_ALGO.AES_CTR, 32 * 8)
        fun getECDSA_SECP256R1_SHA256(): CryptoSettings.Asymmetric = ECAsymmetric(ASYM_ALGO.ECDSA_SECP256R1_SHA256)
        fun getRSA_OAEP_SHA256(): CryptoSettings.Asymmetric
                = RSAAsymmetric(ASYM_ALGO.RSA_OAEP_SHA256, 2048, PUBLIC_EXPONENT.F4)
        fun getRSASSA_PKCS1_v1_5_SHA256(): CryptoSettings.Asymmetric
                = RSAAsymmetric(ASYM_ALGO.RSASSA_PKCS1_v1_5_SHA256,2048, PUBLIC_EXPONENT.F4)

        fun setDefaultEC(cryptoSettings: CryptoSettings): CryptoSettings {
            cryptoSettings.signature = getECDSA_SECP256R1_SHA256()
            return cryptoSettings
        }

        fun setDefaultRSA(cryptoSettings: CryptoSettings): CryptoSettings {
            cryptoSettings.asymmetric = getRSA_OAEP_SHA256()

            cryptoSettings.signature = getRSASSA_PKCS1_v1_5_SHA256()

            return cryptoSettings
        }

        val default: CryptoSettings
            get() {
                val cryptoSettings = CryptoSettings()

                setDefaultRSA(cryptoSettings)

                cryptoSettings.symmetric = getAES_CTR()

                cryptoSettings.kdf = KDF(KDF_ALGO.PBKDF2_SHA256, KDFParams(100000), 32 * 8,
                        SYM_ALGO.AES_CTR)

                return cryptoSettings
            }
    }
}
