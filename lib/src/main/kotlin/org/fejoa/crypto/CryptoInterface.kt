package org.fejoa.crypto

import org.fejoa.support.*


interface CryptoInterface {
    suspend fun deriveKey(secret: String, salt: ByteArray, settings: CryptoSettings.KDF) : SecretKey

    suspend fun generateKeyPair(settings: CryptoSettings.Asymmetric): KeyPair
    suspend fun generateSymmetricKey(settings: CryptoSettings.Symmetric): SecretKey

    fun generateBits(sizeInBits: Int): ByteArray {
        return generateBytes(sizeInBits / 8)
    }

    fun generateBytes(sizeInBytes: Int): ByteArray {
        val buffer = ByteArray(sizeInBytes)
        val random = Random()
        random.read(buffer)
        return buffer
    }

    fun generateSalt16(): ByteArray {
        return generateBits(16 * 8)
    }

    fun generateSalt(): ByteArray {
        return generateBits(32 * 8)
    }

    suspend fun encryptAsymmetric(input: ByteArray, key: PublicKey, algo: CryptoSettings.ASYM_ALGO): ByteArray
    suspend fun decryptAsymmetric(input: ByteArray, key: PrivateKey, algo: CryptoSettings.ASYM_ALGO): ByteArray

    suspend fun encryptSymmetric(input: ByteArray, secretKey: SecretKey, iv: ByteArray,
                                 algo: CryptoSettings.SYM_ALGO): ByteArray
    suspend fun decryptSymmetric(input: ByteArray, secretKey: SecretKey, iv: ByteArray,
                                 algo: CryptoSettings.SYM_ALGO): ByteArray

    fun encryptSymmetricAsync(input: ByteArray, secretKey: SecretKey, iv: ByteArray,
                              algo: CryptoSettings.SYM_ALGO): Future<ByteArray>
    fun decryptSymmetricAsync(input: ByteArray, secretKey: SecretKey, iv: ByteArray,
                              algo: CryptoSettings.SYM_ALGO): Future<ByteArray>

    suspend fun encryptSymmetric(input: ByteArray, credentials: SecretKeyIvData): ByteArray {
        return encryptSymmetric(input, credentials.key, credentials.iv, credentials.algo)
    }
    suspend fun decryptSymmetric(input: ByteArray, credentials: SecretKeyIvData): ByteArray {
        return decryptSymmetric(input, credentials.key, credentials.iv, credentials.algo)
    }

    suspend fun encode(key: Key): ByteArray

    suspend fun secretKeyFromRaw(key: ByteArray, algo: CryptoSettings.SYM_ALGO): SecretKey
    suspend fun privateKeyFromRaw(key: ByteArray, algo: CryptoSettings.ASYM_ALGO): PrivateKey
    suspend fun publicKeyFromRaw(key: ByteArray, algo: CryptoSettings.ASYM_ALGO): PublicKey

    suspend fun sign(input: ByteArray, key: PrivateKey, algo: CryptoSettings.ASYM_ALGO): ByteArray
    suspend fun verifySignature(message: ByteArray, signature: ByteArray, key: PublicKey,
                                algo: CryptoSettings.ASYM_ALGO): Boolean
}
