package org.fejoa.crypto

interface Key

interface PublicKey : Key  {
    val algo: CryptoSettings.ASYM_ALGO
}

interface PrivateKey: Key {
    val algo: CryptoSettings.ASYM_ALGO
}

interface SecretKey: Key {
    val algo: CryptoSettings.SYM_ALGO
}
