package org.fejoa.storage

import kotlinx.serialization.*
import kotlinx.serialization.internal.StringDescriptor


@Serializer(forClass = HashValue::class)
object HashValueDataSerializer : KSerializer<HashValue> {
    override val descriptor: SerialDescriptor = StringDescriptor.withName("org.fejoa.storage.HashValue")

    override fun serialize(encoder: Encoder, obj: HashValue) {
        encoder.encodeString(obj.toHex())
    }

    override fun deserialize(decoder: Decoder): HashValue {
        return HashValue.fromHex(decoder.decodeString())
    }
}