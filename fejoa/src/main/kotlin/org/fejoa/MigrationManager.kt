package org.fejoa

import kotlinx.serialization.json.Json
import org.fejoa.command.MigrationCommand
import org.fejoa.command.post
import org.fejoa.network.RemotePullJob
import org.fejoa.network.ReturnType
import org.fejoa.network.StartMigrationJob
import org.fejoa.repository.sync.AccessRight
import org.fejoa.support.await


class MigrationManager(val client: Client) {
    // Must not be called from the looper
    suspend fun migrate(oldRemote: Remote, newRemote: Remote) {
        val branchesToMigrate: MutableList<Branch> = ArrayList()
        val accessList = client.withLooper {
            userData.getBranches().filter {
                it.locations.listRemoteRefs().find { it.remoteId == oldRemote.id } != null
            }.associate {
                branchesToMigrate += it
                it.branch.toHex() to AccessRight.PULL_CHUNK_STORE
            }.toMutableMap()
        }
        val builder = TokenBuilder.create(AccessToken.create(accessList))
        val serverToken = builder.getServerToken()
        val contactToken = builder.getContactToken()

        val result = client.connectionAuthManager.send(
                StartMigrationJob(oldRemote.user, serverToken), oldRemote, LoginAuthInfo()).await()
        if (result.code != ReturnType.OK)
            throw Exception(result.message)

        for (branch in branchesToMigrate) {
            val remoteJobResult = client.connectionAuthManager.send(RemotePullJob(RemotePullJob.Params(
                    newRemote.user, branch.branch.toHex(), oldRemote, contactToken)), newRemote, LoginAuthInfo())
                    .await()
            if (remoteJobResult.code != ReturnType.OK)
                throw Exception(remoteJobResult.message)
        }

        // notify contacts
        client.withLooper {
            userData.contacts.list.listContacts().forEach { contact ->
                val command = MigrationCommand(userData.id.get(), newRemote)
                getOutQueueManagers().firstOrNull()?.outQueue?.post(
                        Json.stringify(MigrationCommand.serializer(), command), userData, contact)
                        ?: throw Exception("Missing out queue")
            }
        }
    }
}