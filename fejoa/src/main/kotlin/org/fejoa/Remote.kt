package org.fejoa

import kotlinx.serialization.SerialId
import kotlinx.serialization.Serializable
import org.fejoa.crypto.CryptoHelper


@Serializable
data class Remote(@SerialId(0) val id: String, @SerialId(1) val user: String,
                  @SerialId(2) val server: String) {
    companion object {
        suspend fun create(user: String, server: String): Remote {
            return Remote(CryptoHelper.generateSha256Id().toHex(), user, server)
        }
    }

    fun address(): String {
        return "$user@$server"
    }
}
