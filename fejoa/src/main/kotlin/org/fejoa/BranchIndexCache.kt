package org.fejoa

import org.fejoa.network.RemoteIndexJob
import org.fejoa.network.ReturnType
import org.fejoa.support.Future


class BranchIndexCache(val context: FejoaContext, val authManager: ConnectionAuthManager) {
    // remote id -> RemoteIndexJob.Result
    private val cache: MutableMap<String, RemoteIndexJob.Result> = HashMap()

    fun getRemoteIndexBranch(remote: Remote): Future<RemoteIndexJob.Result> {
        cache[remote.id]?.let { return Future.completedFuture(it) }

        val authInfo = LoginAuthInfo()
        val job = authManager.send(RemoteIndexJob(remote.user), remote, authInfo)
        job.whenCompleted { result, _ ->
            if (result != null && result.code == ReturnType.OK)
                cache[remote.id] = result
        }

        return job
    }

    fun invalidateRemote(remoteId: String) {
        cache.remove(remoteId)
    }
}
