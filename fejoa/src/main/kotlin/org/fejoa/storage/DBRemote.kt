package org.fejoa.storage

import kotlinx.serialization.json.Json
import org.fejoa.Remote
import org.fejoa.support.PathUtils


class RemoteDBValue(parent: DBObject, relativePath: String) : DBValue<Remote>(parent, relativePath) {
    override suspend fun write(obj: Remote) {
        dir.writeString(path, Json.stringify(Remote.serializer(), obj))
    }

    override suspend fun get(): Remote {
        return Json.parse(Remote.serializer(), dir.readString(path))
    }
}

class DBMapRemote(dir: IOStorageDir, path: String) : DBMap<HashValue, RemoteDBValue>(dir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listFiles(path)
    }

    /**
     * @param key remoteID
     */
    override fun get(key: HashValue): RemoteDBValue {
        return RemoteDBValue(this, key.toHex())
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }

    suspend fun listRemotes(): Collection<Remote> {
        return list().map { get(HashValue.fromHex(it)).get() }
    }
}
