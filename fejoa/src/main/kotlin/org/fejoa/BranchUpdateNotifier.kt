package org.fejoa

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.fejoa.command.BranchUpdateCommand
import org.fejoa.command.post
import org.fejoa.network.RemoteIndexJob
import org.fejoa.network.ReturnType
import org.fejoa.repository.Repository
import org.fejoa.storage.*
import org.fejoa.support.Future
import org.fejoa.support.PathUtils
import org.fejoa.support.await


interface NotifyStatus {
    enum class StatusType {
        LAST_NOTIFIED,
        WAIT_FOR_ACCESS_STORE
    }

    val type: StatusType

    fun toJson(): String

    @Serializable
    class NotifyStatusParser(val type: StatusType)

    companion object {
        fun parse(value: String): NotifyStatus {
            val typeParser = Json(strictMode = false).parse(NotifyStatusParser.serializer(), value)
            return when (typeParser.type) {
                NotifyStatus.StatusType.LAST_NOTIFIED -> Json.parse(LastNotified.serializer(), value)
                NotifyStatus.StatusType.WAIT_FOR_ACCESS_STORE -> Json.parse(WaitForAccessStore.serializer(), value)
            }
        }
    }
}


@Serializable
class LastNotified(@Serializable(with = HashValueDataSerializer::class) val lastNotifiedHead: HashValue,
                   override val type: NotifyStatus.StatusType = NotifyStatus.StatusType.LAST_NOTIFIED)
    : NotifyStatus {
    override fun toJson(): String {
        return Json.stringify(LastNotified.serializer(), this)
    }
}

@Serializable
class WaitForAccessStore(@Serializable(with = HashValueDataSerializer::class) val accessStore: HashValue,
                         override val type: NotifyStatus.StatusType = NotifyStatus.StatusType.WAIT_FOR_ACCESS_STORE)
    : NotifyStatus {
    override fun toJson(): String {
        return Json.stringify(WaitForAccessStore.serializer(), this)
    }
}

class DBNotifyStatus(dir: IOStorageDir, path: String) : DBValue<NotifyStatus>(dir, path) {
    constructor(parent: DBObject, relativePath: String)
            : this(parent.dir, PathUtils.appendDir(parent.path, relativePath))

    override suspend fun write(obj: NotifyStatus) {
        dir.writeString(path, obj.toJson())
    }

    override suspend fun get(): NotifyStatus {
        return NotifyStatus.parse(dir.readString(path))
    }
}

class DBMapContactNotifyStatus(val storageDir: StorageDir, path: String) : DBMap<HashValue, DBNotifyStatus>(storageDir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listFiles(path)
    }

    /**
     * @param key contacID
     */
    override fun get(key: HashValue): DBNotifyStatus {
        return DBNotifyStatus(storageDir, PathUtils.appendDir(path, key.toHex()))
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }
}

class DBMapBranchContact(val storageDir: StorageDir, path: String) : DBMap<HashValue, DBMapContactNotifyStatus>(storageDir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listDirectories(path)
    }

    /**
     * @param key branch
     */
    override fun get(key: HashValue): DBMapContactNotifyStatus {
        return DBMapContactNotifyStatus(storageDir, PathUtils.appendDir(path, key.toHex()))
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }
}

class DBMapContextBranch(val storageDir: StorageDir, path: String) : DBMap<String, DBMapBranchContact>(storageDir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listDirectories(path)
    }

    /**
     * @param key context
     */
    override fun get(key: String): DBMapBranchContact {
        return DBMapBranchContact(storageDir, PathUtils.appendDir(path, key))
    }

    override suspend fun remove(key: String) {
        dir.remove(PathUtils.appendDir(path, key))
    }
}

class DBMapRemoteContext(val storageDir: StorageDir, path: String) : DBMap<HashValue, DBMapContextBranch>(storageDir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listDirectories(path)
    }

    /**
     * @param key remoteId
     */
    override fun get(key: HashValue): DBMapContextBranch {
        return DBMapContextBranch(storageDir, PathUtils.appendDir(path, key.toHex()))
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }
}

class NotificationStore(storageDir: StorageDir): StorageDirObject(storageDir) {
    interface Listener {
        fun onStatusUpdated(remote: Remote, branch: HashValue)
    }

    // remotes
    //      branches
    //          contacts -> status
    var remotes = DBMapRemoteContext(storageDir, "remotes")

    val listeners: MutableList<Listener> = ArrayList()

    suspend fun getNotifyStatus(remote: Remote, context: String, branch: HashValue, contactId: HashValue): NotifyStatus? {
        val value = remotes.get(HashValue.fromHex(remote.id)).get(context).get(branch).get(contactId)
        if (!value.exists())
            return null
        return value.get()
    }

    suspend fun updateNotifyStatus(remote: Remote, context: String, branch: HashValue, contactId: HashValue, status: NotifyStatus) {
        val value = remotes.get(HashValue.fromHex(remote.id)).get(context).get(branch).get(contactId)
        value.write(status)
        commit()
        listeners.forEach { it.onStatusUpdated(remote, branch) }
    }
}

class BranchUpdateNotifier private constructor(val client: Client, private val notifyStatusStorage: StorageDir) {
    data class EventData(val message: String, val remote: Remote): Logger.EventData

    companion object {
        suspend fun create(client: Client): BranchUpdateNotifier {
            val userData: UserData = client.userData
            val notifyStatusStorage = client.context.getNotifyStatusStorage(userData.masterKey, userData.storageDir.getCommitSignature())
            return BranchUpdateNotifier(client, notifyStatusStorage)
        }
    }

    val notificationStore = NotificationStore(notifyStatusStorage)
    private var started = false
    private var fetchBranchIndexJobs: MutableList<Pair<Remote, Future<RemoteIndexJob.Result>>> = ArrayList()
    private val watchList: MutableList<Pair<StorageDir, StorageDir.Listener>> = ArrayList()

    init {
        // Listen to changes in the NotificationStore, e.g. to detect when an existing branch has been added.
        notificationStore.listeners += object : NotificationStore.Listener {
            override fun onStatusUpdated(remote: Remote, branch: HashValue) {
                GlobalScope.async {
                    val result = client.branchIndexCache.getRemoteIndexBranch(remote).await()
                    client.context.looper.schedule {
                        val remoteIndexDir = client.context.getRemoteIndexStorage(remote.id, result.branch)
                        val remoteBranchIndex = BranchIndex(remoteIndexDir)
                        work(remote, remoteBranchIndex)
                    }
                }
            }
        }
    }

    inner class BranchIndexListener(val remote: Remote, val branchIndex: BranchIndex) : StorageDir.Listener {
        override suspend fun onAfterCommit(base: HashValue, newTip: HashValue) {
            client.context.looper.schedule {
                work(remote, branchIndex)
            }
        }
    }

    private suspend fun postUpdate(branchRemote: Remote, contactId: HashValue, branch: Branch,
                                   branchHead: HashValue) {
        val contact = client.userData.contacts.list.listContacts().firstOrNull { it.id.get() == contactId } ?: run {
            reportError(EventData("Can't find contact $contactId", branchRemote))
            return
        }

        val userData = client.userData
        val command = BranchUpdateCommand(contactId, userData.id.get(), branchRemote.id, branch.branch)
        client.getOutQueueManagers().firstOrNull()?.outQueue?.post(Json.stringify(BranchUpdateCommand.serializer(),
                command), userData, contact)
                ?: throw Exception("Missing out queue")

        notificationStore.updateNotifyStatus(branchRemote, branch.context, branch = branch.branch,
                contactId = contactId, status = LastNotified(branchHead))
    }

    private suspend fun isUpdated(lastNotified: LastNotified, branch: StorageDir): Boolean {
        val localHead = branch.getHead() ?: return true
        return localHead.value == lastNotified.lastNotifiedHead
    }

    private suspend fun isSynced(branch: StorageDir, remoteBranchIndex: BranchIndex): Boolean {
        val remoteIndexContent = remoteBranchIndex.list()
        val remoteLogTip = remoteIndexContent[branch.branch] ?: return false

        val repo = branch.getBackingDatabase() as Repository
        val localBranchLogTip = repo.branchBackend.getBranchLog().getHead()?.entryId ?: return false

        return remoteLogTip == localBranchLogTip
    }

    /**
     * To be called from the looper
     */
    private suspend fun work(remote: Remote, remoteBranchIndex: BranchIndex) {
        val contextBranchMap = notificationStore.remotes.get(HashValue.fromHex(remote.id))
        contextBranchMap.list().forEach { branchContext ->
            val branchContactMap = contextBranchMap.get(branchContext)
            branchContactMap.list().forEach { branchHex ->
                val branch = HashValue.fromHex(branchHex)
                val contactStatusMap = branchContactMap.get(branch)
                for (contactHex in contactStatusMap.list()) {
                    val contact = HashValue.fromHex(contactHex)
                    val status = contactStatusMap.get(contact).get()

                    val localBranch = client.userData.getBranch(branchContext, branch)
                    val localStorageDir = client.userData.getStorageDir(localBranch)
                    if (!isSynced(localStorageDir, remoteBranchIndex))
                        continue

                    val updateHead = localStorageDir.getHead()?.value ?: Config.newDataHash()
                    when (status) {
                        is LastNotified -> {
                            if (!isUpdated(status, localStorageDir))
                                postUpdate(remote, contactId = contact, branch = localBranch, branchHead = updateHead)
                        }
                        is WaitForAccessStore -> {
                            client.userData.getAccessStores().firstOrNull { it.branch == status.accessStore }?.let {
                                val storageDir = client.userData.getStorageDir(it)
                                if (isSynced(storageDir, remoteBranchIndex))
                                    postUpdate(remote, contactId = contact, branch = localBranch, branchHead = updateHead)
                            }
                        }
                    }
                }
            }
        }
    }

    private suspend fun startWatching(remote: Remote, remoteBranchIndex: BranchIndex) {
        val listener = BranchIndexListener(remote, remoteBranchIndex)
        remoteBranchIndex.storageDir.addListener(listener)
        watchList.add(remoteBranchIndex.storageDir to listener)

        work(remote, remoteBranchIndex)
    }

    private fun stopWatching() {
        watchList.forEach { it.first.removeListener(it.second) }
        watchList.clear()
    }

    private fun reportInfo(event: EventData) {
        client.logger.onInfo(Logger.Event(Logger.TYPE.BRANCH_UPDATE_NOTIFIER, event))
    }

    private fun reportError(event: EventData) {
        client.logger.onError(Logger.Event(Logger.TYPE.BRANCH_UPDATE_NOTIFIER, event))
    }

    suspend fun start(list: Collection<Remote>) {
        if (started)
            return
        started = true

        val jobs = list.map {
            it to client.branchIndexCache.getRemoteIndexBranch(it)
        }
        fetchBranchIndexJobs = jobs.toMutableList()

        for (pair in jobs) {
            GlobalScope.launch(client.context.coroutineContext) {
                val result = pair.second.await()
                fetchBranchIndexJobs.remove(pair)
                if (result.code != ReturnType.OK) {
                    reportError(EventData(result.message, pair.first))
                    return@launch
                }

                // back to the looper
                client.context.looper.schedule {
                    val remoteIndexDir = client.context.getRemoteIndexStorage(pair.first.id, result.branch)
                    val remoteBranchIndex = BranchIndex(remoteIndexDir)
                    startWatching(pair.first, remoteBranchIndex)
                }
            }
        }
    }

    fun stop() {
        if (!started)
            return
        started = false

        fetchBranchIndexJobs.forEach {
            it.second.cancel()
        }
        fetchBranchIndexJobs.clear()

        stopWatching()
    }
}