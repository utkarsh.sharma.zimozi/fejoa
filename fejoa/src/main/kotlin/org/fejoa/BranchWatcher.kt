package org.fejoa

import org.fejoa.network.SyncManager
import org.fejoa.storage.HashValue
import org.fejoa.storage.StorageDir

/**
 * Watches when branches needs to be synchronized
 *
 * To do so it watches the remote branch index.
 */
class BranchWatcher private constructor(val userData: UserData, val syncManager: SyncManager,
                                        val branchIndexCache: BranchIndexCache, var logger: Logger = DevNullLogger()) {

    private val context = userData.context
    private val remoteWorkers: MutableMap<String, RemoteIndexWatcher> = HashMap()
    private var syncingRemotes: Collection<Remote>? = null

    class DirtyBranch(val remote: Remote, val branch: Branch, val localTipId: HashValue?, val remoteTipId: HashValue?)

    // remote id -> list of dirty branches
    var dirtyBranches: MutableMap<String, MutableList<DirtyBranch>> = HashMap()
        private set
    val listeners: MutableList<Listener> = ArrayList()

    interface Listener {
        /**
         * Called from the event loop
         */
        suspend fun onUpdate(dirtyBranch: DirtyBranch) {}
        suspend fun onDirtyBranchListUpdated(remote: Remote) {}
    }

    fun isSyncing(): Boolean {
        return syncingRemotes != null
    }

    companion object {
        suspend fun create(syncManager: SyncManager, userData: UserData, branchIndexCache: BranchIndexCache,
                           logger: Logger = DevNullLogger()): BranchWatcher {
            val syncer = BranchWatcher(userData, syncManager, branchIndexCache, logger)
            val branchIndex = userData.context.getLocalIndexStorage()
            branchIndex.addListener(object : StorageDir.Listener {
                override suspend fun onAfterCommit(base: HashValue, newTip: HashValue) {
                    if (base != newTip) {
                        syncer.restartSyncing()
                    }
                }
            })
            return syncer
        }
    }

    private suspend fun notifyDirtyBranch(remote: Remote, dirtyBranch: DirtyBranch) {
        /*
        TODO: find out why this is not working (server tests fail)
        val dirtyBranchList = dirtyBranches[remote.id] ?: return
        // Don't notify if the entry is already in the list
        val exist = dirtyBranchList.find { it.remote.id == dirtyBranch.remote.id
                && it.branch.branch == dirtyBranch.branch.branch
                && it.localTipId == dirtyBranch.localTipId
                && it.remoteTipId == dirtyBranch.remoteTipId
        } != null
        if (exist)
            return
            */
        listeners.forEach { it.onUpdate(dirtyBranch) }
    }

    private suspend fun notifyDirtyBranchListUpdated(remote: Remote) {
        listeners.forEach { it.onDirtyBranchListUpdated(remote) }
    }

    private fun onRemoteIndexChanged(remote: Remote, remoteIndexDir: StorageDir) {
        context.looper.schedule {
            try {
                val branches: MutableList<DirtyBranch> = ArrayList()

                val localIndex: MutableMap<String, HashValue> = HashMap(BranchIndex(
                        userData.context.getLocalIndexStorage()).list())
                val remoteIndex = BranchIndex(remoteIndexDir).list()
                // only look at branches that are located at the remote
                val userDataBranches = userData.getBranches().filter { branch ->
                    branch.locations.listRemoteRefs().firstOrNull { it.remoteId == remote.id } != null
                }

                for (entry in remoteIndex) {
                    val remoteBranch = entry.key
                    val localLogHead = localIndex.remove(remoteBranch)
                    if (localLogHead == entry.value)
                        continue

                    for (branch in userDataBranches.filter { it.branch.toHex() == remoteBranch }) {
                        val dirtyBranch = DirtyBranch(remote, branch, localLogHead, entry.value)
                        branches += dirtyBranch
                        notifyDirtyBranch(remote, dirtyBranch)
                    }
                }

                // check for local branches that are not at the remote yet
                for (entry in localIndex) {
                    val localBranch = entry.key
                    for (branch in userDataBranches.filter { it.branch.toHex() == localBranch }) {
                        val dirtyBranch = DirtyBranch(remote, branch, entry.value, null)
                        branches += dirtyBranch
                        notifyDirtyBranch(remote, dirtyBranch)
                    }
                }

                dirtyBranches[remote.id] = branches

                notifyDirtyBranchListUpdated(remote)
            } catch (e: Throwable) {
                logger.syncError(SyncEvent.error(SyncEvent.BranchType.BRANCH, "", "", e.message
                        ?: "Exception while syncing"))
            }
        }
    }

    fun startWatching(remotes: Collection<Remote>) {
        stopWatching()

        remotes.forEach { remote ->
            val worker = RemoteIndexWatcher(userData.context, syncManager, remote, branchIndexCache, logger,
                    this::onRemoteIndexChanged)
            remoteWorkers[remote.id] = worker
            worker.start()
        }

        syncingRemotes = remotes
    }

    fun stopWatching() {
        syncingRemotes = null

        remoteWorkers.forEach { it.value.stop() }
        remoteWorkers.clear()
    }

    fun restartSyncing() {
        syncingRemotes?.let { startWatching(it) }
    }
}