package org.fejoa

import kotlinx.serialization.json.Json
import org.fejoa.storage.*
import org.fejoa.support.PathUtils


class DBServerToken(dir: IOStorageDir, path: String) : DBValue<ServerToken>(dir, path) {
    constructor(parent: DBObject, relativePath: String)
            : this(parent.dir, PathUtils.appendDir(parent.path, relativePath))

    override suspend fun write(obj: ServerToken) {
        dir.writeString(path, Json.stringify(ServerToken.serializer(), obj))
    }

    override suspend fun get(): ServerToken {
        return Json.parse(ServerToken.serializer(), dir.readString(path))
    }
}

class DBMapServerToken(dir: IOStorageDir, path: String) : DBMap<HashValue, DBServerToken>(dir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listFiles(path)
    }

    override fun get(key: HashValue): DBServerToken {
        return DBServerToken(this, key.toHex())
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }

    suspend fun listToken(): Collection<ServerToken> {
        return list().map { get(HashValue.fromHex(it)).get() }
    }
}

class AccessStore(storageDir: StorageDir): StorageDirObject(storageDir) {
    val token = DBMapServerToken(storageDir, "tokens")
}