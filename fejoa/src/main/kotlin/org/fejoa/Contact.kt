package org.fejoa

import kotlinx.serialization.Optional
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.fejoa.storage.*
import org.fejoa.support.PathUtils


class Contact(val storageDir: StorageDir) {
    class Keys(val storageDir: StorageDir) {
        val verificationKeys = DBMapJsonPublicKeyData(storageDir, "verification")
        val asymEncKeys = DBMapJsonPublicKeyData(storageDir, "asymEnc")
    }

    val id = DBHashValue(storageDir, "id")
    val remotes = DBMapRemote(storageDir, "remotes")
    val branches = DBContextBranchMap(storageDir, "branches")
    val keys = Keys(StorageDir(storageDir, "keys"))
}

class Contacts(val storageDir: StorageDir) {
    val list = DBMapContact(storageDir, "list")
    val requested = DBMapContactRequested(storageDir, "requested")
}


@Serializable
class ContactRequest(val remote: Remote, @Optional val verificationKey: JsonPublicKeyData? = null,
                     @Optional val asymEncKey: JsonPublicKeyData? = null)


class DBRequest(dir: IOStorageDir, path: String) : DBValue<ContactRequest>(dir, path) {
    constructor(parent: DBObject, relativePath: String)
            : this(parent.dir, PathUtils.appendDir(parent.path, relativePath))

    override suspend fun write(obj: ContactRequest) {
        dir.writeString(path, Json.stringify(ContactRequest.serializer(), obj))
    }

    override suspend fun get(): ContactRequest {
        return Json.parse(ContactRequest.serializer(), dir.readString(path))
    }
}

class DBMapContactRequested(dir: IOStorageDir, path: String) : DBMap<HashValue, DBRequest>(dir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listFiles(path)
    }

    override fun get(key: HashValue): DBRequest {
        return DBRequest(dir, PathUtils.appendDir(path, key.toHex()))
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }

    suspend fun listRequests(): Collection<Pair<HashValue, ContactRequest>> {
        return list().map { HashValue.fromHex(it) to get(HashValue.fromHex(it)).get() }
    }
}

class DBMapContact(val storageDir: StorageDir, path: String) : DBMap<HashValue, Contact>(storageDir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listDirectories(path)
    }

    override fun get(key: HashValue): Contact {
        return Contact(StorageDir(storageDir, PathUtils.appendDir(path, key.toHex())))
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }

    suspend fun listContacts(): Collection<Contact> {
        return list().map { get(HashValue.fromHex(it)) }
    }
}
