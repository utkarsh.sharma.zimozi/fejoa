package org.fejoa

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.fejoa.network.ReturnType
import org.fejoa.network.SyncManager
import org.fejoa.support.Future
import org.fejoa.support.await


data class SyncEvent(val branchType: BranchType, val status: Status, val context: String, val branch: String,
                     val remote: String, val error: String = ""): Logger.EventData {
    enum class BranchType {
        BRANCH,
        REMOTE_INDEX
    }

    enum class Status {
        SYNCING,
        SYNCED,
        TIMEOUT,
        WATCH_RESULT,
        ERROR
    }

    companion object {
        fun error(branchType: BranchType, context: String, branch: String, error: String): SyncEvent {
            return SyncEvent(branchType, Status.ERROR, context, branch, "", error)
        }

        fun error(branchType: BranchType, branch: Branch, error: String): SyncEvent {
            return error(branchType, branch.context, branch.branch.toHex(), error)
        }

        fun timeout(branchType: BranchType, context: String, branch: String, remote: Remote): SyncEvent {
            return SyncEvent(branchType, Status.TIMEOUT, context, branch, remote.id)
        }

        fun watchResult(branchType: BranchType, context: String, branch: String, remote: Remote): SyncEvent {
            return SyncEvent(branchType, Status.WATCH_RESULT, context, branch, remote.id)
        }

        fun syncing(branchType: BranchType, context: String, branch: String, remote: Remote): SyncEvent {
            return SyncEvent(branchType, Status.SYNCING, context, branch, remote.id)
        }

        fun syncing(branchType: BranchType, branch: Branch, remote: Remote): SyncEvent {
            return syncing(branchType, branch.context, branch.branch.toHex(), remote)
        }

        fun synced(branchType: BranchType, branch: Branch, remote: Remote): SyncEvent {
            return synced(branchType, branch.context, branch.branch.toHex(), remote)
        }

        fun synced(branchType: BranchType, context: String, branch: String, remote: Remote): SyncEvent {
            return SyncEvent(branchType, Status.SYNCED, context, branch, remote.id)
        }
    }
}

fun Logger.syncInfo(event: SyncEvent) {
    onInfo(Logger.Event(Logger.TYPE.SYNC_MANAGER, event))
}

fun Logger.syncError(event: SyncEvent) {
    onError(Logger.Event(Logger.TYPE.SYNC_MANAGER, event))
}


class BranchSyncer(val context: FejoaContext, val branchWatcher: BranchWatcher, val syncManager: SyncManager,
                   val userData: UserData) {
    private val syncJobs: MutableList<Future<SyncManager.SyncResult>> = ArrayList()
    private var logger: Logger = DevNullLogger()
    var isSyncing = false
        private set

    var autoSyncing = true

    private val listener: BranchWatcher.Listener = object : BranchWatcher.Listener {
        override suspend fun onUpdate(dirtyBranch: BranchWatcher.DirtyBranch) {
            if (autoSyncing)
                syncForced(dirtyBranch.branch, dirtyBranch.remote)
        }
    }

    init {
        branchWatcher.listeners += listener
    }

    fun setLogger(logger: Logger) {
        this.logger = logger
        this.branchWatcher.logger = logger
    }

    /**
     * Manually trigger a sync.
     *
     * Sync only happens if a branch is in the watchers dirty branch list.
     *
     * To be called from the event loop
     */
    suspend fun sync(branch: Branch, remote: Remote) {
        val dirtyBranchList = branchWatcher.dirtyBranches[remote.id] ?: return
        dirtyBranchList.firstOrNull { it.branch.branch == branch.branch && it.remote.id == remote.id} ?: return
        syncForced(branch, remote, false)
    }

    /**
     * Called from the event loop
     */
    suspend fun syncForced(branch: Branch, remote: Remote, localMustExist: Boolean = false)
            : Future<SyncManager.SyncResult> {
        // check first if the branch is at the remote
        val remoteRef = branch.locations.listRemoteRefs().firstOrNull { it.remoteId == remote.id }
                ?: return Future.failedFuture("Branch is not listed to be at the remote ${remote.id}")
        val dir = userData.getStorageDir(branch, localMustExist)
        try {
            val job = syncManager.sync(context, dir, remote, remoteRef.authInfo)
            logger.syncInfo(SyncEvent.syncing(SyncEvent.BranchType.BRANCH, branch, remote))

            syncJobs.add(job)
            GlobalScope.launch(context.coroutineContext) {
                try {
                    val syncResult = job.await()

                    this@BranchSyncer.context.looper.runSync {
                        // notify the observer
                        if (syncResult.code == ReturnType.OK)
                            logger.syncInfo(SyncEvent.synced(SyncEvent.BranchType.BRANCH, branch, remote))
                        else
                            logger.syncError(SyncEvent.error(SyncEvent.BranchType.BRANCH, branch, syncResult.message))
                    }
                } catch (exception: Throwable) {
                    this@BranchSyncer.context.looper.runSync {
                        logger.syncError(SyncEvent.error(SyncEvent.BranchType.BRANCH, branch,
                                exception.message ?: "Exception while syncing branch ${branch.branch}"))
                    }
                } finally {
                    syncJobs.remove(job)
                }
            }
            return job
        } finally {
            dir.close()
        }
    }

    fun start(remotes: Collection<Remote>) {
        if (isSyncing) stop()
        isSyncing = true

        branchWatcher.startWatching(remotes)
    }

    fun stop() {
        if (!isSyncing) return else isSyncing = false

        branchWatcher.stopWatching()

        syncJobs.forEach { it.cancel() }
        syncJobs.clear()
    }
}
