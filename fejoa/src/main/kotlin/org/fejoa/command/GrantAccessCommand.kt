package org.fejoa.command

import kotlinx.serialization.Serializable
import org.fejoa.BranchInfo
import org.fejoa.RemoteRef
import org.fejoa.storage.HashValue
import org.fejoa.storage.HashValueDataSerializer


/**
 * @param user the user that owns the branch
 */
@Serializable
class GrantAccessCommand(override val command: String,
                         @Serializable(with = HashValueDataSerializer::class) val user: HashValue,
                         val context: String,
                         val branchInfo: BranchInfo,
                         val locations: MutableList<RemoteRef.TokenRemoteRefIO>) : Command {
    companion object {
        const val NAME = "grantAccess"
    }
}