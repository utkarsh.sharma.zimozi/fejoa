package org.fejoa.command

import kotlinx.serialization.json.Json
import org.fejoa.RemoteRef
import org.fejoa.UserData
import org.fejoa.storage.HashValue


class GrantAccessHandler(val userData: UserData) : InQueueManager.Handler<InQueueManager.EnvelopeHandle> {
    override val command: String
        get() = GrantAccessCommand.NAME

    val handles = InQueueHandleManager<GrantAccessCommand>()

    inner class GrantAccessRequest(handleManager: InQueueHandleManager<GrantAccessCommand>,
                                       command: GrantAccessCommand, handle: InQueueManager.EnvelopeHandle)
        : InQueueHandleManager.PendingRequests<GrantAccessCommand>(handleManager, command, handle) {
        override suspend fun onAccept() {
            val contact = userData.contacts.list.get(handle.sender)
            if (!contact.id.exists())
                throw Exception("GrantAccessRequest: Contact does not exist")

            val branchMap = contact.branches.get(command.context)
            val branch = branchMap.get(HashValue.fromHex(command.branchInfo.branch))
            branch.branchInfo.write(command.branchInfo)
            command.locations.forEach {
                branch.updateRemote(RemoteRef(it.remoteId, it.authInfo))
            }
            userData.commit()
        }
    }

    override suspend fun onNewEntry(entry: InQueueManager.EnvelopeHandle) {
        val command = Json.parse(GrantAccessCommand.serializer(), entry.commandJson)

        handles.addPending(GrantAccessRequest(handles, command, entry))
    }
}