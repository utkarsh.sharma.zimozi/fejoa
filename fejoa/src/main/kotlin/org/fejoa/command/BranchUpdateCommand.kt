package org.fejoa.command

import kotlinx.serialization.Serializable
import org.fejoa.storage.HashValue
import org.fejoa.storage.HashValueDataSerializer


/**
 * Notifies a user about a branch update
 *
 * @param user the contact to be notified
 * @param owner the branch owner
 * @param remote the branch location
 * @param branch the updated branch
 */
@Serializable
class BranchUpdateCommand(@Serializable(with = HashValueDataSerializer::class) val user: HashValue,
                          @Serializable(with = HashValueDataSerializer::class) val owner: HashValue,
                          val remote: String,
                          @Serializable(with = HashValueDataSerializer::class) val branch: HashValue,
                          override val command: String = NAME) : Command {
    companion object {
        const val NAME = "branchUpdate"
    }
}
