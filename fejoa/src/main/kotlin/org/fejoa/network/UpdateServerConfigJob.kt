package org.fejoa.network

import kotlinx.serialization.Optional
import kotlinx.serialization.Serializable
import kotlinx.serialization.serializer
import org.fejoa.LoginParams
import org.fejoa.StorageConfig


class UpdateServerConfigJob(val user: String, val loginParams: LoginParams? = null,
                            val storageConfig: StorageConfig? = null) : RemoteJob<UpdateServerConfigJob.Result>() {
    companion object {
        val METHOD = "updateServerConfig"
    }

    @Serializable
    class RPCParams(val user: String, @Optional val loginParams: LoginParams? = null,
                    @Optional val storageConfig: StorageConfig? = null)

    @Serializable
    class RPCResult

    class Result(code: ReturnType, message: String, val data: RPCResult? = null) : RemoteJob.Result(code, message)

    private fun getHeader(): String {
        return JsonRPCRequest(id = id, method = METHOD, params = RPCParams(user, loginParams, storageConfig))
                .stringify(RPCParams.serializer())
    }

    override suspend fun run(remoteRequest: RemoteRequest): Result {
        val replyString = remoteRequest.send(getHeader()).receiveHeader()

        val response = try {
            JsonRPCResponse.parse(RPCResult.serializer(), replyString, id)
        } catch (e: Exception) {
            val error = JsonRPCError.parse(ErrorMessage.serializer(), replyString, id).error
            return Result(ensureError(error.code), error.message)
        }

        return Result(ReturnType.OK, "ok", response.result)
    }
}
