package org.fejoa.network

import kotlinx.serialization.Serializable
import kotlinx.serialization.internal.StringSerializer
import kotlinx.serialization.serializer
import org.fejoa.AuthToken
import org.fejoa.support.decodeBase64


class TokenAuthJob(val user: String, val token: AuthToken) : RemoteJob<RemoteJob.Result>()  {
    enum class AuthState {
        INIT, // init the auth process
        FINISH // finish the auth process
    }

    companion object {
        val METHOD = "tokenAuth"
    }

    @Serializable
    class InitParams(val user: String = "",
                     val data: InitData,
                     val type: AuthState = AuthState.INIT)

    @Serializable
    class InitData(val tokenId: String)

    @Serializable
    class InitResponse(val verificationToken: String)

    @Serializable
    class FinishParams(val user: String = "",
                       val data: FinishData,
                       val type: AuthState = AuthState.FINISH)

    @Serializable
    class FinishData(val tokenId: String, val token: String, val signature: String)

    private fun getInitRequest(): String {
        return JsonRPCRequest(id = id, method = METHOD,
                params = InitParams(user, InitData(token.id)))
                .stringify(InitParams.serializer())
    }

    private suspend fun getFinishRequest(signature: String): String {
        return JsonRPCRequest(id = id, method = METHOD,
                params = FinishParams(user, FinishData(token.id, token.token, signature)))
                .stringify(FinishParams.serializer())
    }

    override suspend fun run(remoteRequest: RemoteRequest): Result {
        val initResponseString = remoteRequest.send(getInitRequest()).receiveHeader()
        val initResponse = try {
            JsonRPCResponse.parse(InitResponse.serializer(), initResponseString, id)
        } catch (e: Exception) {
            val error = JsonRPCError.parse(ErrorMessage.serializer(), initResponseString, id).error
            return Result(ensureError(error.code), error.message)
        }
        val initResult = initResponse.result
        val signature = token.sign(initResult.verificationToken.decodeBase64())

        val finishResponseString = remoteRequest.send(getFinishRequest(signature)).receiveHeader()
        val response = try {
            JsonRPCResponse.parse(StringSerializer, finishResponseString, id)
        } catch (e: Exception) {
            val error = JsonRPCError.parse(ErrorMessage.serializer(), finishResponseString, id).error
            return Result(ReturnType.ERROR, error.message)
        }

        return Result(ReturnType.OK, response.result)
    }

}