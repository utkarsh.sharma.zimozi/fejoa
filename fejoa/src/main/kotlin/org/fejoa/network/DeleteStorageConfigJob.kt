package org.fejoa.network

import kotlinx.serialization.Serializable
import kotlinx.serialization.serializer


class DeleteStorageConfigJob(val user: String) : RemoteJob<DeleteStorageConfigJob.Result>() {
    companion object {
        val METHOD = "deleteStorageConfig"
    }

    @Serializable
    class RPCParams(val user: String)

    @Serializable
    class RPCResult

    class Result(code: ReturnType, message: String)
        : RemoteJob.Result(code, message)

    private fun getHeader(): String {
        return JsonRPCRequest(id = id, method = METHOD,
                params = RPCParams(user)).stringify(RPCParams.serializer())
    }

    override suspend fun run(remoteRequest: RemoteRequest): Result {
        val replyString = remoteRequest.send(getHeader()).receiveHeader()

        val response = try {
            JsonRPCResponse.parse(RPCResult.serializer(), replyString, id)
        } catch (e: Exception) {
            val error = JsonRPCError.parse(ErrorMessage.serializer(), replyString, id).error
            return Result(ensureError(error.code), error.message)
        }

        return Result(ReturnType.OK, "ok")
    }
}
