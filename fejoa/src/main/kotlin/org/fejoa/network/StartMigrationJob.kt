package org.fejoa.network

import kotlinx.serialization.Serializable
import kotlinx.serialization.internal.StringSerializer
import kotlinx.serialization.serializer
import org.fejoa.ServerToken


class StartMigrationJob(val user: String, val serverToken: ServerToken) : RemoteJob<RemoteJob.Result>() {
    companion object {
        val METHOD = "startMigration"
    }

    @Serializable
    class Params(val user: String, val token: ServerToken)

    private fun getHeader(): String {
        return JsonRPCRequest(id = id, method = METHOD, params = Params(user, serverToken))
                .stringify(Params.serializer())
    }

    override suspend fun run(remoteRequest: RemoteRequest): Result {
        val sender = remoteRequest.sendData(getHeader())

        val reply = sender.send()
        val responseHeader = reply.receiveHeader()
        val response = try {
            JsonRPCResponse.parse(StringSerializer, responseHeader, id)
        } catch (e: Exception) {
            val error = JsonRPCError.parse(ErrorMessage.serializer(), responseHeader, id).error
            return Result(ensureError(error.code), error.message)
        }

        return Result(ReturnType.OK, response.result)
    }
}
