package org.fejoa.network

import kotlinx.serialization.Optional
import kotlinx.serialization.Serializable
import kotlinx.serialization.serializer
import org.fejoa.LoginParams
import org.fejoa.StorageConfig
import org.fejoa.crypto.DH_GROUP
import org.fejoa.crypto.UserKeyParams
import org.fejoa.support.*


class ConfirmRegistrationJob(val user: String, val token: String)
    : RemoteJob<RegisterJob.Result>() {

    companion object {
        val METHOD = "confirmRegistration"
    }

    @Serializable
    class ConfirmParams(val user: String = "", val token: String)

    private fun getHeader(): String {
        return JsonRPCRequest(id = id, method = METHOD, params = ConfirmParams(user, token))
                .stringify(ConfirmParams.serializer())
    }

    override suspend fun run(remoteRequest: RemoteRequest): RegisterJob.Result {
        val reply = remoteRequest.send(getHeader())
        val responseHeader = reply.receiveHeader()

        val response = try {
            JsonRPCResponse.parse(RegisterJob.RegisterReply.serializer(), responseHeader, id)
        } catch (e: Exception) {
            val error = JsonRPCError.parse(ErrorMessage.serializer(), responseHeader, id).error
            return RegisterJob.Result(ReturnType.ERROR, error.message, null)
        }

        return RegisterJob.Result(ReturnType.OK, response.result.message, response.result)
    }
}

class RegisterJob(val user: String, val loginParams: LoginParams, val storageConfig: StorageConfig? = null)
    : RemoteJob<RegisterJob.Result>() {

    enum class ReplyType {
        REGISTERED,
        CONFIRMATION_REQUIRED
    }

    class Result(code: ReturnType, message: String, val reply: RegisterReply?) : RemoteJob.Result(code, message)

    @Serializable
    class Params(val user: String, val loginParams: LoginParams, @Optional val storageConfig: StorageConfig? = null) {
        constructor(user: String, userKeyParams: UserKeyParams, userKey: BigInteger, group: DH_GROUP,
                    storageConfig: StorageConfig?) : this(user,
                LoginParams(userKeyParams, group.params.g.modPow(userKey, group.params.p).toString(16), group),
                storageConfig)
    }

    @Serializable
    class RegisterReply(val result: ReplyType, val message: String)


    companion object {
        val METHOD = "register"
    }

    private fun getHeader(): String {
        return JsonRPCRequest(id = id, method = METHOD, params = Params(user, loginParams, storageConfig))
                .stringify(Params.serializer())
    }

    override suspend fun run(remoteRequest: RemoteRequest): Result {
        val reply = remoteRequest.send(getHeader())
        val responseHeader = reply.receiveHeader()

        val response = try {
            JsonRPCResponse.parse(RegisterReply.serializer(), responseHeader, id)
        } catch (e: Exception) {
            val error = JsonRPCError.parse(ErrorMessage.serializer(), responseHeader, id).error
            return RegisterJob.Result(ReturnType.ERROR, error.message, null)
        }

        return RegisterJob.Result(ReturnType.OK, response.result.message, response.result)
    }
}
