package org.fejoa.network

import kotlinx.serialization.Serializable
import kotlinx.serialization.internal.StringSerializer
import kotlinx.serialization.serializer
import org.fejoa.protocolbufferlight.ProtocolBufferLight


class CommandJob(val user: String, val command: ByteArray, val receiverKey: ByteArray)
    : RemoteJob<RemoteJob.Result>() {

    class CommandData(val command: ByteArray, val receiverKey: ByteArray) {
        companion object {
            const val COMMAND_TAG = 0
            const val RECEIVER_KEY_TAG = 1

            fun read(bytes: ByteArray): CommandData {
                val buffer = ProtocolBufferLight(bytes)
                val command = buffer.getBytes(COMMAND_TAG) ?: throw Exception("Missing command field")
                val receiverKey = buffer.getBytes(RECEIVER_KEY_TAG)
                        ?: throw Exception("Missing receiver key field")
                return CommandData(command, receiverKey)
            }
        }

        fun toProtoBuffer(): ProtocolBufferLight {
            val buffer = ProtocolBufferLight()
            buffer.put(COMMAND_TAG, command)
            buffer.put(RECEIVER_KEY_TAG, receiverKey)
            return buffer
        }
    }

    @Serializable
    class Params(val user: String)

    companion object {
        val METHOD = "command"
    }

    private fun getHeader(): String {
        return JsonRPCRequest(id = id, method = METHOD, params = Params(user)).stringify(Params.serializer())
    }

    override suspend fun run(remoteRequest: RemoteRequest): Result {
        val sender = remoteRequest.sendData(getHeader())
        sender.outStream().write(CommandData(command, receiverKey).toProtoBuffer().toByteArray())
        val reply = sender.send()
        val responseHeader = reply.receiveHeader()

        val response = try {
            JsonRPCResponse.parse(StringSerializer, responseHeader, id)
        } catch (e: Exception) {
            val error = JsonRPCError.parse(ErrorMessage.serializer(), responseHeader, id).error
            return Result(ReturnType.ERROR, error.message)
        }

        return Result(ReturnType.OK, response.result)
    }
}
