package org.fejoa.network

import kotlinx.serialization.Optional
import kotlinx.serialization.Serializable
import kotlinx.serialization.serializer
import org.fejoa.LoginParams
import org.fejoa.StorageConfig


class GetServerConfigJob(val user: String) : RemoteJob<GetServerConfigJob.Result>() {
    companion object {
        val METHOD = "getServerConfig"
    }

    @Serializable
    class RPCParams(val user: String)

    @Serializable
    class RPCResult(val loginParams: LoginParams, @Optional val storageConfig: StorageConfig? = null)

    class Result(code: ReturnType, message: String, val storageConfig: StorageConfig? = null)
        : RemoteJob.Result(code, message)

    private fun getHeader(): String {
        return JsonRPCRequest(id = id, method = METHOD, params = RPCParams(user)).stringify(RPCParams.serializer())
    }

    override suspend fun run(remoteRequest: RemoteRequest): Result {
        val replyString = remoteRequest.send(getHeader()).receiveHeader()

        val response = try {
            JsonRPCResponse.parse(RPCResult.serializer(), replyString, id)
        } catch (e: Exception) {
            val error = JsonRPCError.parse(ErrorMessage.serializer(), replyString, id).error
            return Result(ensureError(error.code), error.message)
        }

        return Result(ReturnType.OK, "ok", response.result.storageConfig)
    }
}
