package org.fejoa.network

import kotlinx.serialization.Serializable
import kotlinx.serialization.serializer


class RequestRegistrationTokenJob(val user: String, val email: String)
    : RemoteJob<RemoteJob.Result>() {

    companion object {
        val METHOD = "requestRegistrationToken"
    }

    enum class Type {
        EMAIL
    }

    @Serializable
    class Params(val user: String, val type: Type = Type.EMAIL, val data: EmailRequest)

    @Serializable
    class EmailRequest(val email: String)

    @Serializable
    class Reply(val message: String)

    private fun getHeader(): String {
        return JsonRPCRequest(id = id, method = METHOD, params = Params(user, data = EmailRequest(email)))
                .stringify(Params.serializer())
    }

    override suspend fun run(remoteRequest: RemoteRequest): RemoteJob.Result {
        val reply = remoteRequest.send(getHeader())
        val responseHeader = reply.receiveHeader()

        val response = try {
            JsonRPCResponse.parse(Reply.serializer(), responseHeader, id)
        } catch (e: Exception) {
            val error = JsonRPCError.parse(ErrorMessage.serializer(), responseHeader, id).error
            return RemoteJob.Result(ReturnType.ERROR, error.message)
        }

        return RemoteJob.Result(ReturnType.OK, response.result.message)
    }
}
