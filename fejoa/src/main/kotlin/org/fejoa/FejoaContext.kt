package org.fejoa

import org.fejoa.chunkcontainer.BoxSpec
import org.fejoa.crypto.BaseKeyCache
import org.fejoa.crypto.CryptoHelper
import org.fejoa.crypto.SecretKeyData
import org.fejoa.repository.*
import org.fejoa.storage.*
import org.fejoa.support.Locker
import org.fejoa.support.PathUtils
import org.fejoa.support.SuspendQueue
import kotlin.coroutines.CoroutineContext


class RefCount<out T>(val value: T, count: Int = 1) {
    var count: Int = count
        private set

    fun release(): Int = synchronized(this) {
        count --
    }

    fun acquire(): Int = synchronized(this) {
        count ++
    }
}

class FejoaContext constructor(val accountType: AccountIO.Type, val context: String, val namespace: String,
                               val coroutineContext: CoroutineContext,
                               val branchIndexLocker: Locker = SuspendQueue(coroutineContext)) {
    var baseKeyCache = BaseKeyCache()
    val accountIO by lazy { platformGetAccountIO(accountType, context, namespace) }
    val platformStorage by lazy { platformCreateStorage(context) }
    // List of open storage dirs. A storage dir is reused when already open.
    private val openStorageDirs: MutableMap<String, RefCount<StorageDir>> = HashMap()
    private var indexStorage: StorageDir? = null
    private var notifyStatusStorage: StorageDir? = null

    val looper = SuspendQueue(coroutineContext)

    fun setBaseKeyCacheStrategy(strategy: BaseKeyCache.DeriveStrategy) {
        baseKeyCache.strategy = strategy
    }

    suspend fun getBranchLog(branch: String): BranchLog? {
        if (!platformStorage.exists(namespace, branch))
            return null
        return platformStorage.open(namespace, branch).getBranchLog()
    }

    private suspend fun getIndexStorage(namespace: String, subNamespace: String, branch: String, init: Boolean)
            : StorageDir {
        val storageDir = getStorage(true, namespace, subNamespace, branch, null, null, null, init)
        return storageDir
    }

    suspend fun getRemoteIndexStorage(remoteId: String, branch: String): StorageDir {
        val remoteIndex = getIndexStorage(namespace, ".branchIndex/remotes/$remoteId", branch,
                false)
        remoteIndex.setAllowCommits(false)
        return remoteIndex
    }

    suspend fun getLocalIndexStorage(): StorageDir {
        return indexStorage ?: let {
            val finalNamespace = PathUtils.appendDir(PathUtils.toPath(namespace), ".branchIndex/local")
            val branches = platformStorage.listBranches(finalNamespace)
            val branch = branches.firstOrNull()?.getBranchName() ?: CryptoHelper.generateSha256Id().toHex()
            getIndexStorage(namespace, ".branchIndex/local", branch, true)
        }.also {
            indexStorage = it
        }
    }

    suspend fun getNotifyStatusStorage(symCredentials: SecretKeyData, commitSignature: CommitSignature?,
                                       ref: RepositoryRef? = null): StorageDir {
        return notifyStatusStorage ?: let {
            val finalNamespace = PathUtils.appendDir(PathUtils.toPath(namespace), ".notifyStatus")
            val branches = platformStorage.listBranches(finalNamespace)
            val branch = branches.firstOrNull()?.getBranchName() ?: CryptoHelper.generateSha256Id().toHex()
            getStorage(true, namespace, ".notifyStatus", branch, symCredentials,
                    commitSignature, ref, true)
        }.also {
            notifyStatusStorage = it
        }
    }

    private suspend fun createOrOpenStorage(namespace: String, branch: String,
                                            symCredentials: SecretKeyData?,
                                            commitSignature: CommitSignature?, ref: RepositoryRef?, init: Boolean)
            : StorageDir {
        val storage = if (platformStorage.exists(namespace, branch)) {
            val backend = platformStorage.open(namespace, branch)
            val repositoryRef = ref ?: let {
                val branchLogIO = getBranchLogIO(symCredentials)
                val headMessage = backend.getBranchLog().getHead()
                if (headMessage == null)
                    null
                else
                branchLogIO.readFromLog(headMessage.message.data)
            }
            val repo = if (repositoryRef != null)
                Repository.open(branch, repositoryRef, backend, symCredentials)
            else
                Repository.create(branch, backend, getRepoConfig(symCredentials), symCredentials, init)
            val headCommit = repo.getHeadCommit()
            if (headCommit != null && commitSignature != null) {
                if (!headCommit.verify(commitSignature))
                    throw Exception("Failed to verify commit")
            }
            repo
        } else {
            if (ref != null)
                throw Exception("Storage dir at $namespace/$branch does not exist")
            val backend = platformStorage.create(namespace, branch)
            Repository.create(branch, backend, getRepoConfig(symCredentials), symCredentials, init)
        }

        return StorageDir(storage, "", commitSignature, looper)
    }

    private fun getStorageDirId(namespace: String, branch: String, ref: RepositoryRef?): String {
        val refId = ref?.head?.hash?.value?.toHex() ?: "HEAD"
        return "$namespace/$branch@$refId"
    }

    private suspend fun getStorage(excludeFromBranchIndex: Boolean, mainNamespace: String, subNamespace: String,
                                   branch: String,
                                   symCredentials: SecretKeyData?, commitSignature: CommitSignature?,
                                   ref: RepositoryRef?, init: Boolean) : StorageDir {
        val namespace = PathUtils.appendDir(PathUtils.toPath(mainNamespace), subNamespace)
        val storageDirId = getStorageDirId(namespace, branch, ref)
        return openStorageDirs[storageDirId]?.let {
            it.acquire()
            // wrap it into a new StorageDir object so that it can have its own listeners
            StorageDir(it.value)
        } ?: let {
            val storageDir = createOrOpenStorage(namespace, branch, symCredentials, commitSignature, ref, init)
            //TODO: Move this to UserData and only monitor user data branches?
            storageDir.addListener(object : StorageDir.Listener {
                override fun callSynchronous(): Boolean {
                    // Call synchronous to make sure the branch index is updated
                    // TODO: a transaction journal is still need.
                    return true
                }

                override suspend fun onBeforeCommit(tip: HashValue) {
                    if (ref != null) // TODO make this possible?
                        throw Exception("Not allowed to commit a none tip version.")
                    if (!excludeFromBranchIndex) {
                        // TODO make it a transaction, i.e. the branch index can get out of sync when we crash after the
                        // commit but before we updated the branch index.
                    }
                }

                override suspend fun onAfterCommit(base: HashValue, newTip: HashValue) {
                    //println("FejoaContext: onAfterCommit ${storageDir.branch} $excludeFromBranchIndex $base, $newTip")
                    if (!excludeFromBranchIndex && base != newTip) {
                        // Update local storage index when committed

                        // TODO finish the transaction (see TODO in onBeforeCommit)

                        // Note: run this blocking. In case the server commits the inqueue we need to ensure that the branch
                        // index is updated.
                        branchIndexLocker.runSync {
                            val branchIndex = BranchIndex(getLocalIndexStorage())
                            val branchLogTip = (storageDir.getBackingDatabase() as Repository)
                                    .branchBackend.getBranchLog().getHead()?.entryId ?: Config.newDataHash()
                            //println("BranchLogTip $branch $branchLogTip")
                            branchIndex.update(branch, branchLogTip)
                            branchIndex.commit()
                        }
                    }
                }

                override suspend fun onClose() {
                    openStorageDirs[storageDirId]?.let {
                        if (it.release() == 0)
                            openStorageDirs.remove(storageDirId)
                    }
                }
            })
            storageDir
        }.also {
            openStorageDirs[storageDirId] = RefCount(it)
        }
    }

    suspend fun getStorage(branch: String, symCredentials: SecretKeyData?, init: Boolean,
                           commitSignature: CommitSignature? = null, ref: RepositoryRef? = null) : StorageDir {
        return getStorage(false, namespace, "", branch, symCredentials,
                commitSignature, ref, init)
    }

    private fun getBranchLogIO(credentials: SecretKeyData?): BranchLogIO {
        return if (credentials == null)
            RepositoryBuilder.getPlainBranchLogIO()
        else
            RepositoryBuilder.getEncryptedBranchLogIO(credentials.key, credentials.algo)
    }

    private fun getRepoConfig(symCredentials: SecretKeyData?): RepositoryConfig {
        val seed = CryptoHelper.crypto.generateSalt16()
        val hashSpec = HashSpec.createCyclicPoly(HashSpec.HashType.FEJOA_CYCLIC_POLY_2KB_8KB, seed)
        //val hashSpec = HashSpec(HashSpec.HashType.FEJOA_FIXED_8K, null)

        val cryptoType = if (symCredentials != null)
            BoxSpec.EncryptionInfo.Type.PARENT
        else
            BoxSpec.EncryptionInfo.Type.PLAIN

        val boxSpec = BoxSpec(
                encInfo = BoxSpec.EncryptionInfo(cryptoType),
                zipType = BoxSpec.ZipType.ZSTANDARD,
                zipBeforeEnc = true
        )

        return RepositoryConfig(
                hashSpec = hashSpec,
                boxSpec = boxSpec
        )
    }
}