package org.fejoa

import org.fejoa.crypto.*
import org.fejoa.repository.DefaultCommitSignature
import org.fejoa.repository.Repository
import org.fejoa.storage.*


class UserData private constructor(val context: FejoaContext, val masterKey: SecretKeyData, storageDir: StorageDir)
    : StorageDirObject(storageDir) {

    class Keys(val storageDir: StorageDir) {
        val signingKeys = DBMapJsonKeyPairData(storageDir, "signing")
        val asymEncKeys = DBMapJsonKeyPairData(storageDir, "asymEnc")
    }

    val id = DBHashValue(storageDir, "id")
    val remotes = DBMapRemote(storageDir, "remotes")
    val branches = DBContextBranchMap(storageDir, "branches")
    val keys = Keys(StorageDir(storageDir, "keys"))
    val contacts = Contacts(StorageDir(storageDir, "contacts"))

    companion object {
        const val USER_DATA_CONTEXT = "userdata"
        const val OUT_QUEUE_CONTEXT = "$USER_DATA_CONTEXT.outqueue"
        const val IN_QUEUE_CONTEXT = "$USER_DATA_CONTEXT.inqueue"
        const val ACCESS_STORE_CONTEXT = "$USER_DATA_CONTEXT.accessstore"

        suspend fun create(context: FejoaContext, settings: CryptoSettings = CryptoSettings.default): UserData {
            val signingKeyPair = CryptoHelper.crypto.generateKeyPair(settings.signature)
            val userId = signingKeyPair.getId()

            val userDataBranch = CryptoHelper.generateSha256Id()
            val userDataKeyData = settings.symmetric.generateSecretKeyData()
            val userDataStorage = context.getStorage(userDataBranch.toHex(), userDataKeyData, true)
            val userData = UserData(context, userDataKeyData, userDataStorage)

            val signAsymKey = JsonKeyPairData.create(signingKeyPair, settings.signature.algo)
            val signingKeyID = signAsymKey.getId()
            userData.id.write(signingKeyID)
            userData.keys.signingKeys.get(signingKeyID).write(signAsymKey)

            // asym enc key
            val asymEncKeyPair = CryptoHelper.crypto.generateKeyPair(settings.asymmetric)
            userData.keys.asymEncKeys.get(asymEncKeyPair.getId()).write(
                    JsonKeyPairData.create(asymEncKeyPair, settings.asymmetric.algo))

            userData.writeBranchInfo(USER_DATA_CONTEXT, userDataBranch, BranchInfo(userDataBranch.toHex(),
                    "User Data (this)", userDataKeyData.asJsonSecretKeyData()))

            userData.createOutQueue(CryptoHelper.generateSha256Id().toHex())

            val commitSignature = DefaultCommitSignature(userId, KeyPairData(signingKeyPair,
                    settings.signature.algo),
                    { signer: HashValue, keyId: HashValue -> userData.getSigningKey(signer, keyId)})
            userDataStorage.setCommitSignature(commitSignature)

            return userData
        }

        suspend fun open(context: FejoaContext, masterKey: SecretKeyData, branch: String): UserData {
            val userDataStorage = context.getStorage(branch, masterKey, false)
            if (userDataStorage.getHead() == null)
                throw Exception("User data does not exist")

            val userData = UserData(context, masterKey, userDataStorage)

            val signCredentials = userData.getSigningCredentials()
            val commitSignature = DefaultCommitSignature(userData.id.get(), signCredentials,
                    { signer: HashValue, keyId: HashValue -> userData.getSigningKey(signer, keyId)})

            userDataStorage.setCommitSignature(commitSignature)

            return userData
        }

        fun generateBranchName(): HashValue {
            return HashValue(CryptoHelper.crypto.generateSalt16())
        }
    }

    /**
     * Can be used to confirm that the correct user is accessing the user data, i.e. that the current user knows the
     * password.
     */
    suspend fun confirmPassword(password: String): Boolean {
        val rawKey = try {
            val platformIO = platformGetAccountIO(AccountIO.Type.CLIENT, context.context, context.namespace)
            val userDataSettings = platformIO.readUserDataConfig()
            val openedSettings = userDataSettings.open(password, context.baseKeyCache)
            CryptoHelper.crypto.encode(openedSettings.first.key)
        } catch (e: Throwable) {
            return false
        }
        val masterKeyRaw = CryptoHelper.crypto.encode(masterKey.key)
        return rawKey contentEquals masterKeyRaw
    }

    /**
     * Gets the default signing credentials
     */
    suspend fun getSigningCredentials(): KeyPairData {
        return keys.signingKeys.listKeys().first().getAsymCredentials()
    }

    suspend fun getAsymEncCredentials(): KeyPairData {
        return keys.asymEncKeys.listKeys().first().getAsymCredentials()
    }

    suspend fun getSigningKey(signer: HashValue, keyId: HashValue): PublicKey? {
        if (signer == id.get())
            return keys.signingKeys.get(keyId).get().getPublicKey()
        return null
    }

    private fun toContextPath(context: String): String {
        return context.replace('/', '.')
    }

    suspend fun createStorageDir(branchContext: String, branch: String, description: String,
                                 credentials: SecretKeyData?): Branch {
        if (context.getBranchLog(branch)?.getHead() != null)
            throw Exception("Branch $branch already exist")

        val storageDir = context.getStorage(branch, credentials, true, storageDir.getCommitSignature())

        // add initial tip to the index
        context.branchIndexLocker.runSync {
            val branchIndex = BranchIndex(context.getLocalIndexStorage())
            val logHead = (storageDir.getBackingDatabase() as Repository).branchBackend.getBranchLog()
                    .getHead()
            branchIndex.update(branch, logHead?.entryId ?: Config.newDataHash())
            branchIndex.commit()
        }

        return writeBranchInfo(branchContext, HashValue.fromHex(branch), BranchInfo(branch, description,
                credentials?.asJsonSecretKeyData()))
    }

    suspend fun getStorageDir(branch: Branch, mustExist: Boolean = true): StorageDir {
        val branchInfo = branch.branchInfo.get()
        val storageDir = context.getStorage(branch.branch.toHex(), branchInfo.key?.asSymBaseCredentials(),
                false, storageDir.getCommitSignature())
        if (mustExist && (storageDir.getBackingDatabase() as Repository).branchBackend.getBranchLog().getHead() == null)
            throw Exception("Branch ${branch.branch} (${branchInfo.description}) is uninitialized")
        return storageDir
    }

    suspend fun writeRemote(remote: Remote) {
        remotes.get(HashValue.fromHex(remote.id)).write(remote)
    }

    suspend fun writeBranchInfo(context: String, branch: HashValue, branchInfo: BranchInfo): Branch {
        val dbBranch = getBranch(context, branch)
        dbBranch.branchInfo.write(branchInfo)
        return dbBranch
    }

    suspend fun getOutQueues(): List<Branch> {
        return getBranches(OUT_QUEUE_CONTEXT)
    }

    suspend fun getInQueues(): List<Branch> {
        return getBranches(IN_QUEUE_CONTEXT)
    }

    suspend fun getAccessStores(): List<Branch> {
        return getBranches(ACCESS_STORE_CONTEXT)
    }

    fun getBranch(context: String, branch: HashValue): Branch {
        return branches.get(toContextPath(context)).get(branch)
    }

    suspend fun getBranches(context: String, recursive: Boolean): List<Branch> {
        val contextPath = toContextPath(context)
        return if (recursive) {
            branches.list().filter { it == contextPath || it.startsWith("$contextPath.") }
                    .flatMap { getBranches(it) }
        } else
            getBranches(context)
    }

    private suspend fun getBranches(context: String) : List<Branch> {
        val contextPath = toContextPath(context)
        return branches.get(contextPath).list()
                .map { branch -> branches.get(contextPath).get(HashValue.fromHex(branch)) }
    }

    suspend fun getBranches(): List<Branch> {
        return branches.list().flatMap { getBranches(it) }
    }

    /**
     * Creates a password protected UserDataConfig
     */
    suspend fun createUserDataConfig(password: String, userKeyParams: UserKeyParams): UserDataConfig {
        return UserDataConfig.create(masterKey.key, password, userKeyParams,
                UserDataRef(branch, masterKey.algo), context.baseKeyCache)
    }

    suspend fun createServerConfig(password: String, userKeyParams: UserKeyParams, inQueue: String, accessStore: String)
            : StorageConfig {
        val outQueue = getOutQueues().firstOrNull() ?: throw Exception("Not out queue defined")
        val userDataConfig = createUserDataConfig(password, userKeyParams)
        return StorageConfig(userDataConfig, outQueue.branchInfo.get().branch, inQueue, accessStore)
    }

    suspend fun getServerConfig(inQueue: String, accessStore: String): StorageConfig? {
        return try {
            val userDataConfig = context.accountIO.readUserDataConfig()
            val outQueue = getOutQueues().firstOrNull() ?: throw Exception("Not out queue defined")
            StorageConfig(userDataConfig, outQueue.branchInfo.get().branch, inQueue, accessStore)
        } catch (e: Exception) {
            null
        }
    }

    override suspend fun commit() {
        super.commit()
    }

    suspend fun close() {
        storageDir.close()
    }

    private suspend fun createOutQueue(outQueue: String) {
        createStorageDir(OUT_QUEUE_CONTEXT, outQueue, "Out queue", null)
    }

    suspend fun createInQueue(inQueueName: String, remote: Remote) {
        // create storage
        val branch = createStorageDir(IN_QUEUE_CONTEXT, inQueueName,
                "In queue (${remote.address()})", null)
        val branchInfo = branch.branchInfo.get()
        branch.updateRemote(remote, LoginAuthInfo())
        branch.branchInfo.write(branchInfo)
    }

    suspend fun createAccessStore(accessStore: String, remote: Remote) {
        val branch = createStorageDir(ACCESS_STORE_CONTEXT, accessStore,
                "Access store (${remote.address()})", null)
        val branchInfo = branch.branchInfo.get()
        branch.updateRemote(remote, LoginAuthInfo())
        branch.branchInfo.write(branchInfo)
    }
}
