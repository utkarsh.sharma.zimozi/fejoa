package org.fejoa.server.ui

import kotlinx.html.*
import kotlinx.html.dom.create
import org.w3c.dom.get
import org.w3c.dom.url.URL
import kotlin.browser.document
import kotlin.browser.window


fun main(args: Array<String>) {
    document.addEventListener("DOMContentLoaded", {
        start()
    })
}

const val loadingDivId = "loading-container"

fun start() {
    // build view
    val mainDiv = document.create.div {
        div("container") {
            div {
                style = "position:relative;"
                span("fa fa-spinner fa-spin fa-fw") {
                    style = "position:absolute; right: 20px; top: 10px;"
                    id = loadingDivId
                }
            }
            h2 {
                +"Fejoa Account Management"
            }

            buildErrorDiv(this)

            buildSendEmailDiv(this)
            LoggedInView.buildLoggedInDiv(this)
            buildLoggedOutDiv(this)
        }
    }
    document.getElementById("fejoa-account-container")?. apply {
        appendChild(mainDiv)
    } ?: run { // just append it to the body
        document.body!!.appendChild(mainDiv)
    }

    val FEJOA_PORTAL_PATH = "fejoa.portal"

    val fejoaPortalPath = document.getElementById("fejoa-portal")
            ?.getAttribute("data-path") ?: FEJOA_PORTAL_PATH
    val model = MainModel(URL(window.location.href), fejoaPortalPath)
    MainViewModel(model)
    LogViewModel(model.logModel)
    PendingRegistrationViewModel(model)
}