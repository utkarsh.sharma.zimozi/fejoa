package org.fejoa.server.ui

import kotlinx.html.*
import kotlinx.html.dom.create
import org.w3c.dom.HTMLButtonElement
import org.w3c.dom.HTMLUListElement
import kotlin.browser.document


object LoggedInView {
    const val loggedInDivId = "logged-in-container"
    const val storageListId = "account-storage-list"
    const val storageListEmptyId = "account-storage-list-empty"
    const val userNameId = "user-name"
    const val fetchingId = "remote-branching-fetching-status"

    const val storageConfigContainerId = "storage-config-container"
    const val storageConfigDisplayId = "storage-config-display"
    const val noStorageConfigId = "no-storage-config"
    const val deleteStorageConfigButtonId = "delete-storage-config-button"
    const val deleteStorageConfigContainerId = "delete-storage-config-container"


    fun buildLoggedInDiv(parent: DIV) = parent.apply {
        div("container-fluid") {
            id = loggedInDivId
            hidden = true

            h4 {
                +"Welcome "
                span {
                    id = userNameId
                }
            }

            span {
                id = fetchingId
                +"Fetching..."
            }

            div("row") {
                h4("mr-auto") {
                    +"Remote Storage Config:"
                }
            }

            div("alert alert-info") {
                attributes["role"] = "alert"
                +("Contains information about the remote storage, e.g. encrypted information about the used" +
                        "encryption or the name of the main user data storage branch.")
            }

            p {
                id = noStorageConfigId
                +"No storage config at the remote"
            }
            p {
                id = storageConfigContainerId
                div("container-fluid") {
                    a(classes = "pull-left") {
                        +"StorageConfig.json"
                        href="#$storageConfigDisplayId"
                        attributes += "data-toggle" to "collapse"
                    }

                    button(classes = "btn btn-sm pull-right ml-1") {
                        attributes += "data-toggle" to "collapse"
                        attributes += "data-target" to "#$deleteStorageConfigContainerId"
                        +"Delete"
                    }
                }
                div("container-fluid collapse pull-right mt-1") {
                    id = deleteStorageConfigContainerId

                    div("alert alert-warning pull-right") {
                        attributes["role"] = "alert"

                        span("mr-2") {
                            +"Are you sure you want to delete the storage config?"
                        }

                        button(classes = "btn pull-right") {
                            attributes += "data-toggle" to "collapse"
                            attributes += "data-target" to "#$deleteStorageConfigContainerId"
                            +"Cancel"
                        }
                        button(classes = "btn pull-right btn-danger") {
                            id = deleteStorageConfigButtonId
                            attributes += "data-toggle" to "collapse"
                            attributes += "data-target" to "#$deleteStorageConfigContainerId"
                            +"Yes"
                        }
                    }
                }

                // StorageConfig display
                div("container-fluid collapse") {
                    style = "white-space: pre-wrap;\n" +
                            "background-color: #F0F0F0;\n" +
                            "font-size: 90%;\n" +
                            "padding: 0.3em 0.3em;"
                    id = storageConfigDisplayId
                }
            }

            div("row") {
                h4("mr-auto") {
                    +"Remote storage branches:"
                }
            }

            div("alert alert-info") {
                attributes["role"] = "alert"
                +("This is a view of the storage branches at the remote server. Since the server doesn't know the " +
                        "details about what is stored in the branches and can't decrypt the branches there is not " +
                        "much to see here")
            }

            p {
                id = storageListEmptyId
                +"No branches at the remote"
            }

            ul("list-group") {
                id = storageListId
            }
        }
    }

    private fun byteSizeToString(sizeInBytes: Long): String {
        val kB = 1000L
        val MB = kB * 1000L
        val GB = MB * 1000L

        return when {
            sizeInBytes < 10000 -> "$sizeInBytes bytes"
            sizeInBytes < MB -> "${sizeInBytes / kB} kB"
            sizeInBytes < GB -> "${sizeInBytes / MB} MB"
            else -> "${sizeInBytes / GB} GB"
        }
    }

    fun appendStorageEntry(list: HTMLUListElement, entry: RemoteStorageModel.BranchInfo): HTMLButtonElement {
        val deleteButtonId = "delete-branch-${entry.branch}-button"
        val branchNameId = if (entry.branch.startsWith(".")) entry.branch.substring(1)
            else entry.branch
        val deleteBranchConfirmContainerId = "delete-branch-confirm-$branchNameId-container"

        val li = document.create.li("list-group-item") {
            div("container-fluid") {
                span("pull-left") {
                    val deletingString = if (entry.state == RemoteStorageModel.Action.DELETING)
                        " (deleting)" else ""
                    +"${entry.branch}$deletingString"
                }
                button(classes = "btn btn-sm pull-right ml-1") {
                    attributes += "data-toggle" to "collapse"
                    attributes += "data-target" to "#$deleteBranchConfirmContainerId"

                    +"Delete"
                    if (entry.state == RemoteStorageModel.Action.DELETING)
                        hidden = true
                }

                span("pull-right") {
                    +"(size ${byteSizeToString(entry.stats.size)})"
                }
            }
            div("container-fluid collapse mt-1") {
                id = deleteBranchConfirmContainerId

                div("alert alert-warning pull-right") {
                    attributes["role"] = "alert"

                    span("mr-2") {
                        +"Are you sure you want to delete this remote branch?"
                    }

                    button(classes = "btn pull-right") {
                        attributes += "data-toggle" to "collapse"
                        attributes += "data-target" to "#$deleteBranchConfirmContainerId"
                        +"Cancel"
                    }
                    button(classes = "btn pull-right btn-danger") {
                        id = deleteButtonId
                        attributes += "data-toggle" to "collapse"
                        attributes += "data-target" to "#$deleteBranchConfirmContainerId"
                        +"Yes"
                    }
                }
            }
        }
        list.appendChild(li)
        return document.getElementById(deleteButtonId).unsafeCast<HTMLButtonElement>()
    }
}
