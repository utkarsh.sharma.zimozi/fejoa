package org.fejoa.server.ui

import kotlin.properties.Delegates


class LogModel {
    var error: String by Delegates.observable("", { _, _, _ ->
            notifyListeners()
    })

    var info: String by Delegates.observable("", { _, _, _ ->
        notifyListeners()
    })

    private fun notifyListeners() {
        listeners.forEach { it.invoke() }
    }

    val listeners: MutableList<() -> Unit> = ArrayList()

    fun clear() {
        error = ""
        info = ""
    }
}
