package org.fejoa.server.ui

import org.fejoa.StorageConfig
import org.fejoa.network.DeleteBranchJob
import org.fejoa.network.DeleteStorageConfigJob
import org.fejoa.network.GetServerConfigJob
import org.fejoa.network.ListBranchesJob
import org.fejoa.storage.StorageBackend


class RemoteStorageModel(val jobSender: JobSender) {
    enum class Action {
        FETCHING,
        FETCHED,
        DELETING,
    }

    interface Listener {
        fun onAllBranchesUpdated()
        fun onServerConfigUpdate()
    }

    class BranchInfo(val branch: String, val stats: StorageBackend.Stats, var state: Action)
    val branches: MutableList<BranchInfo> = ArrayList()
    val listeners: MutableList<Listener> = ArrayList()

    var storageConfig: Pair<StorageConfig?, Action> = null to Action.FETCHING

    val errors: MutableList<String> = ArrayList()

    var user: String? = null
        set(value) {
            if (field == value)
                return
            field = value
            if (value != null) {
                listBranches(value)
                getServerConfig(value)
            } else {
                branches.clear()
                notifyAllBranchesUpdate()
            }
        }

    private fun notifyAllBranchesUpdate() {
        listeners.forEach { it.onAllBranchesUpdated() }
    }

    private fun notifyServerConfigUpdate() {
        listeners.forEach { it.onServerConfigUpdate() }
    }

    private fun addError(message: String) {
        errors += message
        notifyAllBranchesUpdate()
    }

    private var isFetchingBranches = false
    private var isFetchingServerConfig = false

    var isFetching: Boolean = false
        private set
        get() = isFetchingBranches || isFetchingServerConfig

    private fun getServerConfig(user: String) {
        isFetchingServerConfig = true
        jobSender.sendJob(GetServerConfigJob(user)).whenCompleted { value, error ->
            isFetchingServerConfig = false
            if (error != null) {
                addError(error.message ?: "Error when fetching branches")
                return@whenCompleted
            }
            storageConfig = value?.storageConfig to Action.FETCHED

            notifyServerConfigUpdate()
        }
    }

    fun deleteServerConfig() {
        val currentUser = user ?: return
        storageConfig = storageConfig.first to Action.DELETING
        notifyServerConfigUpdate()

        val future = jobSender.sendJob(DeleteStorageConfigJob(currentUser))
        future.whenCompleted { value, error ->
            if (error != null) {
                storageConfig = storageConfig.first to Action.FETCHED
                addError(error.message ?: "Error while deleting a storage config")
                notifyServerConfigUpdate()
                return@whenCompleted
            }
            if (value == null)
                return@whenCompleted

            storageConfig = null to Action.FETCHED
            notifyServerConfigUpdate()
        }
    }

    private fun listBranches(user: String) {
        isFetchingBranches = true
        jobSender.sendJob(ListBranchesJob(user)).whenCompleted { value, error ->
            isFetchingBranches = false
            if (error != null) {
                addError(error.message ?: "Error when fetching branches")
                notifyAllBranchesUpdate()
                return@whenCompleted
            }
            if (value != null) {
                branches.clear()
                branches.addAll(value.branches.map {
                    BranchInfo(it.branch, it.stats, Action.FETCHED)
                })
            }
            notifyAllBranchesUpdate()
        }
    }

    fun deleteBranch(branch: String) {
        val currentUser = user ?: return
        val currentBranch = branches.firstOrNull { it.branch == branch } ?: return
        currentBranch.state = Action.DELETING
        notifyAllBranchesUpdate()

        val future = jobSender.sendJob(DeleteBranchJob(currentUser, listOf(branch)))
        future.whenCompleted { value, error ->
            if (error != null) {
                currentBranch.state = Action.FETCHED
                addError(error.message ?: "Error while deleting a branch")
                notifyAllBranchesUpdate()
                return@whenCompleted
            }
            if (value == null)
                return@whenCompleted

            // remove the deleted entry
            branches.firstOrNull { it.branch == branch }?.let {
                branches.remove(it)
            }
            notifyAllBranchesUpdate()
        }
    }
}