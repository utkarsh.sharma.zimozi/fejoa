function init() {
    ZstdCodec.run(zstd => {
        window.zstdCodecSimple = new zstd.Simple();
        window.__karma__.start();
    });
}

requirejs.config({
    // path where all main libs are located, e.g. kotlin.js
    baseUrl: '/base/cloudefsJS/build/node_modules',

    paths: { // paths are relative to this file
        'big-integer': '../../../jsbindings/node_modules/big-integer/BigInteger.min',
        'pako': '../../../jsbindings/node_modules/pako/dist/pako.min',
        'zstd-codec': '../../../jsbindings/node_modules/zstd-codec/dist/bundle',

        'cloudefsJS': '../../build/classes/kotlin/main/cloudefsJS',
        'cloudefsJS_test': '../../build/classes/kotlin/test/cloudefsJS_test',
    },

    deps: ['big-integer', 'pako', 'zstd-codec', 'cloudefsJS_test'],

    // start tests when done
    callback: init
});
