var timeout = 2 * 60 * 1000;

module.exports = function (config) {
    config.set({
            frameworks: ['mocha', 'requirejs'],
            reporters: ['mocha'],
            basePath: '../',
            files: [
                { pattern: "jsbindings/node_modules/big-integer/BigInteger.min.js", included: false},
                { pattern: "jsbindings/node_modules/pako/dist/pako.min.js", included: false},
                { pattern: "jsbindings/node_modules/zstd-codec/dist/bundle.js", included: false},

                "cloudefsJS/src/test/resources/testmain.js",

                { pattern: 'cloudefsJS/build/node_modules/*.js', included: false},
                { pattern: 'cloudefsJS/build/classes/kotlin/main/*.js', included: false},
                { pattern: 'cloudefsJS/build/classes/kotlin/test/*.js', included: false},
            ],
            client: {
                mocha: {
                        timeout: timeout
                }
            },
            exclude: [],
            colors: true,
            autoWatch: false,
            browsers: [
                //'PhantomJS'
                //'Chrome'
                "Firefox"
            ],
            singleRun: true,

            browserDisconnectTimeout: timeout,
            processKillTimeout: timeout,
            browserNoActivityTimeout: timeout,

            preprocessors: {
                //'**/*.js': ['requirejs']
            }
        }
    )
};