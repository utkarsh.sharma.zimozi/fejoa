package org.fejoa.cli

import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.cli.command.ContactCommandResult
import org.fejoa.cli.command.CREATE_COMMAND_NAME
import org.fejoa.server.JettyServer
import org.fejoa.server.JettyServer.Companion.DEFAULT_PORT
import org.fejoa.server.JettyServer.Companion.FEJOA_PORTAL_PATH
import org.fejoa.support.StorageLib
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.io.File
import java.util.ArrayList
import kotlin.test.assertFalse
import kotlin.test.assertTrue


class CLITest {
    private val cleanUpDirs = ArrayList<String>()

    internal val TEST_DIR = "cliTest"
    internal val HOME_DIR = TEST_DIR + "/User"
    internal val HOME_DIR_2 = TEST_DIR + "/User2"
    internal val SERVER_TEST_DIR = TEST_DIR + "/Server"

    val server = JettyServer(SERVER_TEST_DIR, JettyServer.DEFAULT_PORT)

    @Before
    fun setup() {
        cleanUpDirs.add(TEST_DIR)
        cleanUp()

        File(HOME_DIR).mkdirs()
        File(HOME_DIR_2).mkdirs()
        File(TEST_DIR).mkdirs()

        server.start()
    }

    @After
    fun cleanUp() {
        server.stop()

        for (dir in cleanUpDirs)
            StorageLib.recursiveDeleteFile(File(dir))
    }

    @Test
    fun testOpenCloseTest() {
        val pipeName = "testOpenCloseTest.pipe"
        val starter = DaemonClient.LocalStarter(10 * 1000, pipeName)
        val client = DaemonClient(starter, File(HOME_DIR), pipeName)

        client.exec(arrayOf(CREATE_COMMAND_NAME, "-p=password"))
        client.exec(arrayOf("close"))
        assertFalse(client.exec(arrayOf("open", "-p=wrongpassword")) is Done)
        assertTrue(client.exec(arrayOf("open", "-p=password")) is Done)

        starter.daemon?.shutdown()
    }

    private fun waitForContactRequest(client: DaemonClient): String? {
        for (i in 1..10) {
            val result = client.exec(arrayOf("contact")) as? Done ?: return null
            val contactResult = ObjectMapper().readValue(result.returnValue, ContactCommandResult::class.java)
            if (contactResult.incomingRequests.isNotEmpty())
                return contactResult.incomingRequests[0]
            Thread.sleep(100)
        }
        return null
    }

    private fun waitForContact(client: DaemonClient): String? {
        for (i in 1..10) {
            val result = client.exec(arrayOf("contact")) as? Done ?: return null
            val contactResult = ObjectMapper().readValue(result.returnValue, ContactCommandResult::class.java)
            if (contactResult.contacts.isNotEmpty())
                return contactResult.contacts[0]
            Thread.sleep(100)
        }
        return null
    }

    @Test
    fun testClient() {
        val pipeName = "testClient.pipe"
        val starter1 = DaemonClient.LocalStarter(10 * 1000, pipeName)
        val client = DaemonClient(starter1, File(HOME_DIR), pipeName)
        val server = "http://localhost:$DEFAULT_PORT/$FEJOA_PORTAL_PATH"
        client.exec(arrayOf(CREATE_COMMAND_NAME, "-p=password"))
        client.exec(arrayOf("register", "-p=password", "user1", server))
        client.exec(arrayOf("remote"))

        val starter2 = DaemonClient.LocalStarter(10 * 1000, pipeName)
        val client2 = DaemonClient(starter2, File(HOME_DIR_2), pipeName)
        client2.exec(arrayOf(CREATE_COMMAND_NAME, "-p=password"))
        client2.exec(arrayOf("register", "-p=password", "user2", server))

        // wait till clients are uploaded
        // TODO find a better solution
        Thread.sleep(2000)

        client.exec(arrayOf("contact", "request", "user2", server))
        val requestedContact = waitForContactRequest(client2) ?: return assertFalse(true)
        client2.exec(arrayOf("contact", "accept", requestedContact))

        waitForContact(client) ?: return assertFalse(true)
        waitForContact(client2) ?: return assertFalse(true)

        starter1.daemon?.shutdown()
        starter2.daemon?.shutdown()
    }
}