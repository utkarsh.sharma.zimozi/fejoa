package org.fejoa.cli

import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.coroutines.experimental.runBlocking
import org.fejoa.support.StorageLib
import org.junit.After
import org.junit.Test
import org.junit.Assert.*
import java.io.File
import java.util.ArrayList


class RPCTest {
    private val cleanUpDirs = ArrayList<String>()

    @After
    fun cleanUp() {
        for (dir in cleanUpDirs)
            StorageLib.recursiveDeleteFile(File(dir))
    }

    @Test
    fun testServerAlreadyRunning() {
        val pipeName = "test.pipe"
        val pipeFile = getPipeFile(pipeName)
        cleanUpDirs.add(pipeFile.path)
        cleanUp()

        runBlocking {
            val server1 = JsonRPCServer(pipeFile)
            try {
                val server2 = JsonRPCServer(pipeFile)
                assertFalse(true)
            } catch (e: Exception) {

            }
            server1.close()
        }
    }

    @Test
    fun testRPC() {
        val serverCommand = object : IRemoteCommand {
            override fun start(jobName: String, path: String, jsonArgs: String?): IRemoteCommand.JobId? {
                return IRemoteCommand.JobId(jsonArgs ?: "none")
            }

            override fun jobCtrl(job: IRemoteCommand.JobId, jsonArgs: String?): IRemoteCommand.JobStatus {
                return IRemoteCommand.JobStatus.progress(ProgressReport(jsonArgs))
            }

            override fun cancel(job: IRemoteCommand.JobId) {
                assertEquals("args", job.id)
            }

        }

        val pipeName = "test.pipe"
        val pipeFile = getPipeFile(pipeName)
        cleanUpDirs.add(pipeFile.path)
        cleanUp()

        val server = JsonRPCServer(pipeFile)
        installCommand(server, serverCommand)

        val outPipe = JsonRPCClient.connectToServer(pipeName, 100) ?: throw Exception("Can't connect to server")
        val client = JsonRPCClient(outPipe)

        val stub = ClientStub(client)
        val jobId = stub.start("test", File(".").absolutePath, "args") ?: throw Exception()
        assertEquals("args", jobId?.id)

        val jobStatus = stub.jobCtrl(jobId, "Hello")
        assertEquals(IRemoteCommand.Status.PROGRESS, jobStatus.status)

        val mapper = ObjectMapper()
        val progressReport = mapper.readValue(jobStatus.jsonResponse, ProgressReport::class.java)
        assertEquals("Hello", progressReport.message)

        stub.cancel(jobId)
    }
}
