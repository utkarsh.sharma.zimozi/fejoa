package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.coroutines.experimental.future.future
import org.fejoa.AccountIO
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.Daemon
import org.fejoa.cli.DaemonJob
import org.fejoa.cli.IClientCommand
import org.fejoa.platformGetAccountIO
import picocli.CommandLine
import java.io.File


const val STATUS_COMMAND_NAME = "status"

class StatusClientCommand : IClientCommand {
    @CommandLine.Command(name = STATUS_COMMAND_NAME, description = ["Query the current client status"])
    class StatusArgs : ArgsBase()

    override fun createArgs(): Any = StatusArgs()

    override fun start(argObject: Any): String? {
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class StatusDaemonJob(private val daemon: Daemon) : DaemonJob() {
    override suspend fun runInternal(path: String, args: String?): Any? {
        daemon.clientManager.getOpenClient(path)?.client?.let {
            // open
            return future(it.context.coroutineContext) {
                reportProgress("Client open for user: " + it.userData.id.get())
            }.join()
        }

        try {
            // account files exist?
            val file = File(path)
            val accountIO = platformGetAccountIO(AccountIO.Type.CLIENT, file.parent, file.name)
            val settings = accountIO.readUserDataConfig()
            return reportProgress("Closed account found. \"fejoa open\" to open the account")
        } catch (e: Exception) {
            return reportProgress("No account at: $path")
        }
    }
}
