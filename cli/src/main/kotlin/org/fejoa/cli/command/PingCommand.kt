package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.coroutines.experimental.delay
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.Daemon
import org.fejoa.cli.DaemonJob
import org.fejoa.cli.IClientCommand
import picocli.CommandLine


const val PING_COMMAND_NAME = "ping"

class PingClientCommand : IClientCommand {
    @CommandLine.Command(name = PING_COMMAND_NAME, description = ["Test if the daemon is running"])
    class PingArgs : ArgsBase() {
        @CommandLine.Option(names = ["-n"], description = ["Number of pings"],
                required = false)
        var nPings: Int? = null
    }

    val args = PingArgs()

    override fun createArgs(): Any = args

    override fun start(argObject: Any): String? {
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class PingDaemonJob(private val daemon: Daemon) : DaemonJob() {
    override suspend fun runInternal(path: String, args: String?): Any? {
        val pingArgs = ObjectMapper().readValue(args, PingClientCommand.PingArgs::class.java)
        var nPings = pingArgs.nPings ?: -1
        while (nPings != 0) {
            if (nPings > 0)
                nPings --
            reportProgress("ping: daemon ${daemon.id} is still alive...")
            delay(1000 * 1)
        }
        return null
    }
}