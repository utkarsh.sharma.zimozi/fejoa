package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.Client
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.ClientManager
import org.fejoa.cli.Daemon
import org.fejoa.cli.IClientCommand
import picocli.CommandLine

const val REMOTE_COMMAND_NAME = "remote"

class RemoteCommand : IClientCommand {
    @CommandLine.Command(name = REMOTE_COMMAND_NAME, description = arrayOf("Manage remotes (CSPs)"))
    class RemoteArgs : ArgsBase()

    override fun createArgs(): Any = RemoteArgs()

    override fun start(argObject: Any): String? {
        assert(argObject is RemoteArgs)
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class RemoteDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    override suspend fun runInternal(entry: ClientManager.ClientEntry, path: String, jsonArgs: String?): Any? {
        entry.client.userData.remotes.listRemotes().forEach {
            reportProgress(it.address())
        }
        return null
    }
}