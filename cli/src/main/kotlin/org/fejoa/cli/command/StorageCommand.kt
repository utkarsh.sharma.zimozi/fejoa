package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.UserData
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.ClientManager
import org.fejoa.cli.Daemon
import org.fejoa.cli.IClientCommand
import picocli.CommandLine


const val STORAGE_COMMAND_NAME = "storage"

class StorageClientCommand : IClientCommand {
    @CommandLine.Command(name = STORAGE_COMMAND_NAME, description = arrayOf("List storage branches"))
    class StorageArgs : ArgsBase()

    override fun createArgs(): Any = StorageArgs()

    override fun start(argObject: Any): String? {
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class StorageDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    override suspend fun runInternal(entry: ClientManager.ClientEntry, path: String, jsonArgs: String?): Any? {
        entry.client.userData.getBranches(UserData.STORAGE_CONTEXT, true).forEach {
            val branchInfo = it.branchInfo.get()
            reportProgress("${branchInfo.branch}: ${branchInfo.description}")
        }
        return null
    }
}
