package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.Client
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.ClientManager
import org.fejoa.cli.Daemon
import org.fejoa.cli.IClientCommand
import org.fejoa.storage.HashValue
import picocli.CommandLine


const val CONTACT_COMMAND_NAME = "contact"

class ContactClientCommand : IClientCommand {
    @CommandLine.Command(name = CONTACT_COMMAND_NAME, description = ["Manage contacts"])
    class ContactArgs : ArgsBase()

    override fun createArgs(): Any = ContactArgs()

    override fun start(argObject: Any): String? {
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class ContactCommandResult {
    val contacts: MutableList<String> = ArrayList()
    val requestedContacts: MutableList<String> = ArrayList()
    val incomingRequests: MutableList<String> = ArrayList()
}

class ContactDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    override suspend fun runInternal(entry: ClientManager.ClientEntry, path: String, jsonArgs: String?): Any? {
        val result = ContactCommandResult()

        val client = entry.client
        val contacts = client.userData.contacts
        val contactList = contacts.list.list()
        val requestedContacts = contacts.requested.listRequests()
        val incomingRequests
                = client.contactRequestHandler.handleManager.getPendingRequests()

        result.contacts.addAll(contacts.list.list())
        result.requestedContacts.addAll(requestedContacts.map { it.first.toHex() })
        result.incomingRequests.addAll(incomingRequests.map { it.command.user.toHex() })

        if (contactList.isNotEmpty())
            reportProgress("Contacts:")
        contactList.forEach {
            var entry = it
            val contact = contacts.list.get(HashValue.fromHex(it))
            contact.remotes.listRemotes().forEach {
                entry += " ${it.address()}"
            }
            reportProgress(entry)
        }

        if (requestedContacts.isNotEmpty())
            reportProgress("Requested contacts:")
        requestedContacts.forEach {
            var entry = it.first.toHex()
            entry += " ${it.second.remote.address()}"
            reportProgress(entry)
        }

        if (incomingRequests.isNotEmpty())
            reportProgress("Incoming requests:")
        incomingRequests.forEach {
            val command = it.command
            var entry = command.user.toHex()
            entry += " ${command.userRemote.address()}"
            reportProgress(entry)
        }
        return result
    }
}



