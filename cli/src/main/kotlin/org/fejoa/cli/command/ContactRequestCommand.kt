package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.Client
import org.fejoa.Remote
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.ClientManager
import org.fejoa.cli.Daemon
import org.fejoa.cli.IClientCommand
import picocli.CommandLine


const val CONTACT_REQUEST_COMMAND_NAME = "request"

class ContactRequestClientCommand : IClientCommand {
    @CommandLine.Command(name = CONTACT_REQUEST_COMMAND_NAME, description = ["Send a contact request"])
    class ContactRequestArgs : ArgsBase() {
        @CommandLine.Parameters(index = "0", description = ["user name"])
        var userName: String? = null

        @CommandLine.Parameters(index = "1", description = ["server"])
        var server: String? = null
    }

    override fun createArgs(): Any = ContactRequestArgs()

    override fun start(argObject: Any): String? {
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class ContactRequestDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    override suspend fun runInternal(entry: ClientManager.ClientEntry, path: String, jsonArgs: String?): Any? {
        val mapper = ObjectMapper()
        val argsObject : ContactRequestClientCommand.ContactRequestArgs
                = mapper.readValue(jsonArgs.toString(), ContactRequestClientCommand.ContactRequestArgs::class.java)

        entry.client.sendContactRequest(Remote.create(argsObject.userName!!, argsObject.server!!), null,
                null, true)
        reportProgress("Contact ${argsObject.userName}@${argsObject.server} requested")
        return null
    }
}



