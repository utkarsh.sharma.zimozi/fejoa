package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.UserData
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.ClientManager
import org.fejoa.cli.Daemon
import org.fejoa.cli.IClientCommand
import org.fejoa.crypto.CryptoSettings
import org.fejoa.crypto.generateSecretKeyData
import picocli.CommandLine

const val ADD_STORAGE_NAME = "add"

@CommandLine.Command(name = ADD_STORAGE_NAME, description = ["Add a storage branch"])
class AddStorageArgs : ArgsBase() {
    @CommandLine.Parameters(index = "0", description = ["description"])
    var description: String? = null
}

class AddStorageCommand : IClientCommand {

    override fun createArgs(): Any = AddStorageArgs()

    override fun start(argObject: Any): String? {
        assert(argObject is RemoteAddArgs)
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class AddStorageDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    override suspend fun runInternal(entry: ClientManager.ClientEntry, path: String, jsonArgs: String?): Any? {
        val mapper = ObjectMapper()
        val args : AddStorageArgs = mapper.readValue(jsonArgs.toString(), AddStorageArgs::class.java)

        val client = entry.client
        val branchName = UserData.generateBranchName()
        val keyData = CryptoSettings.default.symmetric.generateSecretKeyData()
        val addRemoteResult = client.userData.createStorageDir(UserData.STORAGE_CONTEXT, branchName.toHex(),
                args.description ?: "", keyData)
        client.userData.commit()
        return addRemoteResult
    }
}