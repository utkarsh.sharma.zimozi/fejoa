package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.Client
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.ClientManager
import org.fejoa.cli.Daemon
import org.fejoa.cli.IClientCommand
import picocli.CommandLine


const val CONTACT_ACCEPT_COMMAND_NAME = "accept"

class ContactAcceptClientCommand : IClientCommand {
    @CommandLine.Command(name = CONTACT_ACCEPT_COMMAND_NAME, description = ["Accept a contact request"])
    class ContactAcceptArgs : ArgsBase() {
        @CommandLine.Parameters(index = "0", description = ["contact id"])
        var contactId: String = ""
    }

    override fun createArgs(): Any = ContactAcceptArgs()

    override fun start(argObject: Any): String? {
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class ContactAcceptDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    override suspend fun runInternal(entry: ClientManager.ClientEntry, path: String, jsonArgs: String?): Any? {
        val mapper = ObjectMapper()
        val argsObject : ContactAcceptClientCommand.ContactAcceptArgs
                = mapper.readValue(jsonArgs.toString(), ContactAcceptClientCommand.ContactAcceptArgs::class.java)

        val contactRequest = entry.client.contactRequestHandler
                .handleManager.getPendingRequests()
                .firstOrNull {
            it.command.user.toHex() == argsObject.contactId
        } ?: return failJob("No incoming contact request for contact ${argsObject.contactId} found")
        contactRequest.accept()
        reportProgress("Contact ${argsObject.contactId} accepted")
        return null
    }
}



