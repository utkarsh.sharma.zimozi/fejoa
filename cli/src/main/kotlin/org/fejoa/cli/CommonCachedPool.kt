package org.fejoa.cli

import kotlinx.coroutines.experimental.CoroutineDispatcher
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.coroutines.experimental.CoroutineContext


object CommonCachedPool : CoroutineDispatcher() {
    @Volatile
    private var pool: ExecutorService? = null

    private fun getOrCreatePool(): ExecutorService {
        return pool ?: Executors.newCachedThreadPool().also { pool = it }
    }

    override fun dispatch(context: CoroutineContext, block: Runnable) {
        getOrCreatePool().execute(block)
    }
}