package org.fejoa.cli

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.cli.Daemon.Companion.DAEMON_PIPE
import org.fejoa.cli.command.CommandFactory
import picocli.CommandLine
import picocli.CommandLine.Command
import java.io.File
import java.util.concurrent.atomic.AtomicBoolean


open class ArgsBase {
    @JsonIgnore
    @CommandLine.Option(names = arrayOf( "-h", "--help"), help = true,
            description = arrayOf("Show help"))
    var helpRequested: Boolean = false
}

@Command(name = "fejoa")
class MainArgs : ArgsBase()


interface IClientCommand {
    fun createArgs(): Any
    fun start(argObject: Any): String?
}

fun getPipeDir(): File {
    return File(platform.getHomeDir(), ".fejoa/cli/pipes")
}

fun getPipeFile(name: String): File {
    return File(getPipeDir(), name)
}

class DaemonClient(private val daemonStarter: IStarter = DaemonClient.NewProcessStarter(),
                   private val homeDir: File = File("."), private val daemonPipeName: String = Daemon.DAEMON_PIPE) {
    private var currentJob: IRemoteCommand.JobId? = null
    private val canceled = AtomicBoolean(false)

    interface IStarter {
        fun fork()
    }

    class NewProcessStarter : IStarter {
        override fun fork() {
            fork(Daemon::class.java)
        }

        /**
         * Runs a class in a new process.
         *
         * From https://stackoverflow.com/questions/636367/executing-a-java-application-in-a-separate-process
         */
        fun fork(klass: Class<*>) {
            if (System.getProperty("os.name").startsWith("Windows")) {
                // todo test this code on windows
                val javaHome = System.getProperty("java.home")
                val javaBin = javaHome + File.separator + "bin" + File.separator + "java"
                val classpath = System.getProperty("java.class.path")
                val className = klass.canonicalName

                ProcessBuilder(javaBin, "-cp", classpath, className).inheritIO().start()
            } else {
                // use a sh script to make daemon independent from the current java process
                //ProcessBuilder("cli/daemon.sh").inheritIO().start()
                ProcessBuilder("bash", "-c", "source ~/.bash_aliases && fejoadaemon").inheritIO().start()
            }
        }
    }

    class LocalStarter(val lifeTime: Long, val daemonPipeName: String = DAEMON_PIPE) : IStarter {
        var daemon: Daemon? = null
            private set

        override fun fork() {
            try {
                daemon = Daemon(lifeTime, daemonPipeName)
            } catch (e: Exception) {
                println("Failed to start local Daemon: " + e.localizedMessage)
            }
        }
    }

    private fun connect(): ClientStub {
        var connection = JsonRPCClient.connectToServer(daemonPipeName, 100)
        if (connection != null)
            return ClientStub(JsonRPCClient(connection))

        // try to start the daemon if we failed to establish a connection
        daemonStarter.fork()

        for (i in 1..20) {
            Thread.sleep(20)
            if (getPipeFile(daemonPipeName).exists())
                break
        }
        connection = JsonRPCClient.connectToServer(daemonPipeName, 100)
                ?: throw Exception("Failed to start daemon")
        return ClientStub(JsonRPCClient(connection))
    }

    private fun runCommand(rpcName: String, remoteCommand: IRemoteCommand, path: String, command: IClientCommand, arg: Any): Result {
        synchronized(this) {
            if (canceled.get())
                throw Exception("Canceled")
            currentJob = remoteCommand.start(rpcName, path, command.start(arg))
                    ?: throw Exception("Failed to start command: " + rpcName)
        }

        val jsonMapper = ObjectMapper()
        var jsonArgs: String? = null
        loop@ do {
            val jobStatus = remoteCommand.jobCtrl(currentJob!!, jsonArgs)
            jsonArgs = null
            when (jobStatus.status) {
                IRemoteCommand.Status.ERROR -> {
                    val error = jsonMapper.readValue(jobStatus.jsonResponse, Error::class.java)
                    return handleError(error)
                }
                IRemoteCommand.Status.DONE -> {
                    val done = jsonMapper.readValue(jobStatus.jsonResponse, Done::class.java)
                    return done
                }
                IRemoteCommand.Status.INPUT_REQUEST -> {
                    val inputRequest = jsonMapper.readValue(jobStatus.jsonResponse, InputRequest::class.java)
                    jsonArgs = handleInputRequest(inputRequest).toJson()
                }
                IRemoteCommand.Status.PROGRESS -> {
                    val progressReport = jsonMapper.readValue(jobStatus.jsonResponse, ProgressReport::class.java)
                    handleUpdate(progressReport)
                }
            }
        } while (true)
    }

    private fun runCommand(rpcName: String, command: IClientCommand, path: String, arg: Any): Result {
        val remoteCommand = connect()
        try {
            return runCommand(rpcName, remoteCommand, path, command, arg)
        } finally {
            remoteCommand.close()
        }
    }

    private fun handleInputRequest(inputRequest: InputRequest): InputReply {
        val console = System.console()
        console.printf(inputRequest.request)
        val reply: String
        if (inputRequest.hideInput)
            reply = console.readPassword().contentToString()
        else
            reply = console.readLine()
        return InputReply(reply)
    }

    private fun handleUpdate(progressReport: ProgressReport) {
        println(progressReport.message ?: "no update message")
    }

    private fun handleError(error: Error): Error {
        println("Error: " + (error.message ?: "no error message"))
        return error
    }

    /**
     * Find the deepest sub-command or the first command with help requested
     */
    private fun findCommandToExecute(recognized: List<CommandLine>): Pair<CommandLine, Any> {
        val firstHelpCommand = recognized.firstOrNull { (it.command as ArgsBase).helpRequested }
        if (firstHelpCommand != null)
            return firstHelpCommand to firstHelpCommand.command

        val lastSubCommand = recognized.last()
        return lastSubCommand to lastSubCommand.command
    }

    private fun buildSubCommandLine(commandLine: CommandLine, subCommands: List<CommandFactory.SubCommand>,
                                 argsMap: MutableMap<Any, CommandFactory.Factory>) {
        subCommands.forEach { subCommand ->
            val factory = subCommand.getFactory()
            val clientCommand = factory.createClientCommand()
            val arg = clientCommand.createArgs()
            argsMap[arg] = factory
            if (subCommand.subCommands.size == 0)
                commandLine.addSubcommand(subCommand.name, arg)
            else {
                val subCommandLine = CommandLine(arg)
                commandLine.addSubcommand(subCommand.name, subCommandLine)
                buildSubCommandLine(subCommandLine, subCommand.subCommands, argsMap)
            }
        }
    }

    /**
     * Build the commandline
     *
     * @return map of arguments used in the commandline to the according factory
     */
    private fun buildCommandLine(commandLine: CommandLine, factory: CommandFactory): Map<Any, CommandFactory.Factory> {
        val map: MutableMap<Any, CommandFactory.Factory> = HashMap()
        buildSubCommandLine(commandLine, factory.root.subCommands, map)
        return map
    }

    fun exec(arguments: Array<String>): Result {
        // parse arguments
        val mainArgs = MainArgs()
        val mainCommandLine = CommandLine(mainArgs)
        // create args object for parsing
        val factory = CommandFactory()
        val argsMap: Map<Any, CommandFactory.Factory> = buildCommandLine(mainCommandLine, factory)

        try {
            val recognized = mainCommandLine.parse(*arguments)
            if (recognized.size == 1) {
                mainCommandLine.usage(System.out)
                return Error()
            }
            val pair = findCommandToExecute(recognized)
            val commandLine = pair.first
            val command = pair.second

            if ((command as ArgsBase).helpRequested) {
                commandLine.usage(System.out)
                return Error()
            }

            val factory = argsMap[command] ?: throw Exception("Internal error: unmatched sub command")
            return runCommand(factory.rpcName, factory.createClientCommand(), homeDir.absolutePath, command)
        } catch (e: Exception) {
            if (canceled.get()) {
                println("Command canceled by user")
                return Error()
            }

            when (e) {
                is CommandLine.MissingParameterException,
                is IllegalArgumentException,
                is CommandLine.UnmatchedArgumentException -> {
                    println(e.localizedMessage)
                    mainCommandLine.usage(System.out)
                    return Error()
                }
            }

            // unknown error
            e.printStackTrace()
            return Error()
        }
    }

    fun cancel() {
        synchronized(this) {
            canceled.set(true)
            currentJob?.let {
                // open a new connection and cancel the current job
                val remoteCommand = connect()
                remoteCommand.cancel(it)
                remoteCommand.close()
            }
        }
    }
}


fun main(args: Array<String>) {
    val command = DaemonClient()

    // shut down gracefully, e.g. on ctrl-c
    val shutdownThread = object : Thread() {
        override fun run() {
            command.cancel()
        }
    }
    shutdownThread.isDaemon = true
    Runtime.getRuntime().addShutdownHook(shutdownThread)

    command.exec(args)
}
