package org.fejoa.cli

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.TreeNode
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import kotlinx.coroutines.experimental.*
import kotlinx.coroutines.experimental.channels.ClosedReceiveChannelException
import java.io.File
import java.io.IOException
import java.util.concurrent.atomic.AtomicBoolean
import java.util.logging.Level
import java.util.logging.Logger
import kotlin.coroutines.experimental.CoroutineContext

class KeepAsJsonDeserialzier : JsonDeserializer<String>() {
    @Throws(IOException::class, JsonProcessingException::class)
    override fun deserialize(jsonParser: JsonParser, context: DeserializationContext): String {
        val tree = jsonParser.codec.readTree<TreeNode>(jsonParser)
        return tree.toString()
    }
}


class JsonRPCCall(var method: String?, var params: Any?, var id: Int) {
    var jsonrpc: String? = "2.0"
}


class JsonRPCResponse(var id: Int = -1, var result: Any? = null) {
    var jsonrpc: String? = "2.0"

    fun validate(id: Int) {
        if (jsonrpc != "2.0" || id != this.id)
            throw Exception("Invalid json RPC response")
    }
}

/**
 * To be used for deserialization
 */
class JsonRPCCallReceiver(var method: String? = null, var id: Int = -1) {
    var jsonrpc: String? = "2.0"
    @JsonDeserialize(using = KeepAsJsonDeserialzier::class)
    var params: String? = null
}

class JsonRPCResponseReceiver(var id: Int = -1) {
    var jsonrpc: String? = "2.0"
    @JsonDeserialize(using = KeepAsJsonDeserialzier::class)
    var result: String? = null

    fun validate(id: Int) {
        if (jsonrpc != "2.0" || id != this.id)
            throw Exception("Invalid json RPC response")
    }
}

interface RPCArgs

class PipeArguments(var replyPipe: String = "", var params: RPCArgs? = null)
class PipeArgumentsReceiver(var replyPipe: String = "") {
    @JsonDeserialize(using = KeepAsJsonDeserialzier::class)
    var params: String? = null
}

class JsonRPCClient(val serverPipe: OutputNamedPipe) {
    private val replyPipeName = "client_${System.currentTimeMillis()}_${getRandomId()}.pipe"
    private val jsonMapper = ObjectMapper()
    private val replyPipe: InputNamedPipe

    init {
        val pipeFile = getPipeFile(replyPipeName)
        NamedPipe.create(pipeFile)
        replyPipe = NamedPipe.openForRead(pipeFile)
    }

    companion object {
        fun connectToServer(pileName: String, timeout: Long): OutputNamedPipe? {
            try {
                return NamedPipe.openForWrite(getPipeFile(pileName), timeout)
            } catch (e: Exception) {
                return null
            }
        }
    }

    fun close() {
        serverPipe.close()
        replyPipe.close()
        NamedPipe.delete(getPipeFile(replyPipeName))
    }

    private fun getRandomId(): Int {
        return (10000000 * Math.random()).toInt()
    }

    fun call(method: String, params: RPCArgs?): String {
        return call(method, params, -1)!!
    }

    fun call(method: String, params: RPCArgs?, timeout: Int): String? {
        val id = getRandomId()
        val jsonCall = jsonMapper.writeValueAsBytes(
                JsonRPCCall(method, PipeArguments(replyPipeName, params), id))
        serverPipe.writeDataPackage(jsonCall)
        val reply = replyPipe.readDataPackage(timeout)
        val response = jsonMapper.readValue(reply, JsonRPCResponseReceiver::class.java)
        response.validate(id)

        return response.result
    }
}


/**
 * Throws if a pipe with pipeName already exists, i.e. another server is already running.
 */
class JsonRPCServer(val pipeFile: File) {
    /**
     * Handler for a remote method call
     */
    interface IMethodHandler {
        /**
         * Indicates if method has to run now in a new thread or can run in the common pool.
         *
         * For example, if the common pool is busy the cancel method can't cancel ongoing methods -> dead lock
         */
        fun needsToRunNow(): Boolean {
            return false
        }
        fun methodName(): String
        fun handle(params: String?): Any?
    }

    private val LOGGER = Logger.getLogger(this.javaClass.name)
    private val running = AtomicBoolean(true)
    private val handlers: MutableList<IMethodHandler> = ArrayList()
    private var serverPipe: InputNamedPipe? = null

    /** Use a dedicated thread to always be able to quickly read calls from the pipe. Jobs are then handled on a thread
     * pool. This ensures that incoming calls can always be accepted (with low latency) even if the thread pool is busy.
     */
    private var receiverJob: Deferred<Unit>

    init {
        NamedPipe.create(pipeFile)

        receiverJob = async(newSingleThreadContext("JsonRPCServer incoming request reader")) {
            serverPipe = NamedPipe.openForRead(pipeFile)
            val jsonMapper = ObjectMapper()
            while (running.get()) {
                try {
                    val dataPackage = serverPipe!!.readDataPackage()
                    val call = jsonMapper.readValue(dataPackage, JsonRPCCallReceiver::class.java)
                    val pipeArgument = jsonMapper.readValue(call.params, PipeArgumentsReceiver::class.java)
                    handle(call.method!!, pipeArgument, call.id)
                } catch (e: Exception) {
                    // if we are still running something bad happen, otherwise the read was just canceld
                    if (running.get())
                        LOGGER.log(Level.SEVERE, e) { e.message ?: "Unknown exception" }
                    else
                        assert(e is ClosedReceiveChannelException)
                }
            }
        }
    }

    fun addHandler(handler: IMethodHandler) {
        handlers.add(handler)
    }

    private fun handle(method: String, params: PipeArgumentsReceiver, id: Int) {
        val handler = handlers.firstOrNull { it.methodName() == method }
        var context: CoroutineContext = CommonCachedPool
        if (handler != null) {
            if (handler.needsToRunNow())
                context = newSingleThreadContext("Method needs to run now thread")
        }
        launch(context) {
            var result: Any?
            try {
                result = handler?.handle(params.params)
            } catch (e: Exception) {
                LOGGER.log(Level.SEVERE, e) { e.message ?: "Unknown exception" }
                result = e.message
            }
            val mapper = ObjectMapper()
            val response = mapper.writeValueAsBytes(JsonRPCResponse(id, result))

            try {
                val pipe = NamedPipe.openForWrite(getPipeFile(params.replyPipe), 300)
                pipe.writeDataPackage(response)
                pipe.close()
            } catch (e: Exception) {
                LOGGER.log(Level.INFO, "Client is gone: couldn't send reply")
            }
        }
    }

    fun join() = runBlocking {
        receiverJob.await()
    }

    fun close() {
        running.set(false)
        serverPipe?.close()
        NamedPipe.delete(pipeFile)
    }
}