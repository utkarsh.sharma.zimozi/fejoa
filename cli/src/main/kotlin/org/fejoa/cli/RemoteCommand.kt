package org.fejoa.cli

import com.fasterxml.jackson.databind.ObjectMapper


private val START_METHOD = "start"
private val JOB_CTRL_METHOD = "jobCrtl"
private val CANCEL_METHOD = "cancel"

interface IRemoteCommand {
    enum class Status {
        ERROR,
        DONE,
        INPUT_REQUEST,
        PROGRESS
    }

    data class JobId(val id: String = "-1")
    data class JobStatus(val status: Status = Status.ERROR, val jsonResponse: String? = null) {
        companion object Templates {
            fun error(error: Error): JobStatus {
                return JobStatus(Status.ERROR, error.toJson())
            }

            fun error(message: String?): JobStatus {
                return error(Error(message))
            }

            fun done(done: Done): JobStatus {
                return JobStatus(Status.DONE, done.toJson())
            }

            fun inputRequest(inputRequest: InputRequest): JobStatus {
                return JobStatus(Status.INPUT_REQUEST, inputRequest.toJson())
            }

            fun progress(progress: ProgressReport): JobStatus {
                return JobStatus(Status.PROGRESS, progress.toJson())
            }
        }
    }

    /**
     * Starts a command.
     *
     * The daemon should return immediately returning a job id. To receive the response the blocking jobCtrl should be
     * used.
     *
     * If returned JobId is null no job with jobName is found
     */
    fun start(jobName: String, path: String, jsonArgs: String?) : JobId?

    /**
     * Queries a job status. Blocks till status is received or job is canceled.
     */
    fun jobCtrl(job: JobId, jsonArgs: String?) : JobStatus

    fun cancel(job: JobId)

}

class StartArgs(var jobName: String = "", var path: String = ".", var jsonArgs: String? = null) : RPCArgs
class JobCtrlArgs(var job: IRemoteCommand.JobId = IRemoteCommand.JobId("-1"),
                          var jsonArgs: String? = null) : RPCArgs
class CancelArgs(var job: IRemoteCommand.JobId = IRemoteCommand.JobId("-1")) : RPCArgs

fun installCommand(server: JsonRPCServer, target: IRemoteCommand) {
    server.addHandler(object : JsonRPCServer.IMethodHandler {
        override fun methodName(): String {
            return START_METHOD
        }

        override fun handle(params: String?): Any? {
            val mapper = ObjectMapper()
            var startArgs = mapper.readValue(params, StartArgs::class.java)
            return target.start(startArgs.jobName, startArgs.path, startArgs.jsonArgs)
        }
    })

    server.addHandler(object : JsonRPCServer.IMethodHandler {
        override fun methodName(): String {
            return JOB_CTRL_METHOD
        }

        override fun handle(params: String?): Any? {
            val mapper = ObjectMapper()
            var jobCtrlArgs = mapper.readValue(params, JobCtrlArgs::class.java)
            return target.jobCtrl(jobCtrlArgs.job, jobCtrlArgs.jsonArgs)
        }
    })

    server.addHandler(object : JsonRPCServer.IMethodHandler {
        override fun needsToRunNow(): Boolean {
            return true
        }

        override fun methodName(): String {
            return CANCEL_METHOD
        }

        override fun handle(params: String?): Any? {
            val mapper = ObjectMapper()
            var cancelArgs = mapper.readValue(params, CancelArgs::class.java)
            return target.cancel(cancelArgs.job)
        }
    })
}

class ClientStub(private val client: JsonRPCClient) : IRemoteCommand {

    override fun start(jobName: String, path: String, jsonArgs: String?): IRemoteCommand.JobId? {
        val result = client.call(START_METHOD, StartArgs(jobName, path, jsonArgs))
        return ObjectMapper().readValue(result, IRemoteCommand.JobId::class.java)
    }

    override fun jobCtrl(job: IRemoteCommand.JobId, jsonArgs: String?): IRemoteCommand.JobStatus {
        val result = client.call(JOB_CTRL_METHOD, JobCtrlArgs(job, jsonArgs))
        return ObjectMapper().readValue(result, IRemoteCommand.JobStatus::class.java)
    }

    override fun cancel(job: IRemoteCommand.JobId) {
        client.call(CANCEL_METHOD, CancelArgs(job))
    }

    fun close() {
        client.close()
    }
}