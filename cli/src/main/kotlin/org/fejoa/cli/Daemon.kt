package org.fejoa.cli

import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.coroutines.experimental.*
import kotlinx.coroutines.experimental.channels.Channel
import org.fejoa.cli.command.CommandFactory
import org.fejoa.crypto.CryptoHelper
import org.fejoa.support.toHex
import java.util.*
import java.util.concurrent.CancellationException
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicBoolean
import java.util.logging.Level
import java.util.logging.Logger


fun Any.toJson(): String {
    val mapper = ObjectMapper()
    return mapper.writeValueAsString(this)
}


open class Result
class Error(var message: String? = null) : Result()
class Done(var returnValue: String? = null) : Result()

class InputRequest(var request: String? = null, val hideInput: Boolean = false)
class InputReply(var reply: String? = null)
class ProgressReport(var message: String? = null)

abstract class DaemonJob {
    val inChannel: Channel<String> = Channel(Channel.UNLIMITED)
    val replyRequested = AtomicBoolean(false)
    val outChannel: Channel<IRemoteCommand.JobStatus> = Channel(Channel.UNLIMITED)

    suspend fun run(path: String, args: String?) {
        try {
            val returnValue = runInternal(path, args)
            // if the out channel is closed it means an error occurred (see failJob)
            if (!outChannel.isClosedForSend) {
                val jsonReturnValue = ObjectMapper().writeValueAsString(returnValue)
                outChannel.send(IRemoteCommand.JobStatus.done(Done(jsonReturnValue)))
            }
        } catch (e: Exception) {
            if (!outChannel.isClosedForSend)
                outChannel.send(IRemoteCommand.JobStatus.error("In ${this.javaClass.canonicalName}: "
                        + (e.message ?: e.javaClass.canonicalName)))
        } finally {
            close()
        }
    }

    protected abstract suspend fun runInternal(path: String, args: String?): Any?

    /**
     * Send back a request to the client and waits for the reply
     */
    protected suspend fun sendRequest(request: InputRequest): String {
        outChannel.send(IRemoteCommand.JobStatus.inputRequest(request))
        replyRequested.set(true)
        val reply = inChannel.receive()
        replyRequested.set(false)
        return reply
    }


    suspend fun reportProgress(progress: ProgressReport) {
        outChannel.send(IRemoteCommand.JobStatus.progress(progress))
    }

    suspend fun reportProgress(message: String) {
        reportProgress(ProgressReport(message))
    }

    suspend fun failJob(error: Error) {
        outChannel.send(IRemoteCommand.JobStatus.error(error))
        outChannel.close()
    }

    suspend fun failJob(message: String) {
        failJob(Error(message))
    }

    protected fun onCanceled() {

    }

    fun cancel() {
        onCanceled()
        close()
    }

    private fun close() {
        outChannel.close()
        inChannel.close()
    }
}

class Daemon(val lifetime: Long, daemonPipeName: String = DAEMON_PIPE) {
    private val LOGGER = Logger.getLogger(this.javaClass.name)
    private var timer: Timer? = null
    private val jsonRPCServer = JsonRPCServer(getPipeFile(daemonPipeName))
    private val jobFactories: Map<String, CommandFactory.Factory> = CommandFactory().getRpcFactories()
    private val remoteCommand: RemoteCommand = RemoteCommand()

    val id = (100000000 * Math.random()).toInt()

    val clientManager = ClientManager()

    init {
        updateTimer()

        installCommand(jsonRPCServer, remoteCommand)
    }

    abstract class JobFactory(val jobName: String) {
        abstract fun create(): DaemonJob
    }

    /**
     * Is accessed concurrently and must be thread safe
     */
    private class OngoingJob(private val daemonJob: DaemonJob, private val job: Job) {
        fun getOutChannel(): Channel<IRemoteCommand.JobStatus> {
            return daemonJob.outChannel
        }

        fun getInChannel(): Channel<String> {
            return daemonJob.inChannel
        }

        fun cancel() {
            daemonJob.cancel()
            job.cancel(CancellationException("Client canceled the job"))
        }

        fun replyRequested(): Boolean {
            return daemonJob.replyRequested.get()
        }
    }

    inner class RemoteCommand : IRemoteCommand {
        private val jobs: MutableMap<IRemoteCommand.JobId, Daemon.OngoingJob> = ConcurrentHashMap()

        fun hasRunningJobs(): Boolean {
            return jobs.size > 0
        }

        override fun start(jobName: String, path: String, jsonArgs: String?): IRemoteCommand.JobId? {
            updateTimer()

            val factory = jobFactories[jobName] ?: return null
            val handler = factory.createDaemonJob(this@Daemon)
            val jobId = IRemoteCommand.JobId(CryptoHelper.crypto.generateSalt16().toHex())
            // start jobs as coroutines
            val job = launch(CommonCachedPool) {
                handler.run(path, jsonArgs)
            }

            jobs[jobId] = OngoingJob(handler, job)
            return jobId
        }

        override fun jobCtrl(jobId: IRemoteCommand.JobId, jsonArgs: String?): IRemoteCommand.JobStatus {
            val ongoingJob: OngoingJob = jobs[jobId]
                    ?: return IRemoteCommand.JobStatus.error("Job id not in the list of known jobs!")

            // block till we get a response from the job
            return runBlocking {
                try {
                    val channel = ongoingJob.getOutChannel()
                    // if out channel is empty check if we are waiting for a reply
                    if (channel.isEmpty && ongoingJob.replyRequested() && jsonArgs == null)
                        return@runBlocking IRemoteCommand.JobStatus.error("reply expected")
                    else if (jsonArgs != null) {
                        assert(channel.isEmpty)
                        ongoingJob.getInChannel().send(jsonArgs)
                    }

                    val status = channel.receive()

                    // clean up when finished and all messages retrieved
                    if (channel.isClosedForReceive)
                        jobs.remove(jobId)

                    status
                } catch (e: Exception) {
                    IRemoteCommand.JobStatus.error("canceled")
                }
            }
        }

        override fun cancel(job: IRemoteCommand.JobId) {
            val ongoingJob = jobs.remove(job) ?: return
            ongoingJob.cancel()
        }
    }

    private fun updateTimer() {
        synchronized(this) {
            timer?.cancel()
            timer = Timer()
            timer?.schedule(object: TimerTask() {
                override fun run() {
                    if (remoteCommand.hasRunningJobs()) {
                        // jobs are still running; don't shut down but updated the timer
                        updateTimer()
                        return
                    }
                    shutdown()
                    // shut down the timer as well
                    timer?.cancel()
                }
            }, lifetime)
        }
    }

    fun join() {
        jsonRPCServer.join()
    }

    fun shutdown() {
        LOGGER.log(Level.INFO, "Fejoa daemon shut down")
        clientManager.close()
        jsonRPCServer.close()
    }

    companion object {
        val DAEMON_PIPE = "daemon.pipe"

        @JvmStatic fun main(args: Array<String>){
            val daemon = Daemon(60 * 1000)
            daemon.LOGGER.log(Level.INFO, "Daemon running")
            daemon.join()
            daemon.LOGGER.log(Level.INFO, "Daemon existed")
        }
    }
}


