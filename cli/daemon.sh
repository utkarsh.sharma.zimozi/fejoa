#!/bin/bash
source ~/.bash_aliases

nohup java -cp "$FEJOA_CLI_PATH/build/libs/cli-1.0-SNAPSHOT.jar" org.fejoa.cli.Daemon </dev/null >~/.fejoa/daemon.log 2>&1 &
