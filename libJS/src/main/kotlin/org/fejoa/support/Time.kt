package org.fejoa.support

import kotlin.js.Date

actual fun nowNano(): Long {
    return Date.now().toLong() * 1000_000
}