package org.fejoa.support

import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext


actual fun getWorkerCoroutineContext(): CoroutineContext {
    return Dispatchers.Default
}