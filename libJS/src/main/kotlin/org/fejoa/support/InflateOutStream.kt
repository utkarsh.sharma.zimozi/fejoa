package org.fejoa.support

import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.Uint8Array
import io.github.nodeca.pako.inflate


actual class InflateOutStream actual constructor(val outStream: OutStream) : OutStream {
    val buffer = ByteArrayOutStream()

    override fun write(byte: Byte): Int {
        return buffer.write(byte)
    }

    override fun close() {
        val data = buffer.toByteArray()
        val inflated = inflate(Uint8Array(data.unsafeCast<ArrayBuffer>()))
        outStream.write(inflated)
        super.close()
    }
}
