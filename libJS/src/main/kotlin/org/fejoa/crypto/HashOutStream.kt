package org.fejoa.crypto

import kotlinx.coroutines.await
import org.fejoa.jsbindings.crypto
import org.fejoa.support.ByteArrayOutStream
import org.khronos.webgl.Int8Array


open class JSAsyncHashOutStream : AsyncHashOutStream {
    protected constructor() {
        this.algo = "SHA-256"
    }

    constructor(algo: String) {
        this.algo = algo
    }

    val algo: String

    var outStream: ByteArrayOutStream = ByteArrayOutStream()

    override fun reset() {
        outStream = ByteArrayOutStream()
    }

    override suspend fun write(buffer: ByteArray, offset: Int, length: Int): Int {
        outStream.write(buffer, offset, length)
        return length
    }

    override suspend fun hash(): ByteArray {
        val raw = crypto.subtle.digest(algo, outStream.toByteArray()).await()
        return Int8Array(raw).unsafeCast<ByteArray>()
    }
}