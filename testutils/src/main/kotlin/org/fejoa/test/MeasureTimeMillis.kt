package org.fejoa.test


expect inline fun measureTimeMillis(block: () -> Unit) : Long
